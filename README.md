# Kibako64

Simple "headless" N64 Emulator written in C++ for WebAssembly.<br>
Kibako64 is not meant to be a playable emulator, but rather an API to execute code and look into the reusulting data.

The code itself does not use the standard library, has no dynamic memory allocations,<br>
and uses emulated RAM addresses as real addresses (only works in WASM).

## Build

This project uses NodeJS (currently only version 16 is supported) for testing and clang/make for building the actual code.<br>
After checking out the code run:
```sh
# if you don't have the corrent node version
# nvm i 16.16.0 

yarn install
```

To build, run:
```sh
yarn build

# or alternatively (in src/):
make -j
```
This will build the debug and release version.

## Tests

This project contains unit and integration tests (via snapshot-testing).<br>
To run them, execute:
```sh
yarn test
```

## Usage

### API
TODO

### Debugger
You can start an interactive debugger in your shell with:
```sh
yarn debugger [ROM_PATH]
```

TODO: list all commands

## Example Webpage
An example app can be found to run and view the framebuffer in a canvas in you browser.<br>
Run the following command to start the server:
```
yarn serve
```

## Credits
All Test-ROMs in `test/snapshot/peterLemonN64` are taken from the Repo:
https://github.com/PeterLemon/N64