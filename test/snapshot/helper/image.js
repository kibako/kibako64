/**
 * 
 * @param {PNG} imgA 
 * @param {PNG} imgB
 * @param {number[]} min 
 * @param {number[]} max 
 */
export function maskImage(imgA, imgB, min, max)
{
  let i=0;
    for(let y=0; y<imgA.height; ++y) 
    {
      for(let x=0; x<imgA.width; ++x) 
      {
        if(x < min[0] || x > max[0] || 
           y < min[1] || y > max[1]
          ) {
            imgA.data[i+0] = imgB.data[i+0] = 0xFF;
            imgA.data[i+1] = imgB.data[i+1] = 0x00;
            imgA.data[i+2] = imgB.data[i+2] = 0xFF;
            imgA.data[i+3] = imgB.data[i+3] = 0xFF;
          }

        i += 4;
      }
    }
}