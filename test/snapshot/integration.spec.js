/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { createInstance } from '#js/kibako64.js';
import { readFileSync, writeFileSync, readdirSync, existsSync } from 'fs';
import path from 'path';
import {PNG} from 'pngjs';
import pixelmatch from 'pixelmatch';
import {LoggingLevelMask } from '#js/types.js';
import { normalizeFramebuffer } from '#js/renderer/texture/framebuffer.js';
import { Emulator } from '#js/emulator.js';
import { maskImage } from './helper/image.js';

const customRomBasePath = "test/snapshot/custom/";
const peterLemonN64BasePath = "test/snapshot/peterLemonN64/";

const PIXEL_DIFF_DELTA = 1;

const TEST_TIMEOUT_MS = 1000 * 8;
const LIMIT_PER_FRAME = 5_964 * 0x200; 

async function createEmu(romPath, debugMode)
{
  const romBuffer = readFileSync(romPath);
  const emu = await createInstance(romBuffer, debugMode);
  emu.memory.set(romBuffer.slice(0, 0x100000), 0x400);

  emu.exports.setLogLevel(LoggingLevelMask.ERROR);
  emu.exports.boot();
  emu.exports.run(7_000_000);

  return emu;
}

function runForNFrames(emu, frameCount)
{
  for(let f=0; f<frameCount; ++f)
  {
    emu.exports.run(LIMIT_PER_FRAME);
    emu.exports.render();
  }

  const kb64 = new Emulator(emu);
  const fbInfo = kb64.getFramebufferInfo(); 
  return { 
    buffer: normalizeFramebuffer(
      new DataView(emu.memory.buffer, (fbInfo.address >>> 0) & 0x0FFF_FFFF),
      fbInfo,
      emu.isBigEndian),
    info: fbInfo
  };
}

/**
 * 
 * @param {Uint8Array} frameBuffer 
 * @param {*} size 
 * @returns 
 */
function bufferToPNG(frameBuffer, size = [320, 240])
{
  const png = new PNG({
    width: size[0],
    height: size[1],
    filterType: -1
  });

  png.data = frameBuffer;
  return png;
}

function assertFramebuffer(frameData, pathExpected, mask = undefined)
{
  const actualPNG = bufferToPNG(frameData.buffer, [frameData.info.outputSizeX, frameData.info.outputSizeY]);
  const pathDiff  = pathExpected.substring(0, pathExpected.length-3) + "test.png";
  const pathError = pathExpected.substring(0, pathExpected.length-3) + "error.test.png";

  // uncomment to generate new image
  // NOTE: make sure ALL TESTS are ok before doing this.
  //const newExpectedPNG = bufferToPNG(frameBuffer);
  //writeFileSync(pathExpected, PNG.sync.write(newExpectedPNG));

  const expectedPNG = PNG.sync.read(readFileSync(pathExpected));

  const {width, height} = expectedPNG;
  const diff = new PNG({width, height});
  
  if(mask && mask.min && mask.max) {
    maskImage(actualPNG, expectedPNG, mask.min, mask.max);
  }

  try {
    const numDiffPixels = pixelmatch(actualPNG.data, expectedPNG.data, diff.data, width, height, {threshold: 0.1});
    
    if(numDiffPixels > PIXEL_DIFF_DELTA) {
      writeFileSync(pathDiff, PNG.sync.write(diff));
      writeFileSync(pathError, PNG.sync.write(actualPNG));
      throw new Error(`Image is different with '${numDiffPixels}' pixels, file dumped to '${pathDiff}'`);
    }
  } catch(e) {
    writeFileSync(pathError, PNG.sync.write(actualPNG));
    throw e;
  }
}

describe('Kibako64 - Integration - Snapshot', () => 
{
  // test with debug and release mode
  for(let i=0; i<2; ++i) {
    const debugMode = i == 0;
    const testText = debugMode ? "Debug" : "Release";

  // Simple test that generates a few static test-patters with color
  it(`Color Test - frame 1 ${testText}`, async () => 
  {
    const pathExpected = path.join(customRomBasePath, "color_test_1.png");
    const emu = await createEmu(customRomBasePath + "color_test_1.z64", debugMode);

    const frameBuffer = runForNFrames(emu, 3);

    assertFramebuffer(frameBuffer, pathExpected);
  }).timeout(TEST_TIMEOUT_MS);

  // checks all 4 controllers with all buttons/sticks, no buttons are pressed
  it(`Controller Test - No Input ${testText}`, async () => 
  {
    const pathExpected = path.join(customRomBasePath, "controller_test_no_input.png");
    const emu = await createEmu(customRomBasePath + "controller_test.z64", debugMode);
    
    const frameBuffer = runForNFrames(emu, 2);

    assertFramebuffer(frameBuffer, pathExpected);
  }).timeout(TEST_TIMEOUT_MS);

  // checks all 4 controllers with all buttons/sticks, select buttons are pressed after a few frames
  it(`Controller Test - With Input ${testText}`, async () => 
  {
    const pathExpected = path.join(customRomBasePath, "controller_test_with_input.png");
    const emu = await createEmu(customRomBasePath + "controller_test.z64", debugMode);
    
    runForNFrames(emu, 2);

    emu.exports.setGameControllerButton(0, 0xFF00);
    emu.exports.setGameControllerButton(1, 0x00FF);
    emu.exports.setGameControllerButton(2, 0xABCD);
    emu.exports.setGameControllerButton(3, 0xFFFF);

    const frameBuffer = runForNFrames(emu, 6);

    assertFramebuffer(frameBuffer, pathExpected);
  }).timeout(TEST_TIMEOUT_MS);

  it(`Controller Test - With Input ${testText}`, async () => 
  {
    const pathExpected = path.join(customRomBasePath, "controller_test_with_input.png");
    const emu = await createEmu(customRomBasePath + "controller_test.z64", debugMode);
    
    runForNFrames(emu, 2);

    emu.exports.setGameControllerButton(0, 0xFF00);
    emu.exports.setGameControllerButton(1, 0x00FF);
    emu.exports.setGameControllerButton(2, 0xABCD);
    emu.exports.setGameControllerButton(3, 0xFFFF);

    const frameBuffer = runForNFrames(emu, 6);

    assertFramebuffer(frameBuffer, pathExpected);
  }).timeout(TEST_TIMEOUT_MS);

  describe('PeterLemon/N64 Snapshot ' + testText, () => 
  {
    const testFiles = [
      ...readdirSync(peterLemonN64BasePath), 
      ...readdirSync(peterLemonN64BasePath+"/FPU").map(f => "FPU/"+f)
    ];
    if(testFiles.length == 0) {
      throw "No test-files found!";
    }

    const romFiles = testFiles.filter(name => 
      name.toLowerCase().endsWith(".z64") ||
      name.toLowerCase().endsWith(".n64")
    );

    for(let testRom of romFiles) 
    {
      it(`${testText} - ${testRom}`, async () => {
        const pngFile = testRom.substring(0, testRom.length-3) + "png";  
        const pathExpected = path.join(peterLemonN64BasePath, pngFile);
        const configFile = testRom.substring(0, testRom.length-3) + "json";  
        
        let config = {frames: 4, imageMask: undefined};
        if(existsSync(peterLemonN64BasePath + configFile)) {
          config = JSON.parse(readFileSync(peterLemonN64BasePath + configFile, 'utf8'));
        }

        const emu = await createEmu(peterLemonN64BasePath + testRom, debugMode);

        //emu.exports.setLogLevel(0b1111); // for debugging
        const frameData = runForNFrames(emu, config.frames);

        //console.log(frameData.info); // debugging
        //emu.exports.setLogLevel(0xFF); if(emu.exports.cpuLogState)emu.exports.cpuLogState();  // for debugging

        // NOTE: if screenshots have a 1-line offset/gap -> ignore and fix the PNG instead
        assertFramebuffer(frameData, pathExpected, config.imageMask);

      }).timeout(TEST_TIMEOUT_MS);
    }
  });
}});

/* ADD
{
  address: 1048576,
  sizeX: 640,
  sizeY: 480,
  format: 3,
  rect: { minX: 108, maxX: 748, minY: 37, maxY: 511 },
  horizontalSkip: 0
}
*/