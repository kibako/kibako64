/**
 * @copyright 2020 - Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" file in the root directory
 */
import { testInstructions } from './disassemblerTestData.js';
import { createProgram, readStringFromMemory } from './wasmHelper.js';
import chai from "chai"; const {expect} = chai;

describe('Disassembler', () => 
{
    it("Highest Opcode pointer", async () => 
    {
      const { exports, memory } = await createProgram([]);
      const ptr = exports.testOpcodegetHighestPointer();
      console.log("Highest opcode-pointer: " + ptr);
      expect(ptr).lessThan(0xFF);
    });

    for(const testCase of testInstructions)
    {
        it("instruction " + testCase.op, async () => 
        {
            const { exports, memory } = await createProgram([]);
            const opcodeFuncPtr = exports.testGetOpcodeFunction(testCase.data >>> 0);
            const opocdeNamePtr = exports.testGetOpcodeName(opcodeFuncPtr);
            const opcodeName = readStringFromMemory(memory, opocdeNamePtr);

            expect(opcodeName).equal(testCase.op);
        });
    }
});