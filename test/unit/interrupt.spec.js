/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import chai from "chai"; const {expect} = chai;
import { assertHex, assertHexArray, CODE_BASE_OFFSET, createProgram, insertInstructions } from './wasmHelper.js';
import { MIPS_REG } from './registerMapping.js';
import {LoggingLevelMask, InterruptType, RcpInterruptType} from '#js/types.js';

const COP0_COUNTER = 9;
const COP0_COMPARE = 11;
const COP0_STATUS_REG = 12;
const COP0_EXCEPTION_PC = 14;
const COP0_ERROR_EPC = 30;

const MI_RCP_MASK_ADDRESS = 0x0430_000C;

const INTERRUPT_ADDRESS = 0x180;

describe('System - Interrupts', () => 
{
    it("sanity test - no interrupt", async () =>
    {
        const {exports, calls} = await createProgram([
          /* 0x1000 */ "x.log",
          /* 0x1004 */ "x.log",
          /* 0x1008 */ "x.log",
          /* 0x100C */ "x.log",
        ]);

        exports.setPc(CODE_BASE_OFFSET);
        exports.run(2);
        exports.run(2);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([0x1000, 0x1004, 0x1008, 0x100C]);

        expect(exports.testCop0GetRegisterU32(COP0_ERROR_EPC)).equal(0); // no error-return set
    });  

    it("Timer interrupt - not enabled", async () =>
    {
        const {exports, memory, calls} = await createProgram([
          /* 0x1000 */ "x.log",
          /* 0x1004 */ "x.log", // stop here and try to interrupt
          /* 0x1008 */ "x.log",
          /* 0x100C */ "x.log",
        ]);
        insertInstructions(exports, memory, INTERRUPT_ADDRESS, [
          /* 0x0140 */ "x.log",
          /* 0x0140 */ "x.log",
        ]);

        exports.testCop0SetRegisterU32(COP0_STATUS_REG, 0); // disable exceptions

        exports.setPc(CODE_BASE_OFFSET);
        exports.run(2);

        exports.interrupt(InterruptType.TIMER, 0);

        // interrupt ignored
        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([0x1000, 0x1004]);

        assertHex(exports.testCop0GetRegisterU32(COP0_ERROR_EPC),    0);
        assertHex(exports.testCop0GetRegisterU32(COP0_EXCEPTION_PC), 0);
    }); 

    it("Timer interrupt - enabled, not executed", async () =>
    {
        const {exports, memory, calls} = await createProgram([
          /* 0x1000 */ "x.log",
          /* 0x1004 */ "x.log", // stop here and try to interrupt
          /* 0x1008 */ "x.log",
          /* 0x100C */ "x.log",
        ]);
        insertInstructions(exports, memory, INTERRUPT_ADDRESS, [
          /* 0x0140 */ "x.log",
          /* 0x0140 */ "x.log",
        ]);

        exports.testCop0SetRegisterU32(COP0_STATUS_REG, 0b11111111_0000_0001); // enable exceptions

        exports.setPc(CODE_BASE_OFFSET);
        exports.run(2);

        exports.interrupt(InterruptType.TIMER, 0);

        // interrupt ignored
        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([0x1000, 0x1004]);

        assertHex(exports.testCop0GetRegisterU32(COP0_ERROR_EPC),    0);
        assertHex(exports.testCop0GetRegisterU32(COP0_EXCEPTION_PC), 0);
    }); 

    it("Timer interrupt (manually) - enabled & executed", async () =>
    {
        const {exports, memory, calls} = await createProgram([
          /* 0x1000 */ "x.log",
          /* 0x1004 */ "x.log", // stop here and try to interrupt
          /* 0x1008 */ "x.log",
          /* 0x100C */ "x.log",
        ]);
        insertInstructions(exports, memory, INTERRUPT_ADDRESS, [
          /* 0x0180 */ "x.log",
          /* 0x0184 */ "x.log",
        ]);

        exports.testCop0SetRegisterU32(COP0_STATUS_REG, 0b11111111_0000_0001); // enable exceptions

        exports.setPc(CODE_BASE_OFFSET);
        exports.run(2);

        exports.interrupt(InterruptType.TIMER, 0);
        exports.run(2);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([
          0x1000, 0x1004,
          0x8000_0180, 0x8000_0184
        ]);

        assertHex(exports.testCop0GetRegisterU32(COP0_ERROR_EPC),    0);
        assertHex(exports.testCop0GetRegisterU32(COP0_EXCEPTION_PC), 0x1008);
    });

    it("RCP interrupt - enabled, but masked", async () =>
    {
        const {exports, memory, calls} = await createProgram([
          /* 0x1000 */ "x.log",
          /* 0x1004 */ "x.log", // stop here and ignore interrupt
          /* 0x1008 */ "x.log", 
          /* 0x100C */ "x.log",
        ]);
        insertInstructions(exports, memory, INTERRUPT_ADDRESS, [
          /* 0x0180 */ "x.log",
          /* 0x0184 */ "x.log",
          /* 0x0188 */  0x408C_6000, // mtc0 $t4, COP0_STATUS_REG
          /* 0x018C */ "eret",
        ]);
        
        exports.cpuSetRegisterU32(MIPS_REG.$t1, 0x0440_0010); // VI_CURRENT_REG, write clears interrupt flag
        exports.testCop0SetRegisterU32(COP0_STATUS_REG, 0b11111111_0000_0001); // enable exceptions
        //VI is NOT enabled in the RCP-mask

        exports.setPc(CODE_BASE_OFFSET);
        exports.run(2);

        exports.interrupt(InterruptType.RCP, RcpInterruptType.VI);
        exports.run(2);

        // interrupt executed
        expect(exports.testCpuGetErrorCount()).equal(0);
        assertHexArray(calls, [
          0x1000, 0x1004, 0x1008, 0x100C,
        ]);

        assertHex(exports.testCop0GetRegisterU32(COP0_ERROR_EPC),    0);
        assertHex(exports.testCop0GetRegisterU32(COP0_EXCEPTION_PC), 0);
    });

    it("RCP interrupt - enabled, default return", async () =>
    {
        const {exports, memory, calls} = await createProgram([
          /* 0x1000 */ "x.log",
          /* 0x1004 */ "x.log", // stop here and try to interrupt
          /* 0x1008 */ "x.log", // should continue here after ERET
          /* 0x100C */ "x.log",
        ]);
        insertInstructions(exports, memory, INTERRUPT_ADDRESS, [
          /* 0x0180 */ "x.log",
          /* 0x0184 */ "x.log",
          /* 0x0188 */  0x408C_6000, // mtc0 $t4, COP0_STATUS_REG
          /* 0x018C */ "eret",
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$t1, 0x0440_0010); // VI_CURRENT_REG, write clears interrupt flag
        exports.testCop0SetRegisterU32(COP0_STATUS_REG, 0b11111111_0000_0001); // enable exceptions
        exports.testMemBusWriteU32(MI_RCP_MASK_ADDRESS, 0b10000000); //enable VI in the RCP-mask

        exports.setPc(CODE_BASE_OFFSET);
        exports.run(2);

        exports.interrupt(InterruptType.RCP, RcpInterruptType.VI);
        exports.run(7);

        // interrupt executed
        expect(exports.testCpuGetErrorCount()).equal(0);
        assertHexArray(calls, [
          0x1000, 0x1004,
          0x8000_0180, 0x8000_0184,
          0x1008, 0x100C,
        ]);

        assertHex(exports.testCop0GetRegisterU32(COP0_ERROR_EPC),    0);
        assertHex(exports.testCop0GetRegisterU32(COP0_EXCEPTION_PC), 0x1008);
    });

    it("RCP interrupt - enabled, no reset", async () =>
    {
        const {exports, memory, calls} = await createProgram([
          /* 0x1000 */ "x.log",
          /* 0x1004 */ "x.log", // stop here and try to interrupt
          /* 0x1008 */ "x.log", // since we are not clearing the int., it wil never continue here
          /* 0x100C */ "x.log",
        ]);
        insertInstructions(exports, memory, INTERRUPT_ADDRESS, [
          /* 0x0180 */ "x.log",
          /* 0x0184 */ "x.log",
          /* 0x0188 */ "eret",
        ]);

        exports.testCop0SetRegisterU32(COP0_STATUS_REG, 0b11111111_0000_0001); // enable exceptions
        exports.testMemBusWriteU32(MI_RCP_MASK_ADDRESS, 0b10000000); //enable VI in the RCP-mask

        exports.setPc(CODE_BASE_OFFSET);
        exports.run(2);

        exports.interrupt(InterruptType.RCP, RcpInterruptType.VI);
        exports.run(8);

        // interrupt executed
        expect(exports.testCpuGetErrorCount()).equal(0);
        assertHexArray(calls, [
          0x1000, 0x1004,
          0x8000_0180, 0x8000_0184,
          0x8000_0180, 0x8000_0184, // endless loop due to missing clear
          0x8000_0180, 0x8000_0184,
        ]);

        assertHex(exports.testCop0GetRegisterU32(COP0_ERROR_EPC),    0);
        assertHex(exports.testCop0GetRegisterU32(COP0_EXCEPTION_PC), 0x1008);
    });

    it("RCP interrupt - enabled, changed ERROR_EPC return, EPC is used by ERET", async () =>
    {
        const {exports, memory, calls} = await createProgram([
          /* 0x1000 */ "x.log",
          /* 0x1004 */ "x.log", // stop here and try to interrupt
          /* 0x1008 */ "x.log", // should continue here
          /* 0x100C */ "x.log",
          /* 0x1010 */ "x.log",
          /* 0x1014 */ "x.log", // would continue if mtc0 would change different reg.
          /* 0x1018 */ "x.log",

        ]);
        insertInstructions(exports, memory, INTERRUPT_ADDRESS, [
          /* 0x0180 */ "x.log",
          /* 0x0184 */ "x.log",
          /* 0x0188 */ "sw $zero, 0x00($t1)",
          /* 0x018C */  0x408C_F000, // mtc0 $t4, COP0_ERROR_EPC,
          /* 0x0190 */ "eret",
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$t4, 0x0000_1014); // new error-pc
        exports.cpuSetRegisterU32(MIPS_REG.$t1, 0x0440_0010); // VI_CURRENT_REG, write clears interrupt flag

        exports.testCop0SetRegisterU32(COP0_STATUS_REG, 0b11111111_0000_0001); // enable exceptions
        exports.testMemBusWriteU32(MI_RCP_MASK_ADDRESS, 0b10000000); //enable VI in the RCP-mask

        exports.setPc(CODE_BASE_OFFSET);
        exports.run(2);

        exports.interrupt(InterruptType.RCP, RcpInterruptType.VI);
        exports.run(7);

        // interrupt executed with custom return PC
        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([
          0x1000, 0x1004,
          0x8000_0180, 0x8000_0184,
          0x1008, 0x100C,
        ]);

        assertHex(exports.testCop0GetRegisterU32(COP0_ERROR_EPC),    0x1014);
        assertHex(exports.testCop0GetRegisterU32(COP0_EXCEPTION_PC), 0x1008);
    });

    it("RCP interrupt - enabled, changed EPC return, used by ERET", async () =>
    {
        const {exports, memory, calls} = await createProgram([
          /* 0x1000 */ "x.log",
          /* 0x1004 */ "x.log", // stop here and try to interrupt
          /* 0x1008 */ "x.log", // should NOT continue here
          /* 0x100C */ "x.log",
          /* 0x1010 */ "x.log",
          /* 0x1014 */ "x.log", // should continue here after ERET
          /* 0x1018 */ "x.log",
        ]);

        insertInstructions(exports, memory, INTERRUPT_ADDRESS, [
          /* 0x0180 */ "x.log",
          /* 0x0184 */ "x.log",
          /* 0x0188 */ "sw $zero, 0x00($t1)",
          /* 0x018C */  0x408C_7000, // mtc0 $t4, COP0_EXCEPTION_PC,
          /* 0x0190 */ "eret",
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$t4, 0x0000_1014); // new exception-pc
        exports.cpuSetRegisterU32(MIPS_REG.$t1, 0x0440_0010); // VI_CURRENT_REG, write clears interrupt flag
        
        exports.testCop0SetRegisterU32(COP0_STATUS_REG, 0b11111111_0000_0001); // enable exceptions
        exports.testMemBusWriteU32(MI_RCP_MASK_ADDRESS, 0b10000000); //enable VI in the RCP-mask

        exports.setPc(CODE_BASE_OFFSET);
        exports.run(2);

        exports.interrupt(InterruptType.RCP, RcpInterruptType.VI);
        exports.run(7);

        // interrupt executed with custom return PC
        expect(exports.testCpuGetErrorCount()).equal(0);
        assertHexArray(calls, [
          0x1000, 0x1004,
          0x8000_0180, 0x8000_0184,
          0x1014, 0x1018,
        ]);

        assertHex(exports.testCop0GetRegisterU32(COP0_ERROR_EPC),    0);
        assertHex(exports.testCop0GetRegisterU32(COP0_EXCEPTION_PC), 0x1014);
    });

    // @TODO test counter not reached
    // @TODO test mid-branch

    it("Timer interrupt - enabled & executed", async () =>
    {
        const {exports, memory, calls} = await createProgram([
          /* 0x1000 */ "x.log",
          /* 0x1004 */ "x.log",
          /* 0x1008 */ "x.log", // should here and try to interrupt
          /* 0x100C */ "x.log",
        ]);
        insertInstructions(exports, memory, INTERRUPT_ADDRESS, [
          /* 0x0180 */ "x.log",
          /* 0x0184 */ "x.log",
        ]);

        exports.testCop0SetRegisterU32(COP0_STATUS_REG, 0b11111111_0000_0001); // enable exceptions
        exports.testCop0SetRegisterU32(COP0_COMPARE, 4*2); // 2-counts per cycle

        exports.setPc(CODE_BASE_OFFSET);
        exports.run(6);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([
          0x1000, 0x1004, 0x1008,
          0x8000_0180, 0x8000_0184
        ]);

        assertHex(exports.testCop0GetRegisterU32(COP0_ERROR_EPC),    0);
        assertHex(exports.testCop0GetRegisterU32(COP0_EXCEPTION_PC), 0x100C);
    });

    it("Interrupt - pending interrupt auto-execution", async () =>
    {
        const {exports, memory, calls} = await createProgram([
          /* 0x1000 */ "x.log",
          /* 0x1004 */ "x.log", // first stop
          /* 0x1008 */ "x.log", 
          /* 0x100C */ 0x408C6000, //mtc0 $t4, $12; actual second stop, enabled interrupts
          /* 0x1010 */ "x.log",
          /* 0x1014 */ "x.log", // ( theoretical second stop )
        ]);
        insertInstructions(exports, memory, INTERRUPT_ADDRESS, [
          /* 0x0180 */ "x.log",
          /* 0x0184 */ "x.log",
          /* 0x0188 */ "eret",
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$t4, 0b11111111_000_0001); // store enable exceptions flag
        exports.testMemBusWriteU32(MI_RCP_MASK_ADDRESS, 0b10000000); //enable VI in the RCP-mask

        exports.setPc(CODE_BASE_OFFSET);

        // first run
        exports.testCop0SetRegisterU32(COP0_STATUS_REG, 0); // disable interrupt
        exports.run(2);
        
        exports.interrupt(InterruptType.RCP, RcpInterruptType.VI); // this will do nothing, interrupts are disabled

        // second run, interrupts are now enabled it should execute them
        exports.run(5);

        
        expect(exports.testCpuGetErrorCount()).equal(0);
        assertHexArray(calls, [
          0x1000, 0x1004, // first run
          0x1008, // second run
          0x8000_0180, 0x8000_0184, // interrupt
        ]);

        assertHex(exports.testCop0GetRegisterU32(COP0_ERROR_EPC),    0);
        assertHex(exports.testCop0GetRegisterU32(COP0_EXCEPTION_PC), 0x1010);
    });

});
