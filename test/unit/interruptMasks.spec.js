/**
* @copyright 2021 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import chai from "chai"; const {expect} = chai;
import { createProgram } from './wasmHelper.js';
import { InterruptType, RcpInterruptType, RcpInterruptMask} from '#js/types.js';

const COP0_COUNTER = 9;
const COP0_COMPARE = 11;
const COP0_STATUS_REG = 12;
const COP0_CAUSE_REG = 13;

const MI_RCP_INTR_ADDRESS = 0x0430_0008;
const MI_RCP_MASK_ADDRESS = 0x0430_000C;


function checkInterruptSet(exports, type, rcpType)
{
  const cop0Status = exports.testCop0GetRegisterU32(COP0_CAUSE_REG);
  const rcpIntr = exports.testMemBusReadU32(MI_RCP_INTR_ADDRESS);
  expect(cop0Status & type).not.equal(0);
  expect(rcpIntr & rcpType).not.equal(0);
}

function checkInterruptNotSet(exports, type, rcpType)
{
  const cop0Status = exports.testCop0GetRegisterU32(COP0_CAUSE_REG);
  const rcpIntr = exports.testMemBusReadU32(MI_RCP_INTR_ADDRESS);
  expect(cop0Status & type).equal(0);
  expect(rcpIntr & rcpType).equal(0);
}

describe('System - Interrupts Mask', () => 
{
  const SP_STATUS_ADDRESS = 0x04040010;

  it(`RCP - SP - Set`, async () => 
  {
    const {exports, memory} = await createProgram(["nop"]); 
    
    // enable exception masks
    exports.testCop0SetRegisterU32(COP0_STATUS_REG, 0b11111111_0000_0001);
    exports.testMemBusWriteU32(MI_RCP_MASK_ADDRESS, RcpInterruptMask.SET_SP); 
    // initial state
    checkInterruptNotSet(exports, InterruptType.RCP, RcpInterruptType.SP);

    // actual test
    exports.testMemBusWriteU32(SP_STATUS_ADDRESS, (1 << 4)); // should trigger SP
    checkInterruptSet(exports, InterruptType.RCP, RcpInterruptType.SP);
  });

  it(`RCP - SP - Clear`, async () => 
  {
    const {exports, memory} = await createProgram(["nop"]); 

    // enable exception masks
    exports.testCop0SetRegisterU32(COP0_STATUS_REG, 0b11111111_0000_0001);
    exports.testMemBusWriteU32(MI_RCP_MASK_ADDRESS, RcpInterruptMask.SET_SP); 
    exports.testMemBusWriteU32(SP_STATUS_ADDRESS, (1 << 4)); // should trigger SP
    // initial state
    checkInterruptSet(exports, InterruptType.RCP, RcpInterruptType.SP);

    // actual test
    exports.testMemBusWriteU32(SP_STATUS_ADDRESS, (1 << 3)); // should clear SP
    checkInterruptNotSet(exports, InterruptType.RCP, RcpInterruptType.SP);
  });


  const VI_LINE_INTERRUPT = 0x0440_000C;
  const VI_LINE_CURRENT   = 0x0440_0010;

  it(`RCP - VI - Set (unsuccessful)`, async () => 
  {
    const {exports, memory} = await createProgram(["nop"]); 
    // enable exception masks
    exports.testCop0SetRegisterU32(COP0_STATUS_REG, 0b11111111_0000_0001);
    exports.testMemBusWriteU32(MI_RCP_MASK_ADDRESS, RcpInterruptMask.SET_VI); 
    // initial state
    checkInterruptNotSet(exports, InterruptType.RCP, RcpInterruptType.VI);

    // actual test
    exports.testMemBusWriteU32(VI_LINE_CURRENT,   0xFF); // should clear VI
    exports.testMemBusWriteU32(VI_LINE_INTERRUPT, 6);

    exports.testViIncrementCurrentLine(); // 0->2
    exports.testViIncrementCurrentLine(); // 0->4 should NOT trigger

    checkInterruptNotSet(exports, InterruptType.RCP, RcpInterruptType.VI);
  });

  it(`RCP - VI - Set`, async () => 
  {
    const {exports, memory} = await createProgram(["nop"]); 
    // enable exception masks
    exports.testCop0SetRegisterU32(COP0_STATUS_REG, 0b11111111_0000_0001);
    exports.testMemBusWriteU32(MI_RCP_MASK_ADDRESS, RcpInterruptMask.SET_VI); 
    // initial state
    checkInterruptNotSet(exports, InterruptType.RCP, RcpInterruptType.VI);

    // actual test
    exports.testMemBusWriteU32(VI_LINE_CURRENT,   0xFF); // should clear VI
    exports.testMemBusWriteU32(VI_LINE_INTERRUPT, 4);

    exports.testViIncrementCurrentLine(); // 0->2
    exports.testViIncrementCurrentLine(); // 0->4 should trigger

    checkInterruptSet(exports, InterruptType.RCP, RcpInterruptType.VI);
  });

  it(`RCP - VI - Clear`, async () => 
  {
    const {exports, memory} = await createProgram(["nop"]); 

    // enable exception masks
    exports.testCop0SetRegisterU32(COP0_STATUS_REG, 0b11111111_0000_0001);
    exports.testMemBusWriteU32(MI_RCP_MASK_ADDRESS, RcpInterruptMask.SET_VI); 

    // initial state
    exports.testMemBusWriteU32(VI_LINE_CURRENT,   0xFF); // should clear VI
    exports.testMemBusWriteU32(VI_LINE_INTERRUPT, 4); 

    exports.testViIncrementCurrentLine(); // 0->2
    exports.testViIncrementCurrentLine(); // 0->4 should trigger VI

    checkInterruptSet(exports, InterruptType.RCP, RcpInterruptType.VI);

    // actual test
    exports.testMemBusWriteU32(VI_LINE_CURRENT,   0xFF); // should clear VI
    checkInterruptNotSet(exports, InterruptType.RCP, RcpInterruptType.VI);
  });


  const PI_ADDRESS_PTR_RAM         = 0x0460_0000;
  const PI_ADDRESS_PTR_ROM         = 0x0460_0004;
  const PI_ADDRESS_SIZE_ROM_TO_RAM = 0x0460_000C;
  const PI_ADDRESS_STATUS          = 0x0460_0010;

  it(`RCP - PI - Set`, async () => 
  {
    const {exports, memory} = await createProgram(["nop"]); 
    // enable exception masks
    exports.testCop0SetRegisterU32(COP0_STATUS_REG, 0b11111111_0000_0001);
    exports.testMemBusWriteU32(MI_RCP_MASK_ADDRESS, RcpInterruptMask.SET_PI); 
    // initial state
    checkInterruptNotSet(exports, InterruptType.RCP, RcpInterruptType.PI);

    // actual test
    exports.testMemBusWriteU32(PI_ADDRESS_PTR_ROM, 0x1000_1280);
    exports.testMemBusWriteU32(PI_ADDRESS_PTR_RAM, 0x0000_FF00);
    exports.testMemBusWriteU32(PI_ADDRESS_SIZE_ROM_TO_RAM, 0x03); // should copy and set interrupt now

    checkInterruptSet(exports, InterruptType.RCP, RcpInterruptType.PI);
  });

  it(`RCP - PI - Clear`, async () => 
  {
    const {exports, memory} = await createProgram(["nop"]); 

    // enable exception masks
    exports.testCop0SetRegisterU32(COP0_STATUS_REG, 0b11111111_0000_0001);
    exports.testMemBusWriteU32(MI_RCP_MASK_ADDRESS, RcpInterruptMask.SET_PI); 

    // initial state
    exports.testMemBusWriteU32(PI_ADDRESS_PTR_ROM, 0x1000_1280);
    exports.testMemBusWriteU32(PI_ADDRESS_PTR_RAM, 0x0000_FF00);
    exports.testMemBusWriteU32(PI_ADDRESS_SIZE_ROM_TO_RAM, 0x03); // should copy and set interrupt now
    checkInterruptSet(exports, InterruptType.RCP, RcpInterruptType.PI);

    // actual test
    exports.testMemBusWriteU32(PI_ADDRESS_STATUS, 0b10); // should clear PI
    checkInterruptNotSet(exports, InterruptType.RCP, RcpInterruptType.PI);
  });


  const MI_ADDRESS_MODE = 0x0430_0000;

  it(`RCP - DP - Set`, async () => 
  {
    const {exports, memory} = await createProgram(["nop"]); 
    // enable exception masks
    exports.testCop0SetRegisterU32(COP0_STATUS_REG, 0b11111111_0000_0001);
    exports.testMemBusWriteU32(MI_RCP_MASK_ADDRESS, RcpInterruptMask.SET_DP); 
    // initial state
    checkInterruptNotSet(exports, InterruptType.RCP, RcpInterruptType.DP);

    // actual test
    // @TODO add actual trigger once implemented
    exports.interrupt(InterruptType.RCP, RcpInterruptType.DP);

    checkInterruptSet(exports, InterruptType.RCP, RcpInterruptType.DP);
  });

  it(`RCP - DP - Clear`, async () => 
  {
    const {exports, memory} = await createProgram(["nop"]); 

    // enable exception masks
    exports.testCop0SetRegisterU32(COP0_STATUS_REG, 0b11111111_0000_0001);
    exports.testMemBusWriteU32(MI_RCP_MASK_ADDRESS, RcpInterruptMask.SET_DP); 

    // initial state
    exports.interrupt(InterruptType.RCP, RcpInterruptType.DP);
    checkInterruptSet(exports, InterruptType.RCP, RcpInterruptType.DP);

    // actual test
    exports.testMemBusWriteU32(MI_ADDRESS_MODE, (1 << 11)); // should clear DP
    checkInterruptNotSet(exports, InterruptType.RCP, RcpInterruptType.DP);
  });
});
