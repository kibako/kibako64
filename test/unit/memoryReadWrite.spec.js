/**
* @copyright 2021 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { assertHex, assertHexArray, assertHexU64, createProgram } from './wasmHelper.js';
import chai from "chai"; const {expect} = chai;

/**
 * Tests if reading and writing behaves correctly.
 * The main reason being to test if byte-order is correctly emulated
 */
describe('Memory - Byte-Order', () => 
{
  it(`Host Byte-Order Test`, async () => 
  {
    // Tests if the host environment is Little-Endian
    const {exports, memory} = await createProgram(["nop"]); 

    exports.unitTestMemoryWriteNativeTest();
    assertHexArray([...memory.slice(0x100, 0x120)],
      [
        0xAB, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0xBB, 0xAA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0xDD, 0xCC, 0xBB, 0xAA, 0x00, 0x00, 0x00, 0x00, 
        0xDD, 0xCC, 0xBB, 0xAA, 0x44, 0x33, 0x22, 0x11,
      ]
    );
  });

  it(`Write U8, Read U8`, async () => 
  {
    const {memBus} = await createProgram(["nop"]); 

    memBus.writeU8(0x1000, 0x11); memBus.writeU8(0x1001, 0x22);
    memBus.writeU8(0x1002, 0x33); memBus.writeU8(0x1003, 0x44);
    memBus.writeU8(0x1004, 0x55); memBus.writeU8(0x1005, 0x66);
    memBus.writeU8(0x1006, 0x77); memBus.writeU8(0x1007, 0x88);

    assertHex(memBus.readU8(0x1000), 0x11);
    assertHex(memBus.readU8(0x1001), 0x22);
    assertHex(memBus.readU8(0x1002), 0x33);
    assertHex(memBus.readU8(0x1003), 0x44);
    assertHex(memBus.readU8(0x1004), 0x55);
    assertHex(memBus.readU8(0x1005), 0x66);
    assertHex(memBus.readU8(0x1006), 0x77);
    assertHex(memBus.readU8(0x1007), 0x88);
  });

  it(`Write U16, Read U16`, async () => 
  {
    const {memBus} = await createProgram(["nop"]); 

    memBus.writeU16(0x1000, 0x1122);
    memBus.writeU16(0x1002, 0x3344);
    memBus.writeU16(0x1004, 0x5566);
    memBus.writeU16(0x1006, 0x7788);

    assertHex(memBus.readU16(0x1000), 0x1122);
    assertHex(memBus.readU16(0x1002), 0x3344);
    assertHex(memBus.readU16(0x1004), 0x5566);
    assertHex(memBus.readU16(0x1006), 0x7788);
  });

  it(`Write U32, Read U32`, async () => 
  {
    const {memBus} = await createProgram(["nop"]); 

    memBus.writeU32(0x1000, 0x1122_3344);
    memBus.writeU32(0x1004, 0x5566_7788);

    assertHex(memBus.readU32(0x1000), 0x1122_3344);
    assertHex(memBus.readU32(0x1004), 0x5566_7788);
  });

  it(`Write U64, Read U64`, async () => 
  {
    const {memBus} = await createProgram(["nop"]); 

    memBus.writeU64(0x1000, 0x1122_3344_5566_7788n);
    memBus.writeU64(0x1008, 0x99AA_BBCC_DDEE_FF01n);

    assertHexU64(memBus.readU64(0x1000), 0x1122_3344_5566_7788n);
    assertHexU64(memBus.readU64(0x1008), 0x99AA_BBCC_DDEE_FF01n);
  });

  it(`Write F32, Read F32`, async () => 
  {
    const {memBus} = await createProgram(["nop"]); 

    memBus.writeF32(0x1000, 1.125);
    memBus.writeF32(0x1004, -128.25);

    expect(memBus.readF32(0x1000)).equal(1.125);
    expect(memBus.readF32(0x1004)).equal(-128.25);
  });

  it(`Write F64, Read F64`, async () => 
  {
    const {memBus} = await createProgram(["nop"]); 

    memBus.writeF64(0x1000, 1.125);
    memBus.writeF64(0x1008, -128.25);

    expect(memBus.readF64(0x1000)).equal(1.125);
    expect(memBus.readF64(0x1008)).equal(-128.25);
  });

  // Mixed read-write tests

  it(`Write U64, Read U8 / U16 / U32 / ---`, async () => 
  {
    const {memBus} = await createProgram(["nop"]); 

    memBus.writeU64(0x1000, 0x1122_3344_5566_7788n);

    assertHexArray(
      [0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88], [
      memBus.readU8(0x1000), memBus.readU8(0x1001), memBus.readU8(0x1002), memBus.readU8(0x1003),
      memBus.readU8(0x1004), memBus.readU8(0x1005), memBus.readU8(0x1006), memBus.readU8(0x1007),
    ]);

    assertHexArray(
      [0x1122, 0x3344, 0x5566, 0x7788], [
      memBus.readU16(0x1000), memBus.readU16(0x1002), 
      memBus.readU16(0x1004), memBus.readU16(0x1006),
    ]);

    assertHex(memBus.readU32(0x1000), 0x1122_3344);
    assertHex(memBus.readU32(0x1004), 0x5566_7788);    
  });

  it(`Write U32, Read U8 / U16 / --- / U64`, async () => 
  {
    const {memBus} = await createProgram(["nop"]); 

    memBus.writeU32(0x1000, 0x1122_3344);
    memBus.writeU32(0x1004, 0x5566_7788);

    assertHexArray(
      [0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88], [
      memBus.readU8(0x1000), memBus.readU8(0x1001), memBus.readU8(0x1002), memBus.readU8(0x1003),
      memBus.readU8(0x1004), memBus.readU8(0x1005), memBus.readU8(0x1006), memBus.readU8(0x1007),
    ]);

    assertHexArray(
      [0x1122, 0x3344, 0x5566, 0x7788], [
      memBus.readU16(0x1000), memBus.readU16(0x1002), 
      memBus.readU16(0x1004), memBus.readU16(0x1006),
    ]);

    assertHexU64(memBus.readU64(0x1000), 0x1122_3344_5566_7788n);    
  });

  it(`Write U16, Read U8 / --- / U32 / U64`, async () => 
  {
    const {memBus} = await createProgram(["nop"]); 

    memBus.writeU16(0x1000, 0x1122);
    memBus.writeU16(0x1002, 0x3344);
    memBus.writeU16(0x1004, 0x5566);
    memBus.writeU16(0x1006, 0x7788);

    assertHexArray(
      [0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88], [
      memBus.readU8(0x1000), memBus.readU8(0x1001), memBus.readU8(0x1002), memBus.readU8(0x1003),
      memBus.readU8(0x1004), memBus.readU8(0x1005), memBus.readU8(0x1006), memBus.readU8(0x1007),
    ]);

    assertHex(memBus.readU32(0x1000), 0x1122_3344);
    assertHex(memBus.readU32(0x1004), 0x5566_7788);    

    assertHexU64(memBus.readU64(0x1000), 0x1122_3344_5566_7788n);    
  });

  it(`Write U8, Read -- / U16 / U32 / U64`, async () => 
  {
    const {memBus} = await createProgram(["nop"]); 

    memBus.writeU8(0x1000, 0x11); memBus.writeU8(0x1001, 0x22);
    memBus.writeU8(0x1002, 0x33); memBus.writeU8(0x1003, 0x44);
    memBus.writeU8(0x1004, 0x55); memBus.writeU8(0x1005, 0x66);
    memBus.writeU8(0x1006, 0x77); memBus.writeU8(0x1007, 0x88);

    assertHexArray(
      [0x1122, 0x3344, 0x5566, 0x7788], [
      memBus.readU16(0x1000), memBus.readU16(0x1002), 
      memBus.readU16(0x1004), memBus.readU16(0x1006),
    ]);

    assertHex(memBus.readU32(0x1000), 0x1122_3344);
    assertHex(memBus.readU32(0x1004), 0x5566_7788);    

    assertHexU64(memBus.readU64(0x1000), 0x1122_3344_5566_7788n);    
  });

  it(`Write U32, Read F32 / ---`, async () => 
  {
    const {memBus} = await createProgram(["nop"]); 

    memBus.writeU32(0x1000, 0x42C8_4148);
    expect(memBus.readF32(0x1000), 100.1275);
  });

  
  it(`Write U64, Read F32 / F64`, async () => 
  {
    const {memBus} = await createProgram(["nop"]); 

    memBus.writeU64(0x1000, 0xAABB_CCDD_42C8_4148n);
    memBus.writeU64(0x1008, 0x406F_43F3_5B77_1F1Bn);

    expect(memBus.readF32(0x1004), 100.1275);
    expect(memBus.readF64(0x1008)).closeTo(250.1234567, 0.0001);
  });

  // mixed write and read

  it(`Mixed Write - Read U32`, async () => 
  {
    const {memBus} = await createProgram(["nop"]); 

    memBus.writeU16(0x1000, 0x11FF);
    memBus.writeU16(0x1002, 0x3344);
    memBus.writeU8(0x1001, 0x22);

    memBus.writeU32(0x1004, 0xFFFF_77FF);
    memBus.writeU16(0x1004, 0x5566);
    memBus.writeU8(0x1007, 0x88);

    assertHex(memBus.readU32(0x1000), 0x1122_3344);
    assertHex(memBus.readU32(0x1004), 0x5566_7788);
  });

  it(`Mixed Write - Read U64`, async () => 
  {
    const {memBus} = await createProgram(["nop"]); 

    memBus.writeU32(0x1000, 0xFFFF_3344);
    memBus.writeU32(0x1004, 0x5566_FF88);
    memBus.writeU16(0x1000, 0x1122);
    memBus.writeU8(0x1006, 0x77);

    assertHexU64(memBus.readU64(0x1000), 0x1122_3344_5566_7788n);
  });
});