/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { createProgram } from '../wasmHelper.js';
import chai from "chai"; const {expect} = chai;

const SP_STATUS_ADDRESS = 0x04040010;

const getSpStatus = ex => ex.testMemBusReadU32(SP_STATUS_ADDRESS);

describe('SignalProg - Register', () => 
{
  it(`Default - zero`, async () => 
  {
    const {exports, memory} = await createProgram(["nop"]); 

    expect(exports.testMemBusReadU32(SP_STATUS_ADDRESS))
      .equal(0);
  });

  it(`clear/set - Halt`, async () => 
  {
    const {exports, memory} = await createProgram(["nop"]); 

    exports.testMemBusWriteU32(SP_STATUS_ADDRESS, (1 << 1));
    expect(getSpStatus(exports)).equal(0b00_0000_0000_0001);

    exports.testMemBusWriteU32(SP_STATUS_ADDRESS, (1 << 0));
    expect(getSpStatus(exports)).equal(0b00_0000_0000_0000);
  });

  it(`clear/set - single step`, async () => 
  {
    const {exports, memory} = await createProgram(["nop"]); 

    exports.testMemBusWriteU32(SP_STATUS_ADDRESS, (1 << 6));
    expect(getSpStatus(exports)).equal(0b00_0000_0010_0000);

    exports.testMemBusWriteU32(SP_STATUS_ADDRESS, (1 << 5));
    expect(getSpStatus(exports)).equal(0b00_0000_0000_0000);
  });

  it(`clear/set - interrupt on break`, async () => 
  {
    const {exports, memory} = await createProgram(["nop"]); 

    exports.testMemBusWriteU32(SP_STATUS_ADDRESS, (1 << 8));
    expect(getSpStatus(exports)).equal(0b00_0000_0100_0000);

    exports.testMemBusWriteU32(SP_STATUS_ADDRESS, (1 << 7));
    expect(getSpStatus(exports)).equal(0b00_0000_0000_0000);
  });

  const signalValues = [
    {set: 10, clear:  9, target:  7},
    {set: 12, clear: 11, target:  8},
    {set: 14, clear: 13, target:  9},
    {set: 16, clear: 15, target: 10},
    {set: 18, clear: 17, target: 11},
    {set: 20, clear: 19, target: 12},
    {set: 22, clear: 21, target: 13},
    {set: 24, clear: 23, target: 14},
  ];
  for(let i in signalValues)
  {
    const testCase = signalValues[i];
    it(`clear/set - signal ${i}`, async () => 
    {
      const {exports, memory} = await createProgram(["nop"]); 

      exports.testMemBusWriteU32(SP_STATUS_ADDRESS, 1 << testCase.set);
      expect(getSpStatus(exports)).equal(1 << testCase.target);

      exports.testMemBusWriteU32(SP_STATUS_ADDRESS, 1 << testCase.clear);
      expect(getSpStatus(exports)).equal(0b00_0000_0000_0000);
    });
  }

  it(`clear/set - multiple values`, async () => 
  {
    const {exports, memory} = await createProgram(["nop"]); 

    exports.testMemBusWriteU32(SP_STATUS_ADDRESS, (1 << 6));
    exports.testMemBusWriteU32(SP_STATUS_ADDRESS, (1 << 8));
    exports.testMemBusWriteU32(SP_STATUS_ADDRESS, (1 << 14));

    expect(getSpStatus(exports)).equal(0b00_0010_0110_0000);

    exports.testMemBusWriteU32(SP_STATUS_ADDRESS, (1 << 7));
    expect(getSpStatus(exports)).equal(0b00_0010_0010_0000);
  });
});
