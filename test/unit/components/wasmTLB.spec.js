/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { assertHex, assertHexArray, assertHexS64, assertHexU64, createProgram } from '../wasmHelper.js';
import chai from "chai"; const {expect} = chai;

const TLB_MASK_4K    = 0x0000_0000;
const TLB_MASK_16K   = 0x0000_6000;
const TLB_MASK_64K   = 0x0001_E000;
const TLB_MASK_256K  = 0x0007_E000;
const TLB_MASK_1M    = 0x001F_E000;
const TLB_MASK_4M    = 0x007F_E000;
const TLB_MASK_16M   = 0x01FF_E000;

describe('TLB', () => 
{
  it(`Resolve - empty - zero`, async () => 
  {
    const {exports} = await createProgram(["nop"]); 

    expect(exports.testTlbResolveAddress(
      0x8000_0000
    )).equal(0x0000_0000);
  });

  it(`Resolve - entries - zero`, async () => 
  {
    const {exports} = await createProgram(["nop"]); 

    exports.testTlbSetEntry(0x1F, TLB_MASK_4K, 0xC000_0000,  0x0200_0017, 0x0000_0001);

    expect(exports.testTlbResolveAddress(
      0x8000_0000
    )).equal(0x0000_0000);
  });
});
