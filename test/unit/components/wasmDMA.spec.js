/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { assertHex, assertHexArray, insertMemory, createProgram } from '../wasmHelper.js';
import chai from "chai"; const {expect} = chai;

const ROM_OFFSET = (1024 * 1024 * 8);

const DMA_STATE_READY   = 0;
const DMA_STATE_BUSY    = (1 << 1);
const DMA_STATE_IO_BUSY = (1 << 2);
const DMA_STATE_ERROR   = (1 << 3);

const ADDRESS_PTR_RAM         = 0xA460_0000;
const ADDRESS_PTR_ROM         = 0xA460_0004;
const ADDRESS_SIZE_RAM_TO_ROM = 0xA460_0008;
const ADDRESS_SIZE_ROM_TO_RAM = 0xA460_000C;
const ADDRESS_STATUS          = 0xA460_0010;

describe('DMA', () => 
{
  it(`Copy ROM to RAM - correct`, async () => 
  {
    const {exports, memory} = await createProgram(["nop"]); 

    insertMemory(exports, memory, [
      0x11, 0x22, 0x33, 0x44, 0x55, 0x66
    ], ROM_OFFSET + 0x1280);
    
    exports.testMemBusWriteU32(ADDRESS_PTR_ROM, 0x1000_1280);
    expect(memory[0xFF00]).equal(0); // not copied yet

    exports.testMemBusWriteU32(ADDRESS_PTR_RAM, 0x0000_FF00);
    expect(memory[0xFF00]).equal(0); // not copied yet

    exports.testMemBusWriteU32(ADDRESS_SIZE_ROM_TO_RAM, 0x03); // DMA takes size+1 bytes

    // status = success
    expect(exports.testMemBusReadU32(ADDRESS_STATUS))
      .equal(DMA_STATE_READY);

    
    // copied 4 bytes, check a bit before and after that region
    assertHexArray(
      [...memory.slice(0xFF00 - 4, 0xFF04 + 4)], 
      [0,0,0,0, 0x11, 0x22, 0x33, 0x44, 0,0,0,0]
    );
  });

  it(`Copy ROM to RAM - too large (RAM)`, async () => 
  {
    const {exports, memory} = await createProgram(["nop"]); 
    
    expect(exports.testMemBusReadU32(ADDRESS_STATUS)).equal(DMA_STATE_READY);

    exports.testMemBusWriteU32(ADDRESS_PTR_ROM, 0x1000_1280);
    exports.testMemBusWriteU32(ADDRESS_PTR_RAM, 0x0010_0000);

    exports.testMemBusWriteU32(ADDRESS_SIZE_ROM_TO_RAM, 0x70_0000); // 7MB + 1MB >= 8MB RAM

    expect(exports.testMemBusReadU32(ADDRESS_STATUS)).equal(DMA_STATE_ERROR);
  });

  it(`Copy ROM to RAM - too large (ROM)`, async () => 
  {
    const {exports, memory} = await createProgram(["nop"]); 
    
    expect(exports.testMemBusReadU32(ADDRESS_STATUS)).equal(DMA_STATE_READY);

    exports.testMemBusWriteU32(ADDRESS_PTR_ROM, 0x13F0_0000);
    exports.testMemBusWriteU32(ADDRESS_PTR_RAM, 0x0000_1200);
    
    exports.testMemBusWriteU32(ADDRESS_SIZE_ROM_TO_RAM, 0x10_0000); // 63MB + 1MB >= 64MB ROM

    expect(exports.testMemBusReadU32(ADDRESS_STATUS)).equal(DMA_STATE_ERROR);
  });


  it(`Copy ROM to RAM - invalid ROM address`, async () => 
  {
    const {exports, memory} = await createProgram(["nop"]); 
    
    expect(exports.testMemBusReadU32(ADDRESS_STATUS)).equal(DMA_STATE_READY);

    exports.testMemBusWriteU32(ADDRESS_PTR_ROM, 0x0012_3400); // not a ROM address
    exports.testMemBusWriteU32(ADDRESS_PTR_RAM, 0x0000_1000);
    
    exports.testMemBusWriteU32(ADDRESS_SIZE_ROM_TO_RAM, 0x04);

    expect(exports.testMemBusReadU32(ADDRESS_STATUS)).equal(DMA_STATE_ERROR);
  });

});
