/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { createProgram } from './wasmHelper.js';
import chai from "chai"; const {expect} = chai;

describe('WASM - Util-functions', () => 
{
    it("checksumU8 - empty", async () => 
    {
        const {exports, memory} = await createProgram([]);
        memory.fill(0x42, 0x00, 0x2000);

        let checksum = exports.testChecksumU8(0,0);
        expect(checksum).equal(0);

        checksum = exports.testChecksumU8(0x100, 0x100);
        expect(checksum).equal(0);
    });

    it("checksumU8 - zeros", async () => 
    {
        const {exports, memory} = await createProgram([]);
        memory.fill(0x00, 0x00, 0x2000);

        const checksum = exports.testChecksumU8(0x10, 0x100);
        expect(checksum).equal(0);
    });

    it("checksumU8 - example-0", async () => 
    {
        const {exports, memory} = await createProgram([]);

        // randomly generated, this is NOT real game-data
        memory.set([
          0xCA, 0x7D, 0xB9, 0x67, 0xE0, 0x87, 0x61, 0x7B, 0x75, 0x6D, 0x5A, 0xF9, 0x0C, 0xED, 0x28, 0xFB,
          0xF0, 0xAA, 0xF8, 0xDA, 0xE9, 0x17, 0xC3, 0xE0, 0x35, 0xFD, 0xC8, 0x55, 0x66, 0x28, 0x93, 0x93,
          0xDB, 0xF6, 0x0C, 0xD7, 0x4B, 0x2D, 0xFD, 0x16, 0x21, 0x54, 0x88, 0xA0, 0x03, 0xA4, 0xA2, 0x65,
          0x61, 0x88, 0x5E, 0xBA, 0x68, 0x2E, 0x15, 0x67, 0xCD, 0xF0, 0xCA, 0xE1, 0xA8, 0x0D, 0xB9, 0x27 
        ], 0x100);

        const checksum = exports.testChecksumU8(0x100, 0x140);
        expect(checksum).equal(0x0000_22A7);
    });
});
