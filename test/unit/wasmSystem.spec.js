/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { assertHex, assertHexS64, assertHexU64, createProgram, CODE_BASE_OFFSET } from './wasmHelper.js';
import chai from "chai"; const {expect} = chai;

const COP0_COUNT = 9;

describe('WASM - System & Misc.', () => 
{
    it(`Register - u32`, async () => 
    {
        const {exports} = await createProgram([]);
        exports.cpuSetRegisterU32(2, 0xFFFF_FFD6);

        expect(exports.cpuGetRegisterS32(2)).equal(-42);
        expect(exports.cpuGetRegisterU32(2) >>> 0).equal(0xFFFF_FFD6);
    });

    it(`Register - s32`, async () => 
    {
        const {exports} = await createProgram([]);
        exports.cpuSetRegisterS32(2, -42);
        
        expect(exports.cpuGetRegisterS32(2)).equal(-42);
        expect(exports.cpuGetRegisterU32(2) >>> 0).equal(0xFFFF_FFD6);
    });

    it(`Register - u64`, async () => 
    {
        const {exports} = await createProgram([]);

        exports.cpuSetRegisterU64(2, 0xFFFF_FFFF_FFFF_FFD6n);

        assertHexS64(exports.cpuGetRegisterS64(2), -42n);
        assertHexU64(exports.cpuGetRegisterU64(2), 0xFFFF_FFFF_FFFF_FFD6n);
    });

    it(`Register - get bytes`, async () => 
    {
        const {exports} = await createProgram([]);
        exports.cpuSetRegisterU32(2, 0x1234_5678);
        
        expect(exports.testCpuGetRegisterByte32(2, 0)).equal(0x12);
        expect(exports.testCpuGetRegisterByte32(2, 1)).equal(0x34);
        expect(exports.testCpuGetRegisterByte32(2, 2)).equal(0x56);
        expect(exports.testCpuGetRegisterByte32(2, 3)).equal(0x78);
    });

    it(`Register - set bytes`, async () => 
    {
        const {exports} = await createProgram([]);        
        exports.testCpuSetRegisterByte32(2, 0, 0x12);
        exports.testCpuSetRegisterByte32(2, 1, 0x34);
        exports.testCpuSetRegisterByte32(2, 2, 0x56);
        exports.testCpuSetRegisterByte32(2, 3, 0x78);

        assertHex(exports.cpuGetRegisterU32(2), 0x1234_5678);
    });
// @TODO check with memoryBus instead
/*
    it(`Address - VRAM mapping`, async () => 
    {
        const {exports} = await createProgram([]);        
        assertHex(
            exports.testCpuResolveMemoryAddress(0x8000_1234),
            0x0000_1234
        )
    });
*/  


    it(`Register (FPU) - s32`, async () => 
    {
        const {exports} = await createProgram([]);
        exports.fpuSetRegisterS32(2, 0x0ABB_CCDDEE);
        assertHex(exports.fpuGetRegisterS32(2), 0x0ABB_CCDDEE);
    });

    it(`Register (FPU) - f32`, async () => 
    {
        const {exports} = await createProgram([]);
        exports.fpuSetRegisterF32(2, 32.125);
        expect(exports.fpuGetRegisterF32(2)).equal(32.125);
    });

    it(`Register (FPU) - f64`, async () => 
    {
        const {exports} = await createProgram([]);
        exports.fpuSetRegisterF64(2, 32.125);
        assertHex(exports.fpuGetRegisterF64(2), 32.125);
    });

    it(`Register (FPU) - register conversion`, async () => 
    {
        const {exports} = await createProgram([]);

        // Positive
        exports.fpuSetRegisterS32(3, 42);
        exports.fpuSetRegisterF32(5, 245.824);
        exports.unitTestFpuRegisterConvert();

        expect(exports.fpuGetRegisterF32(2)).equal(42.0);
        expect(exports.fpuGetRegisterS32(4)).equal(245);// @TODO add correct rounding, it should be 246

        // Negative
        exports.fpuSetRegisterS32(3, -42);
        exports.fpuSetRegisterF32(5, -245.824);
        exports.unitTestFpuRegisterConvert();

        expect(exports.fpuGetRegisterF32(2)).equal(-42.0);
        expect(exports.fpuGetRegisterS32(4)).equal( -245);
    });

    it(`FPU - WASM Build-ins`, async () => 
    {
        const {exports} = await createProgram([]);

        // (floorf, floor, sqrtf, sqrt, absf) Positive
        exports.unitTestBuildins(2.45, 3.89, 36, 81, 234.5);

        expect(exports.fpuGetRegisterF32(0)).equal(2.0); // floorf
        expect(exports.fpuGetRegisterF32(1)).equal(3.0); // floor
        expect(exports.fpuGetRegisterF32(2)).equal(6.0); // sqrtf
        expect(exports.fpuGetRegisterF32(3)).equal(9.0); // sqrt
        expect(exports.fpuGetRegisterF32(4)).equal(234.5); // absf

        // (floorf, floor, sqrtf, sqrt, absf) Negative
        exports.unitTestBuildins(-2.45, -3.89, -36, -81, -234.5);

        expect(exports.fpuGetRegisterF32(0)).equal(-3.0); // floorf
        expect(exports.fpuGetRegisterF32(1)).equal(-4.0); // floor
        expect(exports.fpuGetRegisterF32(2)).to.be.NaN; // sqrtf
        expect(exports.fpuGetRegisterF32(3)).to.be.NaN; // sqrt
        expect(exports.fpuGetRegisterF32(4)).equal(234.5); // absf
    });

    it(`CPU-Register - reserved-memory`, async () => 
    {
        const {exports, memory} = await createProgram([]);
        for(let i=0; i<32; ++i) {
          exports.cpuSetRegisterU32(i, 0xFFFF_FFFF);
        }
        
        // Check if all components are initialized and don't point to any reserved memory section
        const reservedSize = (8 * 1024 * 1024) + (64 * 1024 * 1024);
        const view = new DataView(memory.buffer);
        for(let i=0; i<reservedSize; i+=4) {
          if(view.getInt32(i) !== 0) {
            throw new Error("Non-Zero value @ " + i);
          }
        }
    });

    it(`FPU-Register - reserved-memory`, async () => 
    {
        const {exports, memory} = await createProgram([]);
        for(let i=0; i<32; ++i) {
          exports.fpuSetRegisterF32(i, 321.123456789);
        }
        
        // Check if all components are initialized and don't point to any reserved memory section
        const reservedSize = (8 * 1024 * 1024) + (64 * 1024 * 1024);
        const view = new DataView(memory.buffer);
        for(let i=0; i<reservedSize; i+=4) {
          if(view.getInt32(i) !== 0) {
            throw new Error("Non-Zero value @ " + i);
          }
        }
    });

    it("COP0 - Count", async () => 
    {
        const {exports, memory} = await createProgram([
          // only NOPs
        ]);
        
        // the increments by 2 per CPU-step
        expect(exports.testCop0GetRegisterU32(COP0_COUNT)).equal(0*2);
        exports.testCpuRun(CODE_BASE_OFFSET, 2);
        expect(exports.testCop0GetRegisterU32(COP0_COUNT)).equal(2*2);
        exports.run(1);
        expect(exports.testCop0GetRegisterU32(COP0_COUNT)).equal(3*2);

        exports.testCop0SetRegisterU32(COP0_COUNT, 0); // manually reset counter

        exports.run(2);
        expect(exports.testCop0GetRegisterU32(COP0_COUNT)).equal(2*2);
        exports.run(2000);
        expect(exports.testCop0GetRegisterU32(COP0_COUNT)).equal(2002*2);

        expect(exports.testCpuGetErrorCount()).equal(0);
    });


/*
    it(`Immediate - 16->32 Padding`, async () => 
    {
        const {exports} = await createProgram([]); 
        // Edge cases
        expect(p.immPad(0x0000)).equal(0);
        expect(p.immPad(0x8000)).equal(-32768);
        expect(p.immPad(0x7FFF)).equal(32767);
        expect(p.immPad(0xFFFF)).equal(-1);

        // Random test values
        expect(p.immPad(0x002A)).equal(42);
        expect(p.immPad(0xFF9C)).equal(-100);
    });
    */
});
