/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { assertHex, CODE_BASE_OFFSET, createProgram } from '../wasmHelper.js';
import { MIPS_REG } from '../registerMapping.js';
import chai from "chai"; const {expect} = chai;

describe('WASM MIPS - Jump', () => 
{
    it("j", async () =>
    {
        const {exports, calls} = await createProgram([
          /* 0x1000 */ "x.log", // executed
          /* 0x1004 */ "j 0x1010", // jump
          /* 0x1008 */ "x.log", // immediate
          /* 0x100C */ "x.log", // <ignored>
          /* 0x1000 */ "x.log", // jump target
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$ra, 0xFFFF);
        exports.testCpuRun(CODE_BASE_OFFSET, 4);

        expect(exports.testCpuGetErrorCount()).equal(0);
        assertHex(exports.cpuGetRegisterU32(MIPS_REG.$ra), 0xFFFF); // RA unmodified
        expect(calls).to.eql([0x1000, 0x1008, 0x1010]);
    });

    it("jr", async () =>
    {
        const {exports, calls} = await createProgram([
          /* 0x1000 */ "x.log", // executed
          /* 0x1004 */ "jr $t1", // jump
          /* 0x1008 */ "x.log", // immediate
          /* 0x100C */ "x.log", // <ignored>
          /* 0x1000 */ "x.log", // <ignored>
          /* 0x1004 */ "x.log", // jump target
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$t1, 0x1014);
        exports.cpuSetRegisterU32(MIPS_REG.$ra, 0xFFFF);
        exports.testCpuRun(CODE_BASE_OFFSET, 4);

        expect(exports.testCpuGetErrorCount()).equal(0);
        assertHex(exports.cpuGetRegisterU32(MIPS_REG.$ra), 0xFFFF); // RA unmodified
        expect(calls).to.eql([0x1000, 0x1008, 0x1014]);
    });

    it("jal", async () =>
    {
        const {exports, calls} = await createProgram([
          /* 0x1000 */ "x.log", // executed
          /* 0x1004 */ "jal 0x1014", // jump
          /* 0x1008 */ "x.log", // immediate
          /* 0x100C */ "x.log", // <ignored> [return address]
          /* 0x1010 */ "x.log", // <ignored>
          /* 0x1014 */ "x.log", // jump target
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$ra, 0xFFFF);
        exports.testCpuRun(CODE_BASE_OFFSET, 4);

        expect(exports.testCpuGetErrorCount()).equal(0);
        assertHex(exports.cpuGetRegisterU32(MIPS_REG.$ra), 0x100C); // RA set
        expect(calls).to.eql([0x1000, 0x1008, 0x1014]);
    });
    
    it("jalr", async () =>
    {
        const {exports, calls} = await createProgram([
          /* 0x1000 */ "x.log", // executed
          /* 0x1004 */ "jalr $t1", // jump
          /* 0x1008 */ "x.log", // immediate
          /* 0x100C */ "x.log", // <ignored> [return address]
          /* 0x1010 */ "x.log", // <ignored>
          /* 0x1014 */ "x.log", // jump target
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$t1, 0x1014);
        exports.cpuSetRegisterU32(MIPS_REG.$ra, 0xFFFF);
        exports.testCpuRun(CODE_BASE_OFFSET, 4);

        expect(exports.testCpuGetErrorCount()).equal(0);
        assertHex(exports.cpuGetRegisterU32(MIPS_REG.$ra), 0x100C); // RA set
        expect(calls).to.eql([0x1000, 0x1008, 0x1014]);
    });
});
