/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { assertHex, assertHexArray, insertMemory, assertHexU64, CODE_BASE_OFFSET, createProgram } from '../wasmHelper.js';
import { MIPS_REG } from '../registerMapping.js';
import chai from "chai"; const {expect} = chai;

describe('WASM MIPS - I/O', () => 
{
  it("lw", async () => 
  {
      const {exports, memory} = await createProgram(["lw $v0, 0x10($v1)"]);

      insertMemory(exports, memory, [
        0xFF, 0xFE, 0x49, 0x97
      ], 0x30);

      exports.cpuSetRegisterU64(MIPS_REG.$v0, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterU32(MIPS_REG.$v1, 0x20);
      exports.testCpuRun(CODE_BASE_OFFSET, 1);

      expect(exports.testCpuGetErrorCount()).equal(0);
      expect(exports.cpuGetRegisterS32(MIPS_REG.$v0)).equal(-112233);
      expect(exports.cpuGetRegisterS64(MIPS_REG.$v0)).equal(-112233n);
  });

  it("lwu", async () => 
  {
      const {exports, memory} = await createProgram(["lwu $v0, 0x10($v1)"]);

      insertMemory(exports, memory, [
        0xFF, 0xFE, 0x49, 0x97
      ], 0x30);

      exports.cpuSetRegisterU64(MIPS_REG.$v0, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterU32(MIPS_REG.$v1, 0x20);
      exports.testCpuRun(CODE_BASE_OFFSET, 1);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHex(   exports.cpuGetRegisterU32(MIPS_REG.$v0),           0xFFFE_4997);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$v0), 0x0000_0000_FFFE_4997n);
  });

  it("ld", async () => 
  {
      const {exports, memory} = await createProgram(["ld $v0, 0x10($v1)"]);
      
      insertMemory(exports, memory, [
          0x11, 0x22, 0x33, 0x44,
          0xAA, 0xBB, 0xCC, 0xDD,
      ], 0x30);

      exports.cpuSetRegisterU64(MIPS_REG.$v0, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterU32(MIPS_REG.$v1, 0x20);
      exports.testCpuRun(CODE_BASE_OFFSET, 1);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$v0), 0x11223344_AABBCCDDn);
  });

  it("lh", async () => 
  {
      const {exports, memory} = await createProgram(["lh $v0, 0x10($v1)"]);
      insertMemory(exports, memory, [
          0xFF, 0xD6, // -42 as s16
      ], 0x30);

      exports.cpuSetRegisterU32(MIPS_REG.$v0, 0xFFFF_FFFF);
      exports.cpuSetRegisterU32(MIPS_REG.$v1, 0x20);
      exports.testCpuRun(CODE_BASE_OFFSET, 1);

      expect(exports.testCpuGetErrorCount()).equal(0);
      expect(exports.cpuGetRegisterS32(MIPS_REG.$v0)).equal(-42);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$v0), 0xFFFF_FFD6);
  });

  it("lhu", async () => 
  {
      const {exports, memory} = await createProgram(["lhu $v0, 0x10($v1)"]);
      insertMemory(exports, memory, [
          0xFF, 0xD6,
      ], 0x30);

      exports.cpuSetRegisterU32(MIPS_REG.$v0, 0xFFFF_FFFF);
      exports.cpuSetRegisterU32(MIPS_REG.$v1, 0x20);
      exports.testCpuRun(CODE_BASE_OFFSET, 1);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$v0), 0x0000_FFD6);
  });

  it("lb", async () => 
  {
      const {exports, memory} = await createProgram(["lb $v0, 0x10($v1)"]);
      insertMemory(exports, memory, [
          0xD6, // -42 as s8
      ], 0x30);

      exports.cpuSetRegisterU32(MIPS_REG.$v0, 0xFFFF_FFFF);
      exports.cpuSetRegisterU32(MIPS_REG.$v1, 0x20);
      exports.testCpuRun(CODE_BASE_OFFSET, 1);

      expect(exports.testCpuGetErrorCount()).equal(0);
      expect(exports.cpuGetRegisterS32(MIPS_REG.$v0)).equal(-42);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$v0), 0xFFFF_FFD6);
  });

  it("lbu", async () => 
  {
      const {exports, memory} = await createProgram(["lbu $v0, 0x10($v1)"]);
      insertMemory(exports, memory, [
          0xD6,
      ], 0x30);

      exports.cpuSetRegisterU32(MIPS_REG.$v0, 0xFFFF_FFFF);
      exports.cpuSetRegisterU32(MIPS_REG.$v1, 0x20);
      exports.testCpuRun(CODE_BASE_OFFSET, 1);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$v0), 0xD6);
  });

  it("sd", async () => 
  {
      const {exports, memory} = await createProgram([
          "sd $v1, 0x10($a1)",
      ]);

      exports.cpuSetRegisterU32(MIPS_REG.$a1, 0x20);
      exports.cpuSetRegisterU64(MIPS_REG.$v1, 0x1122_3344_5566_7788n);

      exports.testCpuRun(CODE_BASE_OFFSET, 1);
      
      expect(exports.testCpuGetErrorCount()).equal(0);
      
      
      expect(memory[0x30]).equal(0x11);
      expect(memory[0x31]).equal(0x22);
      expect(memory[0x32]).equal(0x33);
      expect(memory[0x33]).equal(0x44);
      expect(memory[0x34]).equal(0x55);
      expect(memory[0x35]).equal(0x66);
      expect(memory[0x36]).equal(0x77);
      expect(memory[0x37]).equal(0x88);
  });

  it("sw", async () => 
  {
      const {exports, memory} = await createProgram([
          "sw $v1, 0x10($a1)",
      ]);

      exports.cpuSetRegisterU32(MIPS_REG.$a1, 0x20);
      exports.cpuSetRegisterU32(MIPS_REG.$v1, 0xFF439EB2);

      exports.testCpuRun(CODE_BASE_OFFSET, 1);
      
      expect(exports.testCpuGetErrorCount()).equal(0);
      

      expect(memory[0x30]).equal(0xFF);
      expect(memory[0x31]).equal(0x43);
      expect(memory[0x32]).equal(0x9E);
      expect(memory[0x33]).equal(0xB2);
  });

  it("lw + sw", async () => 
  {
      const {exports, memory} = await createProgram([
          "lw $v0, 0x10($v1)",
          "sw $v0, 0x20($v1)",
      ]);
      insertMemory(exports, memory, [
          0xAA, 0xBB, 0xCC, 0xDD
      ], 0x30);

      exports.cpuSetRegisterU32(MIPS_REG.$v1, 0x20);
      exports.testCpuRun(CODE_BASE_OFFSET, 2);

      expect(exports.testCpuGetErrorCount()).equal(0);
      

      expect(memory[0x40]).equal(0xAA);
      expect(memory[0x41]).equal(0xBB);
      expect(memory[0x42]).equal(0xCC);
      expect(memory[0x43]).equal(0xDD);
  });
  
  it("lwl", async () => 
  {
      const {exports, memory} = await createProgram([
          "lwl $t0, 0x00($a0)",
          "lwl $t1, 0x01($a0)",
          "lwl $t2, 0x02($a0)",
          "lwl $t3, 0x03($a0)",
          "lwl $t4, 0x04($a0)",
      ]);
      insertMemory(exports, memory, [
          0x11, 0x22, 0x33, 0x44
      ], 0x100);

      exports.cpuSetRegisterU32(MIPS_REG.$a0, 0x100);
      exports.cpuSetRegisterU32(MIPS_REG.$t0, 0xFFFF_FFFF);
      exports.cpuSetRegisterU32(MIPS_REG.$t1, 0xFFFF_FFFF);
      exports.cpuSetRegisterU32(MIPS_REG.$t2, 0xFFFF_FFFF);
      exports.cpuSetRegisterU32(MIPS_REG.$t3, 0xFFFF_FFFF);
      exports.cpuSetRegisterU32(MIPS_REG.$t4, 0xFFFF_FFFF);
      exports.testCpuRun(CODE_BASE_OFFSET, 5);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t0), 0x11223344);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t1), 0x223344_FF);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t2), 0x3344_FFFF);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t3), 0x44_FFFFFF);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t4), 0x000000000);
  });

  it("lwr", async () => 
  {
      const {exports, memory} = await createProgram([
          "lwr $t0, 0x00($a0)",
          "lwr $t1, 0x01($a0)",
          "lwr $t2, 0x02($a0)",
          "lwr $t3, 0x03($a0)",
          "lwr $t4, 0x04($a0)",
      ]);
      insertMemory(exports, memory, [
          0x11, 0x22, 0x33, 0x44
      ], 0x100);

      exports.cpuSetRegisterU32(MIPS_REG.$a0, 0x100);
      exports.cpuSetRegisterU32(MIPS_REG.$t0, 0xFFFF_FFFF);
      exports.cpuSetRegisterU32(MIPS_REG.$t1, 0xFFFF_FFFF);
      exports.cpuSetRegisterU32(MIPS_REG.$t2, 0xFFFF_FFFF);
      exports.cpuSetRegisterU32(MIPS_REG.$t3, 0xFFFF_FFFF);
      exports.cpuSetRegisterU32(MIPS_REG.$t4, 0xFFFF_FFFF);
      exports.testCpuRun(CODE_BASE_OFFSET, 5);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t0), 0xFFFFFF_11);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t1), 0xFFFF_1122);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t2), 0xFF_112233);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t3), 0x11223344);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t4), 0xFFFF_FF00);
  });

  it("swl", async () => 
  {
      const {exports, memory} = await createProgram([
          "swl $v1, 0x10($a1)",
          "swl $v1, 0x21($a1)",
          "swl $v1, 0x32($a1)",
          "swl $v1, 0x43($a1)",
      ]);

      exports.cpuSetRegisterU32(MIPS_REG.$a1, 0x200);
      exports.cpuSetRegisterU32(MIPS_REG.$v1, 0x11223344);

      exports.testCpuRun(CODE_BASE_OFFSET, 4);
      expect(exports.testCpuGetErrorCount()).equal(0);

      

      assertHexArray(
          [...memory.slice(0x210, 0x216)],
          [0x11, 0x22, 0x33, 0x44,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x220, 0x226)],
          [0x00, 0x11, 0x22, 0x33,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x230, 0x236)],
          [0x00, 0x00, 0x11, 0x22,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x240, 0x246)],
          [0x00, 0x00, 0x00, 0x11,   0x00, 0x00]
      );
  });

  it("swr", async () => 
  {
      const {exports, memory} = await createProgram([
          "swr $v1, 0x10($a1)",
          "swr $v1, 0x21($a1)",
          "swr $v1, 0x32($a1)",
          "swr $v1, 0x43($a1)",
      ]);

      exports.cpuSetRegisterU32(MIPS_REG.$a1, 0x200);
      exports.cpuSetRegisterU32(MIPS_REG.$v1, 0x11223344);

      exports.testCpuRun(CODE_BASE_OFFSET, 4);
      expect(exports.testCpuGetErrorCount()).equal(0);

      

      assertHexArray(
          [...memory.slice(0x210, 0x216)],
          [0x44, 0x00, 0x00, 0x00,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x220, 0x226)],
          [0x33, 0x44, 0x00, 0x00,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x230, 0x236)],
          [0x22, 0x33, 0x44, 0x00,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x240, 0x246)],
          [0x11, 0x22, 0x33, 0x44,   0x00, 0x00]
      );
  });
/*
  it("ldl", async () => 
  {
      const {exports, memory} = await createProgram([
          "ldl $t0, 0x00($a0)",
          "ldl $t1, 0x01($a0)",
          "ldl $t2, 0x02($a0)",
          "ldl $t3, 0x03($a0)",
          "ldl $t4, 0x04($a0)",
          "ldl $t5, 0x05($a0)",
          "ldl $t6, 0x06($a0)",
          "ldl $t7, 0x07($a0)",
          "ldl $t8, 0x08($a0)",
      ]);
      insertMemory(exports, memory, [
          0x11, 0x22, 0x33, 0x44, 0xAA, 0xBB, 0xCC, 0xDD
      ]), 0x100);

      exports.cpuSetRegisterU32(MIPS_REG.$a0, 0x100);
      exports.cpuSetRegisterU64(MIPS_REG.$t0, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterU64(MIPS_REG.$t1, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterU64(MIPS_REG.$t2, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterU64(MIPS_REG.$t3, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterU64(MIPS_REG.$t4, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterU64(MIPS_REG.$t5, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterU64(MIPS_REG.$t6, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterU64(MIPS_REG.$t7, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterU64(MIPS_REG.$t8, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.testCpuRun(CODE_BASE_OFFSET, 9);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t0), 0x11223344AABBCCDDn);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t1), 0x223344AABBCCDD_FFn);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t2), 0x3344AABBCCDD_FFFFn);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t3), 0x44AABBCCDD_FFFFFFn);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t4), 0xAABBCCDD_FFFFFFFFn);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t5), 0xBBCCDD_FFFFFFFFFFn);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t6), 0xCCDD_FFFFFFFFFFFFn);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t7), 0xDD_FFFFFFFFFFFFFFn);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t8), 0x00000000_00000000n);
  });
*/
  it("ldr", async () => 
  {
      const {exports, memory} = await createProgram([
          "ldr $t0, 0x00($a0)",
          "ldr $t1, 0x01($a0)",
          "ldr $t2, 0x02($a0)",
          "ldr $t3, 0x03($a0)",
          "ldr $t4, 0x04($a0)",
          "ldr $t5, 0x05($a0)",
          "ldr $t6, 0x06($a0)",
          "ldr $t7, 0x07($a0)",
          "ldr $t8, 0x08($a0)",
      ]);
      insertMemory(exports, memory, [
          0x42, 0x42, 0x42, 0x42, // dummy data
          0x11, 0x22, 0x33, 0x44, 0xAA, 0xBB, 0xCC, 0xDD,
          0x42, 0x42, 0x42, 0x42  // dummy data
      ], 0x100-4);

      exports.cpuSetRegisterU32(MIPS_REG.$a0, 0x100);
      exports.cpuSetRegisterU64(MIPS_REG.$t0, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterU64(MIPS_REG.$t1, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterU64(MIPS_REG.$t2, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterU64(MIPS_REG.$t3, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterU64(MIPS_REG.$t4, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterU64(MIPS_REG.$t5, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterU64(MIPS_REG.$t6, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterU64(MIPS_REG.$t7, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterU64(MIPS_REG.$t8, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.testCpuRun(CODE_BASE_OFFSET, 9);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t0), 0xFFFFFFFFFFFFFF_11n);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t1), 0xFFFFFFFFFFFF_1122n);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t2), 0xFFFFFFFFFF_112233n);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t3), 0xFFFFFFFF_11223344n);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t4), 0xFFFFFF_11223344AAn);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t5), 0xFFFF_11223344AABBn);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t6), 0xFF_11223344AABBCCn);

      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t7), 0x11223344_AABBCCDDn);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t8), 0xFFFFFFFF_FFFFFF42n);
  });

  it("sdl", async () => 
  {
      const {exports, memory} = await createProgram([
          "sdl $v1, 0x10($a1)",
          "sdl $v1, 0x21($a1)",
          "sdl $v1, 0x32($a1)",
          "sdl $v1, 0x43($a1)",
          "sdl $v1, 0x54($a1)",
          "sdl $v1, 0x65($a1)",
          "sdl $v1, 0x76($a1)",
          "sdl $v1, 0x87($a1)",
      ]);

      exports.cpuSetRegisterU32(MIPS_REG.$a1, 0x200);
      exports.cpuSetRegisterU64(MIPS_REG.$v1, 0x11223344_AABBCCDDn);

      exports.testCpuRun(CODE_BASE_OFFSET, 8);
      expect(exports.testCpuGetErrorCount()).equal(0);

      

      assertHexArray(
          [...memory.slice(0x210, 0x21A)],
          [0x11, 0x22, 0x33, 0x44, 0xAA, 0xBB, 0xCC, 0xDD,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x220, 0x22A)],
          [0x00, 0x11, 0x22, 0x33, 0x44, 0xAA, 0xBB, 0xCC,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x230, 0x23A)],
          [0x00, 0x00, 0x11, 0x22, 0x33, 0x44, 0xAA, 0xBB,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x240, 0x24A)],
          [0x00, 0x00, 0x00, 0x11, 0x22, 0x33, 0x44, 0xAA,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x250, 0x25A)],
          [0x00, 0x00, 0x00, 0x00, 0x11, 0x22, 0x33, 0x44,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x260, 0x26A)],
          [0x00, 0x00, 0x00, 0x00, 0x00, 0x11, 0x22, 0x33,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x270, 0x27A)],
          [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11, 0x22,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x280, 0x28A)],
          [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11,   0x00, 0x00]
      );
  });

  it("sdr", async () => 
  {
      const {exports, memory} = await createProgram([
          "sdr $v1, 0x10($a1)",
          "sdr $v1, 0x21($a1)",
          "sdr $v1, 0x32($a1)",
          "sdr $v1, 0x43($a1)",
          "sdr $v1, 0x54($a1)",
          "sdr $v1, 0x65($a1)",
          "sdr $v1, 0x76($a1)",
          "sdr $v1, 0x87($a1)",
      ]);

      exports.cpuSetRegisterU32(MIPS_REG.$a1, 0x200);
      exports.cpuSetRegisterU64(MIPS_REG.$v1, 0x11223344_AABBCCDDn);

      exports.testCpuRun(CODE_BASE_OFFSET, 8);
      expect(exports.testCpuGetErrorCount()).equal(0);

      

      assertHexArray(
          [...memory.slice(0x210, 0x21A)],
          [0xDD, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x220, 0x22A)],
          [0xCC, 0xDD, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x230, 0x23A)],
          [0xBB, 0xCC, 0xDD, 0x00, 0x00, 0x00, 0x00, 0x00,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x240, 0x24A)],
          [0xAA, 0xBB, 0xCC, 0xDD, 0x00, 0x00, 0x00, 0x00,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x250, 0x25A)],
          [0x44, 0xAA, 0xBB, 0xCC, 0xDD, 0x00, 0x00, 0x00,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x260, 0x26A)],
          [0x33, 0x44, 0xAA, 0xBB, 0xCC, 0xDD, 0x00, 0x00,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x270, 0x27A)],
          [0x22, 0x33, 0x44, 0xAA, 0xBB, 0xCC, 0xDD, 0x00,   0x00, 0x00]
      );
      assertHexArray(
          [...memory.slice(0x280, 0x28A)],
          [0x11, 0x22, 0x33, 0x44, 0xAA, 0xBB, 0xCC, 0xDD,   0x00, 0x00]
      );
  });

  it("lui", async () => 
  {
      const {exports, memory} = await createProgram([
        "lui $t1, 0x0000",
        "lui $t2, 0x4242",
        "lui $t3, 0x82E0", // -32032
        "lui $t4, 0xFFFF", // -1
        ]);
      
      exports.testCpuRun(CODE_BASE_OFFSET, 4);

      assertHex(exports.testCpuGetErrorCount(), 0);
      assertHex(exports.cpuGetRegisterS32(MIPS_REG.$t1), 0x0000_0000);
      assertHex(exports.cpuGetRegisterS32(MIPS_REG.$t2), 0x4242_0000);
      assertHex(exports.cpuGetRegisterS32(MIPS_REG.$t3), -2099249152);
      assertHex(exports.cpuGetRegisterS32(MIPS_REG.$t4),      -65536);
  });

  it("lui + ori", async () => 
  {
      const {exports, memory} = await createProgram([
        "lui $t1, 0x7FFF",
        "ori $t1, $t1, 0xFFF0"
        ]);
        
      exports.testCpuRun(CODE_BASE_OFFSET, 2);

      assertHex(exports.testCpuGetErrorCount(), 0);
      assertHex(exports.cpuGetRegisterS32(MIPS_REG.$t1), 0x7FFF_FFF0);
  });
});
