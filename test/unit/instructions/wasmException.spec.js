/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { CODE_BASE_OFFSET, createProgram } from '../wasmHelper.js';
import { MIPS_REG } from '../registerMapping.js';
import chai from "chai"; const {expect} = chai;

describe('MIPS - Exception', () => 
{
  /*
    it("teq - not-equal", async () =>
    {
        const {exports, calls} = await createProgram([
          "teq $v0, $v1"
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$v0, 0x42);
        exports.cpuSetRegisterU32(MIPS_REG.$v1, 0xFF);

        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
    });
    */
});
