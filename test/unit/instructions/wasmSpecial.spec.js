/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { insertMemory, CODE_BASE_OFFSET, createProgram } from '../wasmHelper.js';
import { MIPS_REG } from '../registerMapping.js';
import chai from "chai"; const {expect} = chai;

describe('WASM MIPS - Special', () => 
{
  it("x_inject simple", async () => 
  {
      const injectCalls = [];
      const {exports, calls} = await createProgram([
        /* 0x1000 */ "x.log",    // log
        /* 0x1004 */ "x.inject", // should call external function
        /* 0x1008 */ "x.log",    // log
      ], {
        hook_inject: (pc, a0, a1, a2, a3) => { 
          injectCalls.push({pc: pc, args: [a0, a1, a2, a3]});
        }
      });

      exports.cpuSetRegisterU32(MIPS_REG.$a0, 0x11);
      exports.cpuSetRegisterU32(MIPS_REG.$a1, 0x22);
      exports.cpuSetRegisterU32(MIPS_REG.$a2, 0x33);
      exports.cpuSetRegisterU32(MIPS_REG.$a3, 0x44);

      exports.testCpuRun(CODE_BASE_OFFSET, 5);

      expect(exports.testCpuGetErrorCount()).equal(0);
      expect(calls).to.eql([0x1000, 0x1008]);

      expect(injectCalls).to.eql([
        {pc: 0x1004, args: [0x11, 0x22, 0x33, 0x44]}
      ])

      expect(exports.cpuGetRegisterU32(MIPS_REG.$ra)).equal(0); // no RA set
  });

  it("x_inject multiple times", async () => 
  {
      const injectCalls = [];
      const {exports, calls, memory} = await createProgram([
        /* 0x1000 */ "x.log",    // log
        /* 0x1004 */ "x.inject", // should call external function
        /* 0x1008 */ "x.log",    // log
        /* 0x100C */ "lwu $a1, 0x00($t0)",
        /* 0x1010 */ "x.inject", // should call external function
        /* 0x1014 */ "x.inject", // should call external function
      ], {
        hook_inject: (pc, a0, a1, a2, a3) => { 
          injectCalls.push({pc: pc, args: [a0>>>0, a1>>>0, a2>>>0, a3>>>0]});
        }
      });

      insertMemory(exports, memory, [
        0xAA, 0xBB, 0xCC, 0xDD
      ], 0x100);

      exports.cpuSetRegisterU32(MIPS_REG.$t0, 0x100);

      exports.cpuSetRegisterU32(MIPS_REG.$a0, 0x11);
      exports.cpuSetRegisterU32(MIPS_REG.$a1, 0x22);
      exports.cpuSetRegisterU32(MIPS_REG.$a2, 0x33);
      exports.cpuSetRegisterU32(MIPS_REG.$a3, 0x44);

      exports.testCpuRun(CODE_BASE_OFFSET, 7);

      expect(exports.testCpuGetErrorCount()).equal(0);
      expect(calls).to.eql([0x1000, 0x1008]);

      expect(injectCalls).to.eql([
        {pc: 0x1004, args: [0x11, 0x22, 0x33, 0x44]},
        {pc: 0x1010, args: [0x11, 0xAABBCCDD, 0x33, 0x44]},
        {pc: 0x1014, args: [0x11, 0xAABBCCDD, 0x33, 0x44]},
      ]);

      expect(exports.cpuGetRegisterU32(MIPS_REG.$ra)).equal(0); // no RA set
  });

  it("x_inject with patched function", async () => 
  {
      const injectCalls = [];
      const {exports, calls} = await createProgram([
        /* 0x1000 */ "x.log",    // log
        /* 0x1004 */ "jal 0x1018", // call function that should be mocked
        /* 0x1008 */ "nop",
        /* 0x100C */ "nop",
        /* 0x1010 */ "jal 0x2000", // "stop" programm
        /* 0x1014 */ "x.log",
        /* 0x1018 */ "x.log", // function body
        /* 0x101C */ "x.log", // function body
        /* 0x1020 */ "x.log", // function body
        /* 0x1024 */ "jr",    // function body
      ], {
        hook_inject: (pc, a0, a1, a2, a3) => { 
          injectCalls.push({pc: pc, args: [a0, a1, a2, a3]});
        }
      });

      exports.mockFunction(0x1018);

      exports.cpuSetRegisterU32(MIPS_REG.$a0, 0x11);
      exports.cpuSetRegisterU32(MIPS_REG.$a1, 0x22);
      exports.cpuSetRegisterU32(MIPS_REG.$a2, 0x33);
      exports.cpuSetRegisterU32(MIPS_REG.$a3, 0x44);

      exports.testCpuRun(CODE_BASE_OFFSET, 15);

      expect(exports.testCpuGetErrorCount()).equal(0);
      expect(calls).to.eql([0x1000, 0x1014]);

      expect(injectCalls).to.eql([
        {pc: 0x1018, args: [0x11, 0x22, 0x33, 0x44]}
      ])

      expect(exports.cpuGetRegisterU32(MIPS_REG.$ra)).equal(0x1018); // last jr sets this
  });

});
