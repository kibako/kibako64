/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { assertHex, assertHexArray, insertMemory, assertHexU64, CODE_BASE_OFFSET, createProgram } from '../wasmHelper.js';
import { MIPS_REG } from '../registerMapping.js';
import chai from "chai"; const {expect} = chai;

describe('System - FPU', () => 
{
    it("mov.s", async () => 
    {
        const {exports, memory} = await createProgram([
            "mov.s $f1, $f2"
        ]);
        
        expect(exports.fpuGetRegisterF32(MIPS_REG.$f1)).equal(0);

        exports.fpuSetRegisterF32(MIPS_REG.$f2, 1.125);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.fpuGetRegisterF32(MIPS_REG.$f1)).equal(1.125);
    });

    it("mtc1", async () => 
    {
        const {exports, memory} = await createProgram([
            "lw $v0, 0x20($zero)",
            "mtc1 $v0, $f4"
        ]);
        insertMemory(exports, memory, [
            0x10, 0x20, 0x30, 0x40
        ], 0x20);

        exports.testCpuRun(CODE_BASE_OFFSET, 2);

        expect(exports.testCpuGetErrorCount()).equal(0);
        assertHex(exports.fpuGetRegisterS32(4), 0x10203040);
    });

    it("dmtc1", async () => 
    {
        const {exports, memory} = await createProgram([
            "dmtc1 $v0, $f4"
        ]);

        exports.cpuSetRegisterU64(MIPS_REG.$v0, 0x40FE_2400_4000_0000n);

        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        assertHexU64(exports.fpuGetRegisterS64(4), 0x40FE_2400_4000_0000n);
        expect(exports.fpuGetRegisterF64(4)).equal(123456.015625);
    });

    it("mfc1", async () => 
    {
        const {exports, memory} = await createProgram([
            "mfc1 $t1, $f4"
        ]);
        
        exports.fpuSetRegisterF32(MIPS_REG.$f4, 16.5);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.fpuGetRegisterF32(MIPS_REG.$f4)).equal(16.5);
        assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t1), 0x4184_0000);
    });

    it("dmfc1", async () => 
    {
        const {exports, memory} = await createProgram([
            "dmfc1 $t1, $f4"
        ]);
        
        exports.fpuSetRegisterF64(MIPS_REG.$f4, 123456.015625);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.fpuGetRegisterF64(MIPS_REG.$f4)).equal(123456.015625);
        assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t1), 0x40FE_2400_4000_0000n);
    });

    it("swc1", async () => 
    {
        const {exports, memory} = await createProgram([
            "swc1 $f4, 0x10($a1)",
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$a1, 0x20);
        exports.fpuSetRegisterF32(4, 2.003906);

        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        

        assertHexArray(
            [...memory.slice(0x30, 0x34)],
            [0x40, 0x00, 0x3F, 0xFF]
        );
    });

    it("sdc1", async () => 
    {
        const {exports, memory} = await createProgram([
            "sdc1 $f4, 0x10($a1)",
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$a1, 0x20);
        exports.fpuSetRegisterF64(4, 1.123456789);

        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        
        
        assertHexArray(
            [...memory.slice(0x30, 0x38)],
            [0x3F, 0xF1, 0xF9, 0xAD, 0xD3, 0x73, 0x96, 0x36]
        );
    });

    it("lwc1", async () => 
    {
        const {exports, memory} = await createProgram([
            "lwc1 $f4, 0x10($a1)",
        ]);
        insertMemory(exports, memory, [
            0x40, 0x00, 0x3F, 0xFF
        ], 0x30);
        exports.cpuSetRegisterU32(MIPS_REG.$a1, 0x20);

        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.fpuGetRegisterF32(MIPS_REG.$f4)).to.be.closeTo( 2.003906, 0.0001);
    });

    it("ldc1", async () => 
    {
        const {exports, memory} = await createProgram([
            "ldc1 $f4, 0x10($a1)",
        ]);
        insertMemory(exports, memory, [
            0x40, 0x00, 0x5D, 0x8C,
            0x6D, 0x61, 0x2C, 0x68,
        ], 0x30);
        exports.cpuSetRegisterU32(MIPS_REG.$a1, 0x20);

        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.fpuGetRegisterF64(MIPS_REG.$f4)).to.be.closeTo( 2.045678, 0.0001);
    });

    it("cvt.s.w", async () => 
    {
        const {exports, memory} = await createProgram([
            "cvt.s.w $f2, $f1"
        ]);
        
        exports.fpuSetRegisterS32(1, 0x0000_0010);

        expect(exports.fpuGetRegisterF32(2)).equal(0);

        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.fpuGetRegisterS32(1)).equal(0x10);

        // should be a float value, and have its type set
        expect(exports.fpuGetRegisterF32(2)).equal(16.0);
    });

    it("cvt.d.s", async () => 
    {
        const {exports, memory} = await createProgram([
            "lui $t1, 0xBF00",
            "mtc1 $t1, $f1", // $f1 = -0.5
            "cvt.d.s $f2, $f1",
        ]);
        
        exports.testCpuRun(CODE_BASE_OFFSET, 3);

        expect(exports.testCpuGetErrorCount()).equal(0);
        
        assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t1), 0xBF00_0000);
        assertHex(exports.fpuGetRegisterF64(2), -0.5);
    });

    it("cvt.d.w", async () => 
    {
        const {exports, memory} = await createProgram([
            "cvt.d.w $f2, $f4",
        ]);
        
        exports.fpuSetRegisterS32(4, -42);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        
        expect(exports.fpuGetRegisterF64(2)).to.be.closeTo( -42, 0.0001);
    });

    it("cvt.d.l", async () => 
    {
        const {exports, memory} = await createProgram([
            "cvt.d.l $f2, $f4",
        ]);
        
        exports.fpuSetRegisterS64(4, -42n);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        
        expect(exports.fpuGetRegisterF64(2)).to.be.closeTo( -42, 0.0001);
    });

    it("cvt.s.d", async () => 
    {
        const {exports, memory} = await createProgram([
            "ldc1 $f2, 0x10($a1)", // $f2 = 0.045678
            "cvt.s.d $f4, $f2",
        ]);
        insertMemory(exports, memory, [
            0x40, 0x00, 0x5D, 0x8C,
            0x6D, 0x61, 0x2C, 0x68,
        ], 0x30);
        exports.cpuSetRegisterU32(MIPS_REG.$a1, 0x20);

        exports.testCpuRun(CODE_BASE_OFFSET, 2);

        expect(exports.testCpuGetErrorCount()).equal(0);

        expect(exports.fpuGetRegisterF64(MIPS_REG.$f2)).to.be.closeTo( 2.045678, 0.0001);
        expect(exports.fpuGetRegisterF32(MIPS_REG.$f2)).to.not.be.closeTo( 2.045678, 0.0001);

        expect(exports.fpuGetRegisterF32(MIPS_REG.$f4)).to.be.closeTo( 2.045678, 0.0001);

        expect(exports.fpuGetRegisterF64(MIPS_REG.$f4)).to.not.be.closeTo( 2.045678, 0.0001);
    });

    it("trunc.w.s (positive)", async () => 
    {
        const {exports, memory} = await createProgram([
            "trunc.w.s $f1, $f10",
        ]);
        
        exports.fpuSetRegisterF32(10, 19.87654);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        
         // should be a int value, and have its type set
         expect(exports.fpuGetRegisterS32(1)).equal(19);
    });

    it("trunc.w.s (negative)", async () => 
    {
        const {exports, memory} = await createProgram([
            "trunc.w.s $f1, $f10",
        ]);
        
        exports.fpuSetRegisterF32(10, -19.75);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        
         // should be a int value, and have its type set
         expect(exports.fpuGetRegisterS32(1)).equal(-19);
    });

    it("trunc.w.d (positive)", async () => 
    {
        const {exports, memory} = await createProgram([
            "trunc.w.d $f2, $f10",
        ]);
        
        exports.fpuSetRegisterF64(10, 19.87654);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        
         // should be a int value, and have its type set
        expect(exports.fpuGetRegisterS32(2)).equal(19);
    });

    it("trunc.w.d (negative)", async () => 
    {
        const {exports, memory} = await createProgram([
            "trunc.w.d $f2, $f10",
        ]);
        
        exports.fpuSetRegisterF64(10, -1.25);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        
         // should be a int value, and have its type set
        expect(exports.fpuGetRegisterS32(2)).equal(-1);
    });

    it("abs.s", async () => 
    {
        const {exports, memory} = await createProgram([
            "lui $t1, 0xC1C3",
            "mtc1 $t1, $f10", // $f10 = -24.375
            "abs.s $f1, $f10",
        ]);
        
        exports.testCpuRun(CODE_BASE_OFFSET, 3);

        expect(exports.testCpuGetErrorCount()).equal(0);
        
         // should be a float value, and have its type set
         assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t1)>>>0, 0xC1C3_0000);
         //expect(exports.fpuGetRegisterS32(1)).equal(1); // @TODO check!
         expect(exports.fpuGetRegisterF32(10)).equal(-24.375);
         expect(exports.fpuGetRegisterF32(1)).equal(24.375);
    });

    it("add.s", async () => 
    {
        const {exports, memory} = await createProgram([
            "add.s $f0, $f1, $f2",
            "add.s $f3, $f3, $f3"
        ]);

        exports.fpuSetRegisterF32(0, 1.0);
        exports.fpuSetRegisterF32(1, 2.5);
        exports.fpuSetRegisterF32(2, 3.0);
        exports.fpuSetRegisterF32(3, 4.25);

        exports.testCpuRun(CODE_BASE_OFFSET, 2);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.fpuGetRegisterF32(0)).to.be.closeTo(5.5, 0.0001);
        expect(exports.fpuGetRegisterF32(1)).to.be.closeTo(2.5, 0.0001); // unchanged
        expect(exports.fpuGetRegisterF32(2)).to.be.closeTo(3.0, 0.0001); // unchanged
        expect(exports.fpuGetRegisterF32(3)).to.be.closeTo(8.5, 0.0001);
    });

    it("add.d", async () => 
    {
        const {exports, memory} = await createProgram([
            "add.d $f0, $f2, $f4",
            "add.d $f6, $f6, $f6"
        ]);

        exports.fpuSetRegisterF64(0, 1.0);
        exports.fpuSetRegisterF64(2, 2.5);
        exports.fpuSetRegisterF64(4, 3.0);
        exports.fpuSetRegisterF64(6, 4.25);

        exports.testCpuRun(CODE_BASE_OFFSET, 2);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.fpuGetRegisterF64(0)).to.be.closeTo(5.5, 0.0001);
        expect(exports.fpuGetRegisterF64(2)).to.be.closeTo(2.5, 0.0001); // unchanged
        expect(exports.fpuGetRegisterF64(4)).to.be.closeTo(3.0, 0.0001); // unchanged
        expect(exports.fpuGetRegisterF64(6)).to.be.closeTo(8.5, 0.0001);
    });

    it("sub.s", async () => 
    {
        const {exports, memory} = await createProgram([
            "sub.s $f0, $f1, $f2",
            "sub.s $f3, $f3, $f3"
        ]);

        exports.fpuSetRegisterF32(0, 1.0);
        exports.fpuSetRegisterF32(1, 2.5);
        exports.fpuSetRegisterF32(2, 3.0);
        exports.fpuSetRegisterF32(3, 1.25);

        exports.testCpuRun(CODE_BASE_OFFSET, 2);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.fpuGetRegisterF32(0)).to.be.closeTo(-0.5, 0.0001);
        expect(exports.fpuGetRegisterF32(1)).to.be.closeTo(2.5, 0.0001); // unchanged
        expect(exports.fpuGetRegisterF32(2)).to.be.closeTo(3.0, 0.0001); // unchanged
        expect(exports.fpuGetRegisterF32(3)).to.be.closeTo(0.0, 0.0001);
    });

    it("sub.d", async () => 
    {
        const {exports, memory} = await createProgram([
            "sub.d $f0, $f2, $f4",
            "sub.d $f6, $f6, $f6"
        ]);

        exports.fpuSetRegisterF64(0, 1.0);
        exports.fpuSetRegisterF64(2, 2.5);
        exports.fpuSetRegisterF64(4, 3.0);
        exports.fpuSetRegisterF64(6, 1.25);

        exports.testCpuRun(CODE_BASE_OFFSET, 2);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.fpuGetRegisterF64(0)).to.be.closeTo(-0.5, 0.0001);
        expect(exports.fpuGetRegisterF64(2)).to.be.closeTo(2.5, 0.0001); // unchanged
        expect(exports.fpuGetRegisterF64(4)).to.be.closeTo(3.0, 0.0001); // unchanged
        expect(exports.fpuGetRegisterF64(6)).to.be.closeTo(0.0, 0.0001);
    });

    it("mul.s", async () => 
    {
        const {exports, memory} = await createProgram([
            "mul.s $f0, $f1, $f2",
            "mul.s $f3, $f3, $f3"
        ]);

        exports.fpuSetRegisterF32(0, 1.0);
        exports.fpuSetRegisterF32(1, 2.0);
        exports.fpuSetRegisterF32(2, 3.0);
        exports.fpuSetRegisterF32(3, 4.0);

        exports.testCpuRun(CODE_BASE_OFFSET, 2);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.fpuGetRegisterF32(0)).to.be.closeTo(6.0, 0.0001);
        expect(exports.fpuGetRegisterF32(1)).to.be.closeTo(2.0, 0.0001); // unchanged
        expect(exports.fpuGetRegisterF32(2)).to.be.closeTo(3.0, 0.0001); // unchanged
        expect(exports.fpuGetRegisterF32(3)).to.be.closeTo(16.0, 0.0001);
    });

    it("mul.d", async () => 
    {
        const {exports, memory} = await createProgram([
            "mul.d $f0, $f2, $f4",
            "mul.d $f6, $f8, $f8"
        ]);

        exports.fpuSetRegisterF64(0, 1.0);
        exports.fpuSetRegisterF64(2, 2.0);
        exports.fpuSetRegisterF64(4, 3.0);
        exports.fpuSetRegisterF64(8, 4.0);

        exports.testCpuRun(CODE_BASE_OFFSET, 2);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.fpuGetRegisterF64(0)).to.be.closeTo(6.0, 0.0001);
        expect(exports.fpuGetRegisterF64(2)).to.be.closeTo(2.0, 0.0001); // unchanged
        expect(exports.fpuGetRegisterF64(4)).to.be.closeTo(3.0, 0.0001); // unchanged
        expect(exports.fpuGetRegisterF64(6)).to.be.closeTo(16.0, 0.0001);
    });

    it("div.s", async () => 
    {
        const {exports, memory} = await createProgram([
            "div.s $f0, $f1, $f2",
            "div.s $f3, $f3, $f3"
        ]);

        exports.fpuSetRegisterF32(0, 1.0);
        exports.fpuSetRegisterF32(1, 6.0);
        exports.fpuSetRegisterF32(2, 2.0);
        exports.fpuSetRegisterF32(3, 4.0);

        exports.testCpuRun(CODE_BASE_OFFSET, 2);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.fpuGetRegisterF32(0)).to.be.closeTo(3.0, 0.0001);
        expect(exports.fpuGetRegisterF32(1)).to.be.closeTo(6.0, 0.0001); // unchanged
        expect(exports.fpuGetRegisterF32(2)).to.be.closeTo(2.0, 0.0001); // unchanged
        expect(exports.fpuGetRegisterF32(3)).to.be.closeTo(1.0, 0.0001);
    });

    it("div.d", async () => 
    {
        const {exports, memory} = await createProgram([
            "div.d $f0, $f2, $f4",
            "div.d $f6, $f8, $f10"
        ]);

        exports.fpuSetRegisterF64(2, 6.0);
        exports.fpuSetRegisterF64(4, 2.0);
        exports.fpuSetRegisterF64(8, 2.0);
        exports.fpuSetRegisterF64(10, 4.0);

        exports.testCpuRun(CODE_BASE_OFFSET, 2);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.fpuGetRegisterF64(0)).to.be.closeTo(3.0, 0.0001);
        expect(exports.fpuGetRegisterF64(2)).to.be.closeTo(6.0, 0.0001); // unchanged
        expect(exports.fpuGetRegisterF64(4)).to.be.closeTo(2.0, 0.0001); // unchanged
        expect(exports.fpuGetRegisterF64(6)).to.be.closeTo(0.5, 0.0001);

        expect(exports.fpuGetRegisterF64(6)).to.be.closeTo(0.5, 0.0001);
    });

    it("c.le.s", async () => 
    {
        // less than
        const {exports, memory} = await createProgram(["c.le.s $f0, $f1"]);
        exports.fpuSetRegisterF32(0, 1.0);
        exports.fpuSetRegisterF32(1, 6.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(1); // True

        // equal
        exports.fpuSetRegisterF32(0, 1.0);
        exports.fpuSetRegisterF32(1, 1.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(1); // True

        // greater
        exports.fpuSetRegisterF32(0, 6.0);
        exports.fpuSetRegisterF32(1, 1.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(0); // False
    });

    it("c.le.d", async () => 
    {
        // less than
        const {exports, memory} = await createProgram(["c.le.d $f0, $f1"]);
        exports.fpuSetRegisterF64(0, 1.0);
        exports.fpuSetRegisterF64(1, 6.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(1); // True

        // equal
        exports.fpuSetRegisterF64(0, 1.0);
        exports.fpuSetRegisterF64(1, 1.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(1); // True

        // greater
        exports.fpuSetRegisterF64(0, 6.0);
        exports.fpuSetRegisterF64(1, 1.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(0); // False
    });

    it("c.ule.d", async () => 
    {
        // less than
        const {exports, memory} = await createProgram(["c.ule.d $f0, $f1"]);
        exports.fpuSetRegisterF64(0, 1.0);
        exports.fpuSetRegisterF64(1, 6.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(1); // True

        // equal
        exports.fpuSetRegisterF64(0, 1.0);
        exports.fpuSetRegisterF64(1, 1.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(1); // True

        // greater
        exports.fpuSetRegisterF64(0, 6.0);
        exports.fpuSetRegisterF64(1, 1.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(0); // False
    });

    it("c.un.d", async () => 
    {
        // less than
        const {exports, memory} = await createProgram(["c.un.d $f0, $f1"]);
        exports.fpuSetRegisterF64(0, 1.0);
        exports.fpuSetRegisterF64(1, 6.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(0); // False

        // equal
        exports.fpuSetRegisterF64(0, 1.0);
        exports.fpuSetRegisterF64(1, 1.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(0); // False

        // greater
        exports.fpuSetRegisterF64(0, 6.0);
        exports.fpuSetRegisterF64(1, 1.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(0); // False

        // NaN (one)
        exports.fpuSetRegisterF64(0, NaN);
        exports.fpuSetRegisterF64(1, 1.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(1); // True

        // NaN (both)
        exports.fpuSetRegisterF64(0, NaN);
        exports.fpuSetRegisterF64(1, NaN);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(1); // True
    });

    it("c.lt.s", async () => 
    {
        // less than
        const {exports, memory} = await createProgram(["c.lt.s $f0, $f1"]);
        exports.fpuSetRegisterF32(0, 1.0);
        exports.fpuSetRegisterF32(1, 6.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(1); // True

        // equal
        exports.fpuSetRegisterF32(0, 1.0);
        exports.fpuSetRegisterF32(1, 1.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(0); // False

        // greater
        exports.fpuSetRegisterF32(0, 6.0);
        exports.fpuSetRegisterF32(1, 1.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(0); // False
    });

    it("c.lt.d", async () => 
    {
        // less than
        const {exports, memory} = await createProgram(["c.lt.d $f0, $f1"]);
        exports.fpuSetRegisterF64(0, 1.0);
        exports.fpuSetRegisterF64(1, 6.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(1); // True

        // equal
        exports.fpuSetRegisterF64(0, 1.0);
        exports.fpuSetRegisterF64(1, 1.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(0); // False

        // greater
        exports.fpuSetRegisterF64(0, 6.0);
        exports.fpuSetRegisterF64(1, 1.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(0); // False
    });

    it("c.eq.s", async () => 
    {
        // less than
        const {exports, memory} = await createProgram(["c.eq.s $f0, $f1"]);
        exports.fpuSetRegisterF32(0, 1.0);
        exports.fpuSetRegisterF32(1, 6.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(0); // False

        // equal
        exports.fpuSetRegisterF32(0, 1.0);
        exports.fpuSetRegisterF32(1, 1.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(1); // True

        // greater
        exports.fpuSetRegisterF32(0, 6.0);
        exports.fpuSetRegisterF32(1, 1.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(0); // False
    });

    it("c.eq.d", async () => 
    {
        // less than
        const {exports, memory} = await createProgram(["c.eq.d $f0, $f1"]);
        exports.fpuSetRegisterF64(0, 1.0);
        exports.fpuSetRegisterF64(1, 6.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(0); // False

        // equal
        exports.fpuSetRegisterF64(0, 1.0);
        exports.fpuSetRegisterF64(1, 1.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(1); // True

        // greater
        exports.fpuSetRegisterF64(0, 6.0);
        exports.fpuSetRegisterF64(1, 1.0);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.testFpuGetConditionFlag()).equal(0); // False
    });

    it("sqrt.s", async () => 
    {
        const {exports, memory} = await createProgram([
            "sqrt.s $f2, $f1"
        ]);
        
        exports.fpuSetRegisterF32(1, 36);
        expect(exports.fpuGetRegisterF32(2)).equal(0);

        exports.testCpuRun(CODE_BASE_OFFSET, 1);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.fpuGetRegisterF32(1)).equal(36);

        // should be a float value, and have its type set
        expect(exports.fpuGetRegisterF32(2)).equal(6.0);
    });

    it("neg.s", async () => 
    {
        const {exports, memory} = await createProgram([
            "neg.s $f1, $f11",
            "neg.s $f2, $f12",
            "neg.s $f3, $f13",
        ]);
        
        exports.fpuSetRegisterF32(11, +10.5);
        exports.fpuSetRegisterF32(12, -20.5);
        exports.fpuSetRegisterF32(13, 0);

        exports.testCpuRun(CODE_BASE_OFFSET, 3);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(exports.fpuGetRegisterF32(11)).equal(10.5);
        expect(exports.fpuGetRegisterF32(12)).equal(-20.5);
        expect(exports.fpuGetRegisterF32(13)).equal(0);

        expect(exports.fpuGetRegisterF32(1)).equal(-10.5);
        expect(exports.fpuGetRegisterF32(2)).equal(20.5);
        //expect(exports.fpuGetRegisterF32(3)).equal(-0.0);
    });
});