/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { assertHex, assertHexS64, assertHexU64, CODE_BASE_OFFSET, createProgram } from '../wasmHelper.js';
import { MIPS_REG } from '../registerMapping.js';
import chai from "chai"; const {expect} = chai;

describe('MIPS - Arithmetic', () => 
{
  it("add", async () => 
  {
    const {exports} = await createProgram([
        "add $t0, $t1, $t2"
    ]);
    
    exports.cpuSetRegisterU32(MIPS_REG.$t0, 0x00);
    exports.cpuSetRegisterU32(MIPS_REG.$t1, 0x1122_3344);
    exports.cpuSetRegisterU32(MIPS_REG.$t2, 0x1112_1314);
    exports.testCpuRun(CODE_BASE_OFFSET, 1);

    expect(exports.testCpuGetErrorCount()).equal(0);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t0)>>>0, 0x2234_4658);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t1)>>>0, 0x1122_3344);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t2)>>>0, 0x1112_1314);
  });

  it("addiu", async () => 
  {
      const {exports} = await createProgram([
         "addiu $v0 $v1 -0x38"
      ]);
      
      exports.cpuSetRegisterU32(MIPS_REG.$v0, 0x00);
      exports.cpuSetRegisterU32(MIPS_REG.$v1, 0x80C3_61AC);
      exports.testCpuRun(CODE_BASE_OFFSET, 1);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$v0)>>>0, 0x80C3_6174);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$v1)>>>0, 0x80C3_61AC); // unchanged
  });

  it("daddiu", async () => 
  {
      const {exports} = await createProgram([
        "daddiu $t2, $t0, -0x60",
        "daddiu $t4, $t0, 0x60",
      ]);
      
      exports.cpuSetRegisterU64(MIPS_REG.$t0, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.testCpuRun(CODE_BASE_OFFSET, 2);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t0), 0xFFFF_FFFF_FFFF_FFFFn);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t2), 0xFFFF_FFFF_FFFF_FF9Fn);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t4), 0x0000_0000_0000_005Fn);
  });

  it("daddu", async () => 
  {
      const {exports} = await createProgram([
        "daddu $t2, $t0, $a0",
        "daddu $t4, $t0, $a1",
      ]);
      
      exports.cpuSetRegisterU64(MIPS_REG.$t0, 0xFFFF_FFFF_FFFF_FFFFn);
      exports.cpuSetRegisterS64(MIPS_REG.$a0, -0x60n);
      exports.cpuSetRegisterS64(MIPS_REG.$a1,  0x60n);
      exports.testCpuRun(CODE_BASE_OFFSET, 2);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t0), 0xFFFF_FFFF_FFFF_FFFFn);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t2), 0xFFFF_FFFF_FFFF_FF9Fn);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t4), 0x0000_0000_0000_005Fn);
  });

  it("sub", async () => 
  {
      const {exports} = await createProgram([
        "sub $t2, $t0, $a0",
        "sub $t4, $t0, $a1",
      ]);
      
      exports.cpuSetRegisterU32(MIPS_REG.$t0, 1000);
      exports.cpuSetRegisterU32(MIPS_REG.$a0, -10);
      exports.cpuSetRegisterU32(MIPS_REG.$a1,  10);
      exports.testCpuRun(CODE_BASE_OFFSET, 2);

      expect(exports.testCpuGetErrorCount()).equal(0);
      expect(exports.cpuGetRegisterU32(MIPS_REG.$t0)).equal(1000);
      expect(exports.cpuGetRegisterU32(MIPS_REG.$t2)).equal(1010);
      expect(exports.cpuGetRegisterU32(MIPS_REG.$t4)).equal( 990);
  });

  it("dsubu", async () => 
  {
      const {exports} = await createProgram([
        "dsubu $t2, $t0, $a0",
        "dsubu $t4, $t0, $a1",
      ]);
      
      exports.cpuSetRegisterU64(MIPS_REG.$t0, 0xFFFF_FFFF_FFFF_FF00n);
      exports.cpuSetRegisterS64(MIPS_REG.$a0, 0x100n);
      exports.cpuSetRegisterS64(MIPS_REG.$a1, -0x110n);
      exports.testCpuRun(CODE_BASE_OFFSET, 2);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t0), 0xFFFF_FFFF_FFFF_FF00n);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t2), 0xFFFF_FFFF_FFFF_FE00n);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t4), 0x10n); // overflow
  });

  it("div", async () => 
  {
      const {exports} = await createProgram(["div $t1, $t2"]);
      
      exports.cpuSetRegisterS32(MIPS_REG.$t1, -101);
      exports.cpuSetRegisterS32(MIPS_REG.$t2, 3);
      exports.testCpuRun(CODE_BASE_OFFSET, 1);

      expect(exports.testCpuGetErrorCount()).equal(0);
      expect(exports.cpuGetRegisterS32(MIPS_REG.$SP_LOW)).equal(-33);
      expect(exports.cpuGetRegisterS32(MIPS_REG.$SP_HIGH)).equal(-2);
  });

  it("divu", async () => 
  {
      const {exports} = await createProgram(["divu $t1, $t2"]);
      
      exports.cpuSetRegisterU32(MIPS_REG.$t1, 101);
      exports.cpuSetRegisterU32(MIPS_REG.$t2, 3);
      exports.testCpuRun(CODE_BASE_OFFSET, 1);

      expect(exports.testCpuGetErrorCount()).equal(0);
      expect(exports.cpuGetRegisterU32(MIPS_REG.$SP_LOW)>>>0).equal(33);
      expect(exports.cpuGetRegisterU32(MIPS_REG.$SP_HIGH)>>>0).equal(2);
  });

  it("div + mflo/mfhi", async () => 
  {
      const {exports} = await createProgram([
          "div $t1, $t2",
          "nop ",
          "mfhi $t4",
          "mflo $t5",
      ]);
      
      exports.cpuSetRegisterU32(MIPS_REG.$t1, 101);
      exports.cpuSetRegisterU32(MIPS_REG.$t2, 3);
      exports.testCpuRun(CODE_BASE_OFFSET, 4);

      expect(exports.testCpuGetErrorCount()).equal(0);
      expect(exports.cpuGetRegisterS32(MIPS_REG.$t5)).equal(33);
      expect(exports.cpuGetRegisterS32(MIPS_REG.$t4)).equal(2);
  });

  it("ddiv", async () => 
  {
      const {exports} = await createProgram(["ddiv $t0, $t2"]);
      
      exports.cpuSetRegisterU64(MIPS_REG.$t0, -3n);
      exports.cpuSetRegisterU64(MIPS_REG.$t2, -2n);
      exports.testCpuRun(CODE_BASE_OFFSET, 1);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHexS64(exports.cpuGetRegisterS64(MIPS_REG.$SP_LOW), 1n); // result
      assertHexS64(exports.cpuGetRegisterS64(MIPS_REG.$SP_HIGH), -1n); // remainder
  });

  it("ddivu - big/big", async () => 
  {
      const {exports} = await createProgram(["ddivu $t2, $t4"]);
      
      exports.cpuSetRegisterU64(MIPS_REG.$t2, 0xF000_0000_0000_0000n);
      exports.cpuSetRegisterU64(MIPS_REG.$t4, 0x2000_0000_0000_0000n);
      exports.testCpuRun(CODE_BASE_OFFSET, 1);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$SP_LOW), 0x7n);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$SP_HIGH), 0x1000_0000_0000_0000n);
  });

  it("ddivu - big/small", async () => 
  {
      const {exports} = await createProgram(["ddivu $t2, $t4"]);
      
      exports.cpuSetRegisterU64(MIPS_REG.$t2, 0xF000_0000_0000_0000n);
      exports.cpuSetRegisterU64(MIPS_REG.$t4, 0x19n);
      exports.testCpuRun(CODE_BASE_OFFSET, 1);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$SP_LOW), 691752902764108185n);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$SP_HIGH), 15n);
  });
  

  it("multu", async () => 
  {
      const {exports} = await createProgram(["multu $t1, $t2"]);
      
      exports.cpuSetRegisterU32(MIPS_REG.$t1, 0xFFFF_FFFF);
      exports.cpuSetRegisterU32(MIPS_REG.$t2, 0xFFFF_FFF5);
      exports.testCpuRun(CODE_BASE_OFFSET, 1);

      expect(exports.testCpuGetErrorCount()).equal(0);
      expect(exports.cpuGetRegisterU32(MIPS_REG.$SP_HIGH)>>>0).equal(0xFFFF_FFF4);
      expect(exports.cpuGetRegisterU32(MIPS_REG.$SP_LOW)>>>0).equal(0x0000_000B);
  });

  it("mult", async () => 
  {
      const {exports} = await createProgram(["mult $t1, $t2"]);
      
      exports.cpuSetRegisterS32(MIPS_REG.$t1, -510000);
      exports.cpuSetRegisterS32(MIPS_REG.$t2, 100000);
      exports.testCpuRun(CODE_BASE_OFFSET, 1);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$SP_HIGH), 0xFFFF_FFF4);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$SP_LOW),  0x2029_C200);
  });

  it("dmultu", async () => 
  {
      const {exports} = await createProgram(["dmultu $t0, $t2"]);
      
      exports.cpuSetRegisterU64(MIPS_REG.$t0, 0xFFFF_ABCD_FFFF_FFFFn);
      exports.cpuSetRegisterU64(MIPS_REG.$t2, 0xFFFF_ABCD_FFFF_1234n);
      exports.testCpuRun(CODE_BASE_OFFSET, 1);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$SP_LOW), 0x4E35_B60A_0000_EDCCn);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$SP_HIGH), 0xFFFF_579C_1BAF_EBF7n);
  });
});
