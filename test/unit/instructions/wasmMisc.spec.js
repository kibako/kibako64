/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { assertHex, assertHexArray, assertHexS64, assertHexU64, CODE_BASE_OFFSET, createProgram } from '../wasmHelper.js';
import { MIPS_REG } from '../registerMapping.js';
import chai from "chai"; const {expect} = chai;

describe('WASM MIPS - Misc', () => 
{
  it("movn (true / false)", async () => 
  {
      const {exports, memory} = await createProgram([
        "movn $t0, $t1, $t2",
        "movn $t3, $t4, $t5",
        ]);
      
      exports.cpuSetRegisterU32(MIPS_REG.$t1, 0x42);
      exports.cpuSetRegisterU32(MIPS_REG.$t2, 0x10);

      exports.cpuSetRegisterU32(MIPS_REG.$t4, 0x43);
      exports.cpuSetRegisterU32(MIPS_REG.$t5, 0x00);
      
      exports.testCpuRun(CODE_BASE_OFFSET, 2);

      assertHex(exports.testCpuGetErrorCount(), 0);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t0), 0x42); // was set
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t3), 0x00); // was not set
  });

  it("movz (true / false)", async () => 
  {
      const {exports, memory} = await createProgram([
        "movz $t0, $t1, $t2",
        "movz $t3, $t4, $t5",
        ]);
      
      exports.cpuSetRegisterU32(MIPS_REG.$t1, 0x42);
      exports.cpuSetRegisterU32(MIPS_REG.$t2, 0x00);

      exports.cpuSetRegisterU32(MIPS_REG.$t4, 0x43);
      exports.cpuSetRegisterU32(MIPS_REG.$t5, 0x10);
      
      exports.testCpuRun(CODE_BASE_OFFSET, 2);

      assertHexS64(exports.testCpuGetErrorCount(), 0n);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t0), 0x42); // was set
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t3), 0x00); // was not set
  });

  it("sllv", async () => 
  {
      const {exports, memory} = await createProgram(["sllv $t1, $t2, $t3"]);
      
      exports.cpuSetRegisterU32(MIPS_REG.$t1, 0);
      exports.cpuSetRegisterU32(MIPS_REG.$t2, 0b11);
      exports.cpuSetRegisterU32(MIPS_REG.$t3, 5);
      exports.testCpuRun(CODE_BASE_OFFSET, 1);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t1), 0b11_00000);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t2), 0b11);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t3), 5);
  });

  it("dsll32", async () => 
  {
      const {exports, memory} = await createProgram([
        "dsll32 $t2, $t2, 0x00",
        "dsll32 $t4, $t4, 0x04",
        "dsll32 $t6, $t6, 0x18",
        ]);
      
      exports.cpuSetRegisterU64(MIPS_REG.$t2, 0xFFFFn);
      exports.cpuSetRegisterU64(MIPS_REG.$t4, 0xFFFFn);
      exports.cpuSetRegisterU64(MIPS_REG.$t6, 0xFFFFn);
      
      exports.testCpuRun(CODE_BASE_OFFSET, 3);

      expect(exports.testCpuGetErrorCount()).equal(0);
      
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t2), 0x0000_FFFF_0000_0000n);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t4), 0x000F_FFF0_0000_0000n);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t6), 0xFF00_0000_0000_0000n);
  });

  it("dsrl32", async () => 
  {
      const {exports, memory} = await createProgram([
        "dsrl32 $t2, $t2, 0x00",
        "dsrl32 $t4, $t4, 0x04",
        "dsrl32 $t6, $t6, 0x18",
        ]);
      
      exports.cpuSetRegisterU64(MIPS_REG.$t2, 0x1122_3344_0000_0000n);
      exports.cpuSetRegisterU64(MIPS_REG.$t4, 0xFFFF_0000_0000_0000n);
      exports.cpuSetRegisterU64(MIPS_REG.$t6, 0x1122_3344_5566_7788n);
      
      exports.testCpuRun(CODE_BASE_OFFSET, 3);

      expect(exports.testCpuGetErrorCount()).equal(0);
      
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t2), 0x0000_0000_1122_3344n);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t4), 0x0000_0000_0FFF_F000n);
      assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t6), 0x0000_0000_0000_0011n);
  });

  it("mtlo", async () => 
  {
      const {exports, memory} = await createProgram(["mtlo $t3"]);      
      exports.cpuSetRegisterU32(MIPS_REG.$t3, 0xAABBCCDD);

      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$SP_LOW),  0);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$SP_HIGH), 0);

      exports.testCpuRun(CODE_BASE_OFFSET, 1);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t3), 0xAABBCCDD);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$SP_LOW),  0xAABBCCDD);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$SP_HIGH), 0);
  });

  it("mthi", async () => 
  {
      const {exports, memory} = await createProgram(["mthi $t4"]);      
      exports.cpuSetRegisterU32(MIPS_REG.$t4, 0xAABBCCDD);

      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$SP_LOW),  0);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$SP_HIGH), 0);

      exports.testCpuRun(CODE_BASE_OFFSET, 1);

      expect(exports.testCpuGetErrorCount()).equal(0);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t4), 0xAABBCCDD);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$SP_LOW),  0);
      assertHex(exports.cpuGetRegisterU32(MIPS_REG.$SP_HIGH), 0xAABBCCDD);
  });
});
