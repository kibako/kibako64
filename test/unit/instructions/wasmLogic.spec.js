/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { assertHex, assertHexS64, assertHexU64, CODE_BASE_OFFSET, createProgram } from '../wasmHelper.js';
import { MIPS_REG } from '../registerMapping.js';
import chai from "chai"; const {expect} = chai;

describe('WASM MIPS - Logic', () => 
{
  it("andi", async () => 
  {
    const {exports} = await createProgram(["andi $t0, $t1, 0x42"]);
    
    exports.cpuSetRegisterU64(MIPS_REG.$t0, 0x00n);
    exports.cpuSetRegisterU64(MIPS_REG.$t1, 0x1122_3344_FFFF_1240n);
    exports.testCpuRun(CODE_BASE_OFFSET, 1);

    expect(exports.testCpuGetErrorCount()).equal(0);
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t0), 0x0040n);
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t1), 0x1122_3344_FFFF_1240n);
  });

  it("ori", async () => 
  {
    const {exports} = await createProgram(["ori $t0, $t1, 0xE2"]);
    
    exports.cpuSetRegisterU64(MIPS_REG.$t0, 0x00n);
    exports.cpuSetRegisterU64(MIPS_REG.$t1, 0x1122_3344_FFFF_1240n);
    exports.testCpuRun(CODE_BASE_OFFSET, 1);

    expect(exports.testCpuGetErrorCount()).equal(0);
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t0), 0x1122_3344_FFFF_12E2n);
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t1), 0x1122_3344_FFFF_1240n);
  });

  it("xori", async () => 
  {
    const {exports} = await createProgram(["xori $t0, $t1, 0xE2"]);
    
    exports.cpuSetRegisterU64(MIPS_REG.$t0, 0x00n);
    exports.cpuSetRegisterU64(MIPS_REG.$t1, 0x1122_3344_FFFF_1240n);
    exports.testCpuRun(CODE_BASE_OFFSET, 1);

    expect(exports.testCpuGetErrorCount()).equal(0);
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t0), 0x1122_3344_FFFF_12A2n);
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t1), 0x1122_3344_FFFF_1240n);
  });

  it("and", async () => 
  {
    const {exports} = await createProgram(["and $t0, $t1, $t2"]);
    
    exports.cpuSetRegisterU64(MIPS_REG.$t0, 0n);
    exports.cpuSetRegisterU64(MIPS_REG.$t1, 0x4B1A_BA64_5ED4_CCE0n);
    exports.cpuSetRegisterU64(MIPS_REG.$t2, 0x6A7F_6CC7_5BFE_1862n);
    exports.testCpuRun(CODE_BASE_OFFSET, 1);

    expect(exports.testCpuGetErrorCount()).equal(0);
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t0), 0x4A1A_2844_5AD4_0860n);

    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t1), 0x4B1A_BA64_5ED4_CCE0n);
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t2), 0x6A7F_6CC7_5BFE_1862n);
  });

  it("or", async () => 
  {
    const {exports} = await createProgram(["or $t0, $t1, $t2"]);
    
    exports.cpuSetRegisterU64(MIPS_REG.$t0, 0n);
    exports.cpuSetRegisterU64(MIPS_REG.$t1, 0x4B1A_BA64_5ED4_CCE0n);
    exports.cpuSetRegisterU64(MIPS_REG.$t2, 0x6A7F_6CC7_5BFE_1862n);
    exports.testCpuRun(CODE_BASE_OFFSET, 1);

    expect(exports.testCpuGetErrorCount()).equal(0);
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t0), 0x6B7F_FEE7_5FFE_DCE2n);

    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t1), 0x4B1A_BA64_5ED4_CCE0n);
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t2), 0x6A7F_6CC7_5BFE_1862n);
  });

  it("xor", async () => 
  {
    const {exports} = await createProgram(["xor $t0, $t1, $t2"]);
    
    exports.cpuSetRegisterU64(MIPS_REG.$t0, 0n);
    exports.cpuSetRegisterU64(MIPS_REG.$t1, 0x4B1A_BA64_5ED4_CCE0n);
    exports.cpuSetRegisterU64(MIPS_REG.$t2, 0x6A7F_6CC7_5BFE_1862n);
    exports.testCpuRun(CODE_BASE_OFFSET, 1);

    expect(exports.testCpuGetErrorCount()).equal(0);
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t0), 0x2165_D6A3_052A_D482n);

    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t1), 0x4B1A_BA64_5ED4_CCE0n);
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t2), 0x6A7F_6CC7_5BFE_1862n);
  });

  it("nor", async () => 
  {
    const {exports} = await createProgram(["nor $t0, $t1, $t2"]);
    
    exports.cpuSetRegisterU64(MIPS_REG.$t0, 0n);
    exports.cpuSetRegisterU64(MIPS_REG.$t1, 0x4B1A_BA64_5ED4_CCE0n);
    exports.cpuSetRegisterU64(MIPS_REG.$t2, 0x6A7F_6CC7_5BFE_1862n);
    exports.testCpuRun(CODE_BASE_OFFSET, 1);

    expect(exports.testCpuGetErrorCount()).equal(0);
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t0), 0x9480_0118_A001_231Dn);

    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t1), 0x4B1A_BA64_5ED4_CCE0n);
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t2), 0x6A7F_6CC7_5BFE_1862n);
  });

  it("slti", async () => 
  {
    const {exports} = await createProgram([
      "slti $t1, $t0, -0x10", 
      "slti $t2, $t0,  0xF0", 
      "slti $t3, $t0,  0xFF"
    ]);
    
    exports.cpuSetRegisterU32(MIPS_REG.$t0, 0xF0);

    exports.cpuSetRegisterU32(MIPS_REG.$t1, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t2, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t3, 0xDEAD_BEEF);
    exports.testCpuRun(CODE_BASE_OFFSET, 3);

    expect(exports.testCpuGetErrorCount()).equal(0);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t0), 0xF0);

    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t1), 0);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t2), 0);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t3), 1);
  });

  it("sltiu", async () => 
  {
    const {exports} = await createProgram([
      "sltiu $t1, $t0,  -0x10", 
      "sltiu $t2, $t0,  0xF0", 
      "sltiu $t3, $t0,  0xFF"
    ]);
    
    exports.cpuSetRegisterU32(MIPS_REG.$t0, 0xF0);

    exports.cpuSetRegisterU32(MIPS_REG.$t1, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t2, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t3, 0xDEAD_BEEF);
    exports.testCpuRun(CODE_BASE_OFFSET, 3);

    expect(exports.testCpuGetErrorCount()).equal(0);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t0), 0xF0);

    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t1), 1);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t2), 0);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t3), 1);
  });

  it("slt", async () => 
  {
    const {exports} = await createProgram([
      "slt $t1, $t4, $t0", 
      "slt $t2, $t5, $t0", 
      "slt $t3, $t6, $t0"
    ]);
    
    exports.cpuSetRegisterU32(MIPS_REG.$t0, 0x1234_5678);

    exports.cpuSetRegisterU32(MIPS_REG.$t1, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t2, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t3, 0xDEAD_BEEF);

    exports.cpuSetRegisterS32(MIPS_REG.$t4, -0x1234_0000);
    exports.cpuSetRegisterS32(MIPS_REG.$t5, 0x1234_5678);
    exports.cpuSetRegisterS32(MIPS_REG.$t6, 0x5678_0000);

    exports.testCpuRun(CODE_BASE_OFFSET, 3);

    expect(exports.testCpuGetErrorCount()).equal(0);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t0), 0x1234_5678);

    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t1), 1);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t2), 0);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t3), 0);
  });

  it("sltu", async () => 
  {
    const {exports} = await createProgram([
      "sltu $t1, $t4, $t0", 
      "sltu $t2, $t5, $t0", 
      "sltu $t3, $t6, $t0"
    ]);
    
    exports.cpuSetRegisterU32(MIPS_REG.$t0, 0x1234_5678);

    exports.cpuSetRegisterU32(MIPS_REG.$t1, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t2, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t3, 0xDEAD_BEEF);

    exports.cpuSetRegisterU32(MIPS_REG.$t4, 0xFFFF_FEEE);
    exports.cpuSetRegisterU32(MIPS_REG.$t5, 0x1234_5678);
    exports.cpuSetRegisterU32(MIPS_REG.$t6, 0x1234_0000);

    exports.testCpuRun(CODE_BASE_OFFSET, 3);

    expect(exports.testCpuGetErrorCount()).equal(0);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t0), 0x1234_5678);

    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t1), 0);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t2), 0);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t3), 1);
  });

  it("sll", async () => 
  {
    const {exports} = await createProgram([
      "sll $t1, $t4, 0x08", 
      "sll $t2, $t5, 0x09", 
      "sll $t3, $t6, 0x10"
    ]);
    
    exports.cpuSetRegisterU32(MIPS_REG.$t1, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t2, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t3, 0xDEAD_BEEF);

    exports.cpuSetRegisterS32(MIPS_REG.$t4, -0x10000); // => 0xFFFF_0000
    exports.cpuSetRegisterS32(MIPS_REG.$t5, 0x1234_5678);
    exports.cpuSetRegisterS32(MIPS_REG.$t6, 0x5678_0000);

    exports.testCpuRun(CODE_BASE_OFFSET, 3);

    expect(exports.testCpuGetErrorCount()).equal(0);
    
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t1), 0xFF00_0000);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t2), 0x68AC_F000);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t3), 0x0000_0000);
  });

  it("srl", async () => 
  {
    const {exports} = await createProgram([
      "srl $t1, $t4, 0x08", 
      "srl $t2, $t5, 0x09", 
      "srl $t3, $t6, 0x10"
    ]);
    
    exports.cpuSetRegisterU32(MIPS_REG.$t1, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t2, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t3, 0xDEAD_BEEF);

    exports.cpuSetRegisterS32(MIPS_REG.$t4, -0x10000); // => 0xFFFF_0000
    exports.cpuSetRegisterS32(MIPS_REG.$t5, 0x1234_5678);
    exports.cpuSetRegisterS32(MIPS_REG.$t6, 0x5678_0000);

    exports.testCpuRun(CODE_BASE_OFFSET, 3);

    expect(exports.testCpuGetErrorCount()).equal(0);
    
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t1), 0x00FF_FF00);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t2), 0x91A2B);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t3), 0x0000_5678);
  });

  it("dsll", async () => 
  {
    const {exports} = await createProgram([
      "dsll $t1, $t4, 0x08", 
      "dsll $t2, $t5, 0x0C", 
      "dsll $t3, $t6, 0x1F"
    ]);
    
    exports.cpuSetRegisterU64(MIPS_REG.$t4, 0xABCD_1234_4567_EEFFn);
    exports.cpuSetRegisterU64(MIPS_REG.$t5, 0xABCD_1234_4567_EEFFn);
    exports.cpuSetRegisterU64(MIPS_REG.$t6, 0xABCD_1234_4567_EEFFn);

    exports.testCpuRun(CODE_BASE_OFFSET, 3);

    expect(exports.testCpuGetErrorCount()).equal(0);
    
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t1),  0xCD12_3445_67EE_FF00n);
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t2),  0xD123_4456_7EEF_F000n);
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t3),  0x22B3_F77F_8000_0000n);
  });

  it("dsrl", async () => 
  {
    const {exports} = await createProgram([
      "dsrl $t1, $t4, 0x08", 
      "dsrl $t2, $t5, 0x0C", 
      "dsrl $t3, $t6, 0x1F"
    ]);
    
    exports.cpuSetRegisterU64(MIPS_REG.$t4, 0xABCD_1234_4567_EEFFn);
    exports.cpuSetRegisterU64(MIPS_REG.$t5, 0xABCD_1234_4567_EEFFn);
    exports.cpuSetRegisterU64(MIPS_REG.$t6, 0xABCD_1234_4567_EEFFn);

    exports.testCpuRun(CODE_BASE_OFFSET, 3);

    expect(exports.testCpuGetErrorCount()).equal(0);
    
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t1),  0x00AB_CD12_3445_67EEn);
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t2),  0x000A_BCD1_2344_567En);
    assertHexU64(exports.cpuGetRegisterU64(MIPS_REG.$t3),  0x0000_0001_579A_2468n);
  });

  it("sra", async () => 
  {
    // Same as 'srl', but the sign is preserved
    const {exports} = await createProgram([
      "sra $t1, $t4, 0x08", 
      "sra $t2, $t5, 0x09", 
      "sra $t3, $t6, 0x10"
    ]);
    
    exports.cpuSetRegisterU32(MIPS_REG.$t1, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t2, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t3, 0xDEAD_BEEF);

    exports.cpuSetRegisterS32(MIPS_REG.$t4, -0x10000); // => 0xFFFF_0000
    exports.cpuSetRegisterS32(MIPS_REG.$t5, 0x1234_5678);
    exports.cpuSetRegisterS32(MIPS_REG.$t6, 0x5678_0000);

    exports.testCpuRun(CODE_BASE_OFFSET, 3);

    expect(exports.testCpuGetErrorCount()).equal(0);
    
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t1), 0xFFFF_FF00);
    assertHex(exports.cpuGetRegisterS32(MIPS_REG.$t1), -0x100);

    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t2), 0x91A2B);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t3), 0x0000_5678);
  });

  it("sllv", async () => 
  {
    const {exports} = await createProgram([
      "sllv $t1, $t4, $a0", 
      "sllv $t2, $t5, $a1", 
      "sllv $t3, $t6, $a2"
    ]);
    
    exports.cpuSetRegisterU32(MIPS_REG.$a0, 8);
    exports.cpuSetRegisterU32(MIPS_REG.$a1, 9);
    exports.cpuSetRegisterU32(MIPS_REG.$a2, 16);

    exports.cpuSetRegisterU32(MIPS_REG.$t1, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t2, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t3, 0xDEAD_BEEF);

    exports.cpuSetRegisterS32(MIPS_REG.$t4, -0x10000); // => 0xFFFF_0000
    exports.cpuSetRegisterS32(MIPS_REG.$t5, 0x1234_5678);
    exports.cpuSetRegisterS32(MIPS_REG.$t6, 0x5678_0000);

    exports.testCpuRun(CODE_BASE_OFFSET, 3);

    expect(exports.testCpuGetErrorCount()).equal(0);
    
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t1), 0xFF00_0000);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t2), 0x68AC_F000);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t3), 0x0000_0000);
  });

  it("srlv", async () => 
  {
    const {exports} = await createProgram([
      "srlv $t1, $t4, $a0", 
      "srlv $t2, $t5, $a1", 
      "srlv $t3, $t6, $a2"
    ]);
    
    exports.cpuSetRegisterU32(MIPS_REG.$a0, 8);
    exports.cpuSetRegisterU32(MIPS_REG.$a1, 9);
    exports.cpuSetRegisterU32(MIPS_REG.$a2, 16);

    exports.cpuSetRegisterU32(MIPS_REG.$t1, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t2, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t3, 0xDEAD_BEEF);

    exports.cpuSetRegisterS32(MIPS_REG.$t4, -0x10000); // => 0xFFFF_0000
    exports.cpuSetRegisterS32(MIPS_REG.$t5, 0x1234_5678);
    exports.cpuSetRegisterS32(MIPS_REG.$t6, 0x5678_0000);

    exports.testCpuRun(CODE_BASE_OFFSET, 3);

    expect(exports.testCpuGetErrorCount()).equal(0);
    
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t1), 0x00FF_FF00);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t2), 0x91A2B);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t3), 0x0000_5678);
  });

  it("srav", async () => 
  {
    // Same as 'srlv', but the sign is preserved
    const {exports} = await createProgram([
      "srav $t1, $t4, $a0", 
      "srav $t2, $t5, $a1", 
      "srav $t3, $t6, $a2"
    ]);
    
    exports.cpuSetRegisterU32(MIPS_REG.$a0, 8);
    exports.cpuSetRegisterU32(MIPS_REG.$a1, 9);
    exports.cpuSetRegisterU32(MIPS_REG.$a2, 16);

    exports.cpuSetRegisterU32(MIPS_REG.$t1, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t2, 0xDEAD_BEEF);
    exports.cpuSetRegisterU32(MIPS_REG.$t3, 0xDEAD_BEEF);

    exports.cpuSetRegisterS32(MIPS_REG.$t4, -0x10000); // => 0xFFFF_0000
    exports.cpuSetRegisterS32(MIPS_REG.$t5, 0x1234_5678);
    exports.cpuSetRegisterS32(MIPS_REG.$t6, 0x5678_0000);

    exports.testCpuRun(CODE_BASE_OFFSET, 3);

    expect(exports.testCpuGetErrorCount()).equal(0);
    
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t1), 0xFFFF_FF00);
    assertHex(exports.cpuGetRegisterS32(MIPS_REG.$t1), -0x100);

    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t2), 0x91A2B);
    assertHex(exports.cpuGetRegisterU32(MIPS_REG.$t3), 0x0000_5678);
  });
});
