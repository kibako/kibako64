/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { CODE_BASE_OFFSET, createProgram } from '../wasmHelper.js';
import { MIPS_REG } from '../registerMapping.js';
import chai from "chai"; const {expect} = chai;

describe('WASM MIPS - Branch', () => 
{
    it("beq - true", async () =>
    {
        const {exports, calls} = await createProgram([
          /* 0x1000 */ "beq $v0, $v1, 0x02", // jump
          /* 0x1004 */ "x.log", // immediate
          /* 0x1008 */ "x.log", // <ignored>
          /* 0x100C */ "x.log", // jump target
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$v0, 0x10);
        exports.cpuSetRegisterU32(MIPS_REG.$v1, 0x10);
        exports.testCpuRun(CODE_BASE_OFFSET, 4);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([0x1004, 0x100C]);

        expect(exports.cpuGetRegisterU32(MIPS_REG.$ra)).equal(0); // no RA set
    });

    it("beq (zero-jump) - true", async () =>
    {
        const {exports, calls} = await createProgram([
          /* 0x1000 */ "beq $v0, $v1, 0x00", // jump
          /* 0x1004 */ "x.log", // immediate + jump target
          /* 0x1008 */ "x.log", // executed
          /* 0x100C */ "x.log", // executed
        ]);
        
        exports.cpuSetRegisterU32(MIPS_REG.$v0, 0x10);
        exports.cpuSetRegisterU32(MIPS_REG.$v1, 0x10);
        exports.testCpuRun(CODE_BASE_OFFSET, 5);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([0x1004, 0x1004, 0x1008, 0x100C]);

        expect(exports.cpuGetRegisterU32(MIPS_REG.$ra)).equal(0); // no RA set
    });

    it("beq (jump-back)- true", async () =>
    {
        const {exports, calls} = await createProgram([
          /* 0x1000 */ "x.log", // executed after jump
          /* 0x1004 */ "x.log", // executed after jump
          /* 0x1008 */ "beq $v0, $v1, -0x03", // jump to 0x1000
          /* 0x100C */ "x.log", // immediate
          /* 0x1010 */ "x.log", // <ignored>
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$v0, 0x10);
        exports.cpuSetRegisterU32(MIPS_REG.$v1, 0x10);
        exports.testCpuRun(CODE_BASE_OFFSET + 0x08, 5);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([0x100C, 0x1000, 0x1004, 0x100C]);

        expect(exports.cpuGetRegisterU32(MIPS_REG.$ra)).equal(0); // no RA set
    });

    it("bne - false", async () =>
    {
        const {exports, calls} = await createProgram([
          /* 0x1000 */ "bne $v0, $v1, 0xFF", // don't jump
          /* 0x1004 */ "x.log", // immediate
          /* 0x1008 */ "x.log", // executed
          /* 0x100C */ "x.log", // executed
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$v0, 0x10);
        exports.cpuSetRegisterU32(MIPS_REG.$v1, 0x10);
        exports.testCpuRun(CODE_BASE_OFFSET, 4);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([0x1004, 0x1008, 0x100C]);

        expect(exports.cpuGetRegisterU32(MIPS_REG.$ra)).equal(0); // no RA set
    });

    it("bne - true", async () =>
    {
        const {exports, calls} = await createProgram([
          /* 0x1000 */ "bne $v0, $v1, 0x03", // jump
          /* 0x1004 */ "x.log", // immediate
          /* 0x1008 */ "x.log", // <ignored>
          /* 0x100C */ "x.log", // <ignored>
          /* 0x1010 */ "x.log", // jump target
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$v0, 0x20);
        exports.cpuSetRegisterU32(MIPS_REG.$v1, 0x10);
        exports.testCpuRun(CODE_BASE_OFFSET, 4);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([0x1004, 0x1010]);

        expect(exports.cpuGetRegisterU32(MIPS_REG.$ra)).equal(0); // no RA set
    });

    it("beq - false", async () =>
    {
        const {exports, calls} = await createProgram([
          /* 0x1000 */ "beq $v0, $v1, 0xFF", // don't jump
          /* 0x1004 */ "x.log", // immediate
          /* 0x1008 */ "x.log", // executed
          /* 0x100C */ "x.log", // executed
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$v0, 0x20);
        exports.cpuSetRegisterU32(MIPS_REG.$v1, 0x10);
        exports.testCpuRun(CODE_BASE_OFFSET, 4);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([0x1004, 0x1008, 0x100C]);

        expect(exports.cpuGetRegisterU32(MIPS_REG.$ra)).equal(0); // no RA set
    });

    it("beql - true", async () =>
    {
        const {exports, calls} = await createProgram([
          /* 0x1000 */ "beql $v0, $v1, 0x02", // jump
          /* 0x1004 */ "x.log", // immediate taken
          /* 0x1008 */ "x.log", // <ignored>
          /* 0x100C */ "x.log", // jump target
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$v0, 0x10);
        exports.cpuSetRegisterU32(MIPS_REG.$v1, 0x10);
        exports.testCpuRun(CODE_BASE_OFFSET, 4);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([0x1004, 0x100C]);

        expect(exports.cpuGetRegisterU32(MIPS_REG.$ra)).equal(0); // no RA set
    });

    it("beql - false", async () =>
    {
        const {exports, calls} = await createProgram([
          /* 0x1000 */ "beql $v0, $v1, 0xFF", // don't jump
          /* 0x1004 */ "x.log", // immediate ignored
          /* 0x1008 */ "x.log", // executed
          /* 0x100C */ "x.log", // executed
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$v0, 0x20);
        exports.cpuSetRegisterU32(MIPS_REG.$v1, 0x10);
        exports.testCpuRun(CODE_BASE_OFFSET, 4);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([0x1008, 0x100C]);

        expect(exports.cpuGetRegisterU32(MIPS_REG.$ra)).equal(0); // no RA set
    });

    it("bc1t - true", async () =>
    {
        const {exports, calls} = await createProgram([
          /* 0x1000 */ "bc1t 0x02", // jump
          /* 0x1004 */ "x.log", // immediate
          /* 0x1008 */ "x.log", // <ignored>
          /* 0x100C */ "x.log", // jump target
        ]);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 4);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([0x1004, 0x100C]);

        expect(exports.cpuGetRegisterU32(MIPS_REG.$ra)).equal(0); // no RA set
    });

    it("bc1t - false", async () =>
    {
        const {exports, calls} = await createProgram([
          /* 0x1000 */ "bc1t 0xFF", // don't jump
          /* 0x1004 */ "x.log", // immediate
          /* 0x1008 */ "x.log", // executed
          /* 0x100C */ "x.log", // executed
        ]);
        exports.testFpuSetConditionFlag(false);
        exports.testCpuRun(CODE_BASE_OFFSET, 4);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([0x1004, 0x1008, 0x100C]);

        expect(exports.cpuGetRegisterU32(MIPS_REG.$ra)).equal(0); // no RA set
    });

    it("bc1tl - true", async () =>
    {
        const {exports, calls} = await createProgram([
          /* 0x1000 */ "bc1tl 0x02", // jump
          /* 0x1004 */ "x.log", // immediate taken
          /* 0x1008 */ "x.log", // <ignored>
          /* 0x100C */ "x.log", // jump target
        ]);
        exports.testFpuSetConditionFlag(true);
        exports.testCpuRun(CODE_BASE_OFFSET, 4);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([0x1004, 0x100C]);

        expect(exports.cpuGetRegisterU32(MIPS_REG.$ra)).equal(0); // no RA set
    });

    it("bc1tl - false", async () =>
    {
        const {exports, calls} = await createProgram([
          /* 0x1000 */ "bc1tl 0xFF", // don't jump
          /* 0x1004 */ "x.log", // immediate ignored
          /* 0x1008 */ "x.log", // executed
          /* 0x100C */ "x.log", // executed
        ]);
        exports.testFpuSetConditionFlag(false);
        exports.testCpuRun(CODE_BASE_OFFSET, 4);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([0x1008, 0x100C]);

        expect(exports.cpuGetRegisterU32(MIPS_REG.$ra)).equal(0); // no RA set
    });

    it("bgezal - false", async () =>
    {
        const {exports, calls} = await createProgram([
          /* 0x1000 */ "bgezal $v0, 0xFF", // don't jump
          /* 0x1004 */ "x.log", // immediate
          /* 0x1008 */ "x.log", // executed, return address
          /* 0x100C */ "x.log", // executed
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$v0, -10);
        exports.testCpuRun(CODE_BASE_OFFSET, 4);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([0x1004, 0x1008, 0x100C]);

        expect(exports.cpuGetRegisterU32(MIPS_REG.$ra)).equal(0x1008);
    });

    it("bgezal - true", async () =>
    {
        const {exports, calls} = await createProgram([
          /* 0x1000 */ "bgezal $v0, 0x02", // jump
          /* 0x1004 */ "x.log", // immediate taken
          /* 0x1008 */ "x.log", // <ignored>, return address
          /* 0x100C */ "x.log", // jump target
        ]);

        exports.cpuSetRegisterU32(MIPS_REG.$v0, 0x1002);
        exports.testCpuRun(CODE_BASE_OFFSET, 4);

        expect(exports.testCpuGetErrorCount()).equal(0);
        expect(calls).to.eql([0x1004, 0x100C]);

        expect(exports.cpuGetRegisterU32(MIPS_REG.$ra)).equal(0x1008);
    });
});
