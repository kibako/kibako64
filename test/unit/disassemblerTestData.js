/**
 * @copyright 2020 - Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" file in the root directory
 */

import { MIPS_REG } from './registerMapping.js';

const R = MIPS_REG;
export const testInstructions = [
  { data: 0, op: "nop" },
  // { data: 0, op: "j", args: [] },
  { data: 0x0C2E856B, op: "jal", args: [0x00BA15AC] },
  { data: 0x110B000E, op: "beq", args: [R.$t0, R.$t3, 0x0000_AA3C] },
  { data: 0x15E10005, op: "bne", args: [R.$t7, R.$at, 0xAA18] },
  // { data: 0, op: "blez", args: [] },
  // { data: 0, op: "bgtz", args: [] },
  { data: 0x25CE1B0C, op: "addiu", args: [R.$t6, R.$t6, 0x1B0C] },
  { data: 0x28C10010, op: "slti", args: [R.$at, R.$a2, 0x10] },
  { data: 0x2D410006, op: "sltiu", args: [R.$at, R.$t2, 0x06] },
  { data: 0x32290042, op: "andi", args: [R.$t1, R.$s1, 0x42] },
  // { data: 0, op: "ori", args: [] },
  // { data: 0, op: "lui", args: [] },
  // { data: 0, op: "beql", args: [] },
  // { data: 0, op: "bnel", args: [] },
  // { data: 0, op: "blezl", args: [] },
  // { data: 0, op: "bgtzl", args: [] },
  // { data: 0, op: "daddiu", args: [] },
  { data: 0x00A2282D, op: "daddu", args: [] },
  { data: 0x804E0a4E, op: "lb", args: [R.$t6, 0xA4E, R.$v0] },
  { data: 0x84CE019C, op: "lh", args: [R.$t6, 0x019C, R.$a2] },
  { data: 0x894F0004, op: "lwl", args: [R.$t7, 0x04, R.$t2] },
  { data: 0x8FBF0014, op: "lw", args: [R.$ra, 0x14, R.$sp] },
  { data: 0x9C440004, op: "lwu", args: [R.$a0, 0x04, R.$v0] },
  // { data: 0, op: "lbu", args: [] },
  // { data: 0, op: "lhu", args: [] },
  { data: 0x994E0003, op: "lwr", args: [R.$t6, 0x03, R.$t2] },
  // { data: 0, op: "sb", args: [] },
  // { data: 0, op: "sh", args: [] },
  // { data: 0, op: "swl", args: [] },
  // { data: 0, op: "sw", args: [] },
  { data: 0xFFB10028, op: "sd", args: [R.$s1, 0x28, R.$sp] },
  // { data: 0, op: "swr", args: [] },
  { data: 0xC4A60000, op: "lwc1", args: [R.$f6, 0, R.$a1] },
  // { data: 0, op: "ld", args: [] },
  { data: 0xE4B20000, op: "swc1", args: [R.$f18, 0, R.$a1] },
  // { data: 0, op: "sll", args: [] },
  { data: 0x000214B8, op: "dsll", args: [R.$v0, R.$v0, 18] },
  
  // { data: 0, op: "srl", args: [] },
  { data: 0x0004213A, op: "dsrl", args: [R.$a0, R.$a0, 4] },
  { data: 0x00041E3E, op: "dsrl32", args: [R.$v1, R.$a0, 0x18] },
  // { data: 0, op: "sra", args: [] },
  // { data: 0, op: "sllv", args: [] },
  { data: 0x03E00008, op: "jr", args: [R.$ra] },
  // { data: 0, op: "jalr", args: [] },
  // { data: 0, op: "breakpoint", args: [] },
  // { data: 0, op: "mfhi", args: [] },
  // { data: 0, op: "mflo", args: [] },
  { data: 0x03600013, op: "mtlo", args: [R.$k1] },
  { data: 0x00E00011, op: "mthi", args: [R.$a3] },
  { data: 0x00D40018, op: "mult", args: [R.$a2, R.$s4] },
  // { data: 0, op: "multu", args: [] },
  // { data: 0, op: "div", args: [] },
  // { data: 0, op: "dmultu", args: [] },
  // { data: 0, op: "ddiv", args: [] },
  // { data: 0, op: "ddivu", args: [] },
  // { data: 0, op: "addu", args: [] },
  { data: 0x00A42822, op: "sub", args: [R.$a1, R.$a1, R.$a0] },
  // { data: 0, op: "subu", args: [] },
  { data: 0x0082102F, op: "dsubu", args: [R.$v0, R.$a0, R.$v0] },
  { data: 0x014B4824, op: "op_and", args: [R.$t1, R.$t2, R.$t3] },
  { data: 0x00505025, op: "op_or", args: [R.$t2, R.$v0, R.$s0] },
  { data: 0x00854025, op: "op_or", args: [R.$t0, R.$a0, R.$a1] },
  // { data: 0, op: "slt", args: [] },
  // { data: 0, op: "sltu", args: [] },
  // { data: 0, op: "dsll32", args: [] },
  // { data: 0, op: "dsra32", args: [] },
  // { data: 0, op: "bltz", args: [] },
  // { data: 0, op: "bltzl", args: [] },
  // { data: 0, op: "bgezl", args: [] },
  { data: 0x05514700, op: "bgezal", args: [R.$t2, 0x0001_1C04] },
  { data: 0x40086000, op: "mfc0", args: [R.$t0, R.$12] },
  { data: 0x40896000, op: "mtc0", args: [R.$t1, R.$12] },
  { data: 0x44095000, op: "mfc1", args: [R.$t1, R.$f10] },
  { data: 0x44250000, op: "dmfc1", args: [R.$a1, R.$f0] },
  { data: 0x44889000, op: "mtc1", args: [R.$t0, R.$f18] },
  { data: 0x44A20000, op: "dmtc1", args: [R.$v0, R.$f0] },
  { data: 0x460A0401, op: "sub_s", args: [R.$f16, R.$f0, R.$f10] },
  { data: 0x462A4281, op: "sub_d", args: [R.$f10, R.$f8, R.$f10] },
  { data: 0x460A2402, op: "mul_s", args: [R.$f16, R.$f4, R.$f10] },
  { data: 0x46349102, op: "mul_d", args: [R.$f4, R.$f18, R.$f20] },
  { data: 0x46109283, op: "div_s", args: [R.$f10, R.$f18, R.$f16] },
  { data: 0x46261283, op: "div_d", args: [R.$f10, R.$f2, R.$f6] },
  { data: 0x4600428D, op: "trunc_w_s", args: [R.$f10, R.$f8] },
  { data: 0x4620418D, op: "trunc_w_d", args: [R.$f6, R.$f8] },
  // { data: 0, op: "cvt_s_d", args: [] },
  // { data: 0, op: "cvt_s_w", args: [] },
  // { data: 0, op: "cvt_s_l", args: [] },
  { data: 0x12a4020, op: "add", args: [R.$t0, R.$t1, R.$t2] },
  // { data: 0, op: "beqz", args: [] },
  // { data: 0, op: "b", args: [] },
  // { data: 0, op: "bnez", args: [] },
  { data: 0x0441000E, op: "bgez", args: [R.$v0, 0x0000_AA3C] },
  { data: 0x392800E2, op: "xori", args: [R.$t0, R.$t1, 0xE2] },
  { data: 0x01614026, op: "op_xor", args: [R.$t0, R.$t3, R.$at] },
  { data: 0x012a4027, op: "nor", args: [R.$t0, R.$t1, R.$t2] },
  { data: 0x018D580B, op: "movn", args: [R.$t3, R.$t4, R.$t5] },
  { data: 0x018D580A, op: "movz", args: [R.$t3, R.$t4, R.$t5] },
  { data: 0x008C4806, op: "srlv", args: [R.$t1, R.$t4, R.$a0] },
  { data: 0x00AD5007, op: "srav", args: [R.$t2, R.$t5, R.$a1] },
  // { data: 0, op: "lb", args: [] },
  { data: 0x2231FFFF, op: "addi", args: [R.$s1, R.$s1, -1] },
  { data: 0x4501000E, op: "bc1t", args: [0x0000_AA3C] },
  { data: 0x4500000F, op: "bc1f", args: [0x0000_AA40] },

  { data: 0x4503000E, op: "bc1tl", args: [0x0000_AA3C] },
  { data: 0x4502000F, op: "bc1fl", args: [0x0000_AA40] },

  { data: 0x460042A1, op: "cvt_d_s", args: [R.$f10, R.$f8] },
  { data: 0x46809121, op: "cvt_d_w", args: [R.$f4, R.$f18] },
  { data: 0x46A00021, op: "cvt_d_l", args: [R.$f0, R.$f0] },

  { data: 0x46083280, op: "add_s", args: [R.$f10, R.$f6, R.$f8] },
  { data: 0x46020800, op: "add_s", args: [R.$f0, R.$f1, R.$f2] },
  
  { data: 0x46283280, op: "add_d", args: [R.$f10, R.$f6, R.$f8] },

  { data: 0xF7B80038, op: "sdc1", args: [R.$f24, 0x38, R.$sp] },
  // { data: 0xFBB80038, op: "sdc2", args: [R.$f24, 0x38, R.$sp] }, // Unimplemented!

  { data: 0x4600A586, op: "mov_s", args: [R.$f22, R.$f20] },
  { data: 0x46206086, op: "mov_d", args: [R.$f2, R.$f12] },

  { data: 0x46046032, op: "c_eq_s", args: [R.$f12, R.$f4] },
  { data: 0x46222032, op: "c_eq_d", args: [R.$f4, R.$f2] },

  { data: 0x4606203C, op: "c_lt_s", args: [R.$f4, R.$f6] },
  { data: 0x462A403C, op: "c_lt_d", args: [R.$f8, R.$f10] },

  { data: 0x4614803E, op: "c_le_s", args: [R.$f16, R.$20] },
  { data: 0x4630903E, op: "c_le_d", args: [R.$f18, R.$f16] },

  { data: 0x46340037, op: "c_ule_d", args: [R.$f0, R.$f20] },
  { data: 0x46200031, op: "c_un_d", args: [R.$f0, R.$f0] },

  { data: 0x46006005, op: "abs_s", args: [R.$f0, R.$f12] },

  { data: 0xD7B40010, op: "ldc1", args: [R.$f20, 0x10, R.$sp] },
  // { data: 0xDBB40010, op: "ldc2", args: [R.$f20, 0x10, R.$sp] }, // Unimplemented!

  { data: 0x46006087, op: "neg_s", args: [R.$f2, R.$f12] },
  { data: 0x46206087, op: "neg_d", args: [R.$f2, R.$f12] },

  { data: 0x460058C4, op: "sqrt_s", args: [R.$f3, R.$f11] },
  { data: 0x462058C4, op: "sqrt_d", args: [R.$f3, R.$f11] },

  { data: 0xBD080000, op: "cache", args: [8, R.$t0] },
  { data: 0x42000002, op: "tlbwi", args: [] },
  
  { data: 0x44C2F800, op: "ctc1", args: [R.$v0, 31] },
  { data: 0x4443F000, op: "cfc1", args: [R.$v1, 30] },
  
  { data: 0x42000018, op: "eret", args: [] },

  { data: 0xB2080008, op: "sdl", args: [] },
  { data: 0xB608000F, op: "sdr", args: [] },

  { data: 0x6A250028, op: "ldl", args: [] },
  { data: 0x6E29000F, op: "ldr", args: [] },

  { data: 0x022001F4, op: "teq", args: [R.$s1, R.$zero, 7] },

  // Unofficial extensions, only defined in debug-builds
  { data: 0xFFFFFF_00, op: "x_log" },
  { data: 0xFFFFFF_01, op: "x_inject" },
];