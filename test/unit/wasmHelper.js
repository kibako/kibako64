/**
 * @copyright 2020 - Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" file in the root directory
 */

import mipsInst from 'mips-inst';
import {TextDecoder} from 'util';
import { createInstance } from '#js/kibako64.js';
import chai from "chai"; const {expect} = chai;
import {readFileSync} from 'fs';

global.fetch = path => {
  const dir = path.includes("debug") ? "cmake-build-debug" : "cmake-build-release";
  return readFileSync(dir + "/" + path);
};
WebAssembly.compileStreaming = async data => {
  return WebAssembly.compile(data);
};

const parse = mipsInst.parse;
export const CODE_BASE_OFFSET = 0x1000;

export const assertHex = (actual, expected) => {
  return expect((actual >>> 0).toString(16))
      .equal((expected >>> 0).toString(16));
}

export const assertHexU64 = (actual, expected) => {
  return expect(BigInt.asUintN(64, actual).toString(16).padStart(16, '0'))
      .equal(BigInt.asUintN(64, expected).toString(16).padStart(16, '0'));
}

export const assertHexS64 = (actual, expected) => {
  return expect((actual).toString(16).padStart(16, '0'))
      .equal((expected).toString(16).padStart(16, '0'));
}

export const assertHexArray = (expected, actual) => {
  return expect(expected.map(x => (x >>> 0).toString(16).padStart(2, '0')).join(" "))
      .equal(actual.map(x => (x >>> 0).toString(16).padStart(2, '0')).join(" "));
};

export const readStringFromMemory = (buffer, address) => {
  let strBuff = buffer.slice(address);
  let size = strBuff.findIndex(val => val === 0);
  if(size < 0) {
      size = buffer.length - address;
  }

  return new TextDecoder("utf-8").decode(
    buffer.slice(address, address + size)
  );
};

export async function insertInstructions(exports, memory, address, instructions)
{
  const view = new DataView(memory.buffer);
  for(let i=0; i<instructions.length; ++i) 
  {
    let word = 0;
    const instr = instructions[i];

    if(typeof(instr) == "string")
    {
      if(instr === "x.log") {
        word = 0xFFFFFF00;
      } else if(instr === "x.inject") {
        word = 0xFFFFFF01;
      } else if(instr == "eret") {
        word = 0x42000018;
      } else {
        word = parse(instr);
      }
    } else {
      word = instr >>> 0;
    }

    view.setUint32(i*4 + address, word >>> 0, false);
  }
}

export async function createProgram(instructions, envImports = {})
{
    const rom = new Uint8Array();
    const instance = await createInstance(rom, true, envImports);
    const memBus = {
       writeU8: (address, val) => instance.exports.testMemBusWriteU8(address, val >>> 0),
      writeU16: (address, val) => instance.exports.testMemBusWriteU16(address, val >>> 0),
      writeU32: (address, val) => instance.exports.testMemBusWriteU32(address, val >>> 0),
      writeU64: (address, val) => instance.exports.testMemBusWriteU64(address, val),
      writeF32: (address, val) => instance.exports.testMemBusWriteF32(address, val),
      writeF64: (address, val) => instance.exports.testMemBusWriteF64(address, val),

       readU8: (address) => instance.exports.testMemBusReadU8(address),
      readU16: (address) => instance.exports.testMemBusReadU16(address),
      readU32: (address) => instance.exports.testMemBusReadU32(address),
      readU64: (address) => instance.exports.testMemBusReadU64(address),
      readF32: (address) => instance.exports.testMemBusReadF32(address),
      readF64: (address) => instance.exports.testMemBusReadF64(address),
    };

    // Fill instructions into memory
    insertInstructions(instance.exports, instance.memory, CODE_BASE_OFFSET, instructions);

    return {
      exports: instance.exports, 
      memory: instance.memory, 
      calls: instance.calls,
      memBus
    };
}

/**
 * Insert a big-endian buffer into memory
 * @param {Uint8Array} memory 
 * @param {number[]} byteArray
 * @param {number} offset 
 */
export function insertMemory(exports, memory, byteArray, offset) {
  const buffer = new Uint8Array(byteArray);
  memory.set(buffer, offset);
}

