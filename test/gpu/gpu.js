/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { createInstance } from '#js/kibako64.js';
import fs from 'fs';

const loader = (path) => {
    return WebAssembly.compile(fs.readFileSync("cmake-build-debug/kibako64.debug.wasm"))
};

async function main()
{
    // Kibako
    const romBuffer = new Uint8Array();
    const emu = await createInstance(romBuffer, true, {}, loader);
    emu.exports.setLogLevel(0b11111);

    // Memory dump
    const memoryDump = fs.readFileSync("memBlock.bin");
    emu.memory.set(memoryDump.subarray(0, 1024*1024*8));

    // Get seg-address
    emu.exports.gpuF3DEX2Reset();

    const view = new DataView(emu.memory.buffer);
    for(let i=0; i<16; ++i) {
        const address = view.getUint32(0x0016_6FA8 + (i*4));
        //console.log("Seg[" + i + "] = " + address.toString(16).padStart(8, '0'));
        emu.exports.gpuF3DEX2SetSegmentAddress(i, address);
    }

    // Display List
    console.log("Execute Display-List");
    console.time("DPL");

    const resultPtr = emu.exports.gpuF3DEX2GetOutputPointer();
    const res = emu.exports.gpuF3DEX2ParseDisplayList(0x80_19_0000);
    console.log(resultPtr.toString(16), res);

    console.timeEnd("DPL");
}

main().catch(e => console.error(e));
