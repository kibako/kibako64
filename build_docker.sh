# Build as current user with mapped volume
docker run \
  -u $(id -u):$(id -g) \
  --mount type=bind,source=${PWD}/src,target=/wasm \
  -it kibako64-build:v1.0.0

mv src/*.wasm build
