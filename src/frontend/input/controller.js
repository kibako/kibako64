/**
 * @copyright 2020 - Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" file in the root directory
 */

 const ButtonMask =
 {
  A      : (1 << 15), // 1
  B      : (1 << 14), // 0
  Z      : (1 << 13), // 6
  START  : (1 << 12), // 9
  D_UP   : (1 << 11), // 12
  D_DOWN : (1 << 10), // 13
  D_LEFT : (1 <<  9), // 14
  D_RIGHT: (1 <<  8), // 15
  //        (1 << 7),
  //        (1 << 6),
  L      : (1 <<  5), // 4
  R      : (1 <<  4), // 5
  C_UP   : (1 <<  3), 
  C_DOWN : (1 <<  2),
  C_LEFT : (1 <<  1),
  C_RIGHT: (1 <<  0),
};

export class Controller
{
  constructor()
  {
    this.buttonMask = 0x0000;
    this.joystickX = 0.0;
    this.joystickY = 0.0;
  }

  update()
  {
    const gamepad = navigator.getGamepads()[0];
    if(!gamepad)return;

    this.buttonMask = 0;
    this.buttonMask |= gamepad.buttons[ 1].pressed ? ButtonMask.A       : 0;
    this.buttonMask |= gamepad.buttons[ 0].pressed ? ButtonMask.B       : 0;
    this.buttonMask |= gamepad.buttons[ 6].pressed ? ButtonMask.Z       : 0;
    this.buttonMask |= gamepad.buttons[ 9].pressed ? ButtonMask.START   : 0;
    this.buttonMask |= gamepad.buttons[12].pressed ? ButtonMask.D_UP    : 0;
    this.buttonMask |= gamepad.buttons[13].pressed ? ButtonMask.D_DOWN  : 0;
    this.buttonMask |= gamepad.buttons[14].pressed ? ButtonMask.D_LEFT  : 0;
    this.buttonMask |= gamepad.buttons[15].pressed ? ButtonMask.D_RIGHT : 0;
    this.buttonMask |= gamepad.buttons[ 4].pressed ? ButtonMask.L       : 0;
    this.buttonMask |= gamepad.buttons[ 5].pressed ? ButtonMask.R       : 0;

    this.joystickX =  gamepad.axes[0] || 0.0;
    this.joystickY = -gamepad.axes[1] || 0.0;
  }

  getButtonMask()
  {
    return this.buttonMask;
  }

  getJoystickX()
  {
    return this.joystickX;
  }

  getJoystickY()
  {
    return this.joystickY;
  }
};