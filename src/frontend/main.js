/**
 * @copyright 2020 - Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" file in the root directory
 */

 /**
 * NOTE:
 * This code is just hacked together for testing.
 * It's not meant for real-world usage.
 */
import {createInstance} from './kibako64.js';
import {Controller} from './input/controller.js';
import {Emulator} from './emulator.js';
import {LoggingLevelMask, InterruptType, RcpInterruptType} from './types.js';
import {normalizeFramebuffer} from './renderer/texture/framebuffer.js';

import {Renderer} from './renderer/renderer.js';
import Convert from 'ansi-to-html';
var ansiConvert = new Convert();

let singleFrameAdvance = true;
let isPaused = true;
let isStopped = false;
let frameCount = 0;

//console.time = () => {};
//console.timeEnd = () => {};

document.addEventListener('DOMContentLoaded', async () => 
{
  const renderer = new Renderer(canvas);
  await renderer.init();
  renderer.draw();

let emu; // @TODO migrate to 'Emulator'

/**
 * @type Emulator
 */
let kb64 = undefined;
const speedInterruptRender = (1000 / 60);

const controller0 = new Controller();

function tickMain()
{
  if(!isPaused)
  {
    controller0.update();
    emu.exports.setGameControllerButton(0, controller0.getButtonMask());
    emu.exports.setGameControllerJoystick(0, controller0.getJoystickX(), controller0.getJoystickY());

    console.time("tickMain");
    emu.exports.run(550_000);
    
    console.timeEnd("tickMain");
  }

  if(isStopped)return;
  setTimeout(() => tickMain(), 0);
}

function tickInterruptRender()
{
  if(!isPaused)
  {
    console.time("tickInterruptRender");  
    //emu.exports.interrupt(InterruptType.RCP, RcpInterruptType.VI);
    console.timeEnd("tickInterruptRender");
  }

  if(isStopped)return;
  setTimeout(tickInterruptRender, speedInterruptRender);
}

function tickRender()
{
  if(!isPaused) {
    ++frameCount;
    inputFrameCount.value = "Frame: " + frameCount;

    const fbInfo = kb64.getFramebufferInfo();

    console.time("renderBuffer");

    renderer.setFramebuffer(
      normalizeFramebuffer(
        new DataView(emu.memory.buffer, (fbInfo.address >>> 0) & 0x0FFF_FFFF),
        fbInfo,
        emu.isBigEndian
      ),
      fbInfo.sizeX, fbInfo.sizeY
    );

    renderer.draw();
    console.timeEnd("renderBuffer");

    if(singleFrameAdvance) {
      singleFrameAdvance = false;
      isPaused = true;
    }
  }

  if(isStopped)return;
  requestAnimationFrame(tickRender);
}

async function main()
{
  console.log("Starting...");
  
  const isDebug = true;
  let romName = "ctest.z64";

  //romName = "HelloWorldCPU16BPP320X240.N64";
  romName = "COP0Register.N64";
  //romName = "timers.z64";

  const perfTimerStart = {};
  const perfTimerSum = {};

  let outputBuffer = "";

  const rom = await fetch(romName);
  const romBuffer = new Uint8Array(await rom.arrayBuffer());

  emu = await createInstance(romBuffer, isDebug, {
    performance_timer_start: id => {
      perfTimerStart[id] = performance.now();
    },
    performance_timer_stop: id => {
      if(!perfTimerSum[id]) {
        perfTimerSum[id] = 0;
      }
      perfTimerSum[id] += performance.now() - perfTimerStart[id];
    },
    print_char: c => {
      const char = String.fromCharCode(c);
      if(char == "\n") {
        outputBuffer += "<br>";
        log.innerHTML += ansiConvert.toHtml(outputBuffer);
        if(outputBuffer.includes("Infinite loop detected"))isPaused = true;
        outputBuffer = "";
      } else {
        outputBuffer += char;
      }
    }
  });
  kb64 = new Emulator(emu);

  window.controls = {
    togglePause: () => { isPaused = !isPaused },
    stop: () => { isStopped = true; },
    printState: () => {
      emu.exports.setLogLevel(0xFF);
      emu.exports.cpuLogState();
      emu.exports.fpuLogState();
      emu.exports.cop0LogState();
      emu.exports.setLogLevel(0b111);
    },
    step: () => {
      isPaused = false;
      singleFrameAdvance = true;
    },
    clearLog: () => {
      log.innerHTML = "";
    }
  };

  window.romBuffer = romBuffer;
  window.emu = emu;

  emu.exports.disconnectGameController(0);
  emu.exports.disconnectGameController(1);
  emu.exports.disconnectGameController(2);
  emu.exports.disconnectGameController(3);

  // Boot & Run
  emu.memory.set(romBuffer.slice(0, 0x100000), 0x400);
  console.time("boot");
  emu.exports.boot();
  console.timeEnd("boot");

  //emu.exports.setLogLevel(0xFF);
  emu.exports.setLogLevel(0b1111);

  let totalTime = performance.now();
  emu.exports.run(3_500_000);
  //emu.exports.run(40_000_000); // ~1840ms
  totalTime = performance.now() - totalTime;

  console.log("totalTime: " + totalTime);
  totalTime -= perfTimerSum[1382377006] * Object.keys(perfTimerSum).length;
  console.log("totalTime (corrected): " + totalTime);

  for(let id in perfTimerSum) {
    id = parseInt(id);
    const realTime = perfTimerSum[id] - perfTimerSum[1382377006];

    console.log("Timer %d (%s%s%s%s) = %fms (%fms | %f\%)", id, 
    String.fromCharCode((id >> 24) & 0xFF),
    String.fromCharCode((id >> 16) & 0xFF),
    String.fromCharCode((id >>  8) & 0xFF),
    String.fromCharCode(id & 0xFF),
      perfTimerSum[id],
      realTime,
      realTime / totalTime * 100
      );
  }

  tickMain();
  tickInterruptRender();

  requestAnimationFrame(tickRender);
};

window.addEventListener("gamepadconnected", function(e) {
  emu.exports.connectGameController(0);

  console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.",
    e.gamepad.index, e.gamepad.id,
    e.gamepad.buttons.length, e.gamepad.axes.length);
});

window.addEventListener("gamepaddisconnected", function(e) {
  emu.exports.disconnectGameController(0);

  console.log("Gamepad disconnected from index %d: %s",
    e.gamepad.index, e.gamepad.id);
});

main().then().catch(e => console.error(e));

});
