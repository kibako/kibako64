/**
* @copyright 2021 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

export const LoggingLevelMask =
{
  NONE : 0,
  ERROR: (1 << 1),
  WARN : (1 << 2),
  INFO : (1 << 3),
  TRACE: (1 << 4),
  PROG : (1 << 5),
  ALL  : 0xFFFF
};

export const InterruptType =
{
  SOFTWARE_0: (1 <<  8),
  SOFTWARE_1: (1 <<  9),
  RCP       : (1 << 10),
  ROM       : (1 << 11),
  RESET     : (1 << 12),
  RDB_READ  : (1 << 13),
  RDB_WRITE : (1 << 14),
  TIMER     : (1 << 15),
};

export const RcpInterruptType = {
  NONE: 0,
  SP  : (1 << 0), // Signal-Proc Task Done
  SI  : (1 << 1), // Controller input available
  AI  : (1 << 2), // Audio buffer swap
  VI  : (1 << 3), // Vertical-scan
  PI  : (1 << 4), // DMA ROM -> RAM copy done
  DP  : (1 << 5), // GPU/RDP done
};

export const RcpInterruptMask = {
  NONE: 0,
  CLEAR_SP : (1 << 0),
  SET_SP   : (1 << 1),
  CLEAR_SI : (1 << 2),
  SET_SI   : (1 << 3),
  CLEAR_AI : (1 << 4),
  SET_AI   : (1 << 5),
  CLEAR_VI : (1 << 6),
  SET_VI   : (1 << 7),
  CLEAR_PI : (1 << 8),
  SET_PI   : (1 << 9),
  CLEAR_DP : (1 << 10),
  SET_DP   : (1 << 11),
};