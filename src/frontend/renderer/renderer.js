/**
 * @copyright 2021 - Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" file in the root directory
 */

import PicoGL from "picogl";
import vertexShaderSource from "./shader/framebuffer.glsl.vert";
import fragmentShaderSource from "./shader/framebuffer.glsl.frag";

export class Renderer
{
  constructor(canvas)
  {
    this.emuFramebufferSize = [640, 480];

    this.app = PicoGL.createApp(canvas)
      .clearColor(0.0, 0.0, 0.0, 1.0);
    
    this.progFramebuffer = undefined;
  }

  async init()
  {
    const programs = await this.app.createPrograms([vertexShaderSource, fragmentShaderSource]);
    this.progFramebuffer = programs[0];

    // create rectangle to draw the framebuffer that was manually filled in the emu
    const positions = this.app.createVertexBuffer(PicoGL.FLOAT, 2, new Float32Array([
      -1.0, -1.0,  1.0, -1.0,  1.0, 1.0,
       1.0,  1.0,  -1.0, 1.0,  -1.0, -1.0,
    ]));
  
    const colors = this.app.createVertexBuffer(PicoGL.FLOAT, 3, new Float32Array([
      1.0, 0.0, 0.0,
      0.0, 1.0, 0.0,
      0.0, 0.0, 1.0,
      1.0, 1.0, 0.0,
      1.0, 0.0, 1.0,
      0.0, 1.0, 1.0,
    ]));

    this.texFramebuffer = this.app.createTexture2D(
      new Uint8Array(this.emuFramebufferSize[0] * this.emuFramebufferSize[1] * 4), 
      this.emuFramebufferSize[0],
      this.emuFramebufferSize[1],
      {
        minFilter: PicoGL.NEAREST,
        magFilter: PicoGL.NEAREST,
        wrapS: PicoGL.CLAMP_TO_EDGE,
        wrapT: PicoGL.CLAMP_TO_EDGE,
      }
    );
          
    this.drawCallDirectBuffer = this.app.createDrawCall(this.progFramebuffer, 
      this.app.createVertexArray()
        .vertexAttributeBuffer(0, positions)
        .vertexAttributeBuffer(1, colors)
    ).texture("tex", this.texFramebuffer);
  }

  setFramebuffer(buffer, width, height)
  {
    this.texFramebuffer.resize(width, height);
    this.texFramebuffer.data(buffer);
  }

  draw()
  {
    this.app.clear();
    this.drawCallDirectBuffer.draw();
  }

};