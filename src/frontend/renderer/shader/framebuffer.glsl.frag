#version 300 es
 
precision highp float;

in vec3 vColor;
in vec2 vUV;
uniform sampler2D tex;

out vec4 fragColor;

void main() 
{
  //fragColor = vec4(vUV, 0.0, 1.0);
  fragColor = vec4(texture(tex, vUV).rgb, 1.0);
}
