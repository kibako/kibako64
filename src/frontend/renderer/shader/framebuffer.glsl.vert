#version 300 es

layout(location=0) in vec4 position;
layout(location=1) in vec3 color;

out vec2 vUV;

void main() 
{
  vUV = position.xy * 0.5 + 0.5;
  vUV.y = 1.0 - vUV.y;
  gl_Position = position;
}