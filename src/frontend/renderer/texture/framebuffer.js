/**
 * @copyright 2021 - Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" file in the root directory
 */

export function normalizeFramebuffer(view, info, isBE) 
{
  const data = new Uint8Array(info.outputSizeX * info.outputSizeY * 4);
  const dataview = new DataView(data.buffer);

  if(info.format == 3) // RGBA-24 (aka target format)
  {
    let posIn=0;
    let posOut=0;
    for(let line=0; line<info.outputSizeY; ++line) 
    {
      for(let pt=0; pt<info.outputSizeX; ++pt) 
      {
        dataview.setUint32(posOut, view.getUint32(posIn, isBE) | 0x000000FF, false);
        posIn += 4;
        posOut += 4;
      }

      if(info.upscaleY != 1 && (line % info.upscaleY) == 0) {
        posIn -= info.outputSizeX*4;
      }
    }

    return data;
  }

  let isLowHalf = true;
  let i=0;
  let imgIdx = 0;
  let rgba = 0;

  // Format 2: RGB-16
  for(let y=0; y<info.rect.sizeY; ++y)
  {
    for(let x=0; x<info.rect.sizeX; ++x)
    {
      rgba = view.getUint32(i, isBE);
      if(isLowHalf) {
        rgba >>>= 16;
        isLowHalf = false;
      } else {
        rgba &= 0xFFFF;
        isLowHalf = true;
        i += 4;
      }

      data[imgIdx + 0] = ((rgba >> 11) & 0b11111) << 3;
      data[imgIdx + 1] = ((rgba >>  6) & 0b11111) << 3;
      data[imgIdx + 2] = ((rgba >>  1) & 0b11111) << 3;
      data[imgIdx + 3] = 0xFF;
      imgIdx += 4;
    }
  }

  return data;
}