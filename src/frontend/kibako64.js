/**
 * @copyright 2020 - Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" file in the root directory
 */

const RAM_SIZE = (1024 * 1024 * 8);

export async function createInstance(rom, debugMode = false, envImports = {}, compileFunc = undefined)
{
  debugMode = debugMode || !!process.env.DEBUG_MODE;
  const calls = [];

  if(!envImports.debugger_hook_event)envImports.debugger_hook_event = () => {};
  if(!envImports.debugger_hook_step)envImports.debugger_hook_step = () => {};
  if(!envImports.debugger_hook_mem_write)envImports.debugger_hook_mem_write = () => {};
  if(!envImports.debugger_hook_stack_push)envImports.debugger_hook_stack_push = () => {};
  if(!envImports.debugger_hook_stack_pop)envImports.debugger_hook_stack_pop = () => {};
  if(!envImports.hook_inject)envImports.hook_inject = () => {};

  if(!envImports.performance_timer_start)envImports.performance_timer_start = () => {};
  if(!envImports.performance_timer_stop)envImports.performance_timer_stop = () => {};

  const print_char = (process && process.stdout) 
    ? charCode => process.stdout.write(String.fromCharCode(charCode)) 
    : () => {};

  const moduleName = debugMode ? "kibako64.debug.wasm" : "kibako64.wasm";
  const wasmModule = compileFunc
      ? await compileFunc(moduleName)
      : await WebAssembly.compileStreaming(fetch(moduleName));

  const instance = await WebAssembly.instantiate(wasmModule, { env: {
    print_char,
    gpuEmitTexture: () => {},
    log_call: pc => calls.push(pc >>> 0),
    ...envImports,
  }});

  const memory = instance.exports.memory;
  const memoryBuffer = new Uint8Array(memory.buffer);

  memoryBuffer.fill(0, 0, RAM_SIZE)
  memoryBuffer.set(rom, RAM_SIZE); // insert ROM after RAM

  instance.exports.setLogLevel(0b11);
  instance.exports.init();

  return {exports: instance.exports, memory: memoryBuffer, calls};
}
