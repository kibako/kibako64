/**
* @copyright 2021 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

export class Emulator
{
  constructor(instance)
  {
    this.wasmApi = instance.exports;
    this.wasmMemory = instance.memory;

    this.fbInfoView = undefined;
    this.isBigEndian = true;
  }

  getFramebufferInfo()
  {
    const fbInfoPtr = this.wasmApi.getFramebufferInfo();
    
    // pointer never changes, but getFramebufferInfo updates the data inside
    if(!this.fbInfoView) {
      this.fbInfoView = new DataView(this.wasmMemory.buffer, fbInfoPtr);
    }

    const info = {
      address: this.fbInfoView.getUint32(4*0, true),
      sizeX  : this.fbInfoView.getUint32(4*1, true),
      sizeY  : this.fbInfoView.getUint32(4*2, true),
      format : this.fbInfoView.getUint32(4*3, true),
      rect   : {
        minX: this.fbInfoView.getUint32(4*4, true),
        maxX: this.fbInfoView.getUint32(4*5, true),
        minY: this.fbInfoView.getUint32(4*6, true),
        maxY: this.fbInfoView.getUint32(4*7, true),
      },
      horizontalSkip: this.fbInfoView.getUint32(4*8, true)
    };
    info.rect.sizeX = info.rect.maxX - info.rect.minX;
    info.rect.sizeY = info.rect.maxY - info.rect.minY;
    info.address = (info.address >>> 0) & 0x0FFF_FFFF;

    info.outputSizeX = info.sizeX;
    info.outputSizeY = info.sizeY > 300 ? 480 : 240; // @TODO how to calc this?

    info.upscaleY = 1;
    if(info.outputSizeX == 640 && info.outputSizeY == 240) {
      info.outputSizeY = 480;
      info.upscaleY = 2;
    }
  
    return info;
  }
};
