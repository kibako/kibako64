/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import fs from 'fs';
import { parse } from 'path';
import { DebuggerUI } from './ui.js';
import { rgba16bufferToPNG } from './renderer.js';
import {PNG} from 'pngjs';
import {Emulator} from '#js/emulator.js';
import {LoggingLevelMask, InterruptType, RcpInterruptType } from '#js/types.js';
import http from 'http';

/**
 * @param {number} val 
 */
const toHex08 = (val) =>
{
  return val.toString(16).toUpperCase().padStart(8, '0');
};

const INSTRUCTION_PREVIEW_COUNT = 20;

const registerCPU = { 
  zero: 0, at: 1, v0: 2, v1: 3, 
  a0: 4, a1: 5, a2: 6, a3: 7, t0: 8, 
  t1: 9, t2: 10, t3: 11, t4: 12, t5: 13,  t6: 14, t7: 15, 
  s0: 16, s1: 17, s2: 18, s3: 19, s4: 20, s5: 21, s6: 22, s7: 23, 
  t8: 24, t9: 25, k0: 26, k1: 27, gp: 28, sp: 29, fp: 30, ra: 31
};

const ghidraSendRequest = (action, value) =>
{
  try {
    http.request({
      host: "localhost",
      port: 9132,
      path: `?${action}=${value}`,
    }).end();
  } catch(e) {
    console.error(e);
  }
}

export class KibakoDebugger
{
  constructor()
  {
    this.logLevel = LoggingLevelMask.ALL;
    this.MAX_LOG_SIZE = 8_000;
    this.MAX_LOG_RESIZE_SIZE = this.MAX_LOG_SIZE + 1000;

    this.waitForBreakpoint = false;
    this.brSkip = 0;

    this.debugSkip = 0;
    this.breakReason = "";

    this.bufferLog = "";
    this.bufferTemp = "";

    this.lastCommand = "";

    this.breakOnEvent = true;
    this.breakPoints = {
      [0x0020_2B00]: "set interrupt",
      [0x0000_1354 - 12]: "nor"
    };
    this.breakPointKeys = Object.keys(this.breakPoints).map(x => parseInt(x));

    this.callStack = [];
    this.ramView = undefined;

    this.ui = new DebuggerUI(
      cmd => this.executeCommand(cmd),
      () => { 
        this.emu.exports.run(1);
        this.updateUI();
      }
    );
  }

  setEmu(emu)
  {
    this.emu = emu; // @TODO migrate to 'Emulator'.
    this.emulator = new Emulator(emu);
  }

  start()
  {
    this.ui.setLastAction("Booting game...");
    this.ui.setBreakpoints(this.breakPoints);

    this.updateUI();

    setTimeout(() => {
      this.setLogLevel(LoggingLevelMask.ERROR | LoggingLevelMask.WARN | LoggingLevelMask.INFO | LoggingLevelMask.PROG);

      this.emu.exports.boot();

      this.setLogLevel(LoggingLevelMask.ALL);

      this.ui.setLastAction("Game booted.");
      this.ui.setMemory(undefined, 0);
      this.updateUI();
    }, 5);
  }

  setLogLevel(level)
  {
    this.logLevel = level;
    this.emu.exports.setLogLevel(this.logLevel);
  }

  silentCpuStop()
  {
    const oldLogLevel = this.logLevel;
    this.setLogLevel(LoggingLevelMask.NONE);
    this.emu.exports.stop();
    this.setLogLevel(oldLogLevel);
  }

  executeCommand(cmdString)
  {
    let [cmd, arg0, arg1] = cmdString.trim().split(" ");
    let arg0Int = arg0 ? parseInt(arg0, arg0.startsWith("0x") ? 16 : 10) : 0;
    let arg1Int = arg1 ? parseInt(arg1, arg1.startsWith("0x") ? 16 : 10) : 0;

    switch(cmd)
    {
      case "s":
      case "sk":
      case "sm":
      case "step":
        let steps = arg0Int || 1;
        if(cmd === "sk")steps *= 1_000;
        if(cmd === "sm")steps *= 1_000_000;

        const oldLogLevel = this.logLevel;
        const silentMode = !!arg1;

        if(silentMode) {
          this.emu.exports.debuggerHookStepDisable();
          this.setLogLevel(arg1 == "i"  ? (LoggingLevelMask.ERROR | LoggingLevelMask.WARN | LoggingLevelMask.INFO) : LoggingLevelMask.NONE);
        }

        this.ui.setLastAction(`Execute ${steps} steps`);
        this.emu.exports.run(steps);

        if(silentMode) {
          this.emu.exports.debuggerHookStepEnable();
          this.setLogLevel(oldLogLevel);
        }
      break;

      case "b":
      case "break":
        arg0Int >>>= 0;
        this.breakPoints[arg0Int] = arg1 || "manual-breakpoint";
        this.breakPointKeys.push(arg0Int);

        this.ui.setBreakpoints(this.breakPoints);
        this.ui.setLastAction(`Added breakpoint '${this.breakPoints[arg0Int]}' @ 0x${toHex08(arg0Int)}`);
      break;

      case "i":
      case "interrupt":
        const interruptId = InterruptType[arg0 ? arg0.toUpperCase() : "RCP"];
        const rcpId = RcpInterruptType[arg1 ? arg1.toUpperCase() : "VI"] || 0;

        if(interruptId) {
          this.emu.exports.interrupt(interruptId, rcpId);
          this.ui.setLastAction(`Interrupted with type '${interruptId}' (RCP-Type: '${rcpId}')`);
        } else {
          this.ui.setLastError(`Interrupt type '${interruptId}' unknown`);
        }
      break;

      case "c":
      case "continue":
        this.waitForBreakpoint = true;
        this.brSkip = arg0Int || 0;
        const isSilentMode = (arg1 == "silent" || arg1 == "s");

        const oldLoggingLevel = this.logLevel;
        if(isSilentMode)this.setLogLevel(LoggingLevelMask.NONE);

        while(this.waitForBreakpoint) {
          this.emu.exports.run(100_000);
        }

        if(isSilentMode)this.setLogLevel(oldLoggingLevel);

      break;

      case 'log':
        let newLogLevel = LoggingLevelMask.PROG;
        switch(arg0) 
        {
          case "trace": newLogLevel |= LoggingLevelMask.TRACE;
          case "info" : newLogLevel |= LoggingLevelMask.INFO;
          case "warn" : newLogLevel |= LoggingLevelMask.WARN;
          case "error": newLogLevel |= LoggingLevelMask.ERROR;
            this.emu.exports.setLogLevel(newLogLevel);
            this.ui.setLastAction(`Logging level set to 0x${newLogLevel.toString(16)}`);
          break;

          default:
            this.ui.setLastError(`Unknown Logging level '${arg0}'`);
          break;
        }
        
      break;

      case "w":
      case "write":
        this.emu.exports.testMemBusWriteU32(arg0Int >>> 0, arg1Int);
        this.ui.setLastAction(`Value '0x${toHex08(arg0Int >>> 0)}' written to '0x${toHex08(arg1Int)}'`);
      break;

      case "mem":
        const baseAddress = arg0Int >>> 0;
        const memBuffer = new Uint8Array(8 * 42);
        
        for(let i=0; i<memBuffer.byteLength; ++i) {
          memBuffer[i] = this.emu.exports.memBusReadU8(baseAddress + i);
        }

        this.ui.setMemory(new DataView(memBuffer.buffer), baseAddress);
      break;

      case "render":
        this.emu.exports.render();
      break;

      case "draw":
        const fbAddress = (this.emu.exports.getFramebufferAddress() >>> 0) & 0x0FFF_FFFF;

        const png = rgba16bufferToPNG(
          new DataView(this.emu.memory.buffer, fbAddress)
        );
        const buffer = PNG.sync.write(png, {});

        this.ui.showImage(buffer,
          arg0Int || 0, arg1Int || 0,
          320, 240,
        );
      break;

      case "frame": {
        const oldLogLevel = this.logLevel;
        this.emu.exports.debuggerHookStepDisable();
        this.setLogLevel(LoggingLevelMask.NONE);
        const frames = arg0Int || 1;

        for(let i=0; i<frames; ++i) 
        {
          this.emu.exports.run(1_200_000);
          this.emu.exports.interrupt(InterruptType.RCP, RcpInterruptType.VI)
          this.emu.exports.run(300_000);
        }

        this.ui.setLastAction(`Executed for '${frames}' frames.`);

        this.emu.exports.debuggerHookStepEnable();
        this.setLogLevel(oldLogLevel);
      } break;

      case "reg":
        const regName = arg0 ? arg0.trim() : "";
        const regIdx = registerCPU[regName] || 0;
        if(regIdx != 0) 
        {
          this.emu.exports.testCpuSetRegisterU32(regIdx, arg1Int);
          this.ui.setLastAction(`Set CPU-Register '${regName}' to '0x${toHex08(arg1Int)}'`);
        } 
        else if(regName[0] == "f") 
        {
          const valFloat = parseFloat(arg1) || 0;
          this.emu.exports.testFpuSetRegisterF64(regIdx, valFloat);
          this.ui.setLastAction(`Set FPU-Register '${regName}' to '${valFloat}'`);
        } else {
          this.ui.setLastError(`Unknown register '${regName}'`);
        }
      break;

      case "dump":
        const memPath = "mem.bin";
        fs.writeFileSync(memPath, this.emu.memory.slice(0, 8 * 1024 * 1024));
        this.ui.setLastAction(`Memory dumped to '${memPath}'`);
      break;

      case "fbinfo":
        const fbInfo = this.emulator.getFramebufferInfo();
        
        this.log("Framebuffer-Info:\n");
        this.log("  Address: 0x" + toHex08(fbInfo.address));
        this.log("    SizeX: " + fbInfo.sizeX);
        this.log("    SizeY: " + fbInfo.sizeY);
        this.log("   Format: " + fbInfo.format);
      break;

      case "g":
      case "ghidra":
        if(arg0 == "q") {
          ghidraSendRequest("exit", "1");
        } else {
          let address = this.emu.exports.getPc() >>> 0;
          address = (address & 0x0FFF_FFFF) >>> 0;
          address = (address | 0x8000_0000) >>> 0;
          ghidraSendRequest("address", address.toString(16));
        }
      break;

      case "tlb":

        switch(arg0)
        {
          case "status":
          case "s":
            this.emu.exports.tlbPrintStatus();
          break;
          case "resolve":
          case "r":
            const res = this.emu.exports.tlbResolveAddress(arg1Int >>> 0);
            this.ui.setLastAction(`TLB resolved 0x${toHex08(arg1Int >>> 0)} to '${toHex08(res >>> 0)}'`);
          break;
        }

      break;

      case "q":
      case "quit":
      case "kill":
        this.quit();
      break;

      default:
        this.ui.closePopup();
        this.ui.closeImage();
        this.ui.setLastError(`Unknown Command '${cmd}'`);
      break;

    }

    this.updateUI();
  }

  updateUI()
  {
    // Log
    this.bufferLog = this.bufferLog.substring(this.bufferLog.length - this.MAX_LOG_SIZE);
    this.ui.setLog(this.bufferLog);
    
    // State
    const stateText = this.runWithTempBuffer(() => {
      this.emu.exports.cop0LogState();
      this.emu.exports.interruptLogState();
      this.emu.exports.cpuLogState();
      this.emu.exports.fpuLogState();
    });
    this.ui.setState(stateText);

    // Next Instructions
    const instructionText = this.runWithTempBuffer(() => {
      this.emu.exports.printNextInstructions(
        process.stdout.rows / 2 + 2 // estimate of half the screen height
      );
    });
    this.ui.setNextInstructions(instructionText);

    // Stack
    this.ui.setCallStack(this.callStack);

    //Memory view
    if(this.ramView) {
      this.ui.setMemory(this.ramView, this.ramView.byteOffset);
    }
  }

  /**
   * Executes a callback and logs all messages to a temporary buffer.
   * This buffer is then returned and logging switched back to default.
   * @param {function} cb 
   */
  runWithTempBuffer(cb)
  {
    const bufferLogBackup = this.bufferLog;
    this.bufferLog = "";
    
    cb();

    const buffer = this.bufferLog;
    this.bufferLog = bufferLogBackup;
    return buffer;
  }

  quit()
  {
    process.exit(0);
  }

  log(message) 
  {
    this.bufferLog += `[DEBUGGER] ${message}\n`;
  }

  /**
   * Hook the emulator can call to print a single character.
   * @param {string} char 
   */
  hookPrint(char)
  {
    this.bufferLog += char;
    if(this.bufferLog.length > this.MAX_LOG_RESIZE_SIZE) {
      this.bufferLog = this.bufferLog.substring(this.bufferLog.length - this.MAX_LOG_SIZE);
    }
  }

  /**
   * Hook the emulator can call if a write to the memory-bus occurs.
   * @param {number} address 
   * @param {number} value 
   */
  hookMemWrite(address, value)
  {
    return;
    address = address >>> 0;
    value = value || 0;

    if([
    ].includes(address)) {
      this.waitForBreakpoint = false;
      this.silentCpuStop();
      this.ui.setLastAction("Breakpoint: Mem-Write @ 0x" + toHex08(address));
    }
  }

  /**
   * Hook the emulator can call before every instruction.
   * @param {number} pc current PC
   */
  hookStep(pc)
  {
    pc = pc >>> 0;

    if(this.breakPoints[pc]) {

      if(this.waitForBreakpoint && this.brSkip > 0) {
        --this.brSkip;
        return;
      }

    //if(this.breakPointKeys.includes(pc)) { 
      this.waitForBreakpoint = false;
      this.silentCpuStop();
      this.ui.setLastAction("Breakpoint: PC @ 0x" + toHex08(pc) + " | " + this.breakPoints[pc]);
    }
  }

  hookStackPush(pc)
  {
    this.callStack.push(pc);
  }

  hookStackPop()
  {
    this.callStack.pop();
  }

  hookEvent(type)
  {
    if(this.breakOnEvent) {
      this.waitForBreakpoint = false;
      this.silentCpuStop();

      const typeNames = ["<unknown>", "ERET", "Exception-Level change", "Exception-PC change", "Interrupt"];
      this.ui.setLastAction("Breakpoint: Event '" + typeNames[type] + "'");
    }
  }
};