/**
* @copyright 2021 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import blessed from 'blessed';

const STYLE_INPUT = {          
  fg: '#787878',
  bg: '#454545',  
  focus: {        
    fg: '#f6f6f6',
    bg: '#353535' 
  }
};

const toAddress = (val) =>
{
  return "0x" + val.toString(16).toUpperCase().padStart(8, '0');
};

export class DebuggerUI
{
  constructor(cbCommand, cbStep)
  {
    this.cbStep = cbStep;
    this.lastCommand = "";

    this.screen = blessed.screen({
      smartCSR: true
    });
    this.screen.title = 'Kibako64 Debugger';

    this.boxLog = blessed.log({
      top: 0,
      left: 0,
      width: '60%',
      height: '60%',
      content: 'Booting... wait a moment',
      tags: true,
      border: { type: 'line'},
    });
    this.screen.append(this.boxLog);

    this.boxStack = blessed.log({
      top: 0,
      left: '60%',
      width: '15%',
      height: '30%',
      content: 'Stack',
      tags: true,
      border: { type: 'line'},
    });
    this.screen.append(this.boxStack);

    this.boxBreakpoints = blessed.log({
      top: '30%',
      left: '60%',
      width: '15%',
      height: '31%',
      content: 'Breakpoints',
      tags: true,
      border: { type: 'line'},
    });
    this.screen.append(this.boxBreakpoints);

    this.boxMemory = blessed.log({
      top: 0,
      left: '75%',
      width: '25%',
      height: '60%',
      content: 'Memory',
      tags: true,
      border: { type: 'line'},
    });
    this.screen.append(this.boxMemory);

    this.boxNextInstr = blessed.box({
      top: "60%",
      left: 0,
      width: '40%',
      height: '40%',
      content: 'Next Instructions:',
      tags: true,
      border: { type: 'line'},
    });
    this.screen.append(this.boxNextInstr);

    this.boxState = blessed.log({
      top: "60%",
      left: "40%",
      width: '60%',
      height: '40%',
      content: 'State:',
      tags: true,
      border: { type: 'line'},
    });
    this.screen.append(this.boxState);

    this.boxLastAction = blessed.box({
      bottom: 1,        
      height: 1,
      tags: true,
      content: "<No Errors>",
      style: {
        fg: '#111',
        bg: '#AAA',
      }
    });
    this.screen.append(this.boxLastAction);

    const input = blessed.textbox({ 
      bottom: 0,        
      height: 1,        
      inputOnFocus: true,
      style: STYLE_INPUT,
    });
    
    input.on("submit", () => {
      const cmd = input.getValue();
      this.lastCommand = cmd;

      input.clearValue();
      cbCommand(cmd);
      this.screen.render();

      input.focus();
    });
    
    input.key('down', (ch, key) => {
      this.cbStep();
    });

    input.key('up', (ch, key) => {
      const currentVal = input.getValue();
      input.setValue(this.lastCommand);
      this.lastCommand = currentVal;

      this.screen.render();
    });

    this.screen.append(input);

    this.screen.key(['escape', 'q'], (ch, key) => process.exit(0));
    this.screen.key(['c'], () => input.focus());
    input.focus();

    this.screen.render();
  }

  showPopup(content, sizeX, sizeY)
  {
    this.boxPopup = blessed.box({
      top: 0,     
      left: 0,
      width: sizeX,
      height: sizeY,
      draggable: true,
      tags: true,
      content: content,
      scrollable: true,
      scrollbar: true,
      shadow: true,
    });

    this.screen.append(this.boxPopup);
  }

  closePopup()
  {
    if(this.boxPopup) {
      this.screen.remove(this.boxPopup);
      this.boxPopup = undefined;
    }
  }

  showImage(imgBuffer, posX, posY, sizeX, sizeY)
  {
    this.closeImage();
    this.boxImage = blessed.ansiimage({
      top: posY,     
      left: posX,
      file: imgBuffer,
      width: sizeX,
      height: sizeY,
    });

    this.screen.append(this.boxImage);
  }

  closeImage()
  {
    if(this.boxImage) {
      this.screen.remove(this.boxImage);
      this.boxImage = undefined;
    }
  }

  setLastAction(error)
  {
    this.boxLastAction.setText(error);
    this.screen.render(); 
  }

  setLastError(error)
  {
    this.boxLastAction.setContent("{red-bg}ERROR:{/} " + error);
    this.screen.render(); 
  }

  setNextInstructions(instructionText)
  {
    this.boxNextInstr.setContent(instructionText);
    this.screen.render(); 
  }

  setBreakpoints(breakpoints)
  {
    let text = "{bold}Breakpoints:{/}\n";
    for(let address in breakpoints) {
      text += `${toAddress(address)}{grey-fg}: ${breakpoints[address]}{/}\n`;
    }

    this.boxBreakpoints.setContent(text);
    this.screen.render(); 
  }

  /**
   * @param {number[]} stack 
   */
  setCallStack(stack)
  {
    let text = "{bold}Return-Address Stack:{/}\n" 
    + "{grey-fg}IDX  Actual     Masked{/}\n" 
    + stack
      .map((pc, idx) => 
        (idx+"").padStart(3, " ") + ": " +
        toAddress(pc >>> 0) + 
        " {grey-fg}" + toAddress((pc | 0x8000_0000) >>> 0) + "{/}"
      )
      .join("\n");

    this.boxStack.setContent(text);
    this.screen.render(); 
  }

  /**
   * @param {DataView} view 
   */
  setMemory(view, baseAddress)
  {
    if(!view) {
      this.boxMemory.setContent("No memory region selected, use 'mem'");
      this.screen.render(); 
      return;
    }
    let text = `{bold}Memory ${toAddress(baseAddress)}:{/}\n`;

    for(let i=0; i<view.byteLength; i+=8) 
    {
      text += "\n" + toAddress(baseAddress + i) + ": ";

      for(let b=0; b<8; ++b) {
        text += view.getUint8(i+b).toString(16).toUpperCase().padStart(2, 0) + " ";
      }

      text += " |  ";

      for(let b=0; b<8; ++b) {
        const byte = view.getUint8(i+b);
        text += (byte > 0x20 && byte < 127) ? String.fromCharCode(view.getUint8(i+b)) : ".";
        text += " ";
      }
    }
    
    this.boxMemory.setContent(text);
    this.screen.render(); 
  }

  setState(stateText)
  {
    this.boxState.setContent(stateText);
    this.screen.render(); 
  }

  setLog(logText)
  {
    this.boxLog.setContent(logText);
    this.screen.render();
  }
};