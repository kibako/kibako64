/**
 * @copyright 2020 - Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" file in the root directory
 */

import fs from 'fs';
import { createInstance } from '#js/kibako64.js';
import { KibakoDebugger } from './debugger.js';
import { LoggingLevelMask } from '#js/types.js';

import {performance} from 'perf_hooks';

global.fetch = path => {
  return fs.readFileSync("cmake-build-debug/" + path);
};
WebAssembly.compileStreaming = async data => {
  return WebAssembly.compile(data);
};

const DEBUG_MODE = !process.argv.includes("-p");
const NO_DEBUGGER = process.argv.includes("--nd");
const romPath = process.argv[2];

let emu;

async function main()
{
  const kibakoDebugger = new KibakoDebugger();

  // Load File
  const romBuffer = fs.readFileSync(romPath);
  emu = await createInstance(romBuffer, DEBUG_MODE, {
    print_char: charCode => kibakoDebugger.hookPrint(String.fromCharCode(charCode)),
    debugger_hook_step: pc => kibakoDebugger.hookStep(pc),
    debugger_hook_mem_write: (address, value) => kibakoDebugger.hookMemWrite(address, value),
    debugger_hook_stack_push: pc => kibakoDebugger.hookStackPush(pc),
    debugger_hook_stack_pop: () => kibakoDebugger.hookStackPop(),
    debugger_hook_event: type => kibakoDebugger.hookEvent(type),
  });

  emu.exports.setLogLevel(LoggingLevelMask.ERROR | LoggingLevelMask.WARN | LoggingLevelMask.PROG);

  kibakoDebugger.setEmu(emu);
  kibakoDebugger.start();

  return;
}

let timeTotal = 0;
async function mainPerf(run = 0)
{
  // Load File
  const romBuffer = fs.readFileSync(romPath);
  emu = await createInstance(romBuffer, DEBUG_MODE, {
    print_char: charCode => process.stdout.write(String.fromCharCode(charCode)),
  });

  emu.exports.setLogLevel(LoggingLevelMask.ERROR | LoggingLevelMask.WARN | LoggingLevelMask.PROG);
  emu.exports.setLogLevel(0xFF);

/**
controller_test.z64
normal: ~690ms
cache : ~625ms (only opcode)
cache : 
 */

  emu.exports.boot();
  let timeStart = performance.now();
  emu.exports.run(50_000_000);
  let timeAdd = performance.now() -  timeStart;
  timeTotal += timeAdd;
  console.log("Time: " + timeAdd);

  if(run == -1)
  {
    //fs.writeFileSync("mem.bin", emu.memory.slice(0, 8 * 1024 * 1024));
    const ramActual = emu.memory.slice(0, 8 * 1024 * 1024);
    const ramExpected = fs.readFileSync("mem.bin");

    const viewActual = new DataView(ramActual.buffer);
    const viewExpected = new DataView(ramExpected.buffer);

    for(let i=0; i<ramActual.byteLength; i+=4) {
      if(viewActual.getUint32(i, true) != viewExpected.getUint32(i, false)) {
        console.log("NON MATCHING RAM!");
        //throw new Error("Non matching RAM");
        break;
      }
    }
  }

  return;
}

if (!NO_DEBUGGER) {
  main().then().catch(e => console.error(e));
} else {
  const NUM_RUNS = 10;
  (async () => 
  {
    for(let i=0; i<NUM_RUNS; ++i)
      await mainPerf(i);

    console.log("Time-Total: " + (timeTotal / NUM_RUNS));
  })()
    .then()
    .catch(e => console.error(e));
}
