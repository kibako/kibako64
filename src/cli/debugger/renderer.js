/**
* @copyright 2021 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import {PNG} from 'pngjs';

export function rgba16bufferToPNG(frameBuffer)
{
  const png = new PNG({
    width: 320,
    height: 240,
    filterType: -1
  });

  let i=0;
  let pngIdx = 0;
  for(let y=0; y<png.height; y++) 
  {
    for(let x=0; x<png.width; x++) 
    {
      const rgba = frameBuffer.getUint16(i, false);

      const r = ((rgba >> 11) & 0b11111) << 3;
      const g = ((rgba >>  6) & 0b11111) << 3;
      const b = ((rgba >>  1) & 0b11111) << 3;

      png.data[pngIdx+0] = r;
      png.data[pngIdx+1] = g;
      png.data[pngIdx+2] = b;
      png.data[pngIdx+3] = 0xFF;

      i += 2;
      pngIdx += 4;
    }
  }

  return png;
}