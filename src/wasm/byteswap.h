/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "types.h"

#define USE_BUILTINS 1

inline u8 byteswap(u8 val) { return     val; }
inline u8 byteswap(s8 val) { return (u8)val; }

inline u16 byteswap(u16 val) { return __builtin_bswap16(     val); }
inline u16 byteswap(s16 val) { return __builtin_bswap16((u16)val); }

#ifdef USE_BUILTINS
  inline u32 byteswap(u32 val) { return __builtin_bswap32(val); }
#else
  inline u32 byteswap(u32 val) {
     asm(
        "local.get  0        \n"
        "i32.const 0x00FF00FF\n"
        "i32.and             \n"
        "i32.const 8         \n"
        "i32.rotr            \n"

        "local.get 0         \n"
        "i32.const 0xFF00FF00\n"
        "i32.and             \n"
        "i32.const 8         \n"
        "i32.rotl            \n"

        "i32.or              \n"
        //"local.set 0"
      );
      return val;
    }
#endif

inline s32 byteswap(s32 val) { return (s32)byteswap((u32)val); }

inline u64 byteswap(u64 val) { return __builtin_bswap64(val); }
inline u64 byteswap(s64 val) { return __builtin_bswap64((u64)val); }

#define rotr32(val, amount) __builtin_rotateright32(val, amount)
#define rotr64(val, amount) __builtin_rotateright64(val, amount)

#define rotl32(val, amount) __builtin_rotateleft32(val, amount)
#define rotl64(val, amount) __builtin_rotateleft64(val, amount)