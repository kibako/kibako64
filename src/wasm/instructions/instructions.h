/**
 * @copyright 2020 - Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" file in the root directory
 */
#pragma once

#include "../main.h"
#include "../components/cpu.h"
#include "../stdlib/stdlib.h"

namespace MipsiOp
{
  // This generates function-definitions for all implemented OPs.
  // Implementations are grouped by function and split across multiple sources.
  #define OP_FUNCTION(name) void name(u32 word);
  #include "implementedOps.h"
  #undef OP_FUNCTION

  #ifdef FEATURE_OPCODE_NAME_TABLE
    extern const char* opcodeNames[0xFF];
  #endif

  // Can be used in tests to check if all opcodes are smaller than 0xFF.
  // This must be true, since a bunch of other places rely on that
  // @TODO check if there is a non-hacky way to do this at compile time
  u32 getHighestPointer(u32 dummy);

  void fillOpcodeNameTable();
  const char* getOpcodeName(s32 opcodeFunction);
  void printInstruction(u32 op, u32 word);
}
