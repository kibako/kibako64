/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "instructions.h"
#include "./opcodeArgs.h"
#include "../components/cpuRegisters.h"

void MipsiOp::andi(u32 word) { 
	cpu.setRegister<u64>(ARG_RT, cpu.getRegister<u64>(ARG_RS) & ARG_IMM);
}

void MipsiOp::ori(u32 word) { 
	cpu.setRegister<u64>(ARG_RT, cpu.getRegister<u64>(ARG_RS) | ARG_IMM);
}

void MipsiOp::xori(u32 word) { 
	cpu.setRegister<u64>(ARG_RT, cpu.getRegister<u64>(ARG_RS) ^ ARG_IMM);
}

void MipsiOp::op_and(u32 word) { 
	cpu.setRegister<u64>(ARG_RD, cpu.getRegister<u64>(ARG_RS) & cpu.getRegister<u64>(ARG_RT));
}

void MipsiOp::op_or(u32 word) { 
	cpu.setRegister<u64>(ARG_RD, cpu.getRegister<u64>(ARG_RS) | cpu.getRegister<u64>(ARG_RT));
}

void MipsiOp::op_xor(u32 word) { 
	cpu.setRegister<u64>(ARG_RD, cpu.getRegister<u64>(ARG_RS) ^ cpu.getRegister<u64>(ARG_RT));
}

void MipsiOp::nor(u32 word) { 
	cpu.setRegister<u64>(ARG_RD, ~(cpu.getRegister<u64>(ARG_RS) | cpu.getRegister<u64>(ARG_RT)));
}

void MipsiOp::slti(u32 word) { 
	cpu.setRegister<u32>(ARG_RT, (cpu.getRegister<s32>(ARG_RS) < ARG_IMM_EXT) ? 1 : 0);
}

void MipsiOp::sltiu(u32 word) { 
	cpu.setRegister<u32>(ARG_RT, (cpu.getRegister<u32>(ARG_RS) < ((u32)ARG_IMM_EXT)) ? 1 : 0);
}

void MipsiOp::slt(u32 word) { 
	cpu.setRegister<u32>(ARG_RD, (cpu.getRegister<s32>(ARG_RS) < cpu.getRegister<s32>(ARG_RT)) ? 1 : 0);
}

void MipsiOp::sltu(u32 word) { 
	cpu.setRegister<u32>(ARG_RD, (cpu.getRegister<u32>(ARG_RS) < cpu.getRegister<u32>(ARG_RT)) ? 1 : 0);
}

// Note: a NOP (word=0) instruction maps to 'sll' with no shift-amount.
void MipsiOp::sll (u32 word) { 
  cpu.setRegister<s32>(ARG_RD, cpu.getRegister<u32>(ARG_RT) << ARG_SA);
}

void MipsiOp::dsll(u32 word) { cpu.setRegister<u64>(ARG_RD, cpu.getRegister<u64>(ARG_RT) << ARG_SA); }
void MipsiOp::srl (u32 word) { cpu.setRegister<s32>(ARG_RD, cpu.getRegister<u32>(ARG_RT) >> ARG_SA); }
void MipsiOp::dsrl(u32 word) { cpu.setRegister<u64>(ARG_RD, cpu.getRegister<u64>(ARG_RT) >> ARG_SA); }
void MipsiOp::sra (u32 word) { cpu.setRegister<s32>(ARG_RD, cpu.getRegister<s32>(ARG_RT) >> ARG_SA); }
void MipsiOp::dsra (u32 word){ cpu.setRegister<s64>(ARG_RD, cpu.getRegister<s64>(ARG_RT) >> ARG_SA); }
void MipsiOp::sllv(u32 word) { cpu.setRegister<s32>(ARG_RD, cpu.getRegister<u32>(ARG_RT) << (cpu.getRegister<u32>(ARG_RS) & 0b11111)); }
void MipsiOp::dsllv(u32 word){ cpu.setRegister<s64>(ARG_RD, cpu.getRegister<u64>(ARG_RT) << (cpu.getRegister<u64>(ARG_RS) & 0b111111)); }
void MipsiOp::srlv(u32 word) { cpu.setRegister<s32>(ARG_RD, cpu.getRegister<u32>(ARG_RT) >> (cpu.getRegister<u32>(ARG_RS) & 0b11111)); }
void MipsiOp::dsrlv(u32 word){ cpu.setRegister<s64>(ARG_RD, cpu.getRegister<u64>(ARG_RT) >> (cpu.getRegister<u64>(ARG_RS) & 0b111111)); }
void MipsiOp::srav(u32 word) { cpu.setRegister<s32>(ARG_RD, cpu.getRegister<s32>(ARG_RT) >> (cpu.getRegister<u32>(ARG_RS) & 0b11111)); }
void MipsiOp::dsrav(u32 word){ cpu.setRegister<s64>(ARG_RD, cpu.getRegister<s64>(ARG_RT) >> (cpu.getRegister<u64>(ARG_RS) & 0b111111)); }
