/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "fpuStatusReg.h"
#include "instructions.h"
#include "./opcodeArgs.h"
#include "../components/cpuRegisters.h"
#include "../utils/logging.h"

#define COMPARE_REG(operator) (cpu.getRegister<s32>(ARG_RS) operator cpu.getRegister<s32>(ARG_RT))
#define COMPARE_REG_ZERO(operator) (cpu.getRegister<s32>(ARG_RS) operator 0)

inline void branchUnlikely(u32 targetPC, bool32 compareResult) 
{
  CPU_LOG_ALIGN_ERROR(cpu, targetPC, 0b11, "branchUnlikely: unaligned target PC")    
  cpu.stepDeplay(compareResult ? targetPC : (cpu.getPc()+4));
}

inline void branchLikely(u32 targetPC, bool32 compareResult) 
{
  CPU_LOG_ALIGN_ERROR(cpu, targetPC, 0b11, "branchLikely: unaligned target PC")

  if(compareResult) {
    cpu.stepDeplay(targetPC);
  } else {
    // if it's a "likely" branch, ignore the delay-slot if the branch is not taken
    cpu.advancePc();
  }
}

void MipsiOp::beq (u32 word) { branchUnlikely(ARG_BRANCH_OFFSET, COMPARE_REG(==)); }
void MipsiOp::beql(u32 word) { branchLikely  (ARG_BRANCH_OFFSET, COMPARE_REG(==)); }
void MipsiOp::bne (u32 word) { branchUnlikely(ARG_BRANCH_OFFSET, COMPARE_REG(!=)); }
void MipsiOp::bnel(u32 word) { branchLikely  (ARG_BRANCH_OFFSET, COMPARE_REG(!=)); }

void MipsiOp::beqz (u32 word) { branchUnlikely(ARG_BRANCH_OFFSET, COMPARE_REG_ZERO(==)); }
void MipsiOp::bnez (u32 word) { branchUnlikely(ARG_BRANCH_OFFSET, COMPARE_REG_ZERO(!=)); }
void MipsiOp::bltz (u32 word) { branchUnlikely(ARG_BRANCH_OFFSET, COMPARE_REG_ZERO(<));  }
void MipsiOp::bltzl(u32 word) { branchLikely  (ARG_BRANCH_OFFSET, COMPARE_REG_ZERO(<));  }
void MipsiOp::blez (u32 word) { branchUnlikely(ARG_BRANCH_OFFSET, COMPARE_REG_ZERO(<=)); }
void MipsiOp::blezl(u32 word) { branchLikely  (ARG_BRANCH_OFFSET, COMPARE_REG_ZERO(<=)); }
void MipsiOp::bgtz (u32 word) { branchUnlikely(ARG_BRANCH_OFFSET, COMPARE_REG_ZERO(>));  }
void MipsiOp::bgtzl(u32 word) { branchLikely  (ARG_BRANCH_OFFSET, COMPARE_REG_ZERO(>));  }
void MipsiOp::bgez (u32 word) { branchUnlikely(ARG_BRANCH_OFFSET, COMPARE_REG_ZERO(>=)); }
void MipsiOp::bgezl(u32 word) { branchLikely  (ARG_BRANCH_OFFSET, COMPARE_REG_ZERO(>=)); }

// same as beqz, but saved the return address
void MipsiOp::bgezal(u32 word) 
{ 
  cpu.setRegister<u32>((s32)CpuReg::ra, cpu.getPc() + sizeof(u32));
  debugger_hook_stack_push(cpu.getRegister<u32>((s32)CpuReg::ra));

  bool32 compareResult = COMPARE_REG_ZERO(>=);
  u32 newPc = ARG_BRANCH_OFFSET;

  branchUnlikely(newPc, compareResult); 

  // if we jump to our current postion, and the delay slot is a NOP,
  // we can never escape the loop -> stop execution (in this thread).
  if(newPc == cpu.getPc() - 4 && memBus.read<u32>(cpu.getPc()) == 0) {
    print_error("Infinite loop detected!");
    cpu.logState();
    cpu.stop();
    return;
  }
}

// FPU Branches
void MipsiOp::bc1t (u32 word) { branchUnlikely(ARG_BRANCH_OFFSET,  cpu.fpu.getStatusFlag(FPUStatusReg::FCC_COMPARE)); }
void MipsiOp::bc1tl(u32 word) { branchLikely  (ARG_BRANCH_OFFSET,  cpu.fpu.getStatusFlag(FPUStatusReg::FCC_COMPARE)); }
void MipsiOp::bc1f (u32 word) { branchUnlikely(ARG_BRANCH_OFFSET, !cpu.fpu.getStatusFlag(FPUStatusReg::FCC_COMPARE)); }
void MipsiOp::bc1fl(u32 word) { branchLikely  (ARG_BRANCH_OFFSET, !cpu.fpu.getStatusFlag(FPUStatusReg::FCC_COMPARE)); }

// Pseudo instructions
void MipsiOp::b(u32 word) 
{ 
  u32 targetPC = (ARG_BRANCH_OFFSET);
  CPU_LOG_ALIGN_ERROR(cpu, targetPC, 0b11, "branchUnlikely: unaligned target PC")

  cpu.stepDeplay(targetPC);
}