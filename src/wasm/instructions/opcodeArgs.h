/**
 * @copyright 2020 - Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" file in the root directory
 */

// Macros to extract parameters from an instruction word
#define ARG_OP ((word >> 26) & 0b111111)
#define ARG_RS ((word >> 21) & 0b11111)
#define ARG_RT ((word >> 16) & 0b11111)
#define ARG_RD ((word >> 11) & 0b11111)
#define ARG_SA ((word >>  6) & 0b11111)
#define ARG_IMM (word & 0xFFFF)
//#define ARG_IMM_EXT ((s32)((word & 0xFFFF) << 16) >> 16)
#define ARG_IMM_EXT ((s32)((s16)(word)))
#define ARG_ADDRESS ((word & 0x003FFFFF) << 2) // 26-bits
#define ARG_FUNC (word & 0b111111)

// This argument is used only by branch instructions.
// Shift 2 to the left, sign extend, abd convert to an absolute address
// Note: a this point the PC it already in the delay-slot
#define ARG_BRANCH_OFFSET ( \
  (s16)((word & 0xFFFF) << 2)  + cpu.getPc() \
)

// Alias names
#define ARG_FS ARG_RD
#define ARG_FT ARG_RT
#define ARG_FD ARG_SA
#define ARG_BASE ARG_RS
#define ARG_OFFSET ARG_IMM 
#define ARG_OFFSET_EXT ARG_IMM_EXT 

/*
// All Possible combinations
[op.address]];
[op.rsOrBase, op.rt, s16Extend(op.immOrOffset << 2) + op.ramOffset + WORD_SIZE]];
[op.rsOrBase,        s16Extend(op.immOrOffset << 2) + op.ramOffset + WORD_SIZE]];
[op.rt, op.rsOrBase, (op.immOrOffset << 16) >> 16]];
[op.rt, op.rsOrBase, op.immOrOffset]];
[op.rt, op.rsOrBase, s16Extend(op.immOrOffset)]];
[op.rt, (op.immOrOffset << 16) >> 16, op.rsOrBase]];
[op.rd, op.rt, op.sa]];
[op.rd, op.rt, op.rsOrBase]];
[op.rsOrBase]];
[op.rd, op.rsOrBase]];
[op.rd]];
[op.rsOrBase, op.rt]];
[op.rd, op.rsOrBase, op.rt]];
[op.rt, op.rd]];
[s16Extend(op.immOrOffset << 2) + op.ramOffset + WORD_SIZE]]
[op.sa, op.rd, op.rt];
[op.sa, op.rd]
[op.rd, op.rt]
*/