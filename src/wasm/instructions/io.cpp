/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "instructions.h"
#include "./opcodeArgs.h"
#include "../components/cpuRegisters.h"
#include "cache.h"

void MipsiOp::lw(u32 word)
{
	u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
  CPU_LOG_ALIGN_ERROR(cpu, loc, 0b11, "Load unaligned WORD")
	cpu.setRegister<s32>(ARG_RT, (s32)memBus.read<u32>(loc));
}

void MipsiOp::lwu(u32 word)
{
  u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
  CPU_LOG_ALIGN_ERROR(cpu, loc, 0b11, "Load unaligned WORD")
	cpu.setRegister<u32>(ARG_RT, memBus.read<u32>(loc));
}

void MipsiOp::lh(u32 word) {
	u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
	CPU_LOG_ALIGN_ERROR(cpu, loc, 0b1, "Load unaligned HALF")
	cpu.setRegister<s32>(ARG_RT, (s16)memBus.read<u16>(loc));
};

void MipsiOp::ld(u32 word)
{
  u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
	CPU_LOG_ALIGN_ERROR(cpu, loc, 0b111, "Load unaligned DWORD")
	cpu.setRegister<u64>(ARG_RT, memBus.read<u64>(loc));
}

void MipsiOp::lwl(u32 word) {
	u32 offset = ARG_OFFSET_EXT;
	u32 loc = (offset + cpu.getRegister<s32>(ARG_BASE));

	s32 bytesToRead = sizeof(u32) - offset % sizeof(u32);

	for(s32 i=0; i<bytesToRead; ++i) {
		cpu.setRegisterByte32(ARG_RT, i, memBus.read<u8>(loc + i));
	}
	cpu.signExtendS32(ARG_RT);
};

void MipsiOp::lwr(u32 word) {
	u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
	s32 bytesToRead = (ARG_OFFSET % sizeof(u32)) + 1;

	for(s32 i=0; i<bytesToRead; ++i) {
		cpu.setRegisterByte32(ARG_RT, (3-i), memBus.read<u8>(loc--));
	}
	cpu.signExtendS32(ARG_RT);
};

void MipsiOp::swl(u32 word) {
	u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
	s32 bytesToWrite = sizeof(u32) - (ARG_OFFSET % sizeof(u32));

	for(s32 i=0; i<bytesToWrite; ++i)
      memBus.write(loc++, cpu.getRegisterByte32(ARG_RT, i));
};

void MipsiOp::swr(u32 word) {
	u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
	s32 bytesToWrite = (ARG_OFFSET % sizeof(u32)) + 1;

	for(s32 i=0; i<bytesToWrite; ++i)
    memBus.write(loc--, cpu.getRegisterByte32(ARG_RT, 3-i));
};

void MipsiOp::ldl(u32 word) { 
	u32 offset = ARG_OFFSET_EXT;
	u32 loc = (offset + cpu.getRegister<s32>(ARG_BASE));

	s32 bytesToRead = sizeof(u64) - offset % sizeof(u64);

	for(s32 i=0; i<bytesToRead; ++i) {
		cpu.setRegisterByte64(ARG_RT, i, memBus.read<u8>(loc + i));
	}
};

void MipsiOp::ldr(u32 word) {
	u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
	s32 bytesToRead = (ARG_OFFSET % sizeof(u64)) + 1;

	for(s32 i=0; i<bytesToRead; ++i)
		cpu.setRegisterByte64(ARG_RT, (7-i), memBus.read<u8>(loc--));
};

void MipsiOp::sdl(u32 word) {
	u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
	s32 bytesToWrite = sizeof(u64) - (ARG_OFFSET % sizeof(u64));

	for(s32 i=0; i<bytesToWrite; ++i)
      memBus.write(loc++, cpu.getRegisterByte64(ARG_RT, i));
};

void MipsiOp::sdr(u32 word) {
	u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
	s32 bytesToWrite = (ARG_OFFSET % sizeof(u64)) + 1;

	for(s32 i=0; i<bytesToWrite; ++i)
    memBus.write(loc--, cpu.getRegisterByte64(ARG_RT, 7-i));
};

void MipsiOp::lhu(u32 word) {
	u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
	if (loc & 1) {
    CPU_LOG_ERROR(cpu, "load unaligned");
	}
	cpu.setRegister<u32>(ARG_RT, memBus.read<u16>(loc));
};

void MipsiOp::lb(u32 word) {
	u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
	cpu.setRegister<s32>(ARG_RT, (s8)memBus.read<u8>(loc));
};

void MipsiOp::lbu(u32 word) {
	u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
	cpu.setRegister<u32>(ARG_RT, memBus.read<u8>(loc));
};

void MipsiOp::sd(u32 word) {
	u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
	if (loc & 0b111) {
    CPU_LOG_ERROR(cpu, "write unaligned")
	}

  memBus.write(loc, cpu.getRegister<u64>(ARG_RT));
};

void MipsiOp::sw(u32 word) {
	u32 address = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
	if (address & 0b11) {
    CPU_LOG_ERROR(cpu, "write unaligned")
	}

  Cache::clearCache(address);
  memBus.write(address, cpu.getRegister<u32>(ARG_RT));
};

void MipsiOp::sh(u32 word) {
	u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
	if (loc & 1) {
    CPU_LOG_ERROR(cpu, "write unaligned")
	}
  
	u16 valueU16 = cpu.getRegister<u32>(ARG_RT) & 0xFFFF;
  memBus.write(loc, valueU16);
};

void MipsiOp::sb(u32 word) {
  memBus.write(
    (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE)),
    (u8)(cpu.getRegister<u32>(ARG_RT) & 0xFF)
  );
}

void MipsiOp::lui(u32 word) { 
  cpu.setRegister<s32>(ARG_RT, ARG_IMM << 16);
}

void MipsiOp::li_half(u32 word) { 
  cpu.setRegister<u32>(ARG_RT, ARG_IMM);
}

void MipsiOp::ll(u32 word) 
{
	// @TODO: do i need to check writes after this?

	u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
  CPU_LOG_ALIGN_ERROR(cpu, loc, 0b11, "Load unaligned WORD")
	cpu.setRegister<s32>(ARG_RT, (s32)memBus.read<u32>(loc));

	loc = memBus.normalizeAddress(loc);
	cpu.llActive = true;
	cpu.cop0.setRegisterU32(Cop0Register::LL_ADDRESS, loc >> 4);
}

void MipsiOp::lld(u32 word) {
	// @TODO: do i need to check writes after this?

	u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
  CPU_LOG_ALIGN_ERROR(cpu, loc, 0b111, "Load unaligned WORD")
	cpu.setRegister<u64>(ARG_RT, memBus.read<u64>(loc));

	loc = memBus.normalizeAddress(loc);
	cpu.llActive = true;
	cpu.cop0.setRegisterU32(Cop0Register::LL_ADDRESS, loc >> 4);
}

void MipsiOp::sc(u32 word) {
	u32 address = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
	if (address & 0b11) {
    CPU_LOG_ERROR(cpu, "write unaligned")
	}

	if(cpu.llActive) {
  	memBus.write(address, cpu.getRegister<u32>(ARG_RT));
	}

	cpu.setRegister<u32>(ARG_RT, (u32)cpu.llActive);
}

void MipsiOp::scd(u32 word) {
	u32 address = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
	if (address & 0b11) {
    CPU_LOG_ERROR(cpu, "write unaligned")
	}

	if(cpu.llActive) {
  	memBus.write(address, cpu.getRegister<u64>(ARG_RT));
	}

	cpu.setRegister<u64>(ARG_RT, (u64)cpu.llActive);
}
