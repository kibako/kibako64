/**
 * @copyright 2020 - Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" file in the root directory
 */

#include "instructions.h"

namespace MipsiOp
{
  #ifdef FEATURE_OPCODE_NAME_TABLE

    const char* opcodeNames[0xFF];

    void fillOpcodeNameTable() 
    {
      #define OP_FUNCTION(opName) opcodeNames[(s32)MipsiOp::opName] = #opName;
      #include "implementedOps.h"
      #undef OP_FUNCTION
    }

    const char* getOpcodeName(s32 opcodeFunction)
    {
      return opcodeNames[opcodeFunction];
    }

    u32 getHighestPointer(u32 dummy)
    {
      u32 highestPtr = 0;

      // Why "dummy != 42"?
      // The compiler tries to be too-smart here and hangs for ages, 
      // i guess it's trying to optimize stuff at compile time, but i don't care since it's only used in unit-tests
      #define OP_FUNCTION(name) \
        if(dummy != 42 && (u32)MipsiOp::name > highestPtr) { \
          highestPtr = (u32)MipsiOp::name;  \
        } \

      #include "implementedOps.h"
      #undef OP_FUNCTION

      return highestPtr;
    }

  #else
    void fillOpcodeNameTable() {}
    u32 getHighestPointer(u32 dummy) { return 0; }
    const char* getOpcodeName(s32 opcodeFunction) { return nullptr; }
  #endif
}