/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "fpuStatusReg.h"
#include "stdlib/stdlib.h"
#include "instructions.h"
#include "./opcodeArgs.h"

namespace
{
  namespace MOP = MipsiOp;

  /**
   * Generic compare function using compile-time IFs.
   * The template params mimic the table in the MIPS specification.
   */
  template<typename F, bool flagGr, bool flagLess, bool flagEq, bool trueOnNaN, bool raiseOnNaN>
  void compare(s32 argFs, s32 argFt)
  {
    F a = cpu.fpu.getRegister<F>(argFs);
    F b = cpu.fpu.getRegister<F>(argFt);

    bool res = false;
    if constexpr (flagGr)   { res = (a > b); }
    if constexpr (flagLess) { res = (res || (a < b)); }
    if constexpr (flagEq)   { res = (res || (a == b)); }

    if constexpr (raiseOnNaN || trueOnNaN) {
      if (isNaN(a) || isNaN(b)) {
        if constexpr (raiseOnNaN) {
          cpu.fpu.setStatusFlag(FPUStatusReg::CAUSE_INVAL_OP, true);
          cpu.fpu.setStatusFlag(FPUStatusReg::FLAG_INVAL_OP, true);
        }

        if constexpr (trueOnNaN) {
          res = true;
        }
      }
    }

    cpu.fpu.setCompare(res);
  }
}

// compare flags from 1. spec-table:       >  <  =  ?   No-Exception on NaN
void MOP::c_f_s(u32 word)   { compare<f32, 0, 0, 0, 0,  0>(ARG_FS, ARG_FT); }
void MOP::c_f_d(u32 word)   { compare<f64, 0, 0, 0, 0,  0>(ARG_FS, ARG_FT); }
void MOP::c_un_s(u32 word)  { compare<f32, 0, 0, 0, 1,  0>(ARG_FS, ARG_FT); }
void MOP::c_un_d(u32 word)  { compare<f64, 0, 0, 0, 1,  0>(ARG_FS, ARG_FT); }
void MOP::c_eq_s(u32 word)  { compare<f32, 0, 0, 1, 0,  0>(ARG_FS, ARG_FT); }
void MOP::c_eq_d(u32 word)  { compare<f64, 0, 0, 1, 0,  0>(ARG_FS, ARG_FT); }
void MOP::c_ueq_s(u32 word) { compare<f32, 0, 0, 1, 1,  0>(ARG_FS, ARG_FT); }
void MOP::c_ueq_d(u32 word) { compare<f64, 0, 0, 1, 1,  0>(ARG_FS, ARG_FT); }
void MOP::c_olt_s(u32 word) { compare<f32, 0, 1, 0, 0,  0>(ARG_FS, ARG_FT); }
void MOP::c_olt_d(u32 word) { compare<f64, 0, 1, 0, 0,  0>(ARG_FS, ARG_FT); }
void MOP::c_ult_s(u32 word) { compare<f32, 0, 1, 0, 1,  0>(ARG_FS, ARG_FT); }
void MOP::c_ult_d(u32 word) { compare<f64, 0, 1, 0, 1,  0>(ARG_FS, ARG_FT); }
void MOP::c_ole_s(u32 word) { compare<f32, 0, 1, 1, 0,  0>(ARG_FS, ARG_FT); }
void MOP::c_ole_d(u32 word) { compare<f64, 0, 1, 1, 0,  0>(ARG_FS, ARG_FT); }
void MOP::c_ule_s(u32 word) { compare<f32, 0, 1, 1, 1,  0>(ARG_FS, ARG_FT); }
void MOP::c_ule_d(u32 word) { compare<f64, 0, 1, 1, 1,  0>(ARG_FS, ARG_FT); }

// compare flags from 2. spec-table:       >  <  =  ?   Exception on NaN
void MOP::c_sf_s(u32 word)  { compare<f32, 0, 0, 0, 0,  1>(ARG_FS, ARG_FT); }
void MOP::c_sf_d(u32 word)  { compare<f64, 0, 0, 0, 0,  1>(ARG_FS, ARG_FT); }
void MOP::c_ngle_s(u32 word){ compare<f32, 0, 0, 0, 1,  1>(ARG_FS, ARG_FT); }
void MOP::c_ngle_d(u32 word){ compare<f64, 0, 0, 0, 1,  1>(ARG_FS, ARG_FT); }
void MOP::c_seq_s(u32 word) { compare<f32, 0, 0, 1, 0,  1>(ARG_FS, ARG_FT); }
void MOP::c_seq_d(u32 word) { compare<f64, 0, 0, 1, 0,  1>(ARG_FS, ARG_FT); }
void MOP::c_ngl_s(u32 word) { compare<f32, 0, 0, 1, 1,  1>(ARG_FS, ARG_FT); }
void MOP::c_ngl_d(u32 word) { compare<f64, 0, 0, 1, 1,  1>(ARG_FS, ARG_FT); }
void MOP::c_lt_s(u32 word)  { compare<f32, 0, 1, 0, 0,  1>(ARG_FS, ARG_FT); }
void MOP::c_lt_d(u32 word)  { compare<f64, 0, 1, 0, 0,  1>(ARG_FS, ARG_FT); }
void MOP::c_nge_s(u32 word) { compare<f32, 0, 1, 0, 1,  1>(ARG_FS, ARG_FT); }
void MOP::c_nge_d(u32 word) { compare<f64, 0, 1, 0, 1,  1>(ARG_FS, ARG_FT); }
void MOP::c_le_s(u32 word)  { compare<f32, 0, 1, 1, 0,  1>(ARG_FS, ARG_FT); }
void MOP::c_le_d(u32 word)  { compare<f64, 0, 1, 1, 0,  1>(ARG_FS, ARG_FT); }
void MOP::c_ngt_s(u32 word) { compare<f32, 0, 1, 1, 1,  1>(ARG_FS, ARG_FT); }
void MOP::c_ngt_d(u32 word) { compare<f64, 0, 1, 1, 1,  1>(ARG_FS, ARG_FT); }
