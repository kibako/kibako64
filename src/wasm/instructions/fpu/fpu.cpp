/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "stdlib/stdlib.h"
#include "instructions.h"
#include "./opcodeArgs.h"

void MipsiOp::mtc1(u32 word) {
  cpu.fpu.setRegister(ARG_FS, cpu.getRegister<u32>(ARG_RT));
}

void MipsiOp::dmtc1(u32 word) {
  cpu.fpu.setRegister(ARG_FS, cpu.getRegister<s64>(ARG_RT));
}

void MipsiOp::mfc1(u32 word) {
  cpu.setRegister<s32>(ARG_RT, cpu.fpu.getRegister<s32>(ARG_FS));
}

void MipsiOp::dmfc1(u32 word) {
  cpu.setRegister<s64>(ARG_RT, cpu.fpu.getRegister<s64>(ARG_FS));
}

void MipsiOp::swc1(u32 word) {
    u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
    CPU_LOG_ALIGN_ERROR(cpu, loc, 0b11, "storing non aligned float")
    memBus.write(loc, cpu.fpu.getRegister<u32>(ARG_RT));
};

void MipsiOp::sdc1(u32 word) {
    u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
    CPU_LOG_ALIGN_ERROR(cpu, loc, 0b111, "storing non aligned double")
    memBus.write(loc, cpu.fpu.getRegister<f64>(ARG_RT));
};

void MipsiOp::lwc1(u32 word) {
    u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
    CPU_LOG_ALIGN_ERROR(cpu, loc, 0b11, "loading non aligned float")
    cpu.fpu.setRegister(ARG_FT, memBus.read<f32>(loc));
};

void MipsiOp::ldc1(u32 word) {
    u32 loc = (ARG_OFFSET_EXT + cpu.getRegister<s32>(ARG_BASE));
    CPU_LOG_ALIGN_ERROR(cpu, loc, 0b111, "loading non aligned double")
    cpu.fpu.setRegister(ARG_FT, memBus.read<f64>(loc));
};

void MipsiOp::ctc1(u32 word) { cpu.fpu.setControlRegister(ARG_FS, cpu.getRegister<u32>(ARG_RT)); }
void MipsiOp::cfc1(u32 word) { cpu.setRegister<s32>(ARG_RT, (s32)cpu.fpu.getControlRegister(ARG_FS)); }

void MipsiOp::mov_s(u32 word) {cpu.fpu.setRegister(ARG_FD, cpu.fpu.getRegister<f32>(ARG_FS)); }
void MipsiOp::mov_d(u32 word) {cpu.fpu.setRegister(ARG_FD, cpu.fpu.getRegister<f64>(ARG_FS)); }

void MipsiOp::cvt_s_w(u32 word) {cpu.fpu.convertRegister<f32, s32>(ARG_FD, ARG_FS); }
void MipsiOp::cvt_s_d(u32 word) {cpu.fpu.convertRegister<f32, f64>(ARG_FD, ARG_FS); }
void MipsiOp::cvt_s_l(u32 word) {cpu.fpu.convertRegister<f32, s64>(ARG_FD, ARG_FS); }
void MipsiOp::cvt_d_s(u32 word) {cpu.fpu.convertRegister<f64, f32>(ARG_FD, ARG_FS); }
void MipsiOp::cvt_d_w(u32 word) {cpu.fpu.convertRegister<f64, s32>(ARG_FD, ARG_FS); }
void MipsiOp::cvt_d_l(u32 word) {cpu.fpu.convertRegister<f64, s64>(ARG_FD, ARG_FS); }

void MipsiOp::cvt_l_s(u32 word) {cpu.fpu.convertRegister<s64, f32>(ARG_FD, ARG_FS); }
void MipsiOp::cvt_l_d(u32 word) {cpu.fpu.convertRegister<s64, f64>(ARG_FD, ARG_FS); }
void MipsiOp::cvt_w_s(u32 word) {cpu.fpu.convertRegister<s32, f32>(ARG_FD, ARG_FS); }
void MipsiOp::cvt_w_d(u32 word) {cpu.fpu.convertRegister<s32, f64>(ARG_FD, ARG_FS); }

void MipsiOp::trunc_w_s(u32 word) {cpu.fpu.setRegister<s32>(ARG_FD,  truncToS32(cpu.fpu.getRegister<f32>(ARG_FS))); }
void MipsiOp::trunc_w_d(u32 word) {cpu.fpu.setRegister<s32>(ARG_FD,  truncToS32(cpu.fpu.getRegister<f64>(ARG_FS))); }
void MipsiOp::trunc_l_s(u32 word) {cpu.fpu.setRegister<s64>(ARG_FD,  truncToS64(cpu.fpu.getRegister<f32>(ARG_FS))); }
void MipsiOp::trunc_l_d(u32 word) {cpu.fpu.setRegister<s64>(ARG_FD,  truncToS64(cpu.fpu.getRegister<f64>(ARG_FS))); }

void MipsiOp::abs_s    (u32 word) {cpu.fpu.setRegister<f32>(ARG_FD,   absf(cpu.fpu.getRegister<f32>(ARG_FS))); }
void MipsiOp::abs_d    (u32 word) {cpu.fpu.setRegister<f64>(ARG_FD,    abs(cpu.fpu.getRegister<f64>(ARG_FS))); }
void MipsiOp::sqrt_s   (u32 word) {cpu.fpu.setRegister<f32>(ARG_FD,  sqrtf(cpu.fpu.getRegister<f32>(ARG_FS))); }
void MipsiOp::sqrt_d   (u32 word) {cpu.fpu.setRegister<f64>(ARG_FD,   sqrt(cpu.fpu.getRegister<f64>(ARG_FS))); }
void MipsiOp::neg_s    (u32 word) {cpu.fpu.setRegister<f32>(ARG_FD,       -cpu.fpu.getRegister<f32>(ARG_FS));  }
void MipsiOp::neg_d    (u32 word) {cpu.fpu.setRegister<f64>(ARG_FD,       -cpu.fpu.getRegister<f64>(ARG_FS));  }

void MipsiOp::round_l_s (u32 word) {cpu.fpu.setRegister<s64>(ARG_FD, roundf(cpu.fpu.getRegister<f32>(ARG_FS))); }
void MipsiOp::round_l_d (u32 word) {cpu.fpu.setRegister<s64>(ARG_FD,  round(cpu.fpu.getRegister<f64>(ARG_FS))); }

void MipsiOp::round_w_s (u32 word) {cpu.fpu.setRegister<s32>(ARG_FD, roundf(cpu.fpu.getRegister<f32>(ARG_FS))); }
void MipsiOp::round_w_d (u32 word) {cpu.fpu.setRegister<s32>(ARG_FD,  round(cpu.fpu.getRegister<f64>(ARG_FS))); }

void MipsiOp::ceil_l_s(u32 word) {cpu.fpu.setRegister<s64>(ARG_FD, (s64)ceilf(cpu.fpu.getRegister<f32>(ARG_FS))); }
void MipsiOp::ceil_l_d(u32 word) {cpu.fpu.setRegister<s64>(ARG_FD, (s64) ceil(cpu.fpu.getRegister<f64>(ARG_FS))); }
void MipsiOp::ceil_w_s(u32 word) {cpu.fpu.setRegister<s32>(ARG_FD, (s32)ceilf(cpu.fpu.getRegister<f32>(ARG_FS))); }
void MipsiOp::ceil_w_d(u32 word) {cpu.fpu.setRegister<s32>(ARG_FD, (s32) ceil(cpu.fpu.getRegister<f64>(ARG_FS))); }

void MipsiOp::floor_l_s(u32 word) {cpu.fpu.setRegister<s64>(ARG_FD, (s64)floorf(cpu.fpu.getRegister<f32>(ARG_FS))); }
void MipsiOp::floor_l_d(u32 word) {cpu.fpu.setRegister<s64>(ARG_FD, (s64) floor(cpu.fpu.getRegister<f64>(ARG_FS))); }
void MipsiOp::floor_w_s(u32 word) {cpu.fpu.setRegister<s32>(ARG_FD, (s32)floorf(cpu.fpu.getRegister<f32>(ARG_FS))); }
void MipsiOp::floor_w_d(u32 word) {cpu.fpu.setRegister<s32>(ARG_FD, (s32) floor(cpu.fpu.getRegister<f64>(ARG_FS))); }

void MipsiOp::add_s(u32 word) { cpu.fpu.setRegister(ARG_FD, cpu.fpu.opAddRegister<f32>(ARG_FS, ARG_FT)); }
void MipsiOp::add_d(u32 word) { cpu.fpu.setRegister(ARG_FD, cpu.fpu.opAddRegister<f64>(ARG_FS, ARG_FT)); }

void MipsiOp::sub_s(u32 word) { cpu.fpu.setRegister(ARG_FD, cpu.fpu.opSubtractRegister<f32>(ARG_FS, ARG_FT)); }
void MipsiOp::sub_d(u32 word) { cpu.fpu.setRegister(ARG_FD, cpu.fpu.opSubtractRegister<f64>(ARG_FS, ARG_FT)); }

void MipsiOp::mul_s(u32 word) { cpu.fpu.setRegister(ARG_FD, cpu.fpu.opMultiplyRegister<f32>(ARG_FS, ARG_FT)); }
void MipsiOp::mul_d(u32 word) { cpu.fpu.setRegister(ARG_FD, cpu.fpu.opMultiplyRegister<f64>(ARG_FS, ARG_FT)); }

void MipsiOp::div_s(u32 word) { cpu.fpu.setRegister(ARG_FD, cpu.fpu.opDivideRegister<f32>(ARG_FS, ARG_FT)); }
void MipsiOp::div_d(u32 word) { cpu.fpu.setRegister(ARG_FD, cpu.fpu.opDivideRegister<f64>(ARG_FS, ARG_FT)); }
