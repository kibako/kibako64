/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "instructions.h"
#include "./opcodeArgs.h"
#include "../components/cpuRegisters.h"

void MipsiOp::eret(u32 word)
{
  u32 returnAddress;
  u32 isError = cpu.cop0.getStatusFlag(Cop0StatusFlag::ERROR_LEVEL);
  u32 isException = cpu.cop0.getStatusFlag(Cop0StatusFlag::EXCEPTION_LEVEL);

  if(!isException) {
    print_warn("try to ERET while not being in an exception");
  }


  if(isError) {
    cpu.cop0.disableStatusFlag(Cop0StatusFlag::ERROR_LEVEL);
    returnAddress = cpu.cop0.getRegisterU32(Cop0Register::ERROR_EPC);
  } else {
    cpu.cop0.disableStatusFlag(Cop0StatusFlag::EXCEPTION_LEVEL);
    returnAddress = cpu.cop0.getRegisterU32(Cop0Register::EXCEPTION_PC);
  }

  printf_trace("eret: returning from Interrupt [%s] to 0x%08X", isError ? "Error" : "Exception", returnAddress);

  cpu.llActive = false; // @TODO: check if actually true
  cpu.setPc(returnAddress);

  debugger_hook_event(DebuggerEvent::ERET);
}

void MipsiOp::teq(u32 word)
{
  if(cpu.getRegister<u32>(ARG_RS) == cpu.getRegister<u32>(ARG_RT)) {
    print_error("TEQ: trap not implemented");
  }
}