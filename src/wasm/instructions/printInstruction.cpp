/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "instructions.h"
#include "opcodeArgs.h"

#define BASE_STR "[%d][0x%08X]: %08X | "
#define BASE_ARG cpu.getCommandCount(), cpu.getPc(), word

#define R_(reg) MIPS_REG_NAMES[reg]

const char MIPS_REG_NAMES[][6] = {
    "$zero",
    "$at",
    "$v0",
    "$v1",
    "$a0",
    "$a1",
    "$a2",
    "$a3",

    "$t0",
    "$t1",
    "$t2",
    "$t3",
    "$t4",
    "$t5",
    "$t6",
    "$t7",
    
    "$s0",
    "$s1",
    "$s2",
    "$s3",
    "$s4",
    "$s5",
    "$s6",
    "$s7",

    "$t8",
    "$t9",
    "$k0",
    "$k1",
    "$gp",
    "$sp",
    "$fp",
    "$ra"
};

void MipsiOp::printInstruction(u32 op, u32 word)
{
  #if defined FEATURE_OPCODE_NAME_TABLE && defined FEATURE_DEBUG_LOG

    /**
     * This table was partially auto-generated.
     * It's by design not very performant, but rather easy to edit,
     * since it's only used in the debug-build.
     */

    #undef TRACE_STR
    #define TRACE_STR "[instruction] "

    if(op == (u32)MipsiOp::nop){        printf_trace(BASE_STR "  | nop       ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::x_log){      printf_trace(BASE_STR "  | x_log     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::x_inject){   printf_trace(BASE_STR "  | x_inject  ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::j){          printf_trace(BASE_STR "  | j         jump 0x%08X;", BASE_ARG, ARG_ADDRESS); return; }
    if(op == (u32)MipsiOp::jal){        printf_trace(BASE_STR "  | jal       jump 0x%08X;", BASE_ARG, ARG_ADDRESS); return; }
    if(op == (u32)MipsiOp::beq){        printf_trace(BASE_STR "  | beq       if(%s=%08X == %s=%08X)jump %08X;", BASE_ARG, R_(ARG_RS), cpu.getRegister<u32>(ARG_RS), R_(ARG_RT), cpu.getRegister<u32>(ARG_RT), ARG_BRANCH_OFFSET + 4); return; }
    if(op == (u32)MipsiOp::bne){        printf_trace(BASE_STR "  | bne       if(%s=%08X != %s=%08X)jump %08X;", BASE_ARG, R_(ARG_RS), cpu.getRegister<u32>(ARG_RS), R_(ARG_RT), cpu.getRegister<u32>(ARG_RT), ARG_BRANCH_OFFSET + 4); return; }
    if(op == (u32)MipsiOp::blez){       printf_trace(BASE_STR "  | blez      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::bgtz){       printf_trace(BASE_STR "  | bgtz      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::addiu){      printf_trace(BASE_STR "  | addiu     %s = %s=%08X + %08X; (res: %08X)", BASE_ARG, R_(ARG_RT), R_(ARG_RS), cpu.getRegister<u32>(ARG_RS), ARG_IMM_EXT, cpu.getRegister<u32>(ARG_RS) + ARG_IMM_EXT); return; }
    if(op == (u32)MipsiOp::andi){       printf_trace(BASE_STR "  | andi      %s = %s=%08X & %04X;", BASE_ARG, R_(ARG_RT), R_(ARG_RS), cpu.getRegister<u32>(ARG_RS), ARG_IMM); return; }
    if(op == (u32)MipsiOp::ori){        printf_trace(BASE_STR "  | ori       %s = %s=%08X | %04X;", BASE_ARG, R_(ARG_RT), R_(ARG_RS), cpu.getRegister<u32>(ARG_RS), ARG_IMM); return; }
    if(op == (u32)MipsiOp::xori){       printf_trace(BASE_STR "  | xori      %s = %s=%08X ^ %04X;", BASE_ARG, R_(ARG_RT), R_(ARG_RS), cpu.getRegister<u32>(ARG_RS), ARG_IMM); return; }
    if(op == (u32)MipsiOp::lui){        printf_trace(BASE_STR "  | lui       %s = 0x%08X;", BASE_ARG, R_(ARG_RT), ARG_IMM << 16); return; }
    if(op == (u32)MipsiOp::slti){       printf_trace(BASE_STR "  | slti      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::sltiu){      printf_trace(BASE_STR "  | sltiu     %s = %s=%08X < %04X", BASE_ARG, R_(ARG_RT), R_(ARG_RS), cpu.getRegister<u32>(ARG_RS), ARG_IMM_EXT); return; }
    if(op == (u32)MipsiOp::beql){       printf_trace(BASE_STR "  | beql      if(%s=%08X == %s=%08X)jump %08X;", BASE_ARG, R_(ARG_RS), cpu.getRegister<u32>(ARG_RS), R_(ARG_RT), cpu.getRegister<u32>(ARG_RT), ARG_BRANCH_OFFSET + 4); return; }
    if(op == (u32)MipsiOp::bnel){       printf_trace(BASE_STR "  | bnel      if(%s=%08X != %s=%08X)jump %08X;", BASE_ARG, R_(ARG_RS), cpu.getRegister<u32>(ARG_RS), R_(ARG_RT), cpu.getRegister<u32>(ARG_RT), ARG_BRANCH_OFFSET + 4); return; }
    if(op == (u32)MipsiOp::blezl){      printf_trace(BASE_STR "  | blezl     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::bgtzl){      printf_trace(BASE_STR "  | bgtzl     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::daddiu){     printf_trace(BASE_STR "  | daddiu    ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::daddi){      printf_trace(BASE_STR "  | daddi     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::daddu){      printf_trace(BASE_STR "  | daddu     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::lh){         printf_trace(BASE_STR "  | lh        %s = (s16)memory[%s=%08X + 0x%04X];", BASE_ARG, R_(ARG_RT), R_(ARG_BASE), cpu.getRegister<u32>(ARG_BASE), ARG_OFFSET_EXT); return; }
    if(op == (u32)MipsiOp::lwl){        printf_trace(BASE_STR "  | lwl       ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::lw){         printf_trace(BASE_STR "  | lw        %s = memory[%s=%08X + 0x%04X];", BASE_ARG, R_(ARG_RT), R_(ARG_BASE), cpu.getRegister<u32>(ARG_BASE), ARG_OFFSET_EXT); return; }
    if(op == (u32)MipsiOp::ll){         printf_trace(BASE_STR "  | ll        %s = memory[%s=%08X + 0x%04X];", BASE_ARG, R_(ARG_RT), R_(ARG_BASE), cpu.getRegister<u32>(ARG_BASE), ARG_OFFSET_EXT); return; }
    if(op == (u32)MipsiOp::lbu){        printf_trace(BASE_STR "  | lbu       %s = (u8)memory[%s=%08X + 0x%04X];", BASE_ARG, R_(ARG_RT), R_(ARG_BASE), cpu.getRegister<u32>(ARG_BASE), ARG_OFFSET_EXT); return; }
    if(op == (u32)MipsiOp::lhu){        printf_trace(BASE_STR "  | lhu       %s = (u16)memory[%s=%08X + 0x%04X];", BASE_ARG, R_(ARG_RT), R_(ARG_BASE), cpu.getRegister<u32>(ARG_BASE), ARG_OFFSET_EXT); return; }
    if(op == (u32)MipsiOp::lwr){        printf_trace(BASE_STR "  | lwr       ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::sb){         printf_trace(BASE_STR "  | sb        memory[%s=%08X + 0x%04X] = (u8)%s=%02X;", BASE_ARG, R_(ARG_BASE), cpu.getRegister<u32>(ARG_BASE), ARG_OFFSET_EXT, R_(ARG_RT), (u8)cpu.getRegister<u32>(ARG_RT)); return; }
    if(op == (u32)MipsiOp::sh){         printf_trace(BASE_STR "  | sh        ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::swl){        printf_trace(BASE_STR "  | swl       ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::sd){         printf_trace(BASE_STR "  | sd        memory[%s=%08X + 0x%04X] = (u64)%s=%016X;", BASE_ARG, R_(ARG_BASE), cpu.getRegister<u32>(ARG_BASE), ARG_OFFSET_EXT, R_(ARG_RT), cpu.getRegister<u64>(ARG_RT)); return; }
    if(op == (u32)MipsiOp::sw){         printf_trace(BASE_STR "  | sw        memory[%s=%08X + 0x%04X] = %s=%08X;", BASE_ARG, R_(ARG_BASE), cpu.getRegister<u32>(ARG_BASE), ARG_OFFSET_EXT, R_(ARG_RT), cpu.getRegister<u32>(ARG_RT)); return; }
    if(op == (u32)MipsiOp::sc){         printf_trace(BASE_STR "  | sc        %s=%08X, memory[%s=%08X + 0x%04X] = %s=%08X;", BASE_ARG, R_(ARG_RT), cpu.getRegister<u32>(ARG_RT), R_(ARG_BASE), cpu.getRegister<u32>(ARG_BASE), ARG_OFFSET_EXT, R_(ARG_RT), cpu.getRegister<u32>(ARG_RT)); return; }
    if(op == (u32)MipsiOp::swr){        printf_trace(BASE_STR "  | swr       ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::lwc1){       printf_trace(BASE_STR "F | lwc1      $f%d = memory[%s=%08X + 0x%04X]", BASE_ARG, ARG_FT, R_(ARG_BASE), cpu.getRegister<s32>(ARG_BASE), ARG_OFFSET_EXT); return; }
    if(op == (u32)MipsiOp::ld){         printf_trace(BASE_STR "  | ld        %s = (u64)memory[%s=%08X + 0x%04X];", BASE_ARG, R_(ARG_RT), R_(ARG_BASE), cpu.getRegister<u32>(ARG_BASE), ARG_OFFSET_EXT); return; }
    if(op == (u32)MipsiOp::swc1){       printf_trace(BASE_STR "F | swc1      memory[%s=%08X + 0x%04X] = %s=%f;", BASE_ARG, R_(ARG_BASE), cpu.getRegister<u32>(ARG_BASE), ARG_OFFSET_EXT, R_(ARG_RT), cpu.fpu.getRegister<u32>(ARG_RT)); return; }
    if(op == (u32)MipsiOp::sll){        printf_trace(BASE_STR "  | sll       ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::srl){        printf_trace(BASE_STR "  | srl       %s = %s=%08X >> %08X", BASE_ARG, R_(ARG_RD), R_(ARG_RT), cpu.getRegister<u32>(ARG_RT), ARG_SA); return; }
    if(op == (u32)MipsiOp::sra){        printf_trace(BASE_STR "  | sra       %s = ((s32)%s=%08X >> %08X);", BASE_ARG, R_(ARG_RD), R_(ARG_RT), cpu.getRegister<u32>(ARG_RT), ARG_SA); return; }
    if(op == (u32)MipsiOp::dsra){       printf_trace(BASE_STR "  | dsra      %s = ((s32)%s=%08X >> %08X);", BASE_ARG, R_(ARG_RD), R_(ARG_RT), cpu.getRegister<u32>(ARG_RT), ARG_SA); return; }
    if(op == (u32)MipsiOp::sllv){       printf_trace(BASE_STR "  | sllv      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::dsllv){      printf_trace(BASE_STR "  | dsllv     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::jr){         printf_trace(BASE_STR "  | jr        jump %s=%08X;", BASE_ARG, R_(ARG_RS), cpu.getRegister<u32>(ARG_RS)); return; }
    if(op == (u32)MipsiOp::jalr){       printf_trace(BASE_STR "  | jalr      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::breakpoint){ printf_trace(BASE_STR "  | breakpoin ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::sync){       printf_trace(BASE_STR "  | sync      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::mfhi){       printf_trace(BASE_STR "  | mfhi      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::mflo){       printf_trace(BASE_STR "  | mflo      LO = %s=%08X", BASE_ARG, R_(ARG_RS), cpu.getRegister<u32>(ARG_RS)); return; }
    if(op == (u32)MipsiOp::mflo){       printf_trace(BASE_STR "  | mfhi      HI = %s=%08X", BASE_ARG, R_(ARG_RS), cpu.getRegister<u32>(ARG_RS)); return; }
    if(op == (u32)MipsiOp::mtlo){       printf_trace(BASE_STR "  | mtlo      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::mult){       printf_trace(BASE_STR "  | mult      [LO,HI] = %s=%08X * %s=%08X;", BASE_ARG, R_(ARG_RS), cpu.getRegister<s32>(ARG_RS), R_(ARG_RT), cpu.getRegister<s32>(ARG_RT)); return; }
    if(op == (u32)MipsiOp::multu){      printf_trace(BASE_STR "  | multu     [LO,HI] = %s=%08X * %s=%08X;", BASE_ARG, R_(ARG_RS), cpu.getRegister<u32>(ARG_RS), R_(ARG_RT), cpu.getRegister<u32>(ARG_RT)); return; }
    if(op == (u32)MipsiOp::div){        printf_trace(BASE_STR "  | div       [LO,HI] = %s=%08X / %s=%08X;", BASE_ARG, R_(ARG_RS), cpu.getRegister<u32>(ARG_RS), R_(ARG_RT), cpu.getRegister<u32>(ARG_RT)); return; }
    if(op == (u32)MipsiOp::divu){       printf_trace(BASE_STR "  | divu      [LO,HI] = %s=%08X / %s=%08X;", BASE_ARG, R_(ARG_RS), cpu.getRegister<u32>(ARG_RS), R_(ARG_RT), cpu.getRegister<u32>(ARG_RT)); return; }
    if(op == (u32)MipsiOp::dmultu){     printf_trace(BASE_STR "  | dmultu    ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::dmult){      printf_trace(BASE_STR "  | dmult     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::ddiv){       printf_trace(BASE_STR "  | ddiv      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::ddivu){      printf_trace(BASE_STR "  | ddivu     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::addu){       printf_trace(BASE_STR "  | addu      %s = (%s=%08X + %s=%08X);", BASE_ARG, R_(ARG_RD), R_(ARG_RS), cpu.getRegister<u32>(ARG_RS), R_(ARG_RT), cpu.getRegister<u32>(ARG_RT)); return; }
    if(op == (u32)MipsiOp::subu){       printf_trace(BASE_STR "  | subu      %s = (%s=%08X - %s=%08X);", BASE_ARG, R_(ARG_RD), R_(ARG_RS), cpu.getRegister<u32>(ARG_RS), R_(ARG_RT), cpu.getRegister<u32>(ARG_RT)); return; }
    if(op == (u32)MipsiOp::op_and){     printf_trace(BASE_STR "  | op_and    %s = (%s=%08X & %s=%08X);", BASE_ARG, R_(ARG_RD), R_(ARG_RS), cpu.getRegister<u32>(ARG_RS), R_(ARG_RT), cpu.getRegister<u32>(ARG_RT)); return; }
    if(op == (u32)MipsiOp::op_or){      printf_trace(BASE_STR "  | op_or     %s = (%s=%08X | %s=%08X);", BASE_ARG, R_(ARG_RD), R_(ARG_RS), cpu.getRegister<u32>(ARG_RS), R_(ARG_RT), cpu.getRegister<u32>(ARG_RT)); return; }
    if(op == (u32)MipsiOp::op_xor){     printf_trace(BASE_STR "  | op_xor    %s = (%s=%08X ^ %s=%08X);", BASE_ARG, R_(ARG_RD), R_(ARG_RS), cpu.getRegister<u32>(ARG_RS), R_(ARG_RT), cpu.getRegister<u32>(ARG_RT)); return; }
    if(op == (u32)MipsiOp::slt){        printf_trace(BASE_STR "  | slt       %s = (%s=%i < %s=%i);", BASE_ARG, R_(ARG_RD), R_(ARG_RS), cpu.getRegister<s32>(ARG_RS), R_(ARG_RT), cpu.getRegister<s32>(ARG_RT)); return; }
    if(op == (u32)MipsiOp::sltu){       printf_trace(BASE_STR "  | sltu      %s = (%s=%08X < %s=%08X);", BASE_ARG, R_(ARG_RD), R_(ARG_RS), cpu.getRegister<u32>(ARG_RS), R_(ARG_RT), cpu.getRegister<u32>(ARG_RT)); return; }
    if(op == (u32)MipsiOp::dsll32){     printf_trace(BASE_STR "  | dsll32    %s = (%s=%08X%08X << %08X);", BASE_ARG, R_(ARG_RD), R_(ARG_RT), (u32)(cpu.getRegister<u64>(ARG_RT) >> 32), (u32)(cpu.getRegister<u64>(ARG_RT) & 0xFFFF'FFFF), ARG_SA+32); return; }
    if(op == (u32)MipsiOp::dsrl32){     printf_trace(BASE_STR "  | dsrl32    %s = (%s=%08X%08X >> %08X);", BASE_ARG, R_(ARG_RD), R_(ARG_RT), (u32)(cpu.getRegister<u64>(ARG_RT) >> 32), (u32)(cpu.getRegister<u64>(ARG_RT) & 0xFFFF'FFFF), ARG_SA+32); return; }
    if(op == (u32)MipsiOp::dsra32){     printf_trace(BASE_STR "  | dsra32    ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::bltz){       printf_trace(BASE_STR "  | bltz      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::bltzl){      printf_trace(BASE_STR "  | bltzl     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::bgezl){      printf_trace(BASE_STR "  | bgezl     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::mfc0){       printf_trace(BASE_STR "F | mfc0      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::mtc0){       printf_trace(BASE_STR "F | mtc0      cop0.reg[%08X] = %s=%08X;", BASE_ARG, ARG_RD, R_(ARG_RT), cpu.getRegister<s32>(ARG_RT)); return; }
    if(op == (u32)MipsiOp::mfc1){       printf_trace(BASE_STR "F | mfc1      %s = $f%d=%f [0x%08X]", BASE_ARG, R_(ARG_RT), ARG_FS, cpu.fpu.getRegister<f32>(ARG_FS), cpu.fpu.getRegister<s32>(ARG_FS)); return; }
    if(op == (u32)MipsiOp::dmfc1){      printf_trace(BASE_STR "F | dmfc1      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::dmtc1){      printf_trace(BASE_STR "F | dmtc1     $f%d = %s=%08X%08X;", BASE_ARG, ARG_FS, R_(ARG_RT), (u32)(cpu.getRegister<u64>(ARG_RT) >> 32), (u32)(cpu.getRegister<u64>(ARG_RT) & 0xFFFF'FFFF)); return; }
    if(op == (u32)MipsiOp::mtc1){       printf_trace(BASE_STR "F | mtc1      $f%d = %s=%08X;", BASE_ARG, ARG_FS, R_(ARG_RT), cpu.getRegister<s32>(ARG_RT)); return; }
    if(op == (u32)MipsiOp::sub_s){      printf_trace(BASE_STR "F | sub_s     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::sub_d){      printf_trace(BASE_STR "F | sub_d     $f%d = $f%d=%f - $f%d=%f;", BASE_ARG, ARG_FD, ARG_FS, cpu.fpu.getRegister<f64>(ARG_FS), ARG_FT, cpu.fpu.getRegister<f64>(ARG_FT)); return; }
    if(op == (u32)MipsiOp::mul_s){      printf_trace(BASE_STR "F | mul_s     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::mul_d){      printf_trace(BASE_STR "F | mul_d     $f%d = $f%d=%f * $f%d=%f;", BASE_ARG, ARG_FD, ARG_FS, cpu.fpu.getRegister<f64>(ARG_FS), ARG_FT, cpu.fpu.getRegister<f64>(ARG_FT)); return; }
    if(op == (u32)MipsiOp::div_s){      printf_trace(BASE_STR "F | div_s     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::div_d){      printf_trace(BASE_STR "F | div_d     $f%d = $f%d=%f / $f%d=%f;", BASE_ARG, ARG_FD, ARG_FS, cpu.fpu.getRegister<f64>(ARG_FS), ARG_FT, cpu.fpu.getRegister<f64>(ARG_FT)); return; }
    if(op == (u32)MipsiOp::trunc_w_s){  printf_trace(BASE_STR "F | trunc_w_s ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::trunc_w_d){  printf_trace(BASE_STR "F | trunc_w_d $f%d = (s32)$f%d=%f; // res: %d", BASE_ARG, ARG_FD, ARG_FS, cpu.fpu.getRegister<f64>(ARG_FS), (s32)(cpu.fpu.getRegister<f64>(ARG_FS))); return; }
    if(op == (u32)MipsiOp::cvt_s_d){    printf_trace(BASE_STR "F | cvt_s_d   ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::cvt_s_w){    printf_trace(BASE_STR "F | cvt_s_w   $f%d = (f32)$f%d [f:%f, w:%d]", BASE_ARG, ARG_FD, ARG_FS, cpu.fpu.getRegister<f32>(ARG_FS), cpu.fpu.getRegister<s32>(ARG_FS)); return; }
    if(op == (u32)MipsiOp::cvt_s_l){    printf_trace(BASE_STR "F | cvt_s_l   ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::add){        printf_trace(BASE_STR "  | add       ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::beqz){       printf_trace(BASE_STR "  | beqz      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::b){          printf_trace(BASE_STR "  | b         ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::bnez){       printf_trace(BASE_STR "  | bnez      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::bgez){       printf_trace(BASE_STR "  | bgez      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::nor){        printf_trace(BASE_STR "  | nor       %s = ~(%s=%08X | %s=%08X);", BASE_ARG, R_(ARG_RD), R_(ARG_RS), cpu.getRegister<u32>(ARG_RS), R_(ARG_RT), cpu.getRegister<u32>(ARG_RT)); return; }
    if(op == (u32)MipsiOp::movn){       printf_trace(BASE_STR "  | movn      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::movz){       printf_trace(BASE_STR "  | movz      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::srlv){       printf_trace(BASE_STR "  | srlv      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::srav){       printf_trace(BASE_STR "  | srav      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::lb){         printf_trace(BASE_STR "  | lb        %s = (s8)memory[%s=%08X + 0x%04X];", BASE_ARG, R_(ARG_RT), R_(ARG_BASE), cpu.getRegister<u32>(ARG_BASE), ARG_OFFSET_EXT); return; }
    if(op == (u32)MipsiOp::addi){       printf_trace(BASE_STR "  | addi      %s = %s=%08X + 0x%08X", BASE_ARG, R_(ARG_RS), R_(ARG_RT), cpu.getRegister<u32>(ARG_RT), ARG_IMM_EXT); return; }
    if(op == (u32)MipsiOp::bc1t){       printf_trace(BASE_STR "F | bc1t      if(%d) branch 0x%08X;", BASE_ARG, cpu.fpu.getStatusFlag(FPUStatusReg::FCC_COMPARE), ARG_BRANCH_OFFSET + 4); return; }
    if(op == (u32)MipsiOp::bc1f){       printf_trace(BASE_STR "F | bc1f      if(!%d) branch 0x%08X;", BASE_ARG, cpu.fpu.getStatusFlag(FPUStatusReg::FCC_COMPARE), ARG_BRANCH_OFFSET + 4); return; }
    if(op == (u32)MipsiOp::cvt_d_s){    printf_trace(BASE_STR "F | cvt_d_s   $f%d = (s32)$f%d=%f;", BASE_ARG, ARG_FD, ARG_FS, cpu.fpu.getRegister<f32>(ARG_FS)); return; }
    if(op == (u32)MipsiOp::cvt_d_w){    printf_trace(BASE_STR "F | cvt_d_w   $f%d = (s32)$f%d=%d;", BASE_ARG, ARG_FD, ARG_FS, cpu.fpu.getRegister<s32>(ARG_FS)); return; }
    if(op == (u32)MipsiOp::add_d){      printf_trace(BASE_STR "  | add_d     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::add_s){      printf_trace(BASE_STR "  | add_s     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::sdc1){       printf_trace(BASE_STR "  | sdc1      ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::mov_s){      printf_trace(BASE_STR "  | mov_s     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::mov_d){      printf_trace(BASE_STR "  | mov_d     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::c_lt_s){     printf_trace(BASE_STR "F | c_lt_s    ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::c_lt_d){     printf_trace(BASE_STR "F | c_lt_d    conditionFlag = $f%d=%f < $f%d=%f", BASE_ARG, ARG_FS, cpu.fpu.getRegister<f64>(ARG_FS), ARG_FT, cpu.fpu.getRegister<f64>(ARG_FT)); return; }
    if(op == (u32)MipsiOp::c_le_s){     printf_trace(BASE_STR "F | c_le_s    ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::c_le_d){     printf_trace(BASE_STR "F | c_le_d    ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::abs_s){      printf_trace(BASE_STR "F | abs_s     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::abs_d){      printf_trace(BASE_STR "F | abs_d     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::ldc1){       printf_trace(BASE_STR "F | ldc1      $f%d = (f64)memory[%s=%08X + 0x%04X = 0x%08X]", BASE_ARG, ARG_FT, R_(ARG_BASE), cpu.getRegister<s32>(ARG_BASE), ARG_OFFSET_EXT, cpu.getRegister<s32>(ARG_BASE) + ARG_OFFSET_EXT); return; }
    if(op == (u32)MipsiOp::c_eq_s){     printf_trace(BASE_STR "F | c_eq_s    conditionFlag = $f%d=%f == $f%d=%f;", BASE_ARG, ARG_FS, cpu.fpu.getRegister<f32>(ARG_FS), ARG_FT,cpu.fpu.getRegister<f32>(ARG_FT)); return; }
    if(op == (u32)MipsiOp::c_eq_d){     printf_trace(BASE_STR "F | c_eq_d    conditionFlag = $f%d=%f == $f%d=%f;", BASE_ARG, ARG_FS, cpu.fpu.getRegister<f64>(ARG_FS), ARG_FT,cpu.fpu.getRegister<f64>(ARG_FT)); return; }
    if(op == (u32)MipsiOp::neg_s){      printf_trace(BASE_STR "F | neg_s     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::neg_d){      printf_trace(BASE_STR "F | neg_d     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::sqrt_s){     printf_trace(BASE_STR "F | sqrt_s    ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::sqrt_d){     printf_trace(BASE_STR "F | sqrt_d    ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::bc1tl){      printf_trace(BASE_STR "F | bc1tl     if(%d) branch 0x%08X;", BASE_ARG, cpu.fpu.getStatusFlag(FPUStatusReg::FCC_COMPARE), ARG_BRANCH_OFFSET + 4); return; }
    if(op == (u32)MipsiOp::bc1fl){      printf_trace(BASE_STR "F | bc1fl     if(!%d) branch 0x%08X;", BASE_ARG, cpu.fpu.getStatusFlag(FPUStatusReg::FCC_COMPARE), ARG_BRANCH_OFFSET + 4); return; }
    if(op == (u32)MipsiOp::cache){      printf_trace(BASE_STR "  | cache     %d, %08X(%s=%08X)", BASE_ARG, ARG_RT, ARG_OFFSET_EXT, R_(ARG_RS), cpu.getRegister<u32>(ARG_RS)); return; }
    if(op == (u32)MipsiOp::jr_nop){     printf_trace(BASE_STR "  | jr_nop    ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::jal_nop){    printf_trace(BASE_STR "  | jal_nop   ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::li_half){    printf_trace(BASE_STR "  | li_half   ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::tlbwi){      printf_trace(BASE_STR "  | tlbwi     ", BASE_ARG); return; }
    if(op == (u32)MipsiOp::bgezal){     printf_trace(BASE_STR "  | bgezal    %08X", BASE_ARG, ARG_BRANCH_OFFSET + 4); return; }
    if(op == (u32)MipsiOp::eret){       printf_trace(BASE_STR "  | eret      endException(); jump 0x%08X;", BASE_ARG, cpu.cop0.getRegisterU32(Cop0Register::ERROR_EPC)); return; }
    if(op == (u32)MipsiOp::c_un_d){     printf_trace(BASE_STR "F | c_un_d    conditionFlag = isNaN($f%d=%f) || isNaN($f%d=%f);", BASE_ARG, ARG_FS, cpu.fpu.getRegister<f64>(ARG_FS), ARG_FT,cpu.fpu.getRegister<f64>(ARG_FT)); return; }

    printf_trace(BASE_STR "<unknown op> (%d=%s) [RS: %02X(%s), RT: %02X(%s), RD: %02X(%s)]", BASE_ARG, op, getOpcodeName(op), ARG_RS, R_(ARG_RS), ARG_RT, R_(ARG_RT), ARG_RD, R_(ARG_RD));
  #endif
}