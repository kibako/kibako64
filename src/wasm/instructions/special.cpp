/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "instructions.h"
#include "./opcodeArgs.h"
#include "../components/cpuRegisters.h"

namespace {
  constexpr u32 KIBAKO_CMD_LOG_ON   = 0xFFFFFF01;
  constexpr u32 KIBAKO_CMD_LOG_OFF  = 0xFFFFFF02;
  constexpr u32 KIBAKO_CMD_CPU_HALT = 0xFFFFFF03;
}

void MipsiOp::x_log(u32 word) { 
  #ifdef FEATURE_DEBUG_LOG
    cpu.logCall();
  #endif
}

void MipsiOp::x_inject(u32 word) { 
  hook_inject(cpu.getPc() - 4,
    cpu.getRegister<u32>((u32)CpuReg::a0), cpu.getRegister<u32>((u32)CpuReg::a1),
    cpu.getRegister<u32>((u32)CpuReg::a2), cpu.getRegister<u32>((u32)CpuReg::a3)
  );
}

void MipsiOp::x_command(u32 word) 
{
    switch (word)
    {
      case KIBAKO_CMD_LOG_ON:
        printf_prog("x_command: enable logging @0x%08X", cpu.getPc());
        loggingLevel = LoggingLevelMask::ALL;
      break;

      case KIBAKO_CMD_LOG_OFF:
        printf_prog("x_command: disable logging @0x%08X", cpu.getPc());
        loggingLevel = LoggingLevelMask::ERROR | LoggingLevelMask::PROG;
      break;

      case KIBAKO_CMD_CPU_HALT:
        printf_prog("x_command: halt CPU @0x%08X", cpu.getPc());
        cpu.stop();
      break;

      default:
        printf_prog("x_command: %08X", word);
      break;

    }
}