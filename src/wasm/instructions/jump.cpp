/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "instructions.h"
#include "./opcodeArgs.h"
#include "../components/cpuRegisters.h"

void MipsiOp::j(u32 word) { 
  u32 newPc = ARG_ADDRESS;
  cpu.stepDeplay(newPc);

  // if we jump to our current postion, and the delay slot is a NOP,
  // we can never escape the loop -> stop execution (in this thread).
  if(newPc == cpu.getPc() - 4 && memBus.read<u32>(cpu.getPc()) == 0) {
    print_warn("Infinite loop detected!");
    cpu.logState();
    cpu.stop();
    return;
  }
}
void MipsiOp::jr(u32 word) 
{ 
  debugger_hook_stack_pop();
  cpu.stepDeplay(cpu.getRegister<u32>(ARG_RS));
}

void MipsiOp::jr_nop(u32 word) { 
  debugger_hook_stack_pop();
  cpu.setPc(cpu.getRegister<u32>(ARG_RS));
}

void MipsiOp::jal_nop(u32 word) {   
  cpu.setRegister<u32>((s32)CpuReg::ra, cpu.getPc() + sizeof(u32));
  debugger_hook_stack_push(cpu.getRegister<u32>((s32)CpuReg::ra));

  cpu.setPc(ARG_ADDRESS);
}

void MipsiOp::jal(u32 word)
{
  cpu.setRegister<u32>((s32)CpuReg::ra, cpu.getPc() + sizeof(u32)); // pc was already incremented by 4, so $ra is pc + 8 (second instruction after jump)
  debugger_hook_stack_push(cpu.getRegister<u32>((s32)CpuReg::ra));

  cpu.stepDeplay(ARG_ADDRESS);
};

void MipsiOp::jalr(u32 word)
{
  cpu.setRegister<u32>((s32)CpuReg::ra, cpu.getPc() + sizeof(u32)); // pc was already incremented by 4, so $ra is pc + 8 (second instruction after jump
  debugger_hook_stack_push(cpu.getRegister<u32>((s32)CpuReg::ra));

  cpu.stepDeplay(cpu.getRegister<u32>(ARG_RS));
};
