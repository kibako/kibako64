/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "instructions.h"
#include "./opcodeArgs.h"
#include "../components/cpuRegisters.h"

#define BITMASK_U32 0xFFFFFFFF
#define BITMASK_U64 0xFFFFFFFFFFFFFFFF

void MipsiOp::add(u32 word)
{
  cpu.setRegister<s32>(ARG_RD, cpu.getRegister<s32>(ARG_RS) + cpu.getRegister<s32>(ARG_RT)); // @TODO check overflow
}

void MipsiOp::dadd(u32 word)
{
  cpu.setRegister<s64>(ARG_RD, cpu.getRegister<s64>(ARG_RS) + cpu.getRegister<s64>(ARG_RT)); // @TODO check overflow
}

void MipsiOp::addu(u32 word)
{
  cpu.setRegister<u32>(ARG_RD, cpu.getRegister<u32>(ARG_RS) + cpu.getRegister<u32>(ARG_RT)); // @TODO check overflow
}

void MipsiOp::addi(u32 word)
{
  cpu.setRegister<s32>(ARG_RT, cpu.getRegister<s32>(ARG_RS) + ARG_IMM_EXT);
}

void MipsiOp::daddi(u32 word)
{
  cpu.setRegister<s64>(ARG_RT, cpu.getRegister<s64>(ARG_RS) + ARG_IMM_EXT);
}

void MipsiOp::addiu(u32 word)
{
  // @OPT: check for zero ARG_RS -> pseudo op
  cpu.setRegister<u32>(ARG_RT, (cpu.getRegister<u32>(ARG_RS) + ARG_IMM_EXT));
}

void MipsiOp::daddiu(u32 word)
{
  cpu.setRegister<u64>(ARG_RT, cpu.getRegister<u64>(ARG_RS) + (u64)ARG_IMM_EXT);
}

void MipsiOp::daddu(u32 word)
{
  cpu.setRegister<u64>(ARG_RD, cpu.getRegister<u64>(ARG_RS) + cpu.getRegister<u64>(ARG_RT));
}


void MipsiOp::subu(u32 word) // "Untrapped", not "unsigned"
{
  cpu.setRegister<u32>(ARG_RD, cpu.getRegister<u32>(ARG_RS) - cpu.getRegister<u32>(ARG_RT));
}

void MipsiOp::dsub(u32 word)
{
  cpu.setRegister<s64>(ARG_RD, cpu.getRegister<s64>(ARG_RS) - cpu.getRegister<s64>(ARG_RT));
}

void MipsiOp::dsubu(u32 word) // "Untrapped", not "unsigned"
{
  cpu.setRegister<u64>(ARG_RD, cpu.getRegister<u64>(ARG_RS) - cpu.getRegister<u64>(ARG_RT));
}

void MipsiOp::sub(u32 word) // @TODO handle overflow
{
  cpu.setRegister<u32>(ARG_RD, cpu.getRegister<u32>(ARG_RS) - cpu.getRegister<u32>(ARG_RT)); // @TODO check overflow
}

void MipsiOp::mult(u32 word)
{
  u64 res = (s64)cpu.getRegister<s32>(ARG_RS) * (s64)cpu.getRegister<s32>(ARG_RT);

  cpu.setRegister<u32>((s32)CpuReg::SP_LOW, res & BITMASK_U32);
  cpu.setRegister<u32>((s32)CpuReg::SP_HIGH, res >> 32);
}

void MipsiOp::multu(u32 word)
{
  u64 res = (u64)cpu.getRegister<u32>(ARG_RS) * (u64)cpu.getRegister<u32>(ARG_RT);

  cpu.setRegister<u32>((s32)CpuReg::SP_LOW, res & BITMASK_U32);
  cpu.setRegister<u32>((s32)CpuReg::SP_HIGH, res >> 32);
}

void MipsiOp::dmultu(u32 word)
{
  u64 x = cpu.getRegister<u64>(ARG_RS);
  u64 y = cpu.getRegister<u64>(ARG_RT);

  // 64*64->128 Taken from https://stackoverflow.com/a/50958815
  const u64 x0 = (u32)x, x1 = x >> 32;
  const u64 y0 = (u32)y, y1 = y >> 32;
  const u64 p11 = x1 * y1, p01 = x0 * y1;
  const u64 p10 = x1 * y0, p00 = x0 * y0;
  const u64 middle = p10 + (p00 >> 32) + (u32)p01;

  u64 resHigh = p11 + (middle >> 32) + (p01 >> 32);
  u64 resLow = (middle << 32) | (u32)p00;

  cpu.setRegister<u64>((s32)CpuReg::SP_LOW,  resLow);
  cpu.setRegister<u64>((s32)CpuReg::SP_HIGH, resHigh);
}

void MipsiOp::dmult(u32 word)
{
  s64 x = cpu.getRegister<s64>(ARG_RS);
  s64 y = cpu.getRegister<s64>(ARG_RT);

  u64 xHigh = (x >> 32) & BITMASK_U32;
  u64 yHigh = (y >> 32) & BITMASK_U32;
  u64 xLow = x & BITMASK_U32;
  u64 yLow = y & BITMASK_U32;
  u64 p1 = xLow  * yLow;
  u64 p2 = xHigh * yLow;
  u64 p3 = xLow  * yHigh;
  u64 p4 = xHigh * yHigh;
  
  u64 carry = (p1 >> 32) + (p2 & BITMASK_U32) + (p3 & BITMASK_U32);
  carry >>= 32;
  carry &= BITMASK_U32;

  u64 resLow = p1 + (p2 << 32) + (p3 << 32);
  u64 resHigh = p4 + (p2 >> 32) + (p3 >> 32) + carry;

  // Sign
  if(x < 0)resHigh -= y;
  if(y < 0)resHigh -= x;

  cpu.setRegister<s64>((s32)CpuReg::SP_LOW,  resLow);
  cpu.setRegister<s64>((s32)CpuReg::SP_HIGH, resHigh);
}

void MipsiOp::div(u32 word)
{
  s32 valA = cpu.getRegister<s32>(ARG_RS);
  s32 valB = cpu.getRegister<s32>(ARG_RT);

  if(valA == 0) {
    cpu.setRegister<s32>((s32)CpuReg::SP_LOW, 0);
    cpu.setRegister<s32>((s32)CpuReg::SP_HIGH, 0);
    return;
  }

  if(valB == 0) {
    cpu.setRegister<s32>((s32)CpuReg::SP_LOW, valA < 0 ? 1 : -1);
    cpu.setRegister<s32>((s32)CpuReg::SP_HIGH, valA);
    return;
  }

  if(valA == S32_MIN && valB == -1) {
    cpu.setRegister<s32>((s32)CpuReg::SP_LOW, S32_MIN);
    cpu.setRegister<s32>((s32)CpuReg::SP_HIGH, 0);
    return;
  }

  cpu.setRegister<s32>((s32)CpuReg::SP_LOW, valA / valB);
  cpu.setRegister<s32>((s32)CpuReg::SP_HIGH, valA % valB);
}

void MipsiOp::divu(u32 word)
{
  u32 valA = cpu.getRegister<u32>(ARG_RS);
  u32 valB = cpu.getRegister<u32>(ARG_RT);

  if(valA == 0) {
    cpu.setRegister<u32>((s32)CpuReg::SP_LOW, 0);
    cpu.setRegister<u32>((s32)CpuReg::SP_HIGH, 0);
    return;
  }

  if(valB == 0) {
    cpu.setRegister<s32>((s32)CpuReg::SP_LOW, U32_MAX);
    cpu.setRegister<s32>((s32)CpuReg::SP_HIGH, valA);
    return;
  }

  cpu.setRegister<u32>((s32)CpuReg::SP_LOW, valA / valB);
  cpu.setRegister<u32>((s32)CpuReg::SP_HIGH, valA % valB);
}

void MipsiOp::ddiv(u32 word)
{
  s64 valA = cpu.getRegister<s64>(ARG_RS);
  s64 valB = cpu.getRegister<s64>(ARG_RT);

  if(valA == 0) {
    cpu.setRegister<s64>((s32)CpuReg::SP_LOW, 0);
    cpu.setRegister<s64>((s32)CpuReg::SP_HIGH, 0);
    return;
  }

  if(valB == 0) {
    cpu.setRegister<s64>((s32)CpuReg::SP_LOW, valA < 0 ? 1 : -1);
    cpu.setRegister<s64>((s32)CpuReg::SP_HIGH, valA);
    return;
  }

  if(valA == S64_MIN && valB == -1) {
    cpu.setRegister<s64>((s32)CpuReg::SP_LOW, S64_MIN);
    cpu.setRegister<s64>((s32)CpuReg::SP_HIGH, 0);
    return;
  }

  cpu.setRegister<s64>((s32)CpuReg::SP_LOW, valA / valB);
  cpu.setRegister<s64>((s32)CpuReg::SP_HIGH, valA % valB);
}

void MipsiOp::ddivu(u32 word)
{
  u64 valA = cpu.getRegister<u64>(ARG_RS);
  u64 valB = cpu.getRegister<u64>(ARG_RT);
  
  if(valA == 0) {
    cpu.setRegister<u64>((s32)CpuReg::SP_LOW, 0);
    cpu.setRegister<u64>((s32)CpuReg::SP_HIGH, 0);
    return;
  }

  if(valB == 0) {
    cpu.setRegister<s64>((s32)CpuReg::SP_LOW, U64_MAX);
    cpu.setRegister<s64>((s32)CpuReg::SP_HIGH, valA);
    return;
  }

  cpu.setRegister<u64>((s32)CpuReg::SP_LOW, valA / valB);
  cpu.setRegister<u64>((s32)CpuReg::SP_HIGH, valA % valB);
}
