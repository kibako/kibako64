/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "instructions.h"
#include "./opcodeArgs.h"
#include "../components/cpuRegisters.h"
#include "../components/tlb.h"

void MipsiOp::movn(u32 word) 
{  
	if(cpu.getRegister<s32>(ARG_RT) != 0)
		cpu.setRegister<u32>(ARG_RD, cpu.getRegister<u32>(ARG_RS));
}

void MipsiOp::movz(u32 word) 
{
	if(cpu.getRegister<s32>(ARG_RT) == 0)
		cpu.setRegister<u32>(ARG_RD, cpu.getRegister<u32>(ARG_RS));
}

void MipsiOp::mfhi(u32 word) { cpu.setRegister<s64>(ARG_RD, cpu.getRegister<s64>((s32)CpuReg::SP_HIGH)); }
void MipsiOp::mflo(u32 word) { cpu.setRegister<s64>(ARG_RD, cpu.getRegister<s64>((s32)CpuReg::SP_LOW));  }

void MipsiOp::mthi(u32 word) { cpu.setRegister<u64>((u32)CpuReg::SP_HIGH, cpu.getRegister<u64>(ARG_RS)); }
void MipsiOp::mtlo(u32 word) { cpu.setRegister<u64>((u32)CpuReg::SP_LOW,  cpu.getRegister<u64>(ARG_RS)); }

void MipsiOp::dsra32(u32 word) { cpu.setRegister<s64>(ARG_RD, cpu.getRegister<s64>(ARG_RT) >> (ARG_SA+32)); }
void MipsiOp::dsll32(u32 word) { cpu.setRegister<u64>(ARG_RD, cpu.getRegister<u64>(ARG_RT) << (ARG_SA+32)); }
void MipsiOp::dsrl32(u32 word) { cpu.setRegister<u64>(ARG_RD, cpu.getRegister<u64>(ARG_RT) >> (ARG_SA+32)); }

void MipsiOp::mtc0(u32 word) 
{
  u32 sel = word & 0b111;
  if(sel != 0) {
    print_warn("mtc0: SEL is not zero!");
  }

  cpu.cop0.setRegisterU32((Cop0Register)ARG_RD, cpu.getRegister<u32>(ARG_RT));
}

void MipsiOp::mfc0(u32 word) 
{    
  u32 sel = word & 0b111;
  if(sel != 0) {
    print_warn("mtc0: SEL is not zero!");
  }

  cpu.setRegister<u32>(ARG_RT,
    cpu.cop0.getRegisterU32((Cop0Register)ARG_RD)
  );
}

void MipsiOp::tlbwi(u32 word) 
{    
  u32 tableIndex = cpu.cop0.getRegisterU32(Cop0Register::INDEX) & 0x1F;

  TLBEntry entry {
    (TLBPageMask)cpu.cop0.getRegisterU32(Cop0Register::PAGE_MASK),
    cpu.cop0.getRegisterU32(Cop0Register::ENTRY_HIGH),
    cpu.cop0.getRegisterU32(Cop0Register::ENTRY_LOW0),
    cpu.cop0.getRegisterU32(Cop0Register::ENTRY_LOW1),
  };
  memBus.tlb.setEntry(tableIndex, entry);
}

#define OP_CACHE_TYPE_INSTR  0b00
#define OP_CACHE_TYPE_DATA   0b01
#define OP_CACHE_TYPE_TERT   0b10
#define OP_CACHE_TYPE_SECOND 0b11
#define OP_CACHE_OP_INDEX_STORE_TAG 0b010

const char cacheTypeName[4] = {'I', 'D', 'T', 'S'};

void MipsiOp::cache(u32 word) {
  u32 op = ARG_RT;
  u32 opType = op & 0b11;
  op >>= 2;

  if(opType == OP_CACHE_TYPE_DATA) {
    return; // ignode data-cache
  }

  if(op == OP_CACHE_OP_INDEX_STORE_TAG) {
    return; // ignore, used for initialisation
  }

	printf_warn("OP: cache unimplemented %08X", word);
  printf_info("cache: OP: %X %c, %08X($%d) | Reg: %08X", 
    op, cacheTypeName[opType], ARG_OFFSET_EXT, ARG_RS,
    cpu.getRegister<u32>(ARG_RS)
  );
};

void MipsiOp::breakpoint(u32 word) {
	/** @TODO add option to actually break, at least in test mode */
};

void MipsiOp::nop(u32 word) {
  
}

void MipsiOp::sync(u32 word) {
  // load/write cache sync, irrelevant for emulation
}
