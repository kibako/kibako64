/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#define RAM_PTR nullptr
#define RAM_SIZE (8_MB)

#define ROM_PTR constexpr_ptr(RAM_SIZE)
#define ROM_SIZE (64_MB)
