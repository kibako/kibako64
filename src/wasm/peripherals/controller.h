/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../types.h"

namespace Peripherals
{
  enum class ControllerButtons
  {
    A       = (1 << 0),
    B       = (1 << 1),
    Z       = (1 << 2),
    START   = (1 << 3),
    D_UP    = (1 << 4),
    D_DOWN  = (1 << 5),
    D_LEFT  = (1 << 6),
    D_RIGHT = (1 << 7),
    //        (1 << 8),
    //        (1 << 9),
    L       = (1 << 10),
    R       = (1 << 11),
    C_UP    = (1 << 12),
    C_DOWN  = (1 << 13),
    C_LEFT  = (1 << 14),
    C_RIGHT = (1 << 15),
  };

  class Controller 
  {
    private:
      u8 data[4]; // [0,1] buttons (see enum), [2] joystick-X, [3] joystick-Y
      bool32 isPluggedIn;

    public:
      constexpr Controller() 
        : data{0,0,0,0}, isPluggedIn(true)
      {}

      u8 getDataByte(u32 index);
      bool32 mempackPopulated();

      void setButtons(u16 buttonMask);
      void setJoystick(f32 x, f32 y);

      void connect();
      void disconnect();
      bool32 isConnected();
  };
};
