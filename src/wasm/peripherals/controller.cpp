/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "controller.h"

u8 Peripherals::Controller::getDataByte(u32 index)
{
  return data[index & 0b11];
}

bool32 Peripherals::Controller::mempackPopulated()
{
  //@TODO 
  return false;
}

void Peripherals::Controller::setButtons(u16 buttonMask)
{
  data[0] = buttonMask >> 8;
  data[1] = buttonMask & 0xFF;
}

void Peripherals::Controller::setJoystick(f32 x, f32 y)
{
  data[2] = (s8)(x * 127.0f);
  data[3] = (s8)(y * 127.0f);
}

void Peripherals::Controller::connect()
{
  isPluggedIn = true;
}

void Peripherals::Controller::disconnect()
{
  isPluggedIn = false;
}

bool32 Peripherals::Controller::isConnected()
{
  return isPluggedIn;
}
