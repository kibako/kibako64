/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

// All supported WASM types
typedef unsigned int u32;
typedef signed int s32;
typedef unsigned long long u64;
typedef signed long long s64;
typedef float f32;
typedef double f64;

// Extra types (gets compiled to one of the above)
typedef unsigned char u8;
typedef signed char s8;
typedef unsigned short u16;
typedef signed short s16;
typedef bool bool32;

#define S32_MIN (s32)(0x8000'0000)
#define S64_MIN (s64)(0x8000'0000'0000'0000)

#define U8_MAX (u8)(0xFF)
#define U16_MAX (u16)(0xFFFF)
#define U32_MAX (u32)(0xFFFF'FFFF)
#define U64_MAX (u64)(0xFFFF'FFFF'FFFF'FFFF)

enum class Endian {
  LITTLE = 0,
  BIG = 1,
  MIXED = 2,
};

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wreserved-identifier"

// Convenience types
constexpr s32 operator "" _KB(unsigned long long x) { return (s32)(x * 1024); }
constexpr s32 operator "" _MB(unsigned long long x) { return (s32)(x * 1024 * 1024); }
constexpr s32 operator "" _GB(unsigned long long x) { return (s32)(x * 1024 * 1024 * 1024); }

#pragma clang diagnostic pop

// Op-Function
typedef void (*MipsiOpFunction)(u32);

// Hacks

// constexpr for harcoded addresses, for some reason not allowed in standard C++ :/
#define constexpr_ptr(address) __builtin_constant_p((u32*)address) ? (u32*)address : (u32*)address