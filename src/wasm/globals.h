/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

// Super-Globals, keep these at an absolute minimum!

// Peripherals
#include "peripherals/controller.h"
extern Peripherals::Controller gameController[4];

// Components
#include "components/memory/memoryBus.h"
extern MemoryBus memBus;

#include "components/cpu.h"
extern MipsCPU cpu;

#include "components/gpu/gpu.h"
extern GPU gpu;

#include "components/system.h"
extern System system;
