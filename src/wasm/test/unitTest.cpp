/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

/**
 * Most of the tests are not included, it rather exports / exposes functions
 * so they can be used in tests in the JS host.
 */
#ifdef TARGET_DEBUG

#include "../main.h"
#include "../stdlib/stdlib.h"
#include "../utils/checksum.h"
#include "../disassembler/disassembler.h"
#include "../instructions/instructions.h"

// System & generic functions
s32 WASM_EXPORT(testCpuGetErrorCount)(s32 reg) { return cpu.getErrorCount(); }
void WASM_EXPORT(testCpuRun)(u32 pc, s32 limit) { 
  cpu.setPc(pc);
  cpu.run(limit); 
}

void WASM_EXPORT(testMipsInterfaceSetInterruptBit)(u32 mask) {
  memBus.mipsInterfaceReg.setInterruptBit((Interrupt::RcpType)mask);
}

s32 WASM_EXPORT(testGetOpcodeFunction)(u32 word) { return (s32)Disassembler::getOpcodeFunction(word); }
s32 WASM_EXPORT(testGetOpcodeName)(s32 opcodePtr) { return (s32)MipsiOp::getOpcodeName(opcodePtr); }
u32 WASM_EXPORT(testOpcodegetHighestPointer)(u32 rand) { return MipsiOp::getHighestPointer(rand); }

u32 WASM_EXPORT(testChecksumU8)(u8* const dataPtr, u8* const dataPtrEnd) { return checksumU8(dataPtr, dataPtrEnd); }

// Memory BUS
 u8 WASM_EXPORT(testMemBusReadU8)(s32 address) { return memBus.read<u8>(address); }
u16 WASM_EXPORT(testMemBusReadU16)(s32 address) { return memBus.read<u16>(address); }
u32 WASM_EXPORT(testMemBusReadU32)(s32 address) { return memBus.read<u32>(address); }
u64 WASM_EXPORT(testMemBusReadU64)(s32 address) { return memBus.read<u64>(address); }
f32 WASM_EXPORT(testMemBusReadF32)(s32 address) { return memBus.read<f32>(address); }
f64 WASM_EXPORT(testMemBusReadF64)(s32 address) { return memBus.read<f64>(address); }

void WASM_EXPORT(testMemBusWriteU8)(s32 address, u8 value) { memBus.write(address, value); }
void WASM_EXPORT(testMemBusWriteU16)(s32 address, u16 value) { memBus.write(address, value); }
void WASM_EXPORT(testMemBusWriteU32)(s32 address, u32 value) { memBus.write(address, value); }
void WASM_EXPORT(testMemBusWriteU64)(s32 address, u64 value) { memBus.write(address, value); }
void WASM_EXPORT(testMemBusWriteF32)(s32 address, f32 value) { memBus.write(address, value); }
void WASM_EXPORT(testMemBusWriteF64)(s32 address, f64 value) { memBus.write(address, value); }

// TLB
void WASM_EXPORT(testTlbSetEntry)(s32 index, u32 mask, u32 high, u32 low0, u32 low1) { 
  memBus.tlb.setEntry(index, {(TLBPageMask)mask, high, low0, low1});
}

u32 WASM_EXPORT(testTlbResolveAddress)(u32 vAddress) { return memBus.tlb.resolveAddress(vAddress); }

// Cop0
u32 WASM_EXPORT(testCop0GetRegisterU32)(s32 reg) { return cpu.cop0.getRegisterU32((Cop0Register)reg); }
void WASM_EXPORT(testCop0SetRegisterU32)(s32 reg, u32 value) { cpu.cop0.setRegisterU32((Cop0Register)reg, value); }

// VideoInterface
void WASM_EXPORT(testViIncrementCurrentLine)() {
  memBus.videoInterfaceReg.incrementCurrentLine();
}

// Register - 8-bit
u8 WASM_EXPORT(testCpuGetRegisterByte32)(s32 reg, s32 byteIdx) { return cpu.getRegisterByte32(reg, byteIdx); }
void WASM_EXPORT(testCpuSetRegisterByte32)(s32 reg, s32 byteIdx, u8 value) { cpu.setRegisterByte32(reg, byteIdx, value); }

// FPU
void WASM_EXPORT(testFpuSetConditionFlag)(bool32 value) { cpu.fpu.setCompare(value); }
bool32 WASM_EXPORT(testFpuGetConditionFlag)() { return cpu.fpu.getStatusFlag(FPUStatusReg::FCC_COMPARE); }

// Actual Test cases:

void WASM_EXPORT(unitTestMemoryWriteNativeTest)() 
{ 
  u8* ptrU8 = (u8*)0x100;
  *ptrU8 = 0xAB;

  u16* ptrU16 = (u16*)0x108;
  *ptrU16 = 0xAABB;

  u32* ptrU32 = (u32*)0x110;
  *ptrU32 = 0xAABBCCDD;

  u64* ptrU64 = (u64*)0x118;
  *ptrU64 = 0x11223344'AABBCCDD;
}

// see "wasmMipsi.spec.ts"
void WASM_EXPORT(unitTestFpuRegisterConvert)() 
{ 
  cpu.fpu.convertRegister<f32, s32>(2, 3);
  cpu.fpu.convertRegister<s32, f32>(4, 5);
}

// see "wasmMipsi.spec.ts"
void WASM_EXPORT(unitTestBuildins)(f32 valfloorf, f32 valfloor, f32 valsqrtf, f32 valsqrt, f32 valabs) 
{ 
  cpu.fpu.setRegister<f32>(0, floorf(valfloorf));
  cpu.fpu.setRegister<f32>(1, floor(valfloor));
  cpu.fpu.setRegister<f32>(2, sqrtf(valsqrtf));
  cpu.fpu.setRegister<f32>(3, sqrt(valsqrt));
  cpu.fpu.setRegister<f32>(4, absf(valabs));
}

#endif