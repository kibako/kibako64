/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../types.h"

/**
 * Why?
 * Webassembly is an assembly language that doesn't know about C++ and its libs.
 * All std-libs are not available as source or pre-build for WASM-32, and implemtations may
 * use syscalls or special CPU instructions, all of which are not available in WASM.
 * So the best option is to implement all needed functions here.
 */

namespace std {

  // Conditional typing

  template<bool B, typename T, typename F>
  struct conditional { typedef T type; };

  template<typename T, typename F>
  struct conditional<false, T, F> { typedef F type; };

  template <typename, typename> 
  struct is_same { static const bool value = false;};

  template <typename T> 
  struct is_same<T, T> { static const bool value = true;};

  // Casting

  template<typename T> struct make_unsigned { typedef T type; };
  template<> struct make_unsigned<s8> { typedef u8 type; };
  template<> struct make_unsigned<s16> { typedef u16 type; };
  template<> struct make_unsigned<s32> { typedef u32 type; };
  template<> struct make_unsigned<s64> { typedef u64 type; };

  template<typename T>
  using make_unsigned_t = typename make_unsigned<T>::type;

  // Bit-Casting

  template<typename DST, typename SRC>
  inline DST bit_cast(SRC value) {
    return __builtin_bit_cast(DST, value);
  }

  template<s32 SIZE>
  inline void memcpy_inline(void* dst, const void* src)
  {
    __builtin_memcpy_inline(dst, src, SIZE);
  }

}

extern "C" {
  // will compile into single op "memory.fill"
  void* memset(void* ptr, s32 value, u32 numBytes);

  // will compile into single op "memory.copy"
  void* memcpy(void* dst, void* src, u32 numBytes);
}

constexpr f32 STD_PI   = 3.141592653589f;
constexpr f32 STD_PI_2 = 1.5707963267948f;
constexpr f32 STD_PI_4 = 0.7853981633974f;

/**
 * WASM has special instructions for these functions,
 * by using the buildins, they are actually used.
 * 
 * List of all clang-buildins:
 * https://github.com/llvm-mirror/clang/blob/master/include/clang/Basic/Builtins.def
 * https://github.com/llvm/llvm-project/blob/main/clang/include/clang/Basic/BuiltinsWebAssembly.def
 */
inline f32 floorf(f32 value) { return __builtin_floorf(value); }
inline f64 floor(f64 value){ return __builtin_floor(value);  }

inline f32 sqrtf(f32 value)  { return __builtin_sqrtf(value);  }
inline f64 sqrt(f64 value) { return __builtin_sqrt(value);   }

inline f32 ceilf(f32 value)  { return __builtin_ceilf(value);  }
inline f64 ceil(f64 value) { return __builtin_ceil(value);   }

inline f32 absf(f32 value)   { return __builtin_fabsf(value);  }
inline f64 abs(f64 value) { return __builtin_fabs(value);   }

inline f32 atanf(f32 x) {
  return STD_PI_4 * x - x * (absf(x) - 1) * (0.2447f + 0.0663f * absf(x));
}

inline f32 atan2f(f32 y, f32 x) {
	if (x >= 0) { // -pi/2 .. pi/2
		if (y >= 0) { // 0 .. pi/2
			if (y < x) { // 0 .. pi/4
				return atanf(y / x);
			} else { // pi/4 .. pi/2
				return STD_PI_2 - atanf(x / y);
			}
		} else {
			if (-y < x) { // -pi/4 .. 0
				return atanf(y / x);
			} else { // -pi/2 .. -pi/4
				return -STD_PI_2 - atanf(x / y);
			}
		}
	} else { // -pi..-pi/2, pi/2..pi
		if (y >= 0) { // pi/2 .. pi
			if (y < -x) { // pi*3/4 .. pi
				return atanf(y / x) + STD_PI;
			} else { // pi/2 .. pi*3/4
				return STD_PI_2 - atanf(x / y);
			}
		} else { // -pi .. -pi/2
			if (-y < -x) { // -pi .. -pi*3/4
				return atanf(y / x) - STD_PI;
			} else { // -pi*3/4 .. -pi/2
				return -STD_PI_2 - atanf(x / y);
			}
		}
	}
}

inline s32 truncToS32(f32 value) { return __builtin_wasm_trunc_saturate_s_i32_f32(value); }
inline s32 truncToS32(f64 value) { return __builtin_wasm_trunc_saturate_s_i32_f64(value); }
inline s64 truncToS64(f32 value) { return __builtin_wasm_trunc_saturate_s_i64_f32(value); }
inline s64 truncToS64(f64 value) { return __builtin_wasm_trunc_saturate_s_i64_f64(value); }

inline bool32 isNaN(f64 value) { return __builtin_isnan(value);   }
inline bool32 isNaN(f32 value) { return __builtin_isnan(value);   }

inline f32 minf(f32 a, f32 b) { return __builtin_wasm_min_f32(a, b); }
inline f32 maxf(f32 a, f32 b) { return __builtin_wasm_max_f32(a, b); }

inline f32 clampf(f32 a, f32 min, f32 max) {
  return minf(maxf(a, min), max);
}

inline f32 roundf([[maybe_unused]] f32 value)  {
  f32 intPart = (f32)(s32)value;
  if (absf(value - intPart) > 0.5f) {
    return intPart + ((value < 0) ? -1.0f : 1.0f);
  }
  return intPart;
}

inline f64 round(f64 value)  {
  f64 intPart = (f64)(s64)value;
  if (abs(value - intPart) > 0.5) {
    return intPart + ((value < 0) ? -1.0 : 1.0);
  }
  return intPart;
}

// Other hacky stuff:

// disables copy or move constructors for a given class, should be put in the class body
#define CLASS_DISABLE_COPY_AND_MOVE(T) \
    T(const T&) = delete; \
    T(T&&) = delete; \
    T& operator=(const T&) = delete; \
    T& operator=(T&&) = delete; \

// turns the class into a singleton, must be places in the private section
//static T instance;
#define CLASS_STATIC_INSTANCE(T) \
    \
    public: \
      static T& getInstance() { \
        static T instance; \
        return instance; \
      } \
    private: \
    \

// converts any number data-type's bit-representation into an unsigned-integer
// This can be used to force a f32 into a int.
inline  u8 bit_cast_int( u8 value) { return      value; }
inline  u8 bit_cast_int( s8 value) { return  (u8)value; }
inline u16 bit_cast_int(u16 value) { return      value; }
inline u16 bit_cast_int(s16 value) { return (u16)value; }
inline u32 bit_cast_int(u32 value) { return      value; }
inline u32 bit_cast_int(s32 value) { return (u32)value; }
inline u64 bit_cast_int(u64 value) { return      value; }
inline u64 bit_cast_int(s64 value) { return (u64)value; }
inline u32 bit_cast_int(f32 value) { return std::bit_cast<u32>(value); }
inline u64 bit_cast_int(f64 value) { return std::bit_cast<u64>(value); }
