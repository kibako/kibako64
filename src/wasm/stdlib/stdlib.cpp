/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "../main.h"
#include "./stdlib.h"

extern "C" {
  void* memset(void* ptr, s32 value, u32 numBytes)
  {
    #if __has_builtin(__builtin_memset)
      return __builtin_memset(ptr, value, numBytes);
    #else
      u8* ptrCurrent = (u8*)ptr;
      for(u8* ptrEnd = ptrCurrent + numBytes; ptrCurrent<ptrEnd; ++ptrCurrent) {
        *ptrCurrent = (u8)value;
      }
      
      return ptr; 
    #endif
  }

  inline void* memcpy(void* dst, void* src, u32 numBytes)
  {
    #if __has_builtin(__builtin_mempcpy)
      return __builtin_mempcpy(dst, src, numBytes);
    #else
      u8* srcPtr = (u8*)src;
      u8* dstPtr = (u8*)dst;
      u8* srcPtrEnd = dstPtr + numBytes;
      while(srcPtr < srcPtrEnd)
      {
        *dstPtr = *srcPtr;
        ++srcPtr;
        ++dstPtr;
      }
      return dst;
    #endif
  }
}
