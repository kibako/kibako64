/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

/**
 * Global build flags. 
 * Don't use TARGET_DEBUG directly, define feature toggles here.
 */
#ifdef TARGET_DEBUG
  #define FEATURE_MIPS_EXTENSION 1 // slower if enabled, custom non-standard kibako64 MIPS-instructions
  #define FEATURE_DEBUG_LOG 1 // slower if enabled, enabled all printf variants
  #define FEATURE_STRICT_DELAY_SLOT 1 // slower if enabled, check and warns if delay slots are used while already in one
  #define FEATURE_OPCODE_NAME_TABLE 1 // bigger executable size if enabled, used for debugging and unit-testing
  #define FEATURE_DEBUGGER 1 // enables debugging functions, only in NodeJS mode, slower if enabled
#endif

#define FEATURE_INSTRUCTION_CACHE 1
//#define FEATURE_PERFORMANCE_TIMER 1 // exposes functions to measure executino time, slower if enabled
//#define FEATURE_STRICT_REGISTER_IDX 1 // slower if enabled, checks OOB CPU-register access (might cause corruption) @TODO test more
//#define FEATURE_MEM_OOB_CHECK 1 // memBus checks for OOB writes, theoretically not needed, tiny bit slower if enabled
