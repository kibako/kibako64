/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

enum class RomCountryCode
{
	UNKNOWN = 0,
  BETA = '7',
  ASIAN = 'A',
  BRAZILIAN = 'B',
  CHINESE = 'C',
  GERMAN = 'D',
  NORTH_AMERICA = 'E',
  FRENCH = 'F',
  GATEWAY64_NTSC = 'G',
  DUTCH = 'H',
  ITALIAN = 'I',
  JAPAN = 'J',
  KOREAN = 'K',
  GATEWAY64_PAL = 'L',
  CANADIAN = 'N',
  EUROPEAN = 'P',
  SPANISH = 'S',
  AUSTRALIAN = 'U',
  SCANDINAVIAN = 'W',
  EUROPEAN_X = 'X',
  EUROPEAN_Y = 'Y',
};