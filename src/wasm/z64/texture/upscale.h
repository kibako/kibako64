/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "main.h"

namespace Z64::Texture
{
    WASM_EXPORT_NAMED(z64TextureUpscaleClamp)
    u8* upscaleClamp(u32 sizeInX, u32 sizeInY, u32 sizeOutX, u32 sizeOutY);

    WASM_EXPORT_NAMED(z64TextureUpscaleWrapU)
    u8* upscaleWrapU(u32 sizeInX, u32 sizeInY, u32 sizeOutX, u32 sizeOutY);

    WASM_EXPORT_NAMED(z64TextureUpscaleWrapV)
    u8* upscaleWrapV(u32 sizeInX, u32 sizeInY, u32 sizeOutX, u32 sizeOutY);

    WASM_EXPORT_NAMED(z64TextureUpscaleWrapUV)
    u8* upscaleWrapUV(u32 sizeInX, u32 sizeInY, u32 sizeOutX, u32 sizeOutY);

    WASM_EXPORT_NAMED(z64TextureGetUpscaleInputPtr)
    u8* getUpscaleInputPtr();
}