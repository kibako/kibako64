/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#include "upscale.h"
#include "math/vec2.h"
#include "stdlib/stdlib.h"

//#define USE_SIMD 1

#ifdef USE_SIMD
  #include <wasm_simd128.h>
#endif

namespace
{
  const u32 MAX_TEXTURE_SIZE = 512;

  u8 bufferIn[MAX_TEXTURE_SIZE*MAX_TEXTURE_SIZE*4];
  u8 bufferOut[MAX_TEXTURE_SIZE*MAX_TEXTURE_SIZE*4];

  typedef struct {
      u8 r, g, b, a;
  } RGBA;

  template<bool WRAP_U, bool WRAP_V>
  u32 getIndex(u32 width, u32 height, u32 x, u32 y) {
    if constexpr (WRAP_U) {
      x = x % width;
    } else {
      x = x >= width ? (width-1) : x;
    }

    if constexpr (WRAP_V) {
      y = y % height;
    } else {
      y = y >= height ? (height-1) : y;
    }

    return (y * width + x) * 4;
  }

  template<bool WRAP_U, bool WRAP_V>
  void upscaleGeneric(u32 sizeInX, u32 sizeInY, u32 sizeOutX, u32 sizeOutY) {
    u8* buffOut = bufferOut;

    Math::Vec2 ratio{
      (f32)sizeInX / sizeOutX,
      (f32)sizeInY / sizeOutY
    };

    for (u32 i = 0; i < sizeOutY; i++) {
        for (u32 j = 0; j < sizeOutX; j++) {
            auto pf = ratio * Math::Vec2{j, i};
            u32 px = (u32)pf.x();
            u32 py = (u32)pf.y();
            auto d = pf - Math::Vec2{px, py};

            RGBA p1, p2, p3, p4;

            p1.r = bufferIn[getIndex<WRAP_U, WRAP_V>(sizeInX, sizeInY, px, py) + 0];
            p1.g = bufferIn[getIndex<WRAP_U, WRAP_V>(sizeInX, sizeInY, px, py) + 1];
            p1.b = bufferIn[getIndex<WRAP_U, WRAP_V>(sizeInX, sizeInY, px, py) + 2];
            p1.a = bufferIn[getIndex<WRAP_U, WRAP_V>(sizeInX, sizeInY, px, py) + 3];

            p2.r = bufferIn[getIndex<WRAP_U, WRAP_V>(sizeInX, sizeInY, px + 1u, py) + 0u];
            p2.g = bufferIn[getIndex<WRAP_U, WRAP_V>(sizeInX, sizeInY, px + 1u, py) + 1u];
            p2.b = bufferIn[getIndex<WRAP_U, WRAP_V>(sizeInX, sizeInY, px + 1u, py) + 2u];
            p2.a = bufferIn[getIndex<WRAP_U, WRAP_V>(sizeInX, sizeInY, px + 1u, py) + 3u];

            p3.r = bufferIn[getIndex<WRAP_U, WRAP_V>(sizeInX, sizeInY, px, py + 1) + 0];
            p3.g = bufferIn[getIndex<WRAP_U, WRAP_V>(sizeInX, sizeInY, px, py + 1) + 1];
            p3.b = bufferIn[getIndex<WRAP_U, WRAP_V>(sizeInX, sizeInY, px, py + 1) + 2];
            p3.a = bufferIn[getIndex<WRAP_U, WRAP_V>(sizeInX, sizeInY, px, py + 1) + 3];

            p4.r = bufferIn[getIndex<WRAP_U, WRAP_V>(sizeInX, sizeInY, px + 1, py + 1) + 0];
            p4.g = bufferIn[getIndex<WRAP_U, WRAP_V>(sizeInX, sizeInY, px + 1, py + 1) + 1];
            p4.b = bufferIn[getIndex<WRAP_U, WRAP_V>(sizeInX, sizeInY, px + 1, py + 1) + 2];
            p4.a = bufferIn[getIndex<WRAP_U, WRAP_V>(sizeInX, sizeInY, px + 1, py + 1) + 3];

            *(buffOut++) = (u8)((p1.r * (1 - d.x()) * (1 - d.y())) + (p2.r * d.x() * (1 - d.y())) +
                                       (p3.r * (1 - d.x()) * d.y()) + (p4.r * d.x() * d.y()));
            *(buffOut++) = (u8)((p1.g * (1 - d.x()) * (1 - d.y())) + (p2.g * d.x() * (1 - d.y())) +
                                       (p3.g * (1 - d.x()) * d.y()) + (p4.g * d.x() * d.y()));
            *(buffOut++) = (u8)((p1.b * (1 - d.x()) * (1 - d.y())) + (p2.b * d.x() * (1 - d.y())) +
                                       (p3.b * (1 - d.x()) * d.y()) + (p4.b * d.x() * d.y()));
            *(buffOut++) = (u8)((p1.a * (1 - d.x()) * (1 - d.y())) + (p2.a * d.x() * (1 - d.y())) +
                                       (p3.a * (1 - d.x()) * d.y()) + (p4.a * d.x() * d.y()));
        }
    }
  }
}

u8* Z64::Texture::upscaleClamp(u32 sizeInX, u32 sizeInY, u32 sizeOutX, u32 sizeOutY) {
  upscaleGeneric<false, false>(sizeInX, sizeInY, sizeOutX, sizeOutY);
  return bufferOut;
}

u8* Z64::Texture::upscaleWrapU(u32 sizeInX, u32 sizeInY, u32 sizeOutX, u32 sizeOutY) {
  upscaleGeneric<true, false>(sizeInX, sizeInY, sizeOutX, sizeOutY);
  return bufferOut;
}

u8* Z64::Texture::upscaleWrapV(u32 sizeInX, u32 sizeInY, u32 sizeOutX, u32 sizeOutY) {
  upscaleGeneric<false, true>(sizeInX, sizeInY, sizeOutX, sizeOutY);
  return bufferOut;
}

u8* Z64::Texture::upscaleWrapUV(u32 sizeInX, u32 sizeInY, u32 sizeOutX, u32 sizeOutY) {
  upscaleGeneric<true, true>(sizeInX, sizeInY, sizeOutX, sizeOutY);
  return bufferOut;
}

u8 *Z64::Texture::getUpscaleInputPtr() {
  return bufferIn;
}
