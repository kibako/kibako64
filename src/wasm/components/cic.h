/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

namespace CIC
{
  enum class ChipType
  {
    CIC_6101,
    CIC_6102,
    CIC_6103,
    CIC_6104,
    CIC_6105,
    CIC_6106,

    SIZE
  };

  ChipType getChipTypeFromRom(ROM &rom);
  const char* getChipName(ChipType cic);
};