/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "../../main.h"
#include "memoryBus.h"

namespace {
    // first 16bit of an addressed can be used to directly lookup the handler
    constexpr u32 MEM_LOOKUP_SIZE = 0x1000 + 1;

    MemoryRegion *memHandlerLookupTable[MEM_LOOKUP_SIZE] = {nullptr};
}

void MemoryBus::rebuildLookupTable()
{
  MemoryRegion* sections[] = {
    (MemoryRegion*) &rspMem,
    (MemoryRegion*) &romHeaderMem,
    (MemoryRegion*) &dma,
    (MemoryRegion*) &spReg,
    (MemoryRegion*) &ramReg,
    (MemoryRegion*) &ramInterfaceReg,
    (MemoryRegion*) &videoInterfaceReg,
    (MemoryRegion*) &mipsInterfaceReg,
    (MemoryRegion*) &pif,
    (MemoryRegion*) &pifRam,
  };

  for(u32 lookupAddress=0; lookupAddress<MEM_LOOKUP_SIZE; ++lookupAddress) 
  {
    memHandlerLookupTable[lookupAddress] = nullptr;

    for(auto section : sections)
    {
      const u32 sectionStart = (section->getMemoryOffset() >> 16);
      const u32 sectionEnd = ((section->getMemoryEnd()-1) >> 16);

      if(lookupAddress >= sectionStart && lookupAddress <= sectionEnd)
      {
        memHandlerLookupTable[lookupAddress] = section;
        break;
      }
    }
  }
  //printMemoryMap();
}

void MemoryBus::reset()
{
  rebuildLookupTable();
  tlb.reset();
  ramReg.reset();
}

void MemoryBus::logState()
{
  #ifdef FEATURE_DEBUG_LOG
    //cpu.logState(); // might cause an infinite-loop on a bad address, since it fetches the current word again 
    printf_info("CPU-PC: 0x%08X", cpu.getPc());
  #endif
}

void MemoryBus::printMemoryMap()
{
  for(u32 lookupAddress=0; lookupAddress<MEM_LOOKUP_SIZE; ++lookupAddress) 
  {
    if(memHandlerLookupTable[lookupAddress]) {
      auto section = memHandlerLookupTable[lookupAddress];
      const u32 sectionStart = section->getMemoryOffset();
      const u32 sectionEnd = section->getMemoryEnd();
      printf("Lookup [0x%04X] 0x%08X -> %s (%08X - %08X)\n", lookupAddress, ((lookupAddress) << 16), section->getName(), sectionStart, sectionEnd);
    }
  }
}

// Performance: returning a reference / replace nullptr with a dummy section to avoid null-checks is NOT faster
MemoryRegion* MemoryBus::getMemoryHandler(u32 address)
{
  u32 lookupIdx = (address >> 16);
  if(lookupIdx < MEM_LOOKUP_SIZE) {
    MemoryRegion *mem = memHandlerLookupTable[lookupIdx];
    return (mem && mem->isInMemory(address)) ? mem : nullptr;
  }

  // after end of table:
  if(romHeaderMem.isInMemory(address))return &romHeaderMem;
  return nullptr;
}
