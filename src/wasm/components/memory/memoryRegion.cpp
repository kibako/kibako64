/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "memoryRegion.h"
#include "../../byteswap.h"

void MemoryRegion::writeWordDirect(u32 address, u32 value)
{
  dataPointerWord[address >> 2]  = byteswap(value);
}

bool32 MemoryRegion::copyTo(u32 srcOffset, u32 size, u8 *dstRegion, u32 dstRegionSize, u32 dstOffset)
{
  if((dstOffset+size) > dstRegionSize) {
    printf_error("OOB-copy from '%s' 0x%08X to 0x%08X(!), size: %08X", getName(), srcOffset, dstOffset, size);
    return false;
  }

  if((srcOffset+size) > getSize()) {
    printf_error("OOB-copy from '%s' 0x%08X(!) to '%s' 0x%08X, size: %08X", getName(), srcOffset, dstOffset, size);
    return false;
  }

  memcpy(
    dstRegion + dstOffset,
    getDataPointer() + srcOffset,
    size
  );

  return true;
}

// Read and Writes derived from / using the 32-bit versions
// These do not touch the memory directly

template<>
u8 MemoryRegion::read(u32 address)
{
  return dataPointer[address];
}

template<>
u16 MemoryRegion::read(u32 address)
{
  u32 addressU32 = address & (~0b11);
  u32 word = read<u32>(addressU32);

  return (address & 0b11)
   ? (u16)word
   : (word >> 16) & 0xFFFF;
}

template<>
u32 MemoryRegion::read(u32 address)
{
  #ifdef FEATURE_MEM_OOB_CHECK
    if(address >= size){
      print_error("Memory-OOB read %08X in '%s'", address, getName());
      return 0;
    }
  #endif 

  return byteswap(dataPointerWord[address >> 2]);
}

template<>
u64 MemoryRegion::read(u32 address)
{
    return ((u64)read<u32>(address) << 32)
         | ((u64)read<u32>(address + 4));
}

void MemoryRegion::write(u32 address, u8 value)
{
    u32 addressU32 = address & (~0b11);
    u32 byteOffset = 3 - (address & 0b11); // shift amount in bytes
    byteOffset <<= 3; // bytes to bits

    write(
      addressU32,
      read<u32>(addressU32) & ~(0xFF << byteOffset) | (value << byteOffset)
    );
}

void MemoryRegion::write(u32 address, u16 value)
{
  u32 addressU32 = address & (~0b11);
  u32 newValue = read<u32>(addressU32);

  if(address & 0b11) {
    newValue &= 0xFFFF'0000;
    newValue |= value;
  } else {
    newValue &= 0x0000'FFFF;
    newValue |= (value << 16);
  }

  write(addressU32, newValue);
}

void MemoryRegion::write(u32 address, u32 value)
{
  #ifdef FEATURE_MEM_OOB_CHECK
    if((address + sizeof(u32)) > size) {
      print_error("Memory-OOB write %08X in '%s'", address, getName());
    }
  #endif

  if (writeFunction) {
    writeFunction(address, value);
  } else {
    writeWordDirect(address, value);
  }
}

void MemoryRegion::write(u32 address, u64 value)
{
  write(address, (u32)(value >> 32));
  write(address + 4, (u32)value);
}

void MemoryRegion::patchWordAnd(u32 address, u32 valueAnd) {
  writeWordDirect(address, read<u32>(address) & valueAnd);
}

void MemoryRegion::patchWordOr(u32 address, u32 valueAnd) {
  writeWordDirect(address, read<u32>(address) | valueAnd);
}
