/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../../types.h"
#include "../../utils/logging.h"

class MemoryRegion;

typedef void (*memoryWriteFunc)(u32 address, u32 word);

/**
 *  Generic class to map a read & write memory region, emulating big-endian behaviour.
 * 
 *  NOTE: Since some regions behave differently on read and write,
 *  Writing non-word values (u8, u16, u64, f64) might produce incorrect results in these cases.
 */
class MemoryRegion
{
  private:
    CLASS_DISABLE_COPY_AND_MOVE(MemoryRegion)

    u8* const dataPointer;
    u32* const dataPointerWord;

    const u32 memoryOffset;
    const u32 size;

    const char* const name;

    memoryWriteFunc writeFunction;

  protected:
    void writeWordDirect(u32 address, u32 value);

  public:
    constexpr MemoryRegion(u32* const dataPointer, u32 memoryOffset, u32 size, const char* const name, memoryWriteFunc writeFunction = nullptr)
      : dataPointer((u8*)dataPointer),
        dataPointerWord(dataPointer),
        memoryOffset(memoryOffset),
        size(size), name(name),
        writeFunction(writeFunction)
    {}

    // Returns pointer to the data array, this is a real pointer inside this program
    constexpr u8* getDataPointer() {
      return dataPointer;
    }

    constexpr u32* getDataPointerWord() {
      return dataPointerWord;
    }

    // Returns the offset in N64 memory where this region is located
    constexpr u32 getMemoryOffset() const {
      return memoryOffset;
    }

    // Returns the end in the N64 memory.
    // NOTE: this is exclusive, so it wil point to the byte after the end
    constexpr u32 getMemoryEnd() const {
      return memoryOffset + size;
    }

    // Returns the size in bytes of this region
    constexpr u32 getSize() const {
      return size;
    }

    // Checks if an address is inside the N64 memory region
    bool32 isInMemory(u32 address) const {
      return address >= getMemoryOffset() && address < getMemoryEnd();
    }

    // same as isInMemory, but as a const-expression. address needs to be constant.
    constexpr bool32 isInMemoryConst(const u32 address) const {
      return address >= getMemoryOffset() && address < getMemoryEnd();
    }

    constexpr const char* getName() {
      return name;
    }

    // fills the whole memory-region with zeros
    inline void clear(){
      memset(getDataPointer(), 0, getSize());
    }

    // Copies data of this region into another, performs OOB checks.
    // This only copies data and does not change the byte-order.
    bool32 copyTo(u32 srcOffset, u32 size, u8 *dstRegion, u32 dstRegionSize, u32 dstOffset = 0);

    template<typename T>
    T read([[maybe_unused]] u32 address) { return 0; }

    void write(u32 address, u8 value);
    void write(u32 address, u16 value);
    void write(u32 address, u32 value);
    void write(u32 address, u64 value);

    // Convenience functions

    void patchWordAnd(u32 address, u32 valueAnd);
    void patchWordOr(u32 address, u32 valueAnd);
};

template<> u8 MemoryRegion::read(u32 address);
template<> u16 MemoryRegion::read(u32 address);
template<> u32 MemoryRegion::read(u32 address);
template<> u64 MemoryRegion::read(u32 address);