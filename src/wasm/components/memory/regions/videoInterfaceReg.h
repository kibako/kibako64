/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../memoryRegion.h"

enum class VIRegisterOffset {
  STATUS           = 0x00,
  ORIGIN           = 0x04,
  PIXEL_WIDTH      = 0x08,
  VLINE_INTERRUPT  = 0x0C,
  VLINE_CURRENT    = 0x10,
  TIMING           = 0x14,
  SYNC_VERTICAL    = 0x18,
  SYNC_HORIZONTAL  = 0x1C,
  SYNC_LEAP_HORIZ  = 0x20,
  VIDEO_HORIZONTAL = 0x24,
  VIDEO_VERTICAL   = 0x28,
  BURST_VERTICAL   = 0x2C,
  SCALE_X          = 0x30,
  SCALE_Y          = 0x34,

  SIZE,
};

class VideoInterface : public MemoryRegion
{
  private:
    CLASS_DISABLE_COPY_AND_MOVE(VideoInterface)

    u32 data[(u32)VIRegisterOffset::SIZE];

    u32 currentLine;
    u32 interruptLine;

    void checkForInterrupt();

    static void writeWordWrap(u32 address, u32 value);

  public:
    constexpr VideoInterface() : MemoryRegion(data, 0x0440'0000, sizeof(data), "Video-Registers", writeWordWrap),
      data{0}, currentLine(0),
      interruptLine(0)
    {}

    void writeWord(u32 address, u32 value);
    void reset();
    void incrementCurrentLine();

    void printState();
};
