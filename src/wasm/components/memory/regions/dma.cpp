/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "dma.h"
#include "../../../globals.h"
#include "cache.h"

#define DMA_STATE_READY 0
#define DMA_STATE_BUSY    (1 << 1)
#define DMA_STATE_IO_BUSY (1 << 2)
#define DMA_STATE_ERROR   (1 << 3)

#define ADDRESS_PTR_RAM         0x00
#define ADDRESS_PTR_ROM         0x04
#define ADDRESS_SIZE_RAM_TO_ROM 0x08
#define ADDRESS_SIZE_ROM_TO_RAM 0x0C
#define ADDRESS_STATUS          0x10

const char piRegisterNames[][21] = {
  "PI_DRAM_ADDR_REG", "PI_CART_ADDR_REG",
  "PI_RD_LEN_REG", "PI_WR_LEN_REG", "PI_STATUS_REG",
  "PI_BSD_DOM1_LAT_RE", "PI_BSD_DOM1_PWD_REG", "PI_BSD_DOM1_PGS_REG", "PI_BSD_DOM1_RLS_REG",
  "PI_BSD_DOM2_LAT_REG", "PI_BSD_DOM2_PWD_REG", "PI_BSD_DOM2_PGS_REG", "PI_BSD_DOM2_RLS_REG",
};

void DMA::writeWordWrap(u32 address, u32 value) {
  memBus.dma.writeWord(address, value);
}

void DMA::writeWord(u32 address, u32 value)
{
  printf_info("Write to DMA/PI: 0x%08X = %08X (%s)", address, value, piRegisterNames[address >> 2]);

  if(address == ADDRESS_STATUS) {
    if(value & 0b01) {
      //@TODO add constoller-reset / abort OP
    }

    if(value & 0b10) {
      system.interruptHandler.clearRcpInterrupt(Interrupt::RcpType::PI);
    }

    return setStatusRegister(DMA_STATE_READY);
  }

  writeWordDirect(address, value);

  if(address == ADDRESS_SIZE_ROM_TO_RAM) {
    u32 copySize = read<u32>(ADDRESS_SIZE_ROM_TO_RAM) + 1;

    //write(ADDRESS_STATUS, DMA_STATE_BUSY); // only needed if async

    u32 src = read<u32>(ADDRESS_PTR_ROM);
    u32 dst = read<u32>(ADDRESS_PTR_RAM);

    if(src >= 0x1000'0000) {
      src &= 0x0FFF'FFFF;

      if(!memBus.rom.copyTo(src, copySize, RAM_PTR, RAM_SIZE, dst)) {
        printf_error("DMA: Copy OOB: %08X bytes from ROM:%08X to RAM:%08X", copySize, src, dst);
        return setStatusRegister(DMA_STATE_ERROR);
      } else {
        #ifdef FEATURE_INSTRUCTION_CACHE
          Cache::clearCache(dst, copySize);
        #endif
      }

      printf_info("DMA: Copied: %08X bytes from ROM:%08X to RAM:%08X", copySize, src, dst);

      system.interruptHandler.interrupt(Interrupt::Type::RCP, Interrupt::RcpType::PI);

    } else {
      printf_error("DMA: unmapped ROM address: %08X", src);
      return setStatusRegister(DMA_STATE_ERROR); // @TODO remove after implementation
    }

    write(ADDRESS_STATUS, (u32)DMA_STATE_READY);
  }
 
  if(address == ADDRESS_SIZE_RAM_TO_ROM) {
    print_error("DMA: copy RAM to ROM not implemented!");
    return setStatusRegister(DMA_STATE_ERROR); // @TODO remove after implementation
  }
}

void DMA::setStatusRegister(u8 value)
{
  writeWordDirect(ADDRESS_STATUS, value);
}
