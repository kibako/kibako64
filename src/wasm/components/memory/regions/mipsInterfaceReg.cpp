/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "mipsInterfaceReg.h"
#include "../../../utils/logging.h"
#include "../../../globals.h"

#define REG_INIT           (0 * sizeof(u32))
#define REG_VERSION        (1 * sizeof(u32))
#define REG_INTERRUPT      (2 * sizeof(u32))
#define REG_INTERRUPT_MASK (3 * sizeof(u32))

void MipsInterface::writeWordWrap(u32 address, u32 value) {
  memBus.mipsInterfaceReg.writeWord(address, value);
}

void MipsInterface::setInterruptBit(Interrupt::RcpType bit) 
{
  patchWordOr(REG_INTERRUPT, (u32)bit);
}

void MipsInterface::clearInterruptBit(Interrupt::RcpType bit)
{
  patchWordAnd(REG_INTERRUPT, ~(u32)bit);
}

u32 MipsInterface::getMaskedInterruptBits()
{
  return read<u32>(REG_INTERRUPT) & read<u32>(REG_INTERRUPT_MASK);
}

#define BIT_CLEAR_SET(bitIdx) \
  if(value & (1 << (bitIdx*2    )))maskValue &= ~(1 << bitIdx); \
  if(value & (1 << (bitIdx*2 + 1)))maskValue |=  (1 << bitIdx);


void MipsInterface::writeWord(u32 address, u32 value)
{
  printf_trace("Write MIPS-REG: %08X = %08X", address, value);

  if(address == REG_INIT) 
  {
    if(value & (1 << 11)) {
      system.interruptHandler.clearRcpInterrupt(Interrupt::RcpType::DP);
    } else {
      print_warn("Write MI_INIT unimplemented");
    }
  }
  else if(address == REG_INTERRUPT_MASK) 
  {
    u32 maskValue = read<u32>(REG_INTERRUPT_MASK);

    BIT_CLEAR_SET(0)
    BIT_CLEAR_SET(1)
    BIT_CLEAR_SET(2)
    BIT_CLEAR_SET(3)
    BIT_CLEAR_SET(4)
    BIT_CLEAR_SET(5)

    printf_trace("Write MIPS-Interrupt Mask: %08X -> %08X | PC: %08X", read<u32>(REG_INTERRUPT_MASK), maskValue, cpu.getPc());

    writeWordDirect(REG_INTERRUPT_MASK, maskValue);

    // trigger the interrupt check now that we changed the register by hand
    system.interruptHandler.clearRcpInterrupt(Interrupt::RcpType::NONE);
    return;
  }

  printf_info("Write MIPS-REG: %08X = %08X", address, value);
  writeWordDirect(address, value);

  if(address == REG_INTERRUPT) {
    system.interruptHandler.clearRcpInterrupt(Interrupt::RcpType::NONE);
  }
}

