/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../../../types.h"
#include "../../../globalPointer.h"
#include "../../../countryCodes.h"
#include "../memoryRegion.h"

#define BOM_MARKER_LITTLE 0x37

enum class ROMTvEncoding 
{
  PAL = 1,
  NTSC = 2,
  SECAM = 3
};

struct ROMHeader
{
  Endian endian;
  u32 clockRate;
  u32 bootPc;
  u8 romName[21];
  u16 id;
  RomCountryCode country;
  u8 version;
  ROMTvEncoding tvEncoding;
};

class ROM : public MemoryRegion
{
  private:
    CLASS_DISABLE_COPY_AND_MOVE(ROM)

    static void writeWordWrap(u32 address, u32 value);

  public:
    constexpr ROM() : MemoryRegion(ROM_PTR, 0xFFFF'FFFF, ROM_SIZE, "ROM", writeWordWrap)
    {}

    ROMHeader readHeader();
    void printHeader();
};
