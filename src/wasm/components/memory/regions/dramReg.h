/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../memoryRegion.h"

enum class DRAMRegOffset {
  DEVICE_TYPE  = 0x00,
  DEVICE_ID    = 0x04,
  DELAY        = 0x08,
  MODE         = 0x0C,
  REF_INTERVAL = 0x10,
  REF_ROW      = 0x14,
  RAS_INTERVAL = 0x18,
  MIN_INTERVAL = 0x1C,
  ADDR_SELECT  = 0x20,
  DEVICE_MANUF = 0x24,
};

class DRAMReg : public MemoryRegion
{
  private:
    CLASS_DISABLE_COPY_AND_MOVE(DRAMReg)

    u32 data[1_MB / 4]{0};

    static void writeWordWrap(u32 address, u32 value);

  public:
    constexpr DRAMReg() :
      MemoryRegion(data, 0x03F0'0000, sizeof(data), "DRAM-Registers", writeWordWrap)
    {}

    void reset()
    {
      // Docs: https://n64brew.dev/wiki/RDRAM#RDRAM_registers
      // Values from PeterLemon's tests
      writeWordDirect((u32)DRAMRegOffset::DEVICE_TYPE, 0xB419'0010);
      writeWordDirect((u32)DRAMRegOffset::DELAY,       0x2B3B'1A0B);
      writeWordDirect((u32)DRAMRegOffset::RAS_INTERVAL,0x101C'0A04);
    }
};
