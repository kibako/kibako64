/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "pif.h"
#include "../../../globals.h"
#include "cache.h"

const char SiRegisterNames[][12] = {"RAM_ADDRESS", "READ_64B", "UNUSED_2", "UNUSED_3", "WRITE_64", "UNUSED_5", "STATUS", "UNUSED_7"};

#define PIF_ERROR_FLAG_WRONG_SIZE  0x40
#define PIF_ERROR_FLAG_UNAVAILABLE 0x80

#define CONTROLLER_ID 0x05

void PIF::PifChip::writeWordWrap(u32 address, u32 value) {
  memBus.pif.writeWord(address, value);
}

void PIF::PifChip::writeWord(u32 address, u32 value)
{ 
  auto reg = (SiRegisterOffset)address;
  if(reg == SiRegisterOffset::STATUS)
  {
    print_trace("Write to SI_STATUS_REG, clear interrput RCP-SI, SI_REG interrupt");
    u32 newStatus = read<u32>(SiRegisterOffset::STATUS);
    newStatus &= ~(SiStatusBits::INTERRUPT);
    writeWordDirect(SiRegisterOffset::STATUS, newStatus);

    system.interruptHandler.clearRcpInterrupt(Interrupt::RcpType::SI);
    return;
  } 

  writeWordDirect(reg, value);

  u32 memoryAddress = read<u32>(SiRegisterOffset::RAM_ADDRESS);

  if(reg == SiRegisterOffset::READ_64B) {
    copyPifRamToDram(memoryAddress);
  } else if(reg == SiRegisterOffset::WRITE_64) {
    copyDramToPifRam(memoryAddress);
  }
}

void PIF::PifChip::copyDramToPifRam(u32 address)
{
  printf_info("PIF: DRAM->PIF, DRAM address '0x%08X'", address);
  address = memBus.normalizeAddress(address);

  if(RAM::isInMemory(address)) {
    RAM::copyTo(address, PIF_RAM_SIZE, memBus.pifRam);
    runIfNecessary();
  } else {
    printf_warn("PIF: DRAM->PIF, RAM_ADDRESS is not a DRAM address '0x%08X'", address);
  }
}

void PIF::PifChip::copyPifRamToDram(u32 address)
{
  printf_info("PIF: PIF->DRAM, DRAM address '0x%08X'", address);
  address = memBus.normalizeAddress(address);

  if(RAM::isInMemory(address)) {
    // @TODO clear disassembler cache
    memBus.pifRam.copyTo(0, PIF_RAM_SIZE, RAM_PTR, RAM_SIZE, address);
    #ifdef FEATURE_INSTRUCTION_CACHE
      Cache::clearCache(address, PIF_RAM_SIZE);
    #endif
  } else {
    printf_warn("PIF: PIF->DRAM, RAM_ADDRESS is not a DRAM address '0x%08X'", address);
  }
}

u8 PIF::PifChip::readNextByte() {
  return memBus.pifRam.read<u8>(ramOffset++);
}

void PIF::PifChip::writeNextByte(u8 value) {
  return memBus.pifRam.write(ramOffset++, value);
}

void PIF::PifChip::writeErrorFlag(u8 offset, u8 flag) {
  memBus.pifRam.write(offset, (u8)(memBus.pifRam.read<u8>(offset) | flag));
}

void PIF::PifChip::runIfNecessary()
{
  if(memBus.pifRam.isSemaphoreSet()) {
    run();
  } else {
    print_trace("PIF: skip run(), semaphore is not set");
  }
}

void PIF::PifChip::run()
{
  if(isRunning)return;
  isRunning = true;

  memBus.pifRam.printPifRam();

  u8 channel = 0;
  u8 byteType;
  u8 byteLength;
  u8 byteCmd;

  for(ramOffset = 0; ramOffset<memBus.pifRam.getSize();)
  {
    byteType = readNextByte();

    if(byteType == (u8)PifCommand::NOP)continue;
    if(byteType == (u8)PifCommand::END_MARKER)break;

    byteLength = readNextByte();
    byteCmd = readNextByte();

    switch((PifCommand)byteCmd)
    {
      case PifCommand::GAMEPAD_STATUS:
        printf_trace("PIF: Gamepad Status '%02X' from Controller '%d'", byteLength, channel);

        if(gameController[channel].isConnected()) 
        {
          // @TODO check if checking the size is necessary
          writeNextByte(CONTROLLER_ID);
          writeNextByte(0);
          writeNextByte(gameController[channel].mempackPopulated() ? 0x01 : 0x02);
          
        } else {
          writeErrorFlag(ramOffset - 2, PIF_ERROR_FLAG_UNAVAILABLE);
          ramOffset += byteLength;
        }

      break;

      case PifCommand::READ_BUTTONS: 
        printf_trace("PIF: Read Buttons '%02X' from Controller '%d'", byteLength, channel);

        if(gameController[channel].isConnected()) 
        {
          for(s32 i=0; i<byteLength; ++i) {
            writeNextByte(gameController[channel].getDataByte(i));
          }
        } else {
          writeErrorFlag(ramOffset - 2, PIF_ERROR_FLAG_UNAVAILABLE);
          ramOffset += byteLength;
        }

      break;

      default:
        printf_warn("PIF: unhandled command '%02X'", byteCmd);
      break;
    }

    channel = (channel + 1) & 0b11;
  }

  memBus.pifRam.disableSemaphore();
  memBus.pifRam.printPifRam();
  
  isRunning = false;
}
