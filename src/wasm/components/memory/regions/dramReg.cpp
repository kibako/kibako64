/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "dramReg.h"
#include "../../../globals.h"

void DRAMReg::writeWordWrap(u32 address, u32 value) {
  if(address == (u32)DRAMRegOffset::DEVICE_TYPE || address == (u32)DRAMRegOffset::DEVICE_MANUF) {
    print_error("Write to read-only region (DRAM-Registers)");
  } else {
    memBus.ramReg.writeWordDirect(address, value);
  }
}
