/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../../../types.h"
#include "../memoryRegion.h"

class DMA : public MemoryRegion
{
  private:
    CLASS_DISABLE_COPY_AND_MOVE(DMA)

    u32 data[13]{0};

  protected:
    static void writeWordWrap(u32 address, u32 value);

  public:
    constexpr DMA() : MemoryRegion(data, 0x0460'0000, sizeof(data), "DMA", writeWordWrap)
    {}

    void writeWord(u32 address, u32 value);
    void setStatusRegister(u8 value);
};
