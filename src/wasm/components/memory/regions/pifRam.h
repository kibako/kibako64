/**
* @copyright 2021 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../../../types.h"
#include "../memoryRegion.h"

namespace PIF
{
  constexpr u32 PIF_RAM_SIZE = 64;

  class PifRam : public MemoryRegion
  {
    private:
      CLASS_DISABLE_COPY_AND_MOVE(PifRam)

      u32 data[PIF_RAM_SIZE/4]{0};

      static void writeWordWrap(u32 address, u32 value);

    public:
      constexpr PifRam() : MemoryRegion(data, 0x1FC0'07C0, sizeof(data), "PifRam", writeWordWrap)
      {}

      void executeIfNecessary();

      void disableSemaphore();
      bool32 isSemaphoreSet();

      // Debugging functions
      void printPifRam();
  };
};