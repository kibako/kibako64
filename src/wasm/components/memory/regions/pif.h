/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../../../types.h"
#include "../memoryRegion.h"
#include "../../../peripherals/controller.h"
#include "./pifRam.h"

namespace PIF
{
  enum SiRegisterOffset {
    RAM_ADDRESS = 0 * sizeof(u32),
    READ_64B    = 1 * sizeof(u32),
    UNUSED_2    = 2 * sizeof(u32),
    UNUSED_3    = 3 * sizeof(u32),
    WRITE_64    = 4 * sizeof(u32),
    UNUSED_5    = 5 * sizeof(u32),
    STATUS      = 6 * sizeof(u32),
    UNUSED_7    = 7 * sizeof(u32),
  };

  enum SiStatusBits {
    DMA_BUSY     = (1 <<  0),
    IO_READ_BUSY = (1 <<  1),
    DMA_ERROR    = (1 <<  3),
    INTERRUPT    = (1 << 12),
  };

  enum class PifCommand {
    GAMEPAD_STATUS = 0x00,
    READ_BUTTONS   = 0x01,
    READ_MEMPACK   = 0x02,
    WRITE_MEMPACK  = 0x03,
    READ_EEPROM    = 0x04,
    WRITE_EEPROM   = 0x05,

    END_MARKER     = 0xFE,
    NOP            = 0xFF,
  };

  class PifChip : public MemoryRegion
  {
    private:
      CLASS_DISABLE_COPY_AND_MOVE(PifChip)

      u32 registerData[0x10];

      void copyDramToPifRam(u32 address);
      void copyPifRamToDram(u32 address);

      bool32 isRunning;

      u32 ramOffset = 0;

      u8 readNextByte();
      void writeNextByte(u8 value);
      void writeErrorFlag(u8 offset, u8 flag);

      static void writeWordWrap(u32 address, u32 value);

    public:
      constexpr PifChip() : MemoryRegion(registerData, 0x0480'0000, sizeof(registerData), "PIF", writeWordWrap), registerData{0}, isRunning(false),
        ramOffset(0)
      {}

      void writeWord(u32 address, u32 value);

      // Starts execution of all commands in the PIF RAM.
      void run();
      // same as run, but checks the semaphore first
      void runIfNecessary();
  };

};
