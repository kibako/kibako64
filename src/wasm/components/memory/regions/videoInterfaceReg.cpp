/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "videoInterfaceReg.h"
#include "../../../globals.h"

// number of full-lines per frame
#define FULL_LINE_MAX 262
#define HALF_LINE_MAX 524

// only for debugging
const char viRegisterNames[][18] = {"STATUS","ORIGIN","PIXEL_WIDTH","VLINE_INTERRUPT","VLINE_CURRENT","TIMING","SYNC_VERTICAL","SYNC_HORIZONTAL","SYNC_LEAP_HORIZ","VIDEO_HORIZONTAL","VIDEO_VERTICAL","BURST_VERTICAL","SCALE_X","SCALE_Y"};

void VideoInterface::writeWordWrap(u32 address, u32 value) {
  memBus.videoInterfaceReg.writeWord(address, value);
}

void VideoInterface::checkForInterrupt()
{
  if(currentLine == interruptLine) {
    printf_trace("Interrupt VI Line: %08X / %08X", currentLine, interruptLine);
    system.interruptHandler.interrupt(Interrupt::Type::RCP, Interrupt::RcpType::VI);
  }

  // @TODO check V-SYNC
}

void VideoInterface::reset()
{
  clear();
  interruptLine = 0x100;
  writeWordDirect((u32)VIRegisterOffset::VLINE_INTERRUPT, interruptLine << 1);
}

void VideoInterface::incrementCurrentLine()
{
  ++currentLine;
  if(currentLine >= FULL_LINE_MAX) {
    currentLine = 0;
  }

  printf_trace("New V-Line: %08X, interrupt @ %08X", currentLine, interruptLine);

  writeWordDirect((u32)VIRegisterOffset::VLINE_CURRENT, currentLine << 1); // real value must be the half line
  checkForInterrupt();
}

void VideoInterface::writeWord(u32 address, u32 value)
{  
  auto reg = (VIRegisterOffset)address;
  printf_trace("VI-Reg write: %s = %08X", viRegisterNames[(u32)reg>>2], value);

  if(reg == VIRegisterOffset::VLINE_CURRENT) 
  {
    print_trace("Write to VLINE_CURRENT, clear interrput RCP (VI)");
    system.interruptHandler.clearRcpInterrupt(Interrupt::RcpType::VI);
    return;
  }

  writeWordDirect(address, value);

  if(reg == VIRegisterOffset::VLINE_INTERRUPT)
  {
    printf_info("VI set interrupt line: %08X", interruptLine);
    interruptLine = value >> 1; // half line to full-line
    checkForInterrupt();
  }
}


void VideoInterface::printState()
{
  print_info("VI-Registers:");
  for(u32 i=0; i<14; ++i)
  {
    printf_info("  Reg[%02X] %s = %08X", i, viRegisterNames[i], read<u32>(i*4));
  }
}