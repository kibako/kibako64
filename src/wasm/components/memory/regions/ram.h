/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "byteswap.h"
#include "stdlib.h"
#include "globalPointer.h"
#include "memoryRegion.h"

namespace RAM
{
    constexpr bool32 isInMemory(u32 address) {
      return address < RAM_SIZE;
    }

    template<typename T>
    T read(u32 address) {
      if constexpr (std::is_same<T, u32>::value) {
        return byteswap(*(T*)address);
      }

      else if constexpr (std::is_same<T, u64>::value) {
        return ((u64)read<u32>(address) << 32)
             | ((u64)read<u32>(address + 4));
      }

      else if constexpr (std::is_same<T, u16>::value) {
        u32 addressU32 = address & (~0b11u);
        u32 word = read<u32>(addressU32);
        return (address & 0b11) ? (u16)word : (word >> 16) & 0xFFFF;
      }

      else if constexpr (std::is_same<T, u8>::value) {
        return *(T*)address;
      }

      else {
        static_assert(!sizeof(T*), "T is not supported");
      }
    }

    template<typename T>
    void write(u32 address, T value) {
      if constexpr (std::is_same<T, u32>::value) {
        *(T*)address = byteswap(value);
        return;
      }

      else if constexpr (std::is_same<T, u64>::value) {
        write(address, (u32)(value >> 32));
        write(address + 4, (u32)value);
        return;
      }

      else if constexpr (std::is_same<T, u16>::value) {
        u32 addressU32 = address & (~0b11u);
        u32 newValue = read<u32>(addressU32);

        if(address & 0b11) {
          newValue &= 0xFFFF'0000;
          newValue |= value;
        } else {
          newValue &= 0x0000'FFFF;
          newValue |= (value << 16);
        }

        write(addressU32, newValue);
        return;
      }

      else if constexpr (std::is_same<T, u8>::value) {
        u32 addressU32 = address & (~0b11u);
        u32 byteOffset = 3 - (address & 0b11); // shift amount in bytes
        byteOffset <<= 3; // bytes to bits

        write(
          addressU32,
          (read<u32>(addressU32) & ~(0xFF << byteOffset)) | (value << byteOffset)
        );
        return;
      }

      else {
        static_assert(!sizeof(T*), "T is not supported");
      }
    }

    inline bool32 copyTo(u32 srcOffset, u32 size, MemoryRegion &dstRegion, u32 dstOffset = 0) {
      if((dstOffset+size) > dstRegion.getSize()) {
          printf_error("OOB-copy from 'RAM' 0x%08X to 0x%08X(!), size: %08X", srcOffset, dstOffset, size);
          return false;
        }

        if((srcOffset+size) > RAM_SIZE) {
          printf_error("OOB-copy from 'RAM' 0x%08X(!) to 0x%08X, size: %08X", srcOffset, dstOffset, size);
          return false;
        }

        memcpy(
          dstRegion.getDataPointer() + dstOffset,
          (void*)srcOffset,
          size
        );

        return true;
    }
};