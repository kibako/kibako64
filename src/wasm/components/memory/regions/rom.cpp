/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "rom.h"

void ROM::writeWordWrap(u32 address, u32 value) {
  print_warn("Write to read-only region (ROM)");
}

ROMHeader ROM::readHeader()
{
  ROMHeader header;
  header.endian = read<u8>(0) == BOM_MARKER_LITTLE ? Endian::LITTLE : Endian::BIG;
  header.clockRate = read<u32>(0x04);
  header.bootPc = read<u32>(0x08);

  for(auto i=0; i<sizeof(header.romName); ++i) {
    header.romName[i] = read<u8>(0x20 + i);
  }
  header.romName[sizeof(header.romName) - 1] = 0;

  header.id = read<u16>(0x3C);
  header.country = (RomCountryCode)read<u8>(0x3E);
  header.version = read<u8>(0x3F);
  
  switch(header.country)
  {
    case RomCountryCode::UNKNOWN:
    case RomCountryCode::BETA:
    case RomCountryCode::ASIAN:
    case RomCountryCode::NORTH_AMERICA:
    case RomCountryCode::JAPAN:
      header.tvEncoding = ROMTvEncoding::NTSC;
    break;
    default:
      header.tvEncoding = ROMTvEncoding::PAL;
    break;
  }
  
  return header;
}

void ROM::printHeader()
{
  auto header = readHeader();
  printf_info("Rom-Header: \n"
    "    ID       : %c%c \n"
    "    name     : %s \n"
    "    version  : 0x%02X \n"
    "    country  : %c \n"
    "    Endian   : %s \n"
    "    clockRate: 0x%02X \n"
    "    bootPc   : 0x%08X \n"
    ,
    (header.id >> 8) & 0xFF, header.id & 0xFF,
    header.romName, header.version, header.country,
    header.endian == Endian::LITTLE ? "LE" : "BE",
    header.clockRate, header.bootPc
  );
}
