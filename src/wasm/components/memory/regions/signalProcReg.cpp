#include "signalProcReg.h"
#include "../../../instructions/instructions.h"
#include "../../../globals.h"

const char registerNames[][18] = {"MEM_ADDRESS","DRAM_ADDRESS","DMA_LENGTH_READ","DMA_LENGTH_WRITE","STATUS","DMA_FULL","DMA_BUSY","SP_SEMAPHORE"};
void printStatusWrite(u32 value);

void SignalProcReg::writeWordWrap(u32 address, u32 value) {
  memBus.spReg.writeWord(address, value);
}

void SignalProcReg::writeWord(u32 address, u32 value)
{
  printf_info("SignalProcReg: write @0x%08X = %08X", address, value);
  
  if(address < 0x20) {
    setRegisterU32((SignalProcRegOffset)address, value);
  } else if(address == 0x0004'0000) {
    printf_warn("SignalProcReg: set PC to %08X", value);
    writeWordDirect(address, value);
  }
}

#define BIT_CLEAR_SET(bitIdx, targetBit) \
  if(value & (1 << (bitIdx    )))maskValue &= ~(1 << targetBit); \
  if(value & (1 << (bitIdx + 1)))maskValue |=  (1 << targetBit);


void SignalProcReg::setRegisterU32(SignalProcRegOffset offset, u32 value)
{
  printf_warn("SignalProcReg: set register[%d | %s] = 0x%08X", offset, registerNames[(u32)offset >> 2], value);
  switch(offset)
  {
    case SignalProcRegOffset::STATUS:
    {
      printStatusWrite(value);
      
      u32 maskValue = read<u32>((u32)SignalProcRegOffset::STATUS);

      BIT_CLEAR_SET(0, 0) // halt
      if(value & (1 << 2))maskValue &= ~(0b10); // clear broke-flag

      if(value & (1 << 3)) {
        system.interruptHandler.clearRcpInterrupt(Interrupt::RcpType::SP);
      }

      if(value & (1 << 4)) {
        system.interruptHandler.interrupt(
          Interrupt::Type::RCP, Interrupt::RcpType::SP
        );
      }

      BIT_CLEAR_SET(5, 5) // single-step
      BIT_CLEAR_SET(7, 6) // interrupt on break

      // "signal" bit 0-7
      BIT_CLEAR_SET( 9,  7)
      BIT_CLEAR_SET(11,  8)
      BIT_CLEAR_SET(13,  9)
      BIT_CLEAR_SET(15, 10)
      BIT_CLEAR_SET(17, 11)
      BIT_CLEAR_SET(19, 12)
      BIT_CLEAR_SET(21, 13)
      BIT_CLEAR_SET(23, 14)

      writeWordDirect((u32)SignalProcRegOffset::STATUS, maskValue);
    }
    break;

    case SignalProcRegOffset::MEM_ADDRESS:
      printf_info("Set SP Memory Address: %08X (%cMEM)", value & 0b0111'1111'1111, (value & (1<<12)) ? 'I' : 'D');
      writeWordDirect((u32)SignalProcRegOffset::MEM_ADDRESS, value);
    break;

    default:
      writeWordDirect((u32)offset, value);
    break;
  }
}

void printStatusWrite(u32 value)
{
  printf_info("SP-STATUS: %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s", 
    value & (u32)SignalProcStatusWrite::CLEAR_HALT          ? "CLEAR_HALT" : "-",
    value & (u32)SignalProcStatusWrite::SET_HALT            ? "SET_HALT" : "-",
    value & (u32)SignalProcStatusWrite::CLEAR_BROKE         ? "CLEAR_BROKE" : "-",
    value & (u32)SignalProcStatusWrite::CLEAR_INTR          ? "CLEAR_INTR" : "-",
    value & (u32)SignalProcStatusWrite::SET_INTR            ? "SET_INTR" : "-",
    value & (u32)SignalProcStatusWrite::CLEAR_SSTEP         ? "CLEAR_SSTEP" : "-",
    value & (u32)SignalProcStatusWrite::SET_SSTEP           ? "SET_SSTEP" : "-",
    value & (u32)SignalProcStatusWrite::CLEAR_INTR_ON_BREAK ? "CLEAR_INTR_ON_BREAK" : "-",
    value & (u32)SignalProcStatusWrite::SET_INTR_ON_BREAK   ? "SET_INTR_ON_BREAK" : "-",
    value & (u32)SignalProcStatusWrite::CLEAR_SIGNAL_0      ? "CLEAR_SIGNAL_0" : "-",
    value & (u32)SignalProcStatusWrite::SET_SIGNAL_0        ? "SET_SIGNAL_0" : "-",
    value & (u32)SignalProcStatusWrite::CLEAR_SIGNAL_1      ? "CLEAR_SIGNAL_1" : "-",
    value & (u32)SignalProcStatusWrite::SET_SIGNAL_1        ? "SET_SIGNAL_1" : "-",
    value & (u32)SignalProcStatusWrite::CLEAR_SIGNAL_2      ? "CLEAR_SIGNAL_2" : "-",
    value & (u32)SignalProcStatusWrite::SET_SIGNAL_2        ? "SET_SIGNAL_2" : "-",
    value & (u32)SignalProcStatusWrite::CLEAR_SIGNAL_3      ? "CLEAR_SIGNAL_3" : "-",
    value & (u32)SignalProcStatusWrite::SET_SIGNAL_3        ? "SET_SIGNAL_3" : "-",
    value & (u32)SignalProcStatusWrite::CLEAR_SIGNAL_4      ? "CLEAR_SIGNAL_4" : "-",
    value & (u32)SignalProcStatusWrite::SET_SIGNAL_4        ? "SET_SIGNAL_4" : "-",
    value & (u32)SignalProcStatusWrite::CLEAR_SIGNAL_5      ? "CLEAR_SIGNAL_5" : "-",
    value & (u32)SignalProcStatusWrite::SET_SIGNAL_5        ? "SET_SIGNAL_5" : "-",
    value & (u32)SignalProcStatusWrite::CLEAR_SIGNAL_6      ? "CLEAR_SIGNAL_6" : "-",
    value & (u32)SignalProcStatusWrite::SET_SIGNAL_6        ? "SET_SIGNAL_6" : "-",
    value & (u32)SignalProcStatusWrite::CLEAR_SIGNAL_7      ? "CLEAR_SIGNAL_7" : "-",
    value & (u32)SignalProcStatusWrite::SET_SIGNAL_7        ? "SET_SIGNAL_7" : "-"
  );
}