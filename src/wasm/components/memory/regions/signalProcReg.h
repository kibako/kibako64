/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../../../types.h"
#include "../memoryRegion.h"
#include "../../fpu.h"

enum class SignalProcRegOffset
{
  MEM_ADDRESS      = 0 * sizeof(u32),
  DMA_ADDRESS      = 1 * sizeof(u32),
  DMA_LENGTH_READ  = 2 * sizeof(u32),
  DMA_LENGTH_WRITE = 3 * sizeof(u32),
  STATUS           = 4 * sizeof(u32),
  DMA_FULL         = 5 * sizeof(u32),
  DMA_BUSY         = 6 * sizeof(u32),
  SP_SEMAPHORE     = 7 * sizeof(u32),

  DATA_PC =  0x0004'0000
};

enum class SignalProcStatusWrite
{
  CLEAR_HALT          = (1 << 0),
  SET_HALT            = (1 << 1),
  CLEAR_BROKE         = (1 << 2),
  CLEAR_INTR          = (1 << 3),
  SET_INTR            = (1 << 4),
  CLEAR_SSTEP         = (1 << 5),
  SET_SSTEP           = (1 << 6),
  CLEAR_INTR_ON_BREAK = (1 << 7),
  SET_INTR_ON_BREAK   = (1 << 8),
  CLEAR_SIGNAL_0      = (1 << 9),
  SET_SIGNAL_0        = (1 << 10),
  CLEAR_SIGNAL_1      = (1 << 11),
  SET_SIGNAL_1        = (1 << 12),
  CLEAR_SIGNAL_2      = (1 << 13),
  SET_SIGNAL_2        = (1 << 14),
  CLEAR_SIGNAL_3      = (1 << 15),
  SET_SIGNAL_3        = (1 << 16),
  CLEAR_SIGNAL_4      = (1 << 17),
  SET_SIGNAL_4        = (1 << 18),
  CLEAR_SIGNAL_5      = (1 << 19),
  SET_SIGNAL_5        = (1 << 20),
  CLEAR_SIGNAL_6      = (1 << 21),
  SET_SIGNAL_6        = (1 << 22),
  CLEAR_SIGNAL_7      = (1 << 23),
  SET_SIGNAL_7        = (1 << 24),
};

enum class SignalProcStatusRead
{
  HALT               = (1 << 0),
  BROKE              = (1 << 1),
  DMA_BUSY           = (1 << 2),
  DMA_FULL           = (1 << 3),
  IO_FULL            = (1 << 4),
  SINGLE_STEP        = (1 << 5),
  INTERRUPT_ON_BREAK = (1 << 6),
  SIGNAL_0_SET       = (1 << 7),
  SIGNAL_1_SET       = (1 << 8),
  SIGNAL_2_SET       = (1 << 9),
  SIGNAL_3_SET       = (1 << 10),
  SIGNAL_4_SET       = (1 << 11),
  SIGNAL_5_SET       = (1 << 12),
  SIGNAL_6_SET       = (1 << 13),
  SIGNAL_7_SET       = (1 << 14),
};

class SignalProcReg : public MemoryRegion
{
  private:
    CLASS_DISABLE_COPY_AND_MOVE(SignalProcReg)

    u32 data[(0x0408'0008 - 0x0404'0000) / 4]{0};

    static void writeWordWrap(u32 address, u32 value);

  public:
    constexpr SignalProcReg() : MemoryRegion(data, 0x0404'0000, sizeof(data), "SignalProc-Register", writeWordWrap)
    {}

    void writeWord(u32 address, u32 value);
    void setRegisterU32(SignalProcRegOffset offset, u32 value);
};
