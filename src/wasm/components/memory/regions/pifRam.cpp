/**
* @copyright 2021 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "pifRam.h"
#include "../../../globals.h"

#define SEMAPHORE_ADDRESS (PIF_RAM_SIZE-4)

void PIF::PifRam::writeWordWrap(u32 address, u32 value) {
  memBus.pifRam.writeWordDirect(address, value);
  memBus.pifRam.executeIfNecessary();
}

void PIF::PifRam::executeIfNecessary()
{
  if(isSemaphoreSet())
  {
    memBus.pif.run();
  }
}

void PIF::PifRam::disableSemaphore()
{
  u32 oldWord = read<u32>(SEMAPHORE_ADDRESS);
  writeWordDirect(SEMAPHORE_ADDRESS, oldWord & 0xFFFF'FF00);
}

bool32 PIF::PifRam::isSemaphoreSet()
{
  return read<u32>(SEMAPHORE_ADDRESS) & 0xFF;
}

// Debugging Functions

void PIF::PifRam::printPifRam()
{
  printf_trace("PIF-RAM: \n"
    "    %08X | %08X | %08X | %08X \n"
    "    %08X | %08X | %08X | %08X \n"
    "    %08X | %08X | %08X | %08X \n"
    "    %08X | %08X | %08X | %08X:S"
    , 
    read<u32>(0), read<u32>(4), read<u32>(8), read<u32>(12),
    read<u32>(16), read<u32>(20), read<u32>(24), read<u32>(28),
    read<u32>(32), read<u32>(36), read<u32>(40), read<u32>(44),
    read<u32>(48), read<u32>(52), read<u32>(56), read<u32>(60)
  );
}