/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../memoryRegion.h"
#include "../../interrupt.h"

class MipsInterface : public MemoryRegion
{
  private:
    CLASS_DISABLE_COPY_AND_MOVE(MipsInterface)

    u32 data[0x10]{0};

    static void writeWordWrap(u32 address, u32 value);

  public:
    constexpr MipsInterface() : MemoryRegion(data, 0x0430'0000, sizeof(data), "MipsInterface", writeWordWrap)
    {}
    
    void writeWord(u32 address, u32 value);

    void setInterruptBit(Interrupt::RcpType bit);
    void clearInterruptBit(Interrupt::RcpType bit);
    u32 getMaskedInterruptBits();
};
