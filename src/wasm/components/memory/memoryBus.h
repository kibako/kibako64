/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../../main.h"
#include "../../stdlib/stdlib.h"

#include "./memoryRegion.h"

#include "./regions/rom.h"
#include "./regions/dma.h"
#include "./regions/pif.h"
#include "./regions/signalProcReg.h"
#include "./regions/mipsInterfaceReg.h"
#include "./regions/videoInterfaceReg.h"
#include "./regions/dramReg.h"
#include "./regions/pifRam.h"

#include "../tlb.h"
#include "byteswap.h"
#include "globals.h"
#include "ram.h"
#include "cpu.h"

class MemoryBus {
private:
    CLASS_DISABLE_COPY_AND_MOVE(MemoryBus)
    u32 dataRspMem[8_KB / 4] = {0};
    u32 dataRamInterfaceRegMem[1_MB / 4] = {0};
    u32 dataMipsInterfaceRegMem[0x10 / 4] = {0};


    // Dynamically generates a O(1) lookup table for addesses.
    // This maps the highest 16bit of any address directly to the correct memory handler.
    // @TODO: doing this at compile time currently corrupts the memory, check why and fix
    void rebuildLookupTable();

public:
    TLB tlb{}; // unmapped
    ROM rom{}; // unmapped

    // Main memory-map, any memory-address will be mapped to one of these objects below:
    DRAMReg        ramReg            {};                   // 0x03F0'0000
    MemoryRegion   rspMem            {dataRspMem,             0x0400'0000, sizeof(dataRspMem), "RSP-Memory"};
    SignalProcReg  spReg             {};                   // 0x0404'0000
    MipsInterface  mipsInterfaceReg  {};                   // 0x0430'0000
    VideoInterface videoInterfaceReg {};                   // 0x0440'0000
    DMA            dma               {};                   // 0x0460'0000
    MemoryRegion   ramInterfaceReg   {dataRamInterfaceRegMem, 0x0470'0000, sizeof(dataRamInterfaceRegMem), "Ram-Interface"};
    PIF::PifChip   pif               {};                   // 0x0480'0000  aka Serial Interface Registers
    MemoryRegion   romHeaderMem      {rom.getDataPointerWord(),0x1000'0000, 0x0F39'FFFF, "ROM-Header"};//0x40);
    PIF::PifRam    pifRam            {};                   // 0x1FC0'07C0  (ROM @ 0x1FC0'0000 is unmapped)

    // Not directly mapped regions, mostly for convenience:

    // separate RSP data & instruction memory for direct access, only used during boot.
    // This is not mapped since it would bump the lookup table from 16 to 24bit. (64KB -> 1MB)
    MemoryRegion rspDataMem{dataRspMem, 0x0400'0000, 4_KB, "RSP-DMEM"};
    MemoryRegion rspInstructionMem{dataRspMem + 4_KB, 0x0400'1000, 4_KB, "RSP-IMEM"};

    constexpr MemoryBus() {}

    static inline u32 normalizeAddress(u32 address)
    {
      // 0x80000000 - 0x9FFFFFFF (kseg0) - "normal" memory map
      // 0xA0000000 - 0xBFFFFFFF (kseg1) - mirror of kseg0, but uncached
      // 0xC0000000 - 0xDFFFFFFF (ksseg) - TLB mapped
      // 0xE0000000 - 0xFFFFFFFF (kseg3) - TLB mapped
      #ifdef FEATURE_DEBUG_LOG
        if(address >= 0xC000'0000)
        {
          #ifdef FEATURE_DEBUG_LOG
            printf_error("Error resolving TLB-Mapped address type: 0x%08x\n", address);
            //tlb.printStatus();
          #endif
          //return 0;
        }
      #endif

      return address & 0x1FFF'FFFF;
    }

    void reset();

    void logState();

    void printMemoryMap();

    MemoryRegion *getMemoryHandler(u32 address);

    template<typename T>
    T read(u32 address) {
      using UT = std::make_unsigned_t<T>;

      // if somewhere in the code we read a signed-value, stop compilation (done to reduce template functions)
      static_assert(std::is_same<T, UT>::value, "Signed-Read not allowed");

      u32 orgAddress = address;
      address = normalizeAddress(address);

      // RAM access is the most common, use a fast-path here for performance
      if (RAM::isInMemory(address)) {
        if constexpr (std::is_same<UT, f32>::value)return std::bit_cast<f32>(RAM::read<u32>(address));
        else if constexpr (std::is_same<UT, f64>::value)return std::bit_cast<f64>(RAM::read<u64>(address));
        else return RAM::read<T>(address);
      }

      // If it was not RAM, check the lookup table and do the same things as before
      auto memHandler = getMemoryHandler(address);
      if(memHandler == nullptr) 
      {
        printf_warn("Unmapped address read %s: 0x%08x, org: 0x%08X", getTypeName<T>(), address, orgAddress);
        logState();
        return 0;
      }

      address -= memHandler->getMemoryOffset();

      if constexpr(std::is_same<T, f32>::value)return std::bit_cast<f32>(memHandler->read<u32>(address));
      if constexpr(std::is_same<T, f64>::value)return std::bit_cast<f64>(memHandler->read<u64>(address));

      return memHandler->read<UT>(address);
    }

    template<typename T> void write(u32 address, T value) 
    {
      // prevent singed write to keep the number of template-functions down
      using UT = std::make_unsigned_t<T>;
      static_assert(std::is_same<T, UT>::value, "Signed-Write not allowed");

      u32 orgAddress = address;
      address = normalizeAddress(address);
      debugger_hook_mem_write(address, (u32)value); // @TODO support 64-bit

      if (RAM::isInMemory(address)) {
        return RAM::write(address, bit_cast_int(value));
      }

      auto memHandler = getMemoryHandler(address);
      if(memHandler) 
      {
        memHandler->write(
          address - memHandler->getMemoryOffset(), 
          bit_cast_int(value)
        );
      } else {
        printf_warn("Unmapped address write %s: 0x%08x=%08X, org: 0x%08X", getTypeName<T>(), address, (u32)value, orgAddress);
        logState();
      }
    }
};
