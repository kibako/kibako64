/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

namespace Timing
{
  enum class Cycles 
  {
    H_LINE = 5'964,
    FULL_SYNC_60FPS = 1'562'500,
  };

  // counter for each event, counts down to zero and resets
  extern u32 counterHLine;

  // increments the cycle counter, and starts events that should happen at certain points.
  void incrementCycle();

  void logState();
}

