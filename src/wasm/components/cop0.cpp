/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "cop0.h"
#include "../globals.h"
#include "../utils/logging.h"

#define COUNTER_INCREMENT 2

namespace 
{
  const char cop0RegisterNames[][16] = { 
    "INDEX", "RANDOM", "ENTRY_LOW0", "ENTRY_LOW1", "CONTEXT", 
    "PAGE_MASK", "TLB_COUNT", "REG_7", "BAD_VADDRESS", "COUNT", 
    "ENTRY_HIGH", "COMPARE", "STATUS", "CAUSE", "EXCEPTION_PC", "PROCESSOR_ID", 
    "CONFIG", "LL_ADDRESS", "WATCHPT_ADDR", "WATCHPT_CTRL", "XCONTEXT", 
    "REG_21", "REG_22", "REG_23", "REG_24", "REG_25", "REG_26", "CACHE_ERROR", 
    "CACHE_TAG_LO", "CACHE_TAG_HI", "ERROR_EPC", "REG_27"
  };

  constexpr u32 causeMask = 0b00000000'00000000'00000011'00000000;
}

using CopReg = Cop0Register;

void Cop0::reset()
{
  memset(registers, 0, sizeof(registers));
  pendingInterrupt = false;
}

void Cop0::reboot()
{
  registers[(u32)CopReg::RANDOM]       = 0x0000'001F;
	registers[(u32)CopReg::COUNT]        = 0x0000'5000;
  registers[(u32)CopReg::CAUSE]        = 0x0000'005C;
  registers[(u32)CopReg::CONTEXT]      = 0x007F'FFF0;
  registers[(u32)CopReg::EXCEPTION_PC] = 0xFFFF'FFFF;
  registers[(u32)CopReg::BAD_VADDRESS] = 0xFFFF'FFFF;
  registers[(u32)CopReg::ERROR_EPC]    = 0xFFFF'FFFF;
  registers[(u32)CopReg::CONFIG]       = 0x0006'E463;
  registers[(u32)CopReg::STATUS]       = 0x3400'0000;
	registers[(u32)CopReg::PROCESSOR_ID] = 0x0000'0002;
	registers[(u32)CopReg::TLB_COUNT]    = 0x0000'0000;
}

void Cop0::setRegisterU32(Cop0Register reg, u32 value)
{
  u32 oldValue = registers[(u32)reg & 0b11111];

  switch(reg)
  {
    case Cop0Register::STATUS:
      registers[(u32)reg & 0b11111] = value;
      if(value & (u32)Cop0StatusFlag::INTERRUPT_ENABLE) {
        print_trace("Exceptions are now enabled");
      }

      cpu.fpu.fullMode = !!(value & (u32)Cop0StatusFlag::FPU_FULL_MODE);

      printf_trace("Cop0 Status set: %08X", value);

      updatePendingInterrupt();

      #ifdef FEATURE_DEBUGGER
      {
        bool32 oldErrorLvl = oldValue & (u32)Cop0StatusFlag::EXCEPTION_LEVEL;
        bool32 newErrorLvl = value & (u32)Cop0StatusFlag::EXCEPTION_LEVEL;
        if(oldErrorLvl != newErrorLvl) {
          debugger_hook_event(DebuggerEvent::SET_EXCEPTION_LEVEL);
        }
      }
      #endif

    break;

    case Cop0Register::CAUSE:
      // @TODO: fix write bit mask
      //registers[(u32)reg & 0b11111] |= value & causeMask;
      //registers[(u32)reg & 0b11111] &= ~(~value & causeMask);
      registers[(u32)reg & 0b11111] = value;
      updatePendingInterrupt();
    break;

    case Cop0Register::COMPARE:
      registers[(u32)reg & 0b11111] = value;
      print_trace("Cop0: write to COPARE, clear interrupt-pending bit");
      system.interruptHandler.clearInterrupt(Interrupt::Type::TIMER);
    break;

    #ifdef FEATURE_DEBUGGER
      case Cop0Register::EXCEPTION_PC: 
        debugger_hook_event(DebuggerEvent::SET_EXCEPTION_PC);
      // fallthrough
    #endif

    default:
      registers[(u32)reg & 0b11111] = value;
      printf_trace("Cop0 write to: mtc0: [%d] %s = %08X", reg, cop0RegisterNames[(u32)reg], value);
  }
}

u32 Cop0::getRegisterU32(Cop0Register reg)
{
  return registers[(u32)reg & 0b11111];
}

u32 Cop0::getStatusFlag(Cop0StatusFlag flag)
{
  return registers[(u32)Cop0Register::STATUS] & (u32)flag;
}

void Cop0::enableStatusFlag(Cop0StatusFlag flag)
{
  registers[(u32)Cop0Register::STATUS] |= (u32)flag;
}

void Cop0::disableStatusFlag(Cop0StatusFlag flag)
{
  registers[(u32)Cop0Register::STATUS] &= ~(u32)flag;
}

bool32 Cop0::hasPendingInterrupt()
{
 return pendingInterrupt;
}

void Cop0::setPendingInterrupt(Interrupt::Type type)
{
  registers[(u32)Cop0Register::CAUSE] |= (u32)type;
  updatePendingInterrupt();
}

void Cop0::clearPendingInterrupt(Interrupt::Type type)
{
  registers[(u32)Cop0Register::CAUSE] &= ~(u32)type;
  updatePendingInterrupt();
}

void Cop0::clearPendingInterrupt()
{
  registers[(u32)Cop0Register::CAUSE] = 0;
  updatePendingInterrupt();
}


void Cop0::incrementCounter()
{
  #pragma clang loop unroll_count(COUNTER_INCREMENT)
  for(auto i=0; i<COUNTER_INCREMENT; ++i) 
  {
    if(++registers[(u32)Cop0Register::COUNT] == registers[(u32)Cop0Register::COMPARE]) 
    {
      system.interruptHandler.interrupt(Interrupt::Type::TIMER);
    }
  }
}

// Debugging functions:
#define X_TO_STR(s) TO_STR(s)
#define TO_STR(s) #s

#define STAT_FLAG(shiftNum) (status & (1 << shiftNum)) == (oldStatus & (1 << shiftNum)) ? TO_STR(shiftNum)": " : TO_STR(shiftNum)":*", (status & (1 << shiftNum))

void Cop0::printStatus(u32 oldStatus)
{
  u32 status = getRegisterU32(Cop0Register::STATUS);

  u32 privMode = (status >> 4) & 0b11;
  u32 oldPrivMode = (oldStatus >> 4) & 0b11;

  printf_trace("Cop0 Status Flags: \n"
    "   Cop3/Cop2/FPU/Cop0    : %s%s/%s%s/%s%s/%s%s\n"
    "  %sReduce Power  : %s\n"
    "  %sFPU Full-Mode : %s\n"

    "  %sEndian        : %s\n"
    "  %sInstr. trace  : %s\n"
    "  %sExc. Vector   : %s\n"
    "  %sTable shutdown: %s\n"
    "  %sS-Reset | MNI?: %s\n"

    "  %sCop0 Condition: %s\n"
    "   Interrupt Mask : %s%s %s%s %s%s %s%s %s%s %s%s %s%s %s%s\n"

    "   64-Bit Addr. (Kernel/Supervisor/User): %s%s %s%s %s%s\n"

    "  %cPriv. Mode      : %s\n"
    "   Lvl Error/Except: %s%s %s%s\n"
    "  %sGlobal-Interrupt: %s"
,
    STAT_FLAG(31) ? "1" : "0", STAT_FLAG(30) ? "1" : "0",
    STAT_FLAG(29) ? "1" : "0", STAT_FLAG(28) ? "1" : "0",

    STAT_FLAG(27) ? "ON" : "OFF",
    STAT_FLAG(26) ? "ON" : "OFF",

    STAT_FLAG(25) ? "BE=0" : "LE=1",
    STAT_FLAG(24) ? "ON" : "OFF",
    STAT_FLAG(22) ? "Normal" : "Bootstrap",
    STAT_FLAG(21) ? "True" : "False",
    STAT_FLAG(20) ? "True" : "False",

    STAT_FLAG(18) ? "1" : "0",

      STAT_FLAG( 8) ? "S0" : "-", // software
      STAT_FLAG( 9) ? "S1" : "-", // software
      STAT_FLAG(10) ? "RCP" : "-",
      STAT_FLAG(11) ? "ROM" : "-",
      STAT_FLAG(12) ? "Reset" : "-",
      STAT_FLAG(13) ? "RDB-R" : "-",
      STAT_FLAG(14) ? "RDB-W" : "-",
      STAT_FLAG(15) ? "Timer" : "-",

    STAT_FLAG(7) ? "ON" : "OFF",
    STAT_FLAG(6) ? "ON" : "OFF",
    STAT_FLAG(5) ? "ON" : "OFF",

    privMode == oldPrivMode ? ' ' : '*',
    (privMode == 0) ? "Kernel" : (privMode == 1 ? "Supervisor" : "User"),

    STAT_FLAG(2) ? "Error" : "Normal",
    STAT_FLAG(1) ? "Exception" : "Normal",
    STAT_FLAG(0) ? "ON" : "OFF"
  );

  u32 regCause = getRegisterU32(Cop0Register::CAUSE);
  printf_trace("Cop0 Registers:\n"
    "    ERROR_EPC   : 0x%08X\n"
    "    EXCEPTION_PC: 0x%08X\n"
    "    CAUSE       : %s %s %s %s %s %s %s %s"
    ,
    getRegisterU32(Cop0Register::ERROR_EPC),
    getRegisterU32(Cop0Register::EXCEPTION_PC),
    regCause & (1 <<  8) ? "S0"    : "-", // software
    regCause & (1 <<  9) ? "S1"    : "-", // software
    regCause & (1 << 10) ? "RCP"   : "-",
    regCause & (1 << 11) ? "ROM"   : "-",
    regCause & (1 << 12) ? "Reset" : "-",
    regCause & (1 << 13) ? "RDB-R" : "-",
    regCause & (1 << 14) ? "RDB-W" : "-",
    regCause & (1 << 15) ? "Timer" : "-"
  );
}

