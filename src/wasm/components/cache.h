/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../main.h"
#include "types.h"

/**
 * Disassembler cache
 */
namespace Cache
{
#ifdef FEATURE_INSTRUCTION_CACHE
    constexpr static u32 CACHE_SIZE = 1024*1024*8 / 4;
    inline u8 cache[CACHE_SIZE]{0};

    inline void clearCache() {
      memset(cache, 0, CACHE_SIZE);
    }

    inline void clearCache(u32 address) {
      address = (address & 0x0FFF'FFFF) >> 2;
      cache[address & (CACHE_SIZE-1)] = 0;
    }

    inline void clearCache(u32 address, u32 size) {
      address = (address & 0x0FFF'FFFF) >> 2;
      memset(cache + address, 0, size>>2);
    }
#else
    inline void clearCache() {}
    inline void clearCache(u32 address) {}
    inline void clearCache(u32 address, u32 size) {}
#endif
}