/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "../main.h"
#include "./timing.h"

#include "../instructions/instructions.h"
#include "disassembler.h"
#include "globals.h"
#include "cache.h"

// to avoid branching, simply mask out register indices that too high
#define REG_MASK (64 - 1)

#ifdef FEATURE_STRICT_REGISTER_IDX
  #define CHECKED_REG (reg & REG_MASK)
#else
  #define CHECKED_REG (reg)
#endif

namespace {
  constexpr s32 MAX_ERROR_COUNT = 20;
}

void MipsCPU::clear()
{
  reset();
  memset(RAM_PTR, 0, RAM_SIZE);
}

void MipsCPU::reset()
{
  cmdCounter = 0;
  delaySlot = false;
  pc = 0;
  pcAfterDelay = 0;
  errorCount = 0;
  hasPendingInterrupt = false;
  llActive = false;

  fpu.clear();
  cop0.reset();
  memset(registers, 0, sizeof(registers));
}

void MipsCPU::run(u32 limit)
{
  cmdCounter = 0;

  for(;;) 
  {
    step();  
    
    // avoid stopping inside a delay-slot since interrupts might change the PC
    ++cmdCounter;

    if(cmdCounter >= limit && !pcAfterDelay) {
      break;
    }

    //if(errorCount > MAX_ERROR_COUNT)break;
  }

  if(errorCount > MAX_ERROR_COUNT) {
    print_error("Too many errors, abort execution!")
  }
}

void MipsCPU::step()
{
  //performance_timer_start(U32_FRM_CHARS('R','e','f','.'));
  //performance_timer_stop(U32_FROM_CHARS('R','e','f','.'));

  /**
   * This is a single step forward which is roughly a CPU cycle.
   * The most important steps, in order are:
   * 
   * - update timer and cycle counters (might cause interrupts)
   * - handle pending interupts (might change PC)
   * - fetch/execute instruction + increment PC
   * - check delay slot target, if prepared (might change PC)
   * - use delay-slot if we need one, and setup target 
   */

  Timing::incrementCycle();

  // avoid interrupts in (pre-)delay-slots to reduce complexity
  if (pcAfterDelay == 0) {
    system.interruptHandler.handlePending();
  }

  #ifdef FEATURE_DEBUGGER
    if(hookStepEnabled) { debugger_hook_step(pc); }
  #endif

  u32 currentWord = memBus.read<u32>(pc);

  /*if(pc > 0xC0000000) {
    ++errorCount;
  }*/

  // Cache
  #ifdef FEATURE_INSTRUCTION_CACHE
    MipsiOpFunction instruction;
    u32 cacheIdx = (pc << 2) >> 4;
    if(cacheIdx < 0x200000) {
      if(!Cache::cache[cacheIdx]) {
        Cache::cache[cacheIdx] = (u8)(u32)Disassembler::getOpcodeFunction(currentWord);
      }
      instruction = (MipsiOpFunction)(u32)Cache::cache[cacheIdx];
    } else {
      instruction = Disassembler::getOpcodeFunction(currentWord);
    }
  #else
    auto instruction = Disassembler::getOpcodeFunction(currentWord);
  #endif

  MipsiOp::printInstruction((u32)instruction, currentWord);

  advancePc();
  
  if(pcAfterDelay > 0)  {
    cpu.useDelaySlot();
  }

  if(instruction != nullptr) {
    instruction(currentWord);
  } else {
    printf_error("Unknown Instruction @0x%08X = %08X [" PRINTF_U32_BIN_ARG "]", pc, currentWord, PRINTF_U32_BIN_VAL(currentWord));
    logState();
  }

  if(delaySlotInUse()) 
  {
    cpu.freeDelaySlot();
    setPc(pcAfterDelay);
    pcAfterDelay = 0;
  }

  //registers[(u32)CpuReg::zero] = 0; // hard-reset $zero register
}

void MipsCPU::stepDeplay(u32 targetPc)
{
  #ifdef FEATURE_STRICT_DELAY_SLOT
    if (delaySlotInUse()) {
      print_warn("try to delay instruction while in delay slot, continue like normal");
    }
  #endif

  pcAfterDelay = targetPc != 0 
    ? targetPc 
    : 0xFFFF'FFFF;
}

void MipsCPU::stop()
{
  printf_info("CPU issued stop @0x%08X | cmdCounter: %d", pc, cmdCounter);
  logState();
  cmdCounter = 0xFFFF'FFF0;
}

void MipsCPU::setPc(u32 newPc) 
{
  if(newPc == 0 || newPc == 0xFFFF'FFFF)
  {
    print_error("CPU: setting PC to 0 or -1, stop execution");
    stop();
  }

  pc = newPc;
}

void MipsCPU::logError()
{
  ++errorCount;
}

void MipsCPU::logState() 
{ 
  #ifdef FEATURE_DEBUG_LOG
    u32 currentWord = memBus.read<u32>(pc - sizeof(u32));
    auto instruction = Disassembler::getOpcodeFunction(currentWord);

    if(!(loggingLevel & LoggingLevelMask::WARN) || !(loggingLevel & LoggingLevelMask::INFO)) {
      return;
    }

    auto oldLogLevel = loggingLevel;
    loggingLevel = LoggingLevelMask::ALL;

    MipsiOp::printInstruction((u32)instruction, currentWord);
    
    #define REG_(i) (u32)(registers[i] >> 32), (u32)(registers[i] & 0xFFFF'FFFF)

    printf("CPU Status | Delay: %s | PC: 0x%08X | Pending-Interrupt: %s\n", delaySlotInUse() ? "Yes" : "No", getPc(), hasPendingInterrupt ? "Yes" : "No");
    printf("- zr: %08X%08X at: %08X%08X v0: %08X%08X v1: %08X%08X a0: %08X%08X a1: %08X%08X a2: %08X%08X a3: %08X%08X\n", REG_(0), REG_(1), REG_(2), REG_(3), REG_(4), REG_(5), REG_(6), REG_(7));
    printf("- t0: %08X%08X t1: %08X%08X t2: %08X%08X t3: %08X%08X t4: %08X%08X t5: %08X%08X t6: %08X%08X t7: %08X%08X\n", REG_(8), REG_(9), REG_(10), REG_(11), REG_(12), REG_(13), REG_(14), REG_(15));
    printf("- s0: %08X%08X s1: %08X%08X s2: %08X%08X s3: %08X%08X s4: %08X%08X s5: %08X%08X s6: %08X%08X s7: %08X%08X\n", REG_(16), REG_(17), REG_(18), REG_(19), REG_(20), REG_(21), REG_(22), REG_(23));
    printf("- t8: %08X%08X t9: %08X%08X k0: %08X%08X k1: %08X%08X gp: %08X%08X sp: %08X%08X fp: %08X%08X ra: %08X%08X\n", REG_(24), REG_(25), REG_(26), REG_(27), REG_(28), REG_(29), REG_(30), REG_(31));
    
    memBus.videoInterfaceReg.printState();

    #undef REG_

    loggingLevel = oldLogLevel;
    
  #endif
}

void MipsCPU::logCall()
{
  printf_trace("log call @0x%08X", pc - sizeof(u32));
  log_call(pc - sizeof(u32));
}

s32 MipsCPU::getErrorCount()
{
  return errorCount;
}

s32 MipsCPU::getCommandCount()
{
  return cmdCounter;
}

u8 MipsCPU::getRegisterByte32(s32 reg, s32 byteIdx)
{
  return ((u32)registers[reg] >> ((32-8) - (byteIdx*8))) & 0xFF;
}

u8 MipsCPU::getRegisterByte64(s32 reg, s32 byteIdx)
{
  return (registers[reg] >> ((64-8) - (byteIdx*8))) & 0xFF;
}

void MipsCPU::setRegisterByte32(s32 reg, s32 byteIdx, u8 value)
{
  s32 shiftAmount = (32-8) - byteIdx * 8;
  u64 mask = 0xFF << shiftAmount;
  u64 orVal = (u64)value << shiftAmount;
  registers[CHECKED_REG] = (~mask & registers[CHECKED_REG]) | orVal;
}

void MipsCPU::setRegisterByte64(s32 reg, s32 byteIdx, u8 value)
{
  s32 shiftAmount = (64-8) - byteIdx * 8;
  u64 mask = (u64)0xFF << shiftAmount;
  u64 orVal = (u64)value << shiftAmount;
  registers[CHECKED_REG] = (~mask & registers[CHECKED_REG]) | orVal;
}
