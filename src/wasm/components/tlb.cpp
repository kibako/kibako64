/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "tlb.h"
#include "../utils/logging.h"

void TLB::reset()
{
  for(s32 i=0; i<TLB_MAX_ENTRIES; ++i)
  {
    setEntry(i, {TLBPageMask::MASK_4K, 0x80000000, 0, 0});
  }
}

u32 TLB::resolveAddress(u32 vAddress)
{
  printStatus();
  printf_trace("TLB Resolve: %08X", vAddress);
  for(auto entry : entries)
  {
    u32 mask = ((u32)entry.pagemask >> 1) | 0x0000'1FFF;
    u32 pageSize = mask + 1;

    u16 pagemaskU16 = (u32)entry.pagemask | 0x0000'1FFF;
    u32 vPageMap = entry.high & ~pagemaskU16;

    printf_trace("vPageMap %08X | check: %08X", vAddress, vAddress & vPageMap);

    if((vAddress & vPageMap) != vPageMap)
      continue;

    u32 odd = vAddress & pageSize;
    u32 lowValue = odd ? entry.low1 : entry.low0;

    printf_trace("odd %08X | lowValue: %08X", odd, lowValue);

    if(!(lowValue & 2))
      continue;

    u32 pageFrame = (lowValue >> 6) & 0x00FF'FFFF;

    return (0x8000'0000 | (pageFrame * pageSize) | (vAddress & mask));
  }

  printf_error("TLB could not resolve address: 0x%08X", vAddress);
  return 0;
}

void TLB::setEntry(u32 idx, TLBEntry entry)
{
  //printf_info("Setting TLB-Entry[%02X], HI/LO: %08X / %08X %08X, mask: %08X", idx, entry.high, entry.low0, entry.low1, entry.pagemask);
  entries[idx] = entry;
  //printStatus();
}

const char* getMaskName(TLBPageMask mask)
{
  switch(mask)
  {
    case TLBPageMask::MASK_4K  : return "4KB";
    case TLBPageMask::MASK_16K : return "16KB";
    case TLBPageMask::MASK_64K : return "64KB";
    case TLBPageMask::MASK_256K: return "256KB";
    case TLBPageMask::MASK_1M  : return "1MB";
    case TLBPageMask::MASK_4M  : return "4MB";
    case TLBPageMask::MASK_16M : return "16MB";
  } 
  return "<unknown size>";
}

void TLB::printStatus()
{
  #ifdef FEATURE_DEBUG_LOG
    print_info("TLB State: ");
    for(s32 i=0; i<TLB_MAX_ENTRIES; ++i)
    {
      auto entry = entries[i];
      printf_info("TLB[%02X]: mask: %s, high: %08X, low0/1: %08X | %08X", 
        i,
        getMaskName(entry.pagemask), entry.high, entry.low0, entry.low1
      );
    }
  #endif
}