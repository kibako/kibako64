/**
* @copyright 2021 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "../globals.h"
#include "timing.h"

u32 Timing::counterHLine = (u32)Timing::Cycles::H_LINE;

void Timing::incrementCycle()
{
  cpu.cop0.incrementCounter();

  if(--counterHLine == 0) {
    counterHLine = (u32)Cycles::H_LINE;
    memBus.videoInterfaceReg.incrementCurrentLine();
  }
}

void Timing::logState()
{
  printf_info("Timer H-LINE: 0x%08X (initial: 0x%08X)", counterHLine, (u32)Cycles::H_LINE);
}
