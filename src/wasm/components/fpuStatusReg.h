/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "types.h"

/**
 * Flags for the FPU-FCSR (Status) Register.
 * Register: 31
 */
enum class FPUStatusReg: u32
{
    ROUND_MODE_0   = (1u << 0),
    ROUND_MODE_1   = (1u << 1),

    FLAG_INEXACT   = (1u << 2),
    FLAG_UNDERFLOW = (1u << 3),
    FLAG_OVERFLOW  = (1u << 4),
    FLAG_DIV_ZERO  = (1u << 5),
    FLAG_INVAL_OP  = (1u << 6),

    ENABLE_INEXACT   = (1u << 7),
    ENABLE_UNDERFLOW = (1u << 8),
    ENABLE_OVERFLOW  = (1u << 9),
    ENABLE_DIV_ZERO  = (1u << 10),
    ENABLE_INVAL_OP  = (1u << 11),

    ENABLE_MASK = (0b11111u << 7),

    CAUSE_INEXACT    = (1u << 12),
    CAUSE_UNDERFLOW  = (1u << 13),
    CAUSE_OVERFLOW   = (1u << 14),
    CAUSE_DIV_ZERO   = (1u << 15),
    CAUSE_INVAL_OP   = (1u << 16),
    CAUSE_UNIMPL_OP  = (1u << 17),

    CAUSE_MASK = (0b111111u << 12),

    // 5 bits gap

    FCC_COMPARE = (1u << 23),
    FLUSH_ZERO  = (1u << 24)
};