/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "fpu.h"
#include "../stdlib/stdlib.h"
#include "../main.h"
#include "fpuStatusReg.h"
#include "logging.h"

void MipsFPU::clear() {
  memset(registers, 0, sizeof(registers));
  memset(controlRegs, 0, sizeof(controlRegs));
  fullMode = true;
}

void MipsFPU::checkForExceptions() {
  u32 val = getControlRegister(FPU_CONTROL_REG_FCSR);
  u32 cause = val & (u32) FPUStatusReg::CAUSE_MASK;
  u32 enable = val & (u32) FPUStatusReg::ENABLE_MASK;
  u32 exceptions = cause & enable;

  if (exceptions != 0) {
    printf_error("@TODO: FPU triggered exception, mask: %08X", exceptions);
  }
}

void MipsFPU::logState() {
#ifdef FEATURE_DEBUG_LOG

  printf("FPU Status | condition: %c\n", getStatusFlag(FPUStatusReg::FCC_COMPARE) ? '1' : '0');

#define REG_(x) i+x, getRegister<f64>(i+x)

  for (s32 i = 0; i < 32; i += 8) {
    printf("- f%02d: %16.8f f%02d: %16.8f f%02d: %16.8f f%02d: %16.8f f%02d: %16.8f f%02d: %16.8f f%02d: %16.8f f%02d: %16.8f \n",
           REG_(0), REG_(1), REG_(2), REG_(3), REG_(4), REG_(5), REG_(6), REG_(7)
    );
  }

#undef REG_

#define REG_(x) i+x, (u32)(getRegister<u64>(i+x) >> 32), (u32)(getRegister<u64>(i+x) & 0xFFFF'FFFF)

  for (s32 i = 0; i < 32; i += 8) {
    printf("- f%02d: %08X%08X f%02d: %08X%08X f%02d: %08X%08X f%02d: %08X%08X f%02d: %08X%08X f%02d: %08X%08X f%02d: %08X%08X f%02d: %08X%08X \n",
           REG_(0), REG_(1), REG_(2), REG_(3), REG_(4), REG_(5), REG_(6), REG_(7)
    );
  }

#undef REG_

#endif

}
