/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../types.h"

#define TLB_MAX_ENTRIES 32

enum class TLBPageMask
{
  MASK_4K    = 0x0000'0000,
  MASK_16K   = 0x0000'6000,
  MASK_64K   = 0x0001'E000,
  MASK_256K  = 0x0007'E000,
  MASK_1M    = 0x001F'E000,
  MASK_4M    = 0x007F'E000,
  MASK_16M   = 0x01FF'E000,
};

struct TLBEntry
{
  TLBPageMask pagemask;
  u32 high;
  u32 low0;
  u32 low1;
};

class TLB 
{
  private:
    TLBEntry entries[TLB_MAX_ENTRIES];

  public:
    void reset();

    u32 resolveAddress(u32 vAddress);
    void setEntry(u32 idx, TLBEntry entry);

    void printStatus();
};
