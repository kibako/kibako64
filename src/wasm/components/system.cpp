/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "../main.h"
#include "../stdlib/stdlib.h"
#include "system.h"
#include "../disassembler/disassembler.h"
#include "cic.h"

const u32 ROM_BOOT_ADDRESS     = 0x40;
const u32 ROM_BOOT_ADDRESS_END = 0x1000;
const u32 BOOT_ADDRESS = 0xA400'0040;

// Initial execution limit to finish the boot.
// If this is not enough, nothing bad happens, it just skips the first frame
const u32 BOOT_EXEC_LIMIT = 8'000'000;

void System::softReset()
{

}

void System::hardReset()
{
  cpu.reset();
  memBus.videoInterfaceReg.reset();

  // @TODO was broken, add test
  memBus.rom.copyTo(
    ROM_BOOT_ADDRESS, ROM_BOOT_ADDRESS_END - ROM_BOOT_ADDRESS, 
    memBus.rspDataMem.getDataPointer(), memBus.rspDataMem.getSize(), ROM_BOOT_ADDRESS
  );

	cpu.cop0.setRegisterU32(Cop0Register::LL_ADDRESS, 0xFFFF'FFFF);

  // This function was ported from: https://github.com/z2442/daedalus  
  // Original copyright:
  /*
    Copyright (C) 2001 StrmnNrmn

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
  */

  memBus.mipsInterfaceReg.write(0x04, (u32)0x0202'0102);
  memBus.spReg.setRegisterU32(SignalProcRegOffset::STATUS, (u32)SignalProcStatusWrite::SET_HALT);

	cpu.cop0.reboot();

	//gCPUState.FPUControl[0]._u32 = 0x00000511; // @TODO
  memBus.ramInterfaceReg.write(0x0C, (u32)0x0000'0001);

	cpu.setRegister<u64>(CpuReg::v0, 0xFFFF'FFFF'D173'1BE9);
	cpu.setRegister<u64>(CpuReg::v1, 0xFFFF'FFFF'D173'1BE9);
	cpu.setRegister<u64>(CpuReg::a0, 0x0000'0000'0000'1BE9);
	cpu.setRegister<u64>(CpuReg::a1, 0xFFFF'FFFF'F452'31E5);
	cpu.setRegister<u64>(CpuReg::a2, 0xFFFF'FFFF'A400'1F0C);
	cpu.setRegister<u64>(CpuReg::a3, 0xFFFF'FFFF'A400'1F08);
	cpu.setRegister<u64>(CpuReg::t0, 0x0000'0000'0000'00C0);
	cpu.setRegister<u64>(CpuReg::t2, 0x0000'0000'0000'0040);
	cpu.setRegister<u64>(CpuReg::t3, 0xFFFF'FFFF'A400'0040);
	cpu.setRegister<u64>(CpuReg::s4, (u64)romHeader.tvEncoding);
	cpu.setRegister<u64>(CpuReg::s7, 0x0000'0000'0000'0006);
	cpu.setRegister<u64>(CpuReg::t9, 0xFFFF'FFFF'D73f'2993);

	cpu.setRegister<u64>(CpuReg::sp, 0xFFFF'FFFF'A400'1FF0);
	cpu.setRegister<u64>(CpuReg::ra, 0xFFFF'FFFF'A400'1554);

	switch (romHeader.country) {
		case RomCountryCode::GERMAN:
		case RomCountryCode::FRENCH:
		case RomCountryCode::ITALIAN:
		case RomCountryCode::EUROPEAN:
		case RomCountryCode::SPANISH:
		case RomCountryCode::AUSTRALIAN:
		case RomCountryCode::EUROPEAN_X:
		case RomCountryCode::EUROPEAN_Y:
			switch (cicType) 
      {
				case CIC::ChipType::CIC_6102:
					cpu.setRegister<u64>(CpuReg::a1, 0xFFFF'FFFF'C0F1'D859);
					cpu.setRegister<u64>(CpuReg::t6, 0x0000'0000'2DE1'08EA);
					break;
				case CIC::ChipType::CIC_6103:
					cpu.setRegister<u64>(CpuReg::a1, 0xFFFFFFFFD4646273);
					cpu.setRegister<u64>(CpuReg::t6, 0x000000001AF99984);
					break;
				case CIC::ChipType::CIC_6105:
          memBus.rspInstructionMem.write(0x04, 0xBDA8'07FC);
					cpu.setRegister<u64>(CpuReg::a1, 0xFFFFFFFFDECAAAD1);
					cpu.setRegister<u64>(CpuReg::t6, 0x000000000CF85C13);
					cpu.setRegister<u64>(CpuReg::t8, 0x0000000000000002);
					break;
				case CIC::ChipType::CIC_6106:
					cpu.setRegister<u64>(CpuReg::a1, 0xFFFFFFFFB04DC903);
					cpu.setRegister<u64>(CpuReg::t6, 0x000000001AF99984);
					cpu.setRegister<u64>(CpuReg::t8, 0x0000000000000002);
					break;
				default:
					break;
			}

			cpu.setRegister<u64>(CpuReg::s4, 0x0000000000000000);
			cpu.setRegister<u64>(CpuReg::s7, 0x0000000000000006);
			cpu.setRegister<u64>(CpuReg::ra, 0xFFFFFFFFA4001554);
			break;
		case RomCountryCode::BETA:
		case RomCountryCode::ASIAN:
		case RomCountryCode::NORTH_AMERICA:
		case RomCountryCode::JAPAN:
		default:
			switch (cicType) {
				case CIC::ChipType::CIC_6102:
					cpu.setRegister<u64>(CpuReg::a1, 0xFFFFFFFFC95973D5);
					cpu.setRegister<u64>(CpuReg::t6, 0x000000002449A366);
					break;
				case CIC::ChipType::CIC_6103:
					cpu.setRegister<u64>(CpuReg::a1, 0xFFFFFFFF95315A28);
					cpu.setRegister<u64>(CpuReg::t6, 0x000000005BACA1DF);
					break;
				case CIC::ChipType::CIC_6105:
          memBus.rspInstructionMem.write(0x08, 0x8DA8'07FC);
          
					cpu.setRegister<u64>(CpuReg::a1, 0x000000005493FB9A);
					cpu.setRegister<u64>(CpuReg::t6, 0xFFFFFFFFC2C20384);
					break;
				case CIC::ChipType::CIC_6106:
					cpu.setRegister<u64>(CpuReg::a1, 0xFFFFFFFFE067221F);
					cpu.setRegister<u64>(CpuReg::t6, 0x000000005CD2B70F);
					break;
				default:
					break;
			}
			cpu.setRegister<u64>(CpuReg::s4, 0x0000000000000001);
			cpu.setRegister<u64>(CpuReg::s7, 0x0000000000000000);
			cpu.setRegister<u64>(CpuReg::t8, 0x0000000000000003);
			cpu.setRegister<u64>(CpuReg::ra, 0xFFFFFFFFA4001550);
	}

	switch (cicType) {
		case CIC::ChipType::CIC_6101:
			cpu.setRegister<u64>(CpuReg::s6, 0x000000000000003F);
			break;
		case CIC::ChipType::CIC_6102:
			cpu.setRegister<u64>(CpuReg::at, 0x0000'0000'0000'0001);
			cpu.setRegister<u64>(CpuReg::v0, 0x0000'0000'0EBD'A536);
			cpu.setRegister<u64>(CpuReg::v1, 0x0000'0000'0EBD'A536);
			cpu.setRegister<u64>(CpuReg::a0, 0x0000'0000'0000'A536);
			cpu.setRegister<u64>(CpuReg::t4, 0xFFFF'FFFF'ED10'D0B3);
			cpu.setRegister<u64>(CpuReg::t5, 0x0000'0000'1402'A4CC);
			cpu.setRegister<u64>(CpuReg::t7, 0x0000'0000'3103'E121);
			cpu.setRegister<u64>(CpuReg::s6, 0x0000'0000'0000'003F);
			cpu.setRegister<u64>(CpuReg::t9, 0xFFFF'FFFF'9DEB'B54F);
			break;
		case CIC::ChipType::CIC_6103:
			cpu.setRegister<u64>(CpuReg::at, 0x0000'0000'0000'0001);
			cpu.setRegister<u64>(CpuReg::v0, 0x0000'0000'49A5'EE96);
			cpu.setRegister<u64>(CpuReg::v1, 0x0000'0000'49A5'EE96);
			cpu.setRegister<u64>(CpuReg::a0, 0x0000'0000'0000'EE96);
			cpu.setRegister<u64>(CpuReg::t4, 0xFFFF'FFFF'CE9D'FBF7);
			cpu.setRegister<u64>(CpuReg::t5, 0xFFFF'FFFF'CE9D'FBF7);
			cpu.setRegister<u64>(CpuReg::t7, 0x0000'0000'18B6'3D28);
			cpu.setRegister<u64>(CpuReg::s6, 0x0000'0000'0000'0078);
			cpu.setRegister<u64>(CpuReg::t9, 0xFFFF'FFFF'825B'21C9);
			break;
		case CIC::ChipType::CIC_6105:
      
			memBus.rspInstructionMem.write(0x00, (u32)0x3C0D'BFC0);
			memBus.rspInstructionMem.write(0x08, (u32)0x25AD'07C0);
			memBus.rspInstructionMem.write(0x0C, (u32)0x3108'0080);
			memBus.rspInstructionMem.write(0x10, (u32)0x5500'FFFC);
			memBus.rspInstructionMem.write(0x14, (u32)0x3C0D'BFC0);
			memBus.rspInstructionMem.write(0x18, (u32)0x8DA8'0024);
			memBus.rspInstructionMem.write(0x1C, (u32)0x3C0B'B000);

			cpu.setRegister<u64>(CpuReg::at, 0x0000'0000'0000'0000);
			cpu.setRegister<u64>(CpuReg::v0, 0xFFFF'FFFF'F58B'0FBF);
			cpu.setRegister<u64>(CpuReg::v1, 0xFFFF'FFFF'F58B'0FBF);
			cpu.setRegister<u64>(CpuReg::a0, 0x0000'0000'0000'0FBF);
			cpu.setRegister<u64>(CpuReg::t4, 0xFFFF'FFFF'9651'F81E);
			cpu.setRegister<u64>(CpuReg::t5, 0x0000'0000'2D42'AAC5);
			cpu.setRegister<u64>(CpuReg::t7, 0x0000'0000'5658'4D60);
			cpu.setRegister<u64>(CpuReg::s6, 0x0000'0000'0000'0091);
			cpu.setRegister<u64>(CpuReg::t9, 0xFFFF'FFFF'CDCE'565F);
			break;
		case CIC::ChipType::CIC_6106:
			cpu.setRegister<u64>(CpuReg::at, 0x0000'0000'0000'0000);
			cpu.setRegister<u64>(CpuReg::v0, 0xFFFF'FFFF'A959'30A4);
			cpu.setRegister<u64>(CpuReg::v1, 0xFFFF'FFFF'A959'30A4);
			cpu.setRegister<u64>(CpuReg::a0, 0x0000'0000'0000'30A4);
			cpu.setRegister<u64>(CpuReg::t4, 0xFFFF'FFFF'BCB5'9510);
			cpu.setRegister<u64>(CpuReg::t5, 0xFFFF'FFFF'BCB5'9510);
			cpu.setRegister<u64>(CpuReg::t7, 0x0000'0000'7A3C'07F4);
			cpu.setRegister<u64>(CpuReg::s6, 0x0000'0000'0000'0085);
			cpu.setRegister<u64>(CpuReg::t9, 0x0000'0000'465E'3F72);
			break;
		default:
			break;
	}

  // set RAM size
  u32 ramSizeAddress = (cicType == CIC::ChipType::CIC_6105) ? 0x3F0 : 0x318;
  RAM::write(ramSizeAddress, (u32)RAM_SIZE);
}

void System::boot()
{
  romHeader = memBus.rom.readHeader();
  
  cicType = CIC::getChipTypeFromRom(memBus.rom);
  printf_info("Detected CIC-Chip: %s", CIC::getChipName(cicType));
  
  hardReset();

  cpu.setPc(BOOT_ADDRESS);
  //cpu.setPc(romHeader.bootPc);
}

void System::run(u32 limit)
{
  cpu.run(limit);
}
