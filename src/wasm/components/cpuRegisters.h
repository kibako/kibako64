/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

enum class CpuReg {
    zero= 0,
    at= 1,  // Assembler temporary
    v0= 2,  // Function-returns / expression-evaluation	
    v1= 3,  // Function-returns / expression-evaluation	
    a0= 4,  // Function argument 0
    a1= 5,  // Function argument 1
    a2= 6,  // Function argument 2
    a3= 7,  // Function argument 3
    t0= 8,  // Temporary 0
    t1= 9,  // Temporary 1
    t2= 10, // Temporary 2
    t3= 11, // Temporary 3
    t4= 12, // Temporary 4
    t5= 13, // Temporary 5
    t6= 14, // Temporary 6
    t7= 15, // Temporary 7
    s0= 16, // Saved Temp. 0 (preserved)
    s1= 17, // Saved Temp. 1 (preserved)
    s2= 18, // Saved Temp. 2 (preserved)
    s3= 19, // Saved Temp. 3 (preserved)
    s4= 20, // Saved Temp. 4 (preserved)
    s5= 21, // Saved Temp. 5 (preserved)
    s6= 22, // Saved Temp. 6 (preserved)
    s7= 23, // Saved Temp. 7 (preserved)
    t8= 24, // Temporary 8
    t9= 25, // Temporary 9
    k0= 26, // Kernel reserved 0
    k1= 27, // Kernel reserved 1
    gp= 28, // Global pointer (preserved)
    sp= 29, // Stack pointer (preserved)
    fp= 30, // Frame pointer (preserved)
    ra= 31, // Return address

    f0= 0,
    f4= 4,
    f8= 8,
    f12= 12,
    f16= 16,
    f20= 20,
    f24= 24,
    f28= 28,

    // Special registers
    SP_LOW= 32, 
    SP_HIGH= 33
};