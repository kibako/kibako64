/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "cic.h"
#include "interrupt.h"

class System
{
  private:
    ROMHeader romHeader;
    CIC::ChipType cicType;

  public:
    Interrupt::Handler interruptHandler;

    constexpr System()
      : romHeader{}, cicType(CIC::ChipType::CIC_6101),
      interruptHandler(Interrupt::Handler())
    {}

    void softReset();

    // hard reset, setsup the system and simulates the CIC chip
    void hardReset();

    // Boots the game, this however will not start executing code.
    // It's safe to call 'run()' after this call.
    void boot();

    // executes code at the current PC with the given execution limit
    void run(u32 limit);
};
