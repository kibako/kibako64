/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "./memory/regions/rom.h"
#include "cic.h"
#include "../utils/checksum.h"

const char CicChipName[][9] = {
	"CIC_6101",
	"CIC_6102",
	"CIC_6103",
	"CIC_6104",
	"CIC_6105",
	"CIC_6106"
};

CIC::ChipType CIC::getChipTypeFromRom(ROM &rom)
{
  u32 checksum = checksumU8(
    rom.getDataPointer() + 0x0040, 
    rom.getDataPointer() + 0x1000
  );

  printf_trace("CIC-Checksum: %08X", checksum);

  switch(checksum)
	{
    case 0x3421E:
    case 0x33A27: return ChipType::CIC_6101;
    case 0x34044: return ChipType::CIC_6102;
    case 0x357D0: return ChipType::CIC_6103;
    case 0x47A81: return ChipType::CIC_6105;
    case 0x371CC: return ChipType::CIC_6106;

    default:
      printf_warn("Unkown CIC-Checksum: %08X, defaulting to CIC-6102", checksum);
      return ChipType::CIC_6102;
	}
}

const char* CIC::getChipName(ChipType cic)
{
  return (u32)cic < (u32)ChipType::SIZE ? CicChipName[(u32)cic] : "<unmapped>";
}
