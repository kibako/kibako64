/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../main.h"
#include "types.h"
#include "utils/concepts.h"
#include "cpuRegisters.h"
#include "fpu.h"
#include "cop0.h"

// @TODO get rid of these functions and use useful printf messages directly
#ifdef FEATURE_DEBUG_LOG
  #define CPU_LOG_ERROR(cpu, msg) printf_error("CPU-Error PC:%06X: %s", (cpu).getPc() - sizeof(u32), msg); (cpu).logError();
  #define CPU_LOG_ALIGN_ERROR(cpu, loc, mask, msg) if(loc & mask) { print_error(msg); }
#else
  #define CPU_LOG_ERROR(cpu, msg)
  #define CPU_LOG_ALIGN_ERROR(cpu, loc, mask, msg)
#endif

class MipsCPU
{
  private:
    static constexpr u32 REG_MASK = 0b111'111;

    u32 cmdCounter; // number of instructions so far

    bool32 delaySlot; // if true, we are currently in an delay-slot

    u32 pc; // current program-counter
    u32 pcAfterDelay; // program counter to jump to after an delay-slot, only used if not-zero

     // theoretically 32 + 2 (32-GPRs + LOW + HIGH), but to make OOB checks easier make it 64
    s64 registers[64];

    s32 errorCount; // number of errors so far, for metrics only, this will not cause the cpu to stop

    // single step, this can execute multiple instr. if it contains a delay slot
    void step();

  public:
    #ifdef FEATURE_DEBUGGER
      bool32 hookStepEnabled = true;
    #endif

    MipsFPU fpu;
    Cop0 cop0;

    // if this is set to true, an interrupt is peding.
    // This will be set by the interrupt-handler so the CPU can decide when interrupt
    // to prevent int. in delay-slots.
    bool32 hasPendingInterrupt{false};
    bool32 llActive{false};

    constexpr MipsCPU() : 
      cmdCounter(0), delaySlot(false),
      pc(0), pcAfterDelay{0}, registers{0},
      errorCount(0), fpu(MipsFPU()), cop0(Cop0())
    {}

    void clear();
    void reset();

    // execute code for 'limit' steps, starting from the current PC
    void run(u32 limit);

    // executes a delayed instruction and sets a new PC aferwards
    void stepDeplay(u32 targetPc);

    // stops the CPU, this will schedule a stop at the end of 'step()'
    void stop();

    bool32 delaySlotInUse() const { return delaySlot; }
    void useDelaySlot() { delaySlot = true; }
    void freeDelaySlot() { delaySlot = false; }

    u32 getPc() const { return pc; }
    void setPc(u32 newPc);

    void advancePc() { pc += sizeof(u32); }

    // logs the current PC to the JS-host
    void logCall();

    // "logs" an error, this will just increse a counter and has no side-effects
    void logError();
    void logState();

    // basically an "logError()" call counter
    s32 getErrorCount();

    // current number of executed instruction in the current 'run()' function
    s32 getCommandCount();

    template<Concepts::CpuRegType T>
    T getRegister(s32 reg) {
      return static_cast<T>(registers[reg]);
    }

    template<Concepts::CpuRegType T>
    void setRegister(s32 reg, T value) {
      #ifdef FEATURE_STRICT_REGISTER_IDX
        registers[reg & REG_MASK] = value;
      #else
        registers[reg] = value;
      #endif
    }

    template<Concepts::CpuRegType T>
    void setRegister(CpuReg reg, T value) { setRegister<T>((s32)reg, value); }

    void signExtendS32(s32 reg) {
      #ifdef FEATURE_STRICT_REGISTER_IDX
        registers[reg & REG_MASK] = getRegister<s32>(reg);
      #else
        registers[reg] = getRegister<s32>(reg);
      #endif
    }

    u8 getRegisterByte32(s32 reg, s32 byteIdx);
    u8 getRegisterByte64(s32 reg, s32 byteIdx);
    void setRegisterByte32(s32 reg, s32 byteIdx, u8 value);
    void setRegisterByte64(s32 reg, s32 byteIdx, u8 value);
};