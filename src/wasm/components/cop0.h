/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../types.h"
#include "interrupt.h"

enum class Cop0Register 
{
  INDEX        =  0,
  RANDOM       =  1,
  ENTRY_LOW0   =  2,
  ENTRY_LOW1   =  3,
  CONTEXT      =  4,
  PAGE_MASK    =  5,
  TLB_COUNT    =  6, // aka WIRED
  // reserved  =  7,
  BAD_VADDRESS =  8,
  COUNT        =  9,
  ENTRY_HIGH   = 10,
  COMPARE      = 11,
  STATUS       = 12,
  CAUSE        = 13,
  EXCEPTION_PC = 14,
  PROCESSOR_ID = 15,
  CONFIG       = 16,
  LL_ADDRESS   = 17,
  WATCHPT_ADDR = 18,
  WATCHPT_CTRL = 19,
  XCONTEXT     = 20,
  // reserved  = 21-26
  CACHE_ERROR  = 27,
  CACHE_TAG_LO = 28,
  CACHE_TAG_HI = 29,
  ERROR_EPC    = 30,
  // reserved  = 31,
};

enum class Cop0StatusFlag
{
  INTERRUPT_ENABLE = (1 << 0),
  EXCEPTION_LEVEL  = (1 << 1),
  ERROR_LEVEL      = (1 << 2),
  FPU_FULL_MODE    = (1 << 26),
};

// BitMask for the CAUSE register.
// For the exception-mask, use Interrupt::Type Enum
enum class Cop0CauseBits
{
  CO_PROCESSOR_NUM = (1 << 28),
  BRANCH_DELAY     = (1 << 31),
};

class Cop0
{
  private:
    u32 registers[32]{0};
    bool32 pendingInterrupt{false};

    inline void updatePendingInterrupt()
    {
      pendingInterrupt = registers[(u32)Cop0Register::CAUSE] & registers[(u32)Cop0Register::STATUS];
    }

  public:
    constexpr Cop0() {}

    void reset();
    void reboot();

    void setRegisterU32(Cop0Register reg, u32 value);
    u32 getRegisterU32(Cop0Register reg);

    u32 getStatusFlag(Cop0StatusFlag flag);
    void enableStatusFlag(Cop0StatusFlag flag);
    void disableStatusFlag(Cop0StatusFlag flag);

    bool32 hasPendingInterrupt();
    void setPendingInterrupt(Interrupt::Type type);
    void clearPendingInterrupt(Interrupt::Type type);
    void clearPendingInterrupt();

    // increments the internal 'COUNT' register, should be called once per CPU/clock tick
    void incrementCounter();

    // Debugging functions
    void printStatus(u32 oldStatus);

};
