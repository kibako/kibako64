/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

namespace Interrupt 
{
  // values are the same as the bitmask in the cop0 status register
  enum class Type
  {
    SOFTWARE_0 = (1 <<  8),
    SOFTWARE_1 = (1 <<  9),
    RCP        = (1 << 10), // used for RSP stuff, e.g.: vertical-blank, DPL done
    ROM        = (1 << 11),
    RESET      = (1 << 12),
    RDB_READ   = (1 << 13),
    RDB_WRITE  = (1 << 14),
    TIMER      = (1 << 15), // used when COUNT and COMPARE register are the same

    MASK_FULL  = (0b1111'1111 << 8)
  };

  // 'Type::RCP' has these extra flags:
  enum class RcpType {
    NONE = 0,
    SP = (1 << 0), // Signal-Proc Task Done
    SI = (1 << 1), // Controller input available
    AI = (1 << 2), // Audio buffer swap
    VI = (1 << 3), // Vertical-scan
    PI = (1 << 4), // DMA ROM->RAM copy done
    DP = (1 << 5), // RDP (GPU) done
  };

  // code address to jump to on an exception/interrupt
  enum Address {
    DEFAULT     = 0x8000'0180, 
    TLB_REFILL  = 0x8000'0000, 
    XTLB_REFILL = 0x8000'0080, 
    CACHE       = 0xA000'0100, 
  };

  class Handler
  {
    private:
    
    public:
      constexpr Handler() {}

      void checkInterrupts();

      // "Prepares" an interrupt by setting flags in registers, this does NOT jump to the interrupt vector.
      // If an RCP-interrupt was choosen, the type can be specified as the second arg.
      void interrupt(Type interrupt, RcpType rcpType = RcpType::NONE);

      // Directly clears an interrupt, RCP bits are un-effected
      void clearInterrupt(Type interrupt);

      // clears single RCP-specific interrupt bits, if all are cleared, the 'RCP' itself gets cleared
      void clearRcpInterrupt(RcpType interrupt);

      // Check if any interrupts are pending, if so, it interrupts and jumps to vector.
      void handlePending();
  
      // returns true if any interrupt is pending
      // This does NOT check any RCP-specific flags, only the main interrupt mask
      bool32 hasPendingInterrupt();

      void logState();
  };
};
