/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../../types.h"

struct FramebufferRect {
  u32 minX;
  u32 maxX;
  u32 minY;
  u32 maxY;
};

struct FramebufferInfo {
  void* framebuffer;
  u32 resolution[2];
  u32 pixelFormat;
  FramebufferRect rect;
  u32 horizontalSkip;
};

class GPU
{
  private:

  public:
    u32 getFramebufferAddress();
    void render();

    // this returns a pointer, since it will be read-out by the JS-host.
    FramebufferInfo* getFramebufferInfo();
};