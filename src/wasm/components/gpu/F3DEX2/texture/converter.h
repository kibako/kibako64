/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "math/vec2.h"
#include "types.h"
#include "textureTile.h"

namespace F3DEX2::Tex
{
  void convertToRGBA(const Tex::Tile &tile);
}
