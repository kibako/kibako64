/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "converter.h"
#include "F3DEX2/memory.h"
#include "logging.h"
#include "textureTile.h"
#include "byteswap.h"
#include "stdlib.h"
#include "F3DEX2/types/flagsOtherModeHigh.h"

/**
 * Texture format docs:
 * https://n64squid.com/homebrew/n64-sdk/textures/image-formats/
 */

namespace {
  constexpr f32 RGB5_FACTOR = 8.225806451612904f;  // 255.0f / 0b11111.0f;

  constexpr u32 ERR_TEX_MASK_ON  = 0b1111;
  constexpr u32 ERR_TEX_MASK_OFF = 0b1000;

  inline void writeRGBA5551(u8 *ptrOut, u16 color)
  {
    color = byteswap(color); // @TODO remove byteswap
    ptrOut[0] = ((color >> 11) & 0b11111) * 255 / 31;
    ptrOut[1] = ((color >>  6) & 0b11111) * 255 / 31;
    ptrOut[2] = ((color >>  1) & 0b11111) * 255 / 31;
    ptrOut[3] = (color & 1) * 0xFF;
  }

  inline void writeRA88(u8 *ptrOut, u16 color)
  {
    ptrOut[2] = ptrOut[1] = ptrOut[0] = color & 0xFF;
    ptrOut[3] = (color >> 8) & 0xFF;
  }

  inline void writeRA31(u8 *ptrOut, u8 color)
  {
    u8 alpha = (color & 1) * 0xFF;
    color >>= 1;
    u8 red = (u8)((u32)(color & 0b111) * 255 / 0b111);

    ptrOut[0] = red;
    ptrOut[1] = red;
    ptrOut[2] = red;
    ptrOut[3] = alpha;
  }

  inline void writeErrorImage(u8 *ptrOut, u32 width, u32 height)
  {
    for(u32 y=0; y<height; ++y)
    {
      u32 flipX = (y & ERR_TEX_MASK_OFF) ? ERR_TEX_MASK_ON : 0;
      for(u32 x=0; x<width; ++x)
      {
        u8 color = ((x ^ flipX) & ERR_TEX_MASK_OFF) ? 0xFF : 0x00;
        *(ptrOut++) = color;
        *(ptrOut++) = 0x00;
        *(ptrOut++) = color;
        *(ptrOut++) = 0xFF;
      }
    }
  }
}

namespace F3DEX2::Tex
{
  void convertRGBAToRGBA(u8 *data, u32 width, u32 height, u32 bitSize, u32 actualWidth)
  {
    printf_info("RGBA to RGBA: %dx%d [%d] %dbpp (real: 0x%08X)", width, height, width*height, bitSize, data);

    u8* ptrIn = data;
    u8* ptrOut = Mem::outTextureRGBA;

    if(bitSize == 16)
    {
      u32 rowSkip = (actualWidth - width) * 2;
      for(u32 h=0; h<height; ++h) {
        for(u32 w=0; w<width; ++w)
        {
          // RRRR RGGG GGBB BBBA (5,5,5,1)
          u16 rgba = (*((u16*)ptrIn)); ptrIn += 2;
          writeRGBA5551(ptrOut, rgba);
          ptrOut += 4;
        }
        ptrIn += rowSkip;
      }
    } else if(bitSize == 4)
    {
      u32 rowSkip = (actualWidth - width) / 2;
      for(u32 h=0; h<height; ++h) {
        for(u32 w=0; w<width; w+=2)
        {
          u8 byte = *(ptrIn++);
          u8 rgba0 = byte & 0xF;
          u8 rgba1 = (byte >> 4) & 0xF;

          *(ptrOut++) = (rgba0 & 0b0001) ? 0xFF : 0;
          *(ptrOut++) = (rgba0 & 0b0010) ? 0xFF : 0;
          *(ptrOut++) = (rgba0 & 0b0100) ? 0xFF : 0;
          *(ptrOut++) = (rgba0 & 0b1000) ? 0xFF : 0;

          *(ptrOut++) = (rgba1 & 0b0001) ? 0xFF : 0;
          *(ptrOut++) = (rgba1 & 0b0010) ? 0xFF : 0;
          *(ptrOut++) = (rgba1 & 0b0100) ? 0xFF : 0;
          *(ptrOut++) = (rgba1 & 0b1000) ? 0xFF : 0;
        }
        ptrIn += rowSkip;
      }
    } else if(bitSize == 32) {
      u32 rowSkip = (actualWidth - width) * 4;
      for(u32 h=0; h<height; ++h) {
        for(u32 w=0; w<width; ++w) {
          *(ptrOut++) = *(ptrIn++);
          *(ptrOut++) = *(ptrIn++);
          *(ptrOut++) = *(ptrIn++);
          *(ptrOut++) = *(ptrIn++);
        }
        ptrIn += rowSkip;
      }

    } else {
      writeErrorImage(ptrOut, width, height);
      printf_warn("RGBA unimplemented bitSize: %d", bitSize);
    }
  }

  void convertPaletteToRGBA(u8 *data, u32 width, u32 height, u32 bitSize, u32 actualWidth)
  {
      printf_info("PALETTE to RGBA: %dx%d [%d] %dbpp (real: 0x%08X)", width, height, width*height, bitSize, data);
      u8* ptrIn = data;
      u16* ptrPal = (u16*)(data + Mem::addressPAL); // usually +2KB
      u8* ptrOut = Mem::outTextureRGBA;

      bool useIA16 = Mem::Reg::otherModeHigh & F3DEX2::FlagsOtherModeH::PALETTE_IA16;
      auto &paletteFunc = useIA16 ? writeRA88 : writeRGBA5551;

      if(bitSize == 4)
      {
        u32 rowSkip = (actualWidth - width) / 2;
        for(u32 h=0; h<height; ++h) {
          for(u32 w=0; w<width; w+=2) {
            u8 palIdxA = *(ptrIn++);
            u8 palIdxB = palIdxA & 0xF;
            palIdxA = (palIdxA >> 4) & 0xF;

            paletteFunc(ptrOut, ptrPal[palIdxA]); ptrOut += 4;
            paletteFunc(ptrOut, ptrPal[palIdxB]); ptrOut += 4;
          }
          ptrIn += rowSkip;
        }
      }
      else if(bitSize == 8)
      {
        u32 rowSkip = (actualWidth - width);
        for(u32 h=0; h<height; ++h) {
          for(u32 w=0; w<width; ++w) {
            paletteFunc(ptrOut, ptrPal[*(ptrIn++)]);
            ptrOut += 4;
          }
          ptrIn += rowSkip;
        }
      } else {
        writeErrorImage(ptrOut, width, height);
        printf_error("Palette with unknown bitSize: %d", bitSize)
      }
  }

  void convertMonoToRGBA(u8 *data, u32 width, u32 height, u32 bitSize, u32 actualWidth)
  {
    printf_info("MONO to RGBA: %dx%d [%d] %dbpp (real: 0x%08X)", width, height, width*height, bitSize, data);

    u8* ptrIn = data;
    u8* ptrOut = Mem::outTextureRGBA;

    u32 rowSkip = (actualWidth - width) * bitSize / 8;

    for(u32 h=0; h<height; ++h)
    {
      for(u32 w=0; w<width; ++w)
      {
        u8 colorR0 = *(ptrIn++);

        if(bitSize == 16)
        {
          u32 colorR0_16 = (u32)colorR0 | ((u32)(*(ptrIn++)) << 8);
          colorR0 = (colorR0_16 / 0xFFFFu) * 0xFFu; // Note: looses color-depth
        } else if(bitSize == 4) {
          u8 colorR1 = ((colorR0 >> 4) & 0xF) * 0xF;
          colorR0 = (colorR0 & 0xF) * 0xF;

          *(ptrOut++) = colorR1;
          *(ptrOut++) = colorR1;
          *(ptrOut++) = colorR1;
          *(ptrOut++) = colorR1;
          ++w;
        }

        *(ptrOut++) = colorR0;
        *(ptrOut++) = colorR0;
        *(ptrOut++) = colorR0;
        *(ptrOut++) = colorR0;
      }
      ptrIn += rowSkip;
    }
  }

  void convertRedAlphaToRGBA(u8 *data, u32 width, u32 height, u32 bitSize, u32 actualWidth)
  {
    //printf_info("RA to RGBA: %dx%d [%d] %dbpp (real: 0x%08X)", width, height, pixel, bitSize, data);

    u8* ptrIn = data;
    u8* ptrOut = Mem::outTextureRGBA;
    u32 rowSkip = (actualWidth - width) * bitSize / 8;

    if(bitSize == 4)
    {
      for(u32 h=0; h<height; ++h) {
        for(u32 w=0; w<width; w+=2) {
          u8 byte = *(ptrIn++);
          writeRA31(ptrOut, byte);
          ptrOut += 4;
          writeRA31(ptrOut, byte >> 4);
          ptrOut += 4;
        }
        ptrIn += rowSkip;
      }
    }else if(bitSize == 8)
    {
        for(u32 h=0; h<height; ++h) {
          for(u32 w=0; w<width; ++w) {
            u8 byte = *(ptrIn++);
            u8 alpha = (byte & 0xF) * 17;
            u8 color = (byte >> 4) * 17;
            *(ptrOut++) = color;
            *(ptrOut++) = color;
            *(ptrOut++) = color;
            *(ptrOut++) = alpha;
          }
          ptrIn += rowSkip;
        }
    }else if(bitSize == 16)
    {
      for(u32 h=0; h<height; ++h) {
        for(u32 w=0; w<width; w++) {
          u8 color = *(ptrIn++);
          *(ptrOut++) = color;
          *(ptrOut++) = color;
          *(ptrOut++) = color;
          *(ptrOut++) = *(ptrIn++);
        }
        ptrIn += rowSkip;
      }
    } else {
      writeErrorImage(ptrOut, width, height);
      printf_error("RA with unknown bitSize: %d", bitSize)
    }
  }
}

void F3DEX2::Tex::convertToRGBA(const F3DEX2::Tex::Tile &tile)
{
  u8* dataPtr = Mem::texMem + tile.addressTMEM;
  switch (tile.format) {
    using enum ColorFormat;
    case RGBA      : convertRGBAToRGBA(    dataPtr, tile.width, tile.height, tile.bitSize, tile.actualWidth); break;
    case PALETTE   : convertPaletteToRGBA( dataPtr, tile.width, tile.height, tile.bitSize, tile.actualWidth); break;
    case MONO_ALPHA: convertRedAlphaToRGBA(dataPtr, tile.width, tile.height, tile.bitSize, tile.actualWidth); break;
    case MONO      : convertMonoToRGBA(    dataPtr, tile.width, tile.height, tile.bitSize, tile.actualWidth); break;

    default:
      printf_error("Unsupported texture format: %s [%dx%d, %dbpp]",
                   ColorFormatNames[(u32)tile.format], tile.width, tile.height, tile.bitSize)
      writeErrorImage(dataPtr, tile.width, tile.height);
      break;
  }
}
