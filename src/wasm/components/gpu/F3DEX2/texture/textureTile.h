/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "math/vec2.h"
#include "math/vec3.h"
#include "types.h"

namespace F3DEX2::Tex
{
  enum class ColorFormat : u32 {
    RGBA, YUV, PALETTE, MONO_ALPHA, MONO
  };

  const char* const ColorFormatNames[5] = {
    "RGBA", "YUV ", "PAL ", "REDA", "MONO"
  };

  namespace Wrapping {
    constexpr u32 REPEAT = 0;
    constexpr u32 MIRROR = (1 << 0);
    constexpr u32 CLAMP = (1 << 1);
  }

  const char* const WrappingNames[4] = {
    "REPEAT", "MIRROR", "CLAMP", "MIR+CLAMP"
  };

  struct Tile
  {
    Math::Vec2 scaleUV{};
    u32 tileNum{0};
    u32 mipmapLevel{0};
    bool enabled{false};

    u32 wrappingU{};
    u32 wrappingV{};
    u32 maskU{0};
    u32 maskV{0};

    u32 shiftU{0};
    u32 shiftV{0};
    Math::Vec2 shift{};

    u32 width{0}; // image width after which data would repeat / is unique
    u32 height{0};
    u32 actualWidth{0}; // actual with of the data that was loaded (ignored mask and other stuff)

    Math::Vec2 uvConvertFactor{}; // factor to convert s16 coords to normalized f32
    Math::Vec2 uvOffsetNorm{}; // offset to add to the normalized f32 UV
    Math::Vec2 uvEndNorm{}; // offset to add to the normalized f32 UV

    ColorFormat format{ColorFormat::RGBA};
    u32 bitSize{0};
    u32 valuesPerRow{0};
    u32 paletteNum{0};

    u32 loadOffsetX{0};
    u32 loadOffsetY{0};

    Math::Vec2 uvRangeStart{};
    Math::Vec2 uvRangeEnd{};

    u32 addressTMEM{0};

    u32 outputIndex{0};
    bool wasEmitted{false};

    [[nodiscard]] u32 getByteSize() const {
      return width * height * bitSize / 8;
    }

    u32 getHash(const u64* ptrTMEM) const {
      const u64* dataTMEM = (ptrTMEM + (addressTMEM / 8));
      u32 dwordSize = (4096 - addressTMEM) / 8;
      //u32 dwordSize = (getByteSize()) / 8;

      u64 hash = 0;
      for(u32 w=0; w<dwordSize; w+=2) {
        hash ^= dataTMEM[w];
      }

      hash ^= width | (height << 16);
      hash ^= (u32)format << 8;
      return (u32)hash ^ (hash >> 32);
    }

    [[nodiscard]] Math::Vec2 normalizeUV(const Math::Vec2 &uv) const {
      return (uv * uvConvertFactor * shift + uvOffsetNorm)
        * (scaleUV[0] > 0.0f ? scaleUV : Math::Vec2{1.0f, 1.0f})
        ;
    }
  };
}