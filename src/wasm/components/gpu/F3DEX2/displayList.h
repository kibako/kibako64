/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "main.h"
#include "memory.h"
#include "vertex.h"

namespace F3DEX2
{
    WASM_EXPORT_NAMED(gpuF3DEX2ParseDisplayList)
    u32 parseDisplayList(u32 address);

    WASM_EXPORT_NAMED(gpuF3DEX2SetSegmentAddress)
    void setSegmentAddress(u32 segment, u32 address);

    WASM_EXPORT_NAMED(gpuF3DEX2SyncSegmentAddress)
    void syncSegmentAddress(u32 addressRAM);

    WASM_EXPORT_NAMED(gpuF3DEX2Reset)
    void reset();

    WASM_EXPORT_NAMED(gpuF3DEX2GetOutputPointer)
    inline VertexGL* getOutputPointer() {
      return Mem::outVertices;
    }

    inline u32 resolveSegmentAddress(u32 address) {
      u32 type = (address >> 24) & 0x0F;
      return (F3DEX2::Mem::segmentTable[type] + address) & 0x00FFFFFF;
    }

    WASM_IMPORT_NAMED(gpuEmitTexture)
    void emitTexture(u32 address, u32 width, u32 height, u32 format, u32 bpp);
}