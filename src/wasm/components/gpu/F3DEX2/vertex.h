/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "F3DEX2/types/RGBA8.h"
#include "math/vec3.h"
#include "math/vec3S8.h"
#include "types.h"
#include "math/vec4.h"
#include "math/vec2.h"

namespace F3DEX2
{
  // N64's Vertex format
  struct VertexN64
  {
    s16 pos[3]{0};
    u16 unused00{0};
    s16 texcoord[2]{0};
    RGBA8 color{};
  };

  static_assert(sizeof(VertexN64) == 16, "Wrong alignment in VertexN64!");

  // Output Vertex for interleaved WebGL-buffer
  struct VertexGL
  {
    /*[0x00]*/ Math::Vec3 pos{};

    /*[0x0C]*/ u32 faceSettings[2]{0};
    /*[0x14]*/ u32 textureIds[2]{0};

    /*[0x1C]*/ Math::Vec4 uv{};
    /*[0x2C]*/ Math::Vec4 uvEnd{};
    /*[0x3C]*/ Math::Vec3S8 normals;
               s8 faceCulling{};

    /*[0x40]*/ RGBA8 colorA1{};
    /*[0x44]*/ RGBA8 colorB1{};
    /*[0x48]*/ RGBA8 colorC1{};
    /*[0x4C]*/ RGBA8 colorD1{};

    /*[0x50]*/ RGBA8 colorA2{};
    /*[0x54]*/ RGBA8 colorB2{};
    /*[0x58]*/ RGBA8 colorC2{};
    /*[0x5C]*/ RGBA8 colorD2{};
  };

  static_assert(sizeof(VertexGL) == 0x60, "Wrong alignment in VertexGL!");
}