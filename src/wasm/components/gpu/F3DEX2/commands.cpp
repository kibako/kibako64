/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "commands.h"
#include "color/colorCombiner.h"
#include "displayList.h"
#include "geometry.h"
#include "lighting/light.h"
#include "math/mat4.h"
#include "math/vec2.h"
#include "memory.h"
#include "ram.h"
#include "stdlib.h"
#include "texture/textureTile.h"
#include "types/flagsGeometryMode.h"
#include "vertex.h"
#include "F3DEX2/texture/converter.h"

namespace {
  enum class MoveWordType : u32 {
    MATRIX = 0,
    LIGHT_COUNT = 2,
    CLIP = 4,
    SEGMENT = 6,
    FOG = 8,
    LIGHT_COLOR = 10,
    FORCE_MATRIX = 12,
    PERSPECTIVE_NORMAL = 14,
  };

  enum class MoveMemType : u32 {
    VIEWPORT = 8,
    LIGHT = 10,
    MATRIX = 14
  };

  enum class ModifyVertType : u32 {
    RGBA = 0x10,
    UV = 0x14,
    SCREEN_XY = 0x18,
    SCREEN_Z = 0x1C
  };

  namespace MatrixFlags {
    constexpr u32 PUSH    = 1 << 0;
    constexpr u32 LOAD    = 1 << 1;
    constexpr u32 PROJECT = 1 << 2;
  }

  constexpr f32 MATRIX_FRACTION_FACTOR = 1.0f / 65535.f;
  constexpr f32 FIXED_0_16_FACTOR = 1.0f / 65535.f;
  constexpr f32 FIXED_10_5_FACTOR = 0.03125f; // 10.5 fixed to f32

  constexpr f32 SHIFT_VALUES[] = {
    1.0f,
    1.0f / (1 << 1),
    1.0f / (1 << 2),
    1.0f / (1 << 3),
    1.0f / (1 << 4),
    1.0f / (1 << 5),
    1.0f / (1 << 6),
    1.0f / (1 << 7),
    1.0f / (1 << 8),
    1.0f / (1 << 9),
    1.0f / (1 << 10),
    (f32)(1 << 5),
    (f32)(1 << 4),
    (f32)(1 << 3),
    (f32)(1 << 2),
    (f32)(1 << 1),
  };

  void printGeomMode(u32 mode) {
    printf_info("GeomMode: %s %s %s %s %s %s %s %s %s %s",
      mode & F3DEX2::FlagsGeomMode::ZBUFFER ? "ZBUFFER" : "-",
      mode & F3DEX2::FlagsGeomMode::SHADE ? "SHADE" : "-",
      mode & F3DEX2::FlagsGeomMode::CULL_FRONT ? "CULL_FRONT" : "-",
      mode & F3DEX2::FlagsGeomMode::CULL_BACK ? "CULL_BACK" : "-",
      mode & F3DEX2::FlagsGeomMode::FOG ? "FOG" : "-",
      mode & F3DEX2::FlagsGeomMode::LIGHTING ? "LIGHTING" : "-",
      mode & F3DEX2::FlagsGeomMode::TEXTURE_GEN ? "TEXTURE_GEN" : "-",
      mode & F3DEX2::FlagsGeomMode::TEXTURE_GEN_LINEAR ? "TEXTURE_GEN_LINEAR" : "-",
      mode & F3DEX2::FlagsGeomMode::SHADING_SMOOTH ? "SHADING_SMOOTH" : "-",
      mode & F3DEX2::FlagsGeomMode::CLIPPING ? "CLIPPING" : "-"
    )
  }

  u32 emitNewTextures()
  {
    u32 activeTile = 0;
    bool activeTileFound = false;
    bool emitNext = false;

    for(auto &tile : F3DEX2::Mem::tiles) {
      if((tile.enabled || emitNext) && !tile.wasEmitted && tile.width > 0 && tile.height > 0) {
        u32 textureHash = tile.getHash((u64*)F3DEX2::Mem::texMem);
        if(F3DEX2::Mem::textureUUIDs.find(textureHash, tile.outputIndex)) {
          printf_info("Texture already emitted, skip. %d [%dx%d] Hash: %08X", tile.tileNum, tile.width, tile.height, textureHash)
        } else {
          printf_info("Emit new Texture %d [%dx%d] Hash: %08X", tile.tileNum, tile.width, tile.height, textureHash)
          tile.outputIndex = F3DEX2::Mem::currentTextureIdx++;
          F3DEX2::Tex::convertToRGBA(tile);
          F3DEX2::emitTexture((u32)F3DEX2::Mem::outTextureRGBA, tile.width, tile.height, (u32)tile.format, tile.bitSize);
          F3DEX2::Mem::textureUUIDs.insert(textureHash, tile.outputIndex);
        }
        emitNext = emitNext ? false : F3DEX2::Mem::needsSecondTex;
      }
    }

    /*printf_info("Active-Tiles: %d %d %d %d  %d %d %d %d",
       F3DEX2::Mem::tiles[0].enabled, F3DEX2::Mem::tiles[1].enabled,
       F3DEX2::Mem::tiles[2].enabled, F3DEX2::Mem::tiles[3].enabled,
       F3DEX2::Mem::tiles[4].enabled, F3DEX2::Mem::tiles[5].enabled,
       F3DEX2::Mem::tiles[6].enabled, F3DEX2::Mem::tiles[7].enabled
    )*/

    for(auto &tile : F3DEX2::Mem::tiles) {
      if(tile.enabled) {
        tile.wasEmitted = true;
        if(!activeTileFound)activeTile = tile.tileNum;
        activeTileFound = true;
      }
    }
    return activeTile;
  }
};

void F3DEX2::Cmd::loadVertices(u64 data) {
  u32 vertexCount = (data >> 44) & 0b111111;
  u32 bufferIndex = (((data >> 32) & 0xFF) >> 1) - vertexCount;
  u32 offset = resolveSegmentAddress(static_cast<u32>(data));

  printf_trace("Loading vertices: %d to idx=%d from 0x%08X", vertexCount, bufferIndex, offset);

  u32 texIdx = emitNewTextures();

  u32 copySize = vertexCount * sizeof(VertexN64);
  if (!RAM::isInMemory(offset + copySize)) {
    printf_error("OOB Vertex copy from RAM! (offset=%08X, size=%08X)", offset, copySize);
    return;
  }

  auto vertN64Ptr = (VertexN64*)offset;
  for (u32 i = bufferIndex; i < (bufferIndex+vertexCount); ++i) {
    Geometry::parseVertex(Mem::vertexBuffer[i], *vertN64Ptr, texIdx);
    ++vertN64Ptr;
  }
}

void F3DEX2::Cmd::modifyVertex(u64 data)
{
  auto type = (ModifyVertType)((data >> 48) & 0xFF);
  u32 index = ((data >> 32) & 0xFFFF) >> 1;
  u32 value = static_cast<u32>(data);

  switch (type)
  {
    case ModifyVertType::UV:
      printf_info("Modify UV: [%08X] = %08X", index, value)
      Mem::vertexBuffer[index].uv = {
        // @TODO: handle this properly
        Mem::tiles[0].normalizeUV({(f32)(value >> 16), (f32)(value >> 16)}),
        Mem::tiles[1].normalizeUV({(f32)(value >> 16), (f32)(value >> 16)})
      };
    break;

    default:
      printf_warn("Unimplemented modifyVertex: %02X", type)
      break;
  }
}

void F3DEX2::Cmd::moveWord(u64 data)
{
  auto type = (MoveWordType) ((data >> 48) & 0xFF);
  u32 index = (data >> 32) & 0xFFFF;
  u32 value = static_cast<u32>(data);

  switch(type)
  {
    case MoveWordType::SEGMENT:
      setSegmentAddress(index >> 2, value);
      break;

    case MoveWordType::LIGHT_COUNT:
      Mem::lightCount = value / 24;
      break;

    default:
      printf_warn("Unimplemented moveWord (%d) [%d] = %08X", type, index, value);
  }
}

void F3DEX2::Cmd::moveMemory(u64 data) {
  u32 size = (data >> 48) & 0xFF;
  size = ((size >> 3) + 1) * 8;
  u32 offset = ((data >> 40) & 0xFF) * 8;
  auto index = static_cast<MoveMemType>((data >> 32) & 0xFF);
  u32 address = static_cast<u32>(data);

  switch (index) {
    case MoveMemType::LIGHT:
      // convert from address to light index
      offset /= sizeof(Light)*2;
      offset -= 2;
      if(offset < Mem::LIGHT_SLOTS) {
        //printf_info("Loading Light (size=%08X, offset=%08X, idx=%08X, address=%08X)", size, offset, index, address);
        u32 addressRam = resolveSegmentAddress(address);
        memcpy(&Mem::lights[offset], (u8*)addressRam, size);
        //printf_info("  Loaded Light, color=%02X%02X%02X", Mem::lights[offset].color.rgba[0], Mem::lights[offset].color.rgba[1], Mem::lights[offset].color.rgba[2]);
      } else {
        printf_warn("Loading light OOB! (size=%08X, offset=%08X, idx=%08X, address=%08X)", size, offset, index, address);
      }
      break;

    default:
      printf_warn("Unhandled moveMemory! (size=%08X, offset=%08X, idx=%08X, address=%08X)", size, offset, index, address);
  }
}

void F3DEX2::Cmd::renderTriangle(u64 data)
{
  Geometry::emitTriangle(
    ((data >> 48) & 0xFF) >> 1, // @TODO: optimize shift
    ((data >> 40) & 0xFF) >> 1,
    ((data >> 32) & 0xFF) >> 1
  );
}

void F3DEX2::Cmd::renderQuad(u64 data)
{
  Geometry::emitTriangle(
    ((data >> 48) & 0xFF) >> 1,
    ((data >> 40) & 0xFF) >> 1,
    ((data >> 32) & 0xFF) >> 1
  );
  Geometry::emitTriangle(
    ((data >> 16) & 0xFF) >> 1,
    ((data >>  8) & 0xFF) >> 1,
    ((data >>  0) & 0xFF) >> 1
  );
}

void F3DEX2::Cmd::matrixPush(u64 data)
{
  u32 flags = (data >> 32) & 0xFF;
  flags ^= MatrixFlags::PUSH; // why?

  u32 address = resolveSegmentAddress(static_cast<u32>(data));

  printf_info("Load Matrix: 0x%08X [org:0x%08X] (%s, %s, %s), currentIdx: %d", address, (u32)data,
         (flags & MatrixFlags::PUSH) ? "Push" : "Overwrite",
         (flags & MatrixFlags::LOAD) ? "Load" : "Multi",
         (flags & MatrixFlags::PROJECT) ? "Projection" : "ModelView",
         Mem::matrixStackIdx
         );

  if(flags & MatrixFlags::PROJECT) {
    return;
  }

  Math::Mat4 mat{};
  for(u32 i=0; i<16; ++i)
  {
    s16 intPart = (s16)RAM::read<u16>(address + (i*2));
    f32 fractionPart = (f32)RAM::read<u16>(address + (i*2) + 32);
    f32 value = (f32)intPart + (fractionPart * MATRIX_FRACTION_FACTOR);
    mat.getFlat()[i] = value;
    //mat[i%4][i/4] = value;
    //mat[i/4][i%4] = value;
  }

  if(address < 0x1000) // segment might be empty, replace with default matrix
  {
    printf_warn("Tried to load Matrix from weird location!: 0x%08X", address)
    mat = Math::Mat4{};
  }

  if(!(flags & MatrixFlags::LOAD))
  {
    mat = mat * Mem::matrixStack[Mem::matrixStackIdx];
  }

  printf_info("Matrix:\n%.2f %.2f %.2f %.2f\n%.2f %.2f %.2f %.2f\n%.2f %.2f %.2f %.2f\n%.2f %.2f %.2f %.2f",
         mat.getFlat()[0], mat.getFlat()[1], mat.getFlat()[2], mat.getFlat()[3],
         mat.getFlat()[4], mat.getFlat()[5], mat.getFlat()[6], mat.getFlat()[7],
         mat.getFlat()[8], mat.getFlat()[9], mat.getFlat()[10], mat.getFlat()[11],
         mat.getFlat()[12], mat.getFlat()[13], mat.getFlat()[14], mat.getFlat()[15]
         );

  if(flags & MatrixFlags::PUSH)
  {
    ++Mem::matrixStackIdx;
    if(Mem::matrixStackIdx >= (Mem::MATRIX_SLOTS)) {
      --Mem::matrixStackIdx;
      print_error("GPU: Max. Matrix-Stack size reached!");
    }
  }

  Mem::matrixStack[Mem::matrixStackIdx] = mat;
  Mem::matrixStackNormal[Mem::matrixStackIdx] = mat.toNormalMat();
}

void F3DEX2::Cmd::matrixPop(u64 data)
{
  u32 num = static_cast<u32>(data) / 64;
  if(Mem::matrixStackIdx >= num) {
    Mem::matrixStackIdx -= num;
  } else {
    printf_warn("Tried to pop more matrices than set: %d >= %d", num, Mem::matrixStackIdx)
    Mem::matrixStackIdx = 0;
  }
}

void F3DEX2::Cmd::setOtherModeLow(u64 data)
{
  u32 length = ((data >> 32) & 0xFF) + 1;
  u32 shift = 32 - length - ((data >> 40) & 0xFF);
  u32 values = static_cast<u32>(data);
  Mem::Reg::otherModeLow = (Mem::Reg::otherModeLow & ~(((1u << length) - 1u) << shift)) | values;
  //printf_info("OtherL new: " PRINTF_U32_BIN_ARG, PRINTF_U32_BIN_VAL(Mem::Reg::otherModeLow));
}

void F3DEX2::Cmd::setOtherModeHigh(u64 data)
{
  u32 length = ((data >> 32) & 0xFF) + 1;
  u32 shift = 32 - length - ((data >> 40) & 0xFF);
  u32 values = static_cast<u32>(data);
  Mem::Reg::otherModeHigh = (Mem::Reg::otherModeHigh & ~(((1u << length) - 1u) << shift)) | values;
  //printf_info("OtherH: len:%d, shift:%d, val:0x%08X", length, shift, data);
  //printf_info("OtherH new: " PRINTF_U32_BIN_ARG, PRINTF_U32_BIN_VAL(Mem::Reg::otherModeHigh));
}

void F3DEX2::Cmd::setPrimitiveColor(u64 data) {
  Mem::Reg::primitiveLODFrac = (data >> 32) & 0xFF;
  Mem::Reg::primitiveColor = RGBA8::parse((u32)data);
}

void F3DEX2::Cmd::setFillColor(u64 data) {
  Mem::Reg::fillColor = RGBA8::parse((u32)data);
}

void F3DEX2::Cmd::setFogColor(u64 data) {
  Mem::Reg::fogColor = RGBA8::parse((u32)data);
}

void F3DEX2::Cmd::setBlendColor(u64 data) {
  Mem::Reg::blendColor = RGBA8::parse((u32)data);
}

void F3DEX2::Cmd::setEnvColor(u64 data) {
  Mem::Reg::envColor = RGBA8::parse((u32)data);
}

void F3DEX2::Cmd::setGeometryMode(u64 data) {
  u32 setBits = static_cast<u32>(data);
  u32 clearMask = (data >> 32);
  Mem::Reg::geomMode = (Mem::Reg::geomMode & clearMask) | setBits;
  //printGeomMode(Mem::Reg::geomMode);

  Mem::Reg::useLighting = Mem::Reg::geomMode & FlagsGeomMode::LIGHTING;
}

void F3DEX2::Cmd::setCombine(u64 data) {
  #define COMB_ALPHA(IDX, BITS) Mem::Reg::alphaCombine[Color::Idx::IDX] = data & ((1u<<BITS)-1u); data >>= BITS;
  #define COMB_COLOR(IDX, BITS) Mem::Reg::colorCombine[Color::Idx::IDX] = data & ((1u<<BITS)-1u); data >>= BITS;

  COMB_ALPHA(D2, 3);
  COMB_ALPHA(B2, 3);
  COMB_COLOR(D2, 3);
  COMB_ALPHA(D1, 3);
  COMB_ALPHA(B1, 3);
  COMB_COLOR(D1, 3);
  COMB_ALPHA(C2, 3);
  COMB_ALPHA(A2, 3);
  COMB_COLOR(B2, 4);
  COMB_COLOR(B1, 4);
  COMB_COLOR(C2, 5);
  COMB_COLOR(A2, 4);
  COMB_ALPHA(C1, 3);
  COMB_ALPHA(A1, 3);
  COMB_COLOR(C1, 5);
  COMB_COLOR(A1, 4);

  Color::updateColorCombiner();
}

void F3DEX2::Cmd::setTexture(u64 data) {
  u32 tileIdx = (data >> 40) & 0b111;
  auto &tile = Mem::tiles[tileIdx];
  bool enable = data & 0x0000'007F'0000'0000;
  tile.enabled = enable;

  //if(tile.enabled) {
    tile.mipmapLevel = (data >> 43) & 0b111;
    tile.scaleUV[0] = static_cast<f32>((data >> 16) & 0xFFFF) * FIXED_0_16_FACTOR;
    tile.scaleUV[1] = static_cast<f32>(data & 0xFFFF) * FIXED_0_16_FACTOR;
    tile.wasEmitted = false;
  //}

  printf_info("setTexture[%d]: enable=%d, scale=[%.2f, %.2f]", tileIdx, tile.enabled, tile.scaleUV[0], tile.scaleUV[1]);
}

void F3DEX2::Cmd::setImage(u64 data) {

  Mem::currentImage.addressRam = static_cast<u32>(data);
  u32 flags = data >> 51;
  Mem::currentImage.pixelBitSize = flags & 0b11;
  Mem::currentImage.pixelBitSize = 1 << (Mem::currentImage.pixelBitSize+2); // enum to actual bits (4,8,16,32)

  Mem::currentImage.format = (Tex::ColorFormat)((flags >> 2) & 0b111);

  printf_info("SetImage: f:%s, bpp:%d RAM:0x%08X",
    Tex::ColorFormatNames[(u32)Mem::currentImage.format],  Mem::currentImage.pixelBitSize, Mem::currentImage.addressRam
  )
}

void F3DEX2::Cmd::setTile(u64 data) {
  u32 tileIdx = (data >> 24) & 0b111;
  auto &tile = Mem::tiles[tileIdx];

  tile.format = (Tex::ColorFormat)((data >> 53) & 0b111);
  tile.bitSize = (data >> 51) & 0b11;
  tile.bitSize = 1 << (tile.bitSize+2); // enum to actual bits (4,8,16,32)

  tile.valuesPerRow = ((data >> 41) & 0b1'1111'1111);
  tile.addressTMEM = ((data >> 32) & 0b1'1111'1111) * 8;
  tile.paletteNum = (data >> 20) & 0b1111;

  tile.maskV = (data >> 14) & 0b1111;
  tile.shiftV = (data >> 10) & 0b1111;
  tile.wrappingV = ((data >> 18) & 0b11)
    | (tile.maskV == 0 ? F3DEX2::Tex::Wrapping::CLAMP : 0); // force clamp for zero-mask

  tile.maskU = (data >> 4) & 0b1111;
  tile.shiftU = (data >> 0) & 0b1111;
  tile.wrappingU = ((data >> 8) & 0b11)
    | (tile.maskU == 0 ? F3DEX2::Tex::Wrapping::CLAMP : 0); // force clamp for zero-mask

  tile.shift = {SHIFT_VALUES[tile.shiftU], SHIFT_VALUES[tile.shiftV]};

  tile.wasEmitted = false;
  tile.actualWidth = (tile.bitSize > 0)
    ? ((tile.valuesPerRow << 3) * 8 / tile.bitSize)
    : 0;

  if(tile.bitSize == 32)tile.actualWidth *= 2; // I still don't know why...

  printf_info("SetTile[%d]: f:%s, bbp:%d, val/row:%d, width: %d, TMEM:0x%08X, pal:%d, wrap: %s|%s [%d|%d], UV-Mask: %d %d, UV-Shift: %d %d",
    tileIdx, Tex::ColorFormatNames[(u32)tile.format], tile.bitSize, tile.valuesPerRow<<3, tile.actualWidth, tile.addressTMEM, tile.paletteNum,
    Tex::WrappingNames[(u32)tile.wrappingU], Tex::WrappingNames[(u32)tile.wrappingV],
    (u32)tile.wrappingU, (u32)tile.wrappingV,
    tile.maskU, tile.maskV,
    tile.shiftU, tile.shiftV
  )
}

void F3DEX2::Cmd::setTileSize(u64 data) {
  u32 tileIdx = (data >> 24) & 0b111;
  auto &tile = Mem::tiles[tileIdx];

  tile.uvRangeStart[0] = static_cast<f32>((data >> 44) & 0xFFF) * 0.25f;
  tile.uvRangeStart[1] = static_cast<f32>((data >> 32) & 0xFFF) * 0.25f;

  tile.uvRangeEnd[0] = static_cast<f32>((data >> 12) & 0xFFF) * 0.25f;
  tile.uvRangeEnd[1] = static_cast<f32>((data >>  0) & 0xFFF) * 0.25f;

  u32 widthRaw = static_cast<u32>(tile.uvRangeEnd[0] - tile.uvRangeStart[0]) + 1;
  u32 heightRaw = static_cast<u32>(tile.uvRangeEnd[1] - tile.uvRangeStart[1]) + 1;

  tile.width  = tile.maskU == 0 ? widthRaw : (1 << tile.maskU);
  tile.height = tile.maskV == 0 ? heightRaw : (1 << tile.maskV);

  Math::Vec2 invSize{
    1.0f / (f32)tile.width,
    1.0f / (f32)tile.height,
  };

  tile.uvConvertFactor = invSize * FIXED_10_5_FACTOR;

  tile.uvOffsetNorm = {
    (f32)tile.uvRangeStart[0] * -invSize.x(),
    (f32)tile.uvRangeStart[1] * -invSize.y(),
  };
  tile.uvEndNorm = {
    (f32)(tile.uvRangeEnd[0] - tile.uvRangeStart[0]) / (f32)(tile.width),
    (f32)(tile.uvRangeEnd[1] - tile.uvRangeStart[1]) / (f32)(tile.height),
  };

  tile.wasEmitted = false;

  printf_info("SetTileSize[%d]: uv: [%.4f, %.4f] -> [%.4f, %.4f], uvNorm: [%.4f, %.4f] -> [%.4f, %.4f], w/h: %dx%d, scale=[%.4f, %.4f]", tileIdx,
    tile.uvRangeStart[0], tile.uvRangeStart[1],
    tile.uvRangeEnd[0], tile.uvRangeEnd[1],
    tile.uvOffsetNorm[0],tile.uvOffsetNorm[1],
    tile.uvEndNorm[0],tile.uvEndNorm[1],
    tile.width, tile.height,
    tile.scaleUV[0], tile.scaleUV[1]
  )
}

void F3DEX2::Cmd::loadBlock(u64 data) {
  u32 tileIdx = (data >> 24) & 0b111;
  auto &tile = Mem::tiles[tileIdx];

  tile.loadOffsetX = (data >> 32) & 0xFFF;
  tile.loadOffsetY = (data >> 44) & 0xFFF;

  u32 texelCount = ((data >> 12) & 0xFFF) + 1;
  f32 dtx = (f32)(data & 0xFFF) / 2048.0f;

  u32 sizeBytes = texelCount * tile.bitSize / 8;
  u32 addressRam = resolveSegmentAddress(Mem::currentImage.addressRam);

  u8* addressStart = Mem::texMem + tile.addressTMEM;

  printf_info("LoadBlock[%d]: texel:%d@%db (global: %db) f:%d=%s, dtx:%.4f, offset: [%d, %d], val/row:%d, size:%d, 0x%08X -> 0x%08X (real: 0x%08X)",
              tileIdx, texelCount, tile.bitSize, Mem::currentImage.pixelBitSize, (u32)tile.format, Tex::ColorFormatNames[(u32)tile.format],
              dtx, tile.loadOffsetX, tile.loadOffsetY, tile.valuesPerRow, sizeBytes, addressRam, tile.addressTMEM, addressStart)

  if((tile.addressTMEM + sizeBytes) > Mem::TMEM_SIZE) {
    printf_error("TMEM OOB load! (%08X + %08X > %08X)", tile.addressTMEM, sizeBytes, Mem::TMEM_SIZE)
    return;
  }

  memcpy(addressStart, (u8*)addressRam, sizeBytes);
  tile.wasEmitted = false;
}

void F3DEX2::Cmd::loadPalette(u64 data) {
  u32 tileIdx = (data >> 24) & 0b111;
  auto &tile = Mem::tiles[tileIdx];
  u32 count = ((data >> 12) & 0xFFF) + 1;

  u32 sizeBytes = (count * 2);
  u32 addressRam = resolveSegmentAddress(Mem::currentImage.addressRam);

  u8* addressStart = Mem::texMem + tile.addressTMEM;
  if((tile.addressTMEM + sizeBytes) > Mem::TMEM_SIZE) {
    printf_error("TMEM Palette OOB load! (%08X + %08X > %08X)", tile.addressTMEM, sizeBytes, Mem::TMEM_SIZE)
    return;
  }

  Mem::addressPAL = tile.addressTMEM;
  memcpy(addressStart, (u8*)addressRam, sizeBytes);

  printf_info("LoadPalette[%d]: colors:%d,  RAM:0x%08X -> TMEM:0x%08X", tileIdx, count, Mem::currentImage.addressRam, tile.addressTMEM)
}

void F3DEX2::Cmd::setRDPHalfLow(u64 data) {
  Mem::Reg::rdpLow = static_cast<u32>(data);
}

void F3DEX2::Cmd::setRDPHalfHigh(u64 data) {
  Mem::Reg::rdpHigh = static_cast<u32>(data);
}

void F3DEX2::Cmd::branchZ(u64 data) {
  u32 zValue = static_cast<u32>(data);
  u32 vertIndex = (data >> 32) & 0xFFF;
  vertIndex <<= 1;

  printf_info("BranchZ: z=%08X, v=%08X, ptr=%08X", zValue, vertIndex, Mem::Reg::rdpHigh);
}
