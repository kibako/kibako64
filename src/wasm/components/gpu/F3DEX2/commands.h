/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "types.h"

namespace F3DEX2::Cmd
{
  typedef void (*F3DEX2Function)(u64);

  inline void nop([[maybe_unused]] u64 data) {}

  void loadVertices(u64 data);
  void modifyVertex(u64 data);
  void moveWord(u64 data);
  void moveMemory(u64 data);

  void setRDPHalfHigh(u64 data);
  void setRDPHalfLow(u64 data);

  void renderTriangle(u64 data);
  void renderQuad(u64 data);

  void branchZ(u64 data);
  void matrixPush(u64 data);
  void matrixPop(u64 data);

  void setOtherModeLow(u64 data);
  void setOtherModeHigh(u64 data);
  void setGeometryMode(u64 data);
  void setCombine(u64 data);

  void setFillColor(u64 data);
  void setFogColor(u64 data);
  void setBlendColor(u64 data);
  void setPrimitiveColor(u64 data);
  void setEnvColor(u64 data);

  void setTexture(u64 data);
  void setImage(u64 data);
  void setTile(u64 data);
  void setTileSize(u64 data);
  void loadBlock(u64 data); // actually loads data
  void loadPalette(u64 data);

  inline F3DEX2Function cmdTable[0x100] = {
    /* [0x00] */ nop,
    /* [0x01] */ loadVertices,
    /* [0x02] */ modifyVertex, // MODIFYVTX
    /* [0x03] */ nop, // CULLDL
    /* [0x04] */ branchZ, // BRANCH_Z
    /* [0x05] */ renderTriangle,
    /* [0x06] */ renderQuad,
    /* [0x07] */ renderQuad,
    /* [0x08] */ nullptr, // undef
    /* [0x09] */ nullptr, // undef
    /* [0x0A] */ nullptr, // undef
    /* [0x0B] */ nullptr, // undef
    /* [0x0C] */ nullptr, // undef
    /* [0x0D] */ nullptr, // undef
    /* [0x0E] */ nullptr, // undef
    /* [0x0F] */ nullptr, // undef
    /* [0x10] */ nullptr, // undef
    /* [0x11] */ nullptr, // undef
    /* [0x12] */ nullptr, // undef
    /* [0x13] */ nullptr, // undef
    /* [0x14] */ nullptr, // undef
    /* [0x15] */ nullptr, // undef
    /* [0x16] */ nullptr, // undef
    /* [0x17] */ nullptr, // undef
    /* [0x18] */ nullptr, // undef
    /* [0x19] */ nullptr, // undef
    /* [0x1A] */ nullptr, // undef
    /* [0x1B] */ nullptr, // undef
    /* [0x1C] */ nullptr, // undef
    /* [0x1D] */ nullptr, // undef
    /* [0x1E] */ nullptr, // undef
    /* [0x1F] */ nullptr, // undef
    /* [0x20] */ nullptr, // undef
    /* [0x21] */ nullptr, // undef
    /* [0x22] */ nullptr, // undef
    /* [0x23] */ nullptr, // undef
    /* [0x24] */ nullptr, // undef
    /* [0x25] */ nullptr, // undef
    /* [0x26] */ nullptr, // undef
    /* [0x27] */ nullptr, // undef
    /* [0x28] */ nullptr, // undef
    /* [0x29] */ nullptr, // undef
    /* [0x2A] */ nullptr, // undef
    /* [0x2B] */ nullptr, // undef
    /* [0x2C] */ nullptr, // undef
    /* [0x2D] */ nullptr, // undef
    /* [0x2E] */ nullptr, // undef
    /* [0x2F] */ nullptr, // undef
    /* [0x30] */ nullptr, // undef
    /* [0x31] */ nullptr, // undef
    /* [0x32] */ nullptr, // undef
    /* [0x33] */ nullptr, // undef
    /* [0x34] */ nullptr, // undef
    /* [0x35] */ nullptr, // undef
    /* [0x36] */ nullptr, // undef
    /* [0x37] */ nullptr, // undef
    /* [0x38] */ nullptr, // undef
    /* [0x39] */ nullptr, // undef
    /* [0x3A] */ nullptr, // undef
    /* [0x3B] */ nullptr, // undef
    /* [0x3C] */ nullptr, // undef
    /* [0x3D] */ nullptr, // undef
    /* [0x3E] */ nullptr, // undef
    /* [0x3F] */ nullptr, // undef
    /* [0x40] */ nullptr, // undef
    /* [0x41] */ nullptr, // undef
    /* [0x42] */ nullptr, // undef
    /* [0x43] */ nullptr, // undef
    /* [0x44] */ nullptr, // undef
    /* [0x45] */ nullptr, // undef
    /* [0x46] */ nullptr, // undef
    /* [0x47] */ nullptr, // undef
    /* [0x48] */ nullptr, // undef
    /* [0x49] */ nullptr, // undef
    /* [0x4A] */ nullptr, // undef
    /* [0x4B] */ nullptr, // undef
    /* [0x4C] */ nullptr, // undef
    /* [0x4D] */ nullptr, // undef
    /* [0x4E] */ nullptr, // undef
    /* [0x4F] */ nullptr, // undef
    /* [0x50] */ nullptr, // undef
    /* [0x51] */ nullptr, // undef
    /* [0x52] */ nullptr, // undef
    /* [0x53] */ nullptr, // undef
    /* [0x54] */ nullptr, // undef
    /* [0x55] */ nullptr, // undef
    /* [0x56] */ nullptr, // undef
    /* [0x57] */ nullptr, // undef
    /* [0x58] */ nullptr, // undef
    /* [0x59] */ nullptr, // undef
    /* [0x5A] */ nullptr, // undef
    /* [0x5B] */ nullptr, // undef
    /* [0x5C] */ nullptr, // undef
    /* [0x5D] */ nullptr, // undef
    /* [0x5E] */ nullptr, // undef
    /* [0x5F] */ nullptr, // undef
    /* [0x60] */ nullptr, // undef
    /* [0x61] */ nullptr, // undef
    /* [0x62] */ nullptr, // undef
    /* [0x63] */ nullptr, // undef
    /* [0x64] */ nullptr, // undef
    /* [0x65] */ nullptr, // undef
    /* [0x66] */ nullptr, // undef
    /* [0x67] */ nullptr, // undef
    /* [0x68] */ nullptr, // undef
    /* [0x69] */ nullptr, // undef
    /* [0x6A] */ nullptr, // undef
    /* [0x6B] */ nullptr, // undef
    /* [0x6C] */ nullptr, // undef
    /* [0x6D] */ nullptr, // undef
    /* [0x6E] */ nullptr, // undef
    /* [0x6F] */ nullptr, // undef
    /* [0x70] */ nullptr, // undef
    /* [0x71] */ nullptr, // undef
    /* [0x72] */ nullptr, // undef
    /* [0x73] */ nullptr, // undef
    /* [0x74] */ nullptr, // undef
    /* [0x75] */ nullptr, // undef
    /* [0x76] */ nullptr, // undef
    /* [0x77] */ nullptr, // undef
    /* [0x78] */ nullptr, // undef
    /* [0x79] */ nullptr, // undef
    /* [0x7A] */ nullptr, // undef
    /* [0x7B] */ nullptr, // undef
    /* [0x7C] */ nullptr, // undef
    /* [0x7D] */ nullptr, // undef
    /* [0x7E] */ nullptr, // undef
    /* [0x7F] */ nullptr, // undef
    /* [0x80] */ nullptr, // undef
    /* [0x81] */ nullptr, // undef
    /* [0x82] */ nullptr, // undef
    /* [0x83] */ nullptr, // undef
    /* [0x84] */ nullptr, // undef
    /* [0x85] */ nullptr, // undef
    /* [0x86] */ nullptr, // undef
    /* [0x87] */ nullptr, // undef
    /* [0x88] */ nullptr, // undef
    /* [0x89] */ nullptr, // undef
    /* [0x8A] */ nullptr, // undef
    /* [0x8B] */ nullptr, // undef
    /* [0x8C] */ nullptr, // undef
    /* [0x8D] */ nullptr, // undef
    /* [0x8E] */ nullptr, // undef
    /* [0x8F] */ nullptr, // undef
    /* [0x90] */ nullptr, // undef
    /* [0x91] */ nullptr, // undef
    /* [0x92] */ nullptr, // undef
    /* [0x93] */ nullptr, // undef
    /* [0x94] */ nullptr, // undef
    /* [0x95] */ nullptr, // undef
    /* [0x96] */ nullptr, // undef
    /* [0x97] */ nullptr, // undef
    /* [0x98] */ nullptr, // undef
    /* [0x99] */ nullptr, // undef
    /* [0x9A] */ nullptr, // undef
    /* [0x9B] */ nullptr, // undef
    /* [0x9C] */ nullptr, // undef
    /* [0x9D] */ nullptr, // undef
    /* [0x9E] */ nullptr, // undef
    /* [0x9F] */ nullptr, // undef
    /* [0xA0] */ nullptr, // undef
    /* [0xA1] */ nullptr, // undef
    /* [0xA2] */ nullptr, // undef
    /* [0xA3] */ nullptr, // undef
    /* [0xA4] */ nullptr, // undef
    /* [0xA5] */ nullptr, // undef
    /* [0xA6] */ nullptr, // undef
    /* [0xA7] */ nullptr, // undef
    /* [0xA8] */ nullptr, // undef
    /* [0xA9] */ nullptr, // undef
    /* [0xAA] */ nullptr, // undef
    /* [0xAB] */ nullptr, // undef
    /* [0xAC] */ nullptr, // undef
    /* [0xAD] */ nullptr, // undef
    /* [0xAE] */ nullptr, // undef
    /* [0xAF] */ nullptr, // undef
    /* [0xB0] */ nullptr, // undef
    /* [0xB1] */ nullptr, // undef
    /* [0xB2] */ nullptr, // undef
    /* [0xB3] */ nullptr, // undef
    /* [0xB4] */ nullptr, // undef
    /* [0xB5] */ nullptr, // undef
    /* [0xB6] */ nullptr, // undef
    /* [0xB7] */ nullptr, // undef
    /* [0xB8] */ nullptr, // undef
    /* [0xB9] */ nullptr, // undef
    /* [0xBA] */ nullptr, // undef
    /* [0xBB] */ nullptr, // undef
    /* [0xBC] */ nullptr, // undef
    /* [0xBD] */ nullptr, // undef
    /* [0xBE] */ nullptr, // undef
    /* [0xBF] */ nullptr, // undef
    /* [0xC0] */ nullptr, // undef
    /* [0xC1] */ nullptr, // undef
    /* [0xC2] */ nullptr, // undef
    /* [0xC3] */ nullptr, // undef
    /* [0xC4] */ nullptr, // undef
    /* [0xC5] */ nullptr, // undef
    /* [0xC6] */ nullptr, // undef
    /* [0xC7] */ nullptr, // undef
    /* [0xC8] */ nullptr, // undef
    /* [0xC9] */ nullptr, // undef
    /* [0xCA] */ nullptr, // undef
    /* [0xCB] */ nullptr, // undef
    /* [0xCC] */ nullptr, // undef
    /* [0xCD] */ nullptr, // undef
    /* [0xCE] */ nullptr, // undef
    /* [0xCF] */ nullptr, // undef
    /* [0xD0] */ nullptr, // undef
    /* [0xD1] */ nullptr, // undef
    /* [0xD2] */ nullptr, // undef
    /* [0xD3] */ nullptr, // SPECIAL_3
    /* [0xD4] */ nullptr, // SPECIAL_2
    /* [0xD5] */ nullptr, // SPECIAL_1
    /* [0xD6] */ nullptr, // DMA_IO
    /* [0xD7] */ setTexture,
    /* [0xD8] */ matrixPop,
    /* [0xD9] */ setGeometryMode,
    /* [0xDA] */ matrixPush,
    /* [0xDB] */ moveWord,
    /* [0xDC] */ moveMemory,
    /* [0xDD] */ nullptr, // LOAD_UCODE
    /* [0xDE] */ nullptr, // DL // Note: handled in loop
    /* [0xDF] */ nullptr, // ENDDL // Note: handled in loop
    /* [0xE0] */ nullptr, // SPNOOP
    /* [0xE1] */ setRDPHalfHigh,
    /* [0xE2] */ setOtherModeLow,
    /* [0xE3] */ setOtherModeHigh,
    /* [0xE4] */ nullptr, // TEXRECT
    /* [0xE5] */ nullptr, // TEXRECTFLIP
    /* [0xE6] */ nop, // RDPLOADSYNC
    /* [0xE7] */ nop, // RDPPIPESYNC
    /* [0xE8] */ nop, // RDPTILESYNC
    /* [0xE9] */ nullptr, // RDPFULLSYNC
    /* [0xEA] */ nullptr, // SETKEYGB
    /* [0xEB] */ nullptr, // SETKEYR
    /* [0xEC] */ nullptr, // SETCONVERT
    /* [0xED] */ nullptr, // SETSCISSOR
    /* [0xEE] */ nullptr, // SETPRIMDEPTH
    /* [0xEF] */ nullptr, // RDPSETOTHERMODE
    /* [0xF0] */ loadPalette,
    /* [0xF1] */ setRDPHalfLow, // RDPHALF_2
    /* [0xF2] */ setTileSize, // SETTILESIZE,
    /* [0xF3] */ loadBlock, // LOADBLOCK,
    /* [0xF4] */ nullptr, // LOADTILE
    /* [0xF5] */ setTile,
    /* [0xF6] */ nullptr, // FILLRECT
    /* [0xF7] */ setFillColor,
    /* [0xF8] */ setFogColor,
    /* [0xF9] */ setBlendColor,
    /* [0xFA] */ setPrimitiveColor,
    /* [0xFB] */ setEnvColor,
    /* [0xFC] */ setCombine,
    /* [0xFD] */ setImage,
    /* [0xFE] */ nullptr, // SETZIMG
    /* [0xFF] */ nullptr, // SETCIMG
  };
}