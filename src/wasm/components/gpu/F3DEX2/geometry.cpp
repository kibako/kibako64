/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#include "geometry.h"
#include "byteswap.h"
#include "color/colorCombiner.h"
#include "logging.h"
#include "math/vec2.h"
#include "math/vec4.h"
#include "memory.h"
#include "texture/textureTile.h"
#include "types/faceSettingsGL.h"
#include "types/flagsGeometryMode.h"
#include "vertex.h"
#include "math/vec3S8.h"
#include "stdlib/stdlib.h"

namespace FaceFlags = F3DEX2::GpuObjectFaceSettings;

void F3DEX2::Geometry::parseVertex(F3DEX2::VertexGL &vertGL, const F3DEX2::VertexN64 &vertN64, u32 activeTileIdx) {
  const auto &tile = F3DEX2::Mem::tiles[activeTileIdx];
  const auto &tileNext = F3DEX2::Mem::tiles[activeTileIdx + 1];

  const auto &mat = F3DEX2::Mem::matrixStack[F3DEX2::Mem::matrixStackIdx];

  u32 geomMode = F3DEX2::Mem::Reg::geomMode;

  vertGL.faceSettings[0] = 0
     | ((tile.wrappingU & F3DEX2::Tex::Wrapping::MIRROR) ? FaceFlags::TEX_0_U_MIRROR : 0)
     | ((tile.wrappingU & F3DEX2::Tex::Wrapping::CLAMP ) ? FaceFlags::TEX_0_U_CLAMP  : 0)
     | ((tile.wrappingV & F3DEX2::Tex::Wrapping::MIRROR) ? FaceFlags::TEX_0_V_MIRROR : 0)
     | ((tile.wrappingV & F3DEX2::Tex::Wrapping::CLAMP ) ? FaceFlags::TEX_0_V_CLAMP  : 0)

     | ((tileNext.wrappingU & F3DEX2::Tex::Wrapping::MIRROR) ? FaceFlags::TEX_1_U_MIRROR : 0)
     | ((tileNext.wrappingU & F3DEX2::Tex::Wrapping::CLAMP ) ? FaceFlags::TEX_1_U_CLAMP  : 0)
     | ((tileNext.wrappingV & F3DEX2::Tex::Wrapping::MIRROR) ? FaceFlags::TEX_1_V_MIRROR : 0)
     | ((tileNext.wrappingV & F3DEX2::Tex::Wrapping::CLAMP ) ? FaceFlags::TEX_1_V_CLAMP  : 0)

     | ((geomMode & F3DEX2::FlagsGeomMode::TEXTURE_GEN_LINEAR) ? FaceFlags::UV_GEN_LINEAR : 0);

  vertGL.faceSettings[1] = 0; // gets set in the color combiner

  vertGL.faceCulling = 0.0f; // "0"=no-culling, ">0"=font, "<0"=back
  if(geomMode & F3DEX2::FlagsGeomMode::CULL_FRONT)vertGL.faceCulling = 1.0f;
  if(geomMode & F3DEX2::FlagsGeomMode::CULL_BACK)vertGL.faceCulling = -1.0f;

  vertGL.pos[0] = (s16) byteswap(vertN64.pos[0]);
  vertGL.pos[1] = (s16) byteswap(vertN64.pos[1]);
  vertGL.pos[2] = (s16) byteswap(vertN64.pos[2]);

  //printf_info("VI: %.4f %.4f %.4f", vertGL.pos[0], vertGL.pos[1], vertGL.pos[2])

  auto res = (mat * Math::Vec4{vertGL.pos, 1.0f});
  vertGL.pos = res.toVec3();

  u16 coordRawU = byteswap(vertN64.texcoord[0]);
  u16 coordRawV = byteswap(vertN64.texcoord[1]);

  Math::Vec2 uvF32{
    (f32)(s16)coordRawU,
    (f32)(s16)coordRawV
  };

  vertGL.uv = {
    tile.normalizeUV(uvF32),
    tileNext.normalizeUV(uvF32)
  };
  vertGL.uvEnd = {
    tile.uvEndNorm,
    tileNext.uvEndNorm,
  };

  if(F3DEX2::Mem::Reg::useLighting)
  {
    const auto &matNormal = F3DEX2::Mem::matrixStackNormal[F3DEX2::Mem::matrixStackIdx];
    auto faceNormOrg = Math::Vec3{
      (f32) ((s8) vertN64.color.rgba[0]) / 127.0f,
      (f32) ((s8) vertN64.color.rgba[1]) / 127.0f,
      (f32) ((s8) vertN64.color.rgba[2]) / 127.0f,
    };
    auto faceNorm = (matNormal * Math::Vec4{faceNormOrg, 1.0f}).toVec3().normalize();
    vertGL.colorA1 = F3DEX2::calcVertexLight(faceNorm);

    if(geomMode & F3DEX2::FlagsGeomMode::TEXTURE_GEN_LINEAR) {

      // UVs are generated, store normals as UV for later use in shader
      const auto x = faceNormOrg.x();
      const auto y = faceNormOrg.y();
      vertGL.uv[2] = vertGL.uv[0] = atan2f(y, x);
      vertGL.uv[3] = vertGL.uv[1] = atan2f(sqrtf(x*x+y*y), faceNormOrg.z());
    }

  } else {
    // temporary storage for vertexColor, overwritten in color-combiner
    vertGL.colorA1 = vertN64.color;
  }

  vertGL.textureIds[0] = tile.outputIndex;
  vertGL.textureIds[1] = tileNext.outputIndex;

  //printf_info("VE: %.4f %.4f %.4f", vertGL.pos[0], vertGL.pos[1], vertGL.pos[2])
}

namespace
{
  void emitVertex(const F3DEX2::VertexGL &vert, const Math::Vec3S8 &calcNormal)
  {
    auto &out = F3DEX2::Mem::outVertices[F3DEX2::Mem::outVerticesIdx];
    out = vert;
    out.normals = calcNormal;

    F3DEX2::Color::colorCombinerVertex(out);

    ++F3DEX2::Mem::outVerticesIdx;
    if(F3DEX2::Mem::outVerticesIdx >= F3DEX2::Mem::MAX_OUT_VERTEX_COUNT) {
      print_warn("Output vertex buffer full!");
      --F3DEX2::Mem::outVerticesIdx;
    }
  }
}

void F3DEX2::Geometry::emitTriangle(u32 v0, u32 v1, u32 v2)
{
  // Calculate real normals for face-culling
  auto lineAB = (Mem::vertexBuffer[v0].pos - Mem::vertexBuffer[v1].pos);
  auto lineAC = (Mem::vertexBuffer[v0].pos - Mem::vertexBuffer[v2].pos);
  auto normal = lineAB.cross(lineAC).normalize();
  auto normalS8 = Math::Vec3S8{normal};

  emitVertex(Mem::vertexBuffer[v0], normalS8);
  emitVertex(Mem::vertexBuffer[v1], normalS8);
  emitVertex(Mem::vertexBuffer[v2], normalS8);
}
