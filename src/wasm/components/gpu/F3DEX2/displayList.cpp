/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#include "displayList.h"
#include "commandNames.h"
#include "math/mat4.h"
#include "memoryBus.h"
#include "ram.h"
#include "commands.h"
#include "stdlib.h"
#include "memory.h"
#include "types/RGBA8.h"
#include "vertex.h"

namespace {
    constexpr u32 MAX_COMMANDS = 5000;
    constexpr u32 MAX_NOP_COUNT = 10; // abort after X consecutive NOPs

    constexpr u32 CODE_JUMP = 0xDE; // jump to other display-list
    constexpr u32 CODE_END = 0xDF;

    u32 addressStack[16]{0};
    u32 addressStackPos = 0;
}

u32 F3DEX2::parseDisplayList(u32 address) {
  address = resolveSegmentAddress(address);

  if (address >= RAM_SIZE) {
    printf_error("Trying to exec. DPL from non-RAM address (0x%08X)!, ignore", address);
    return 0;
  }

  u32 maxNOPs = MAX_NOP_COUNT;
  for (u32 i = 0; (i < MAX_COMMANDS) && maxNOPs; ++i) {
    u64 dword = RAM::read<u64>(address);
    u8 code = (dword >> (64 - 8));

    auto cmdFunc = Cmd::cmdTable[code];

    maxNOPs = (code == 0 || !cmdFunc) ? (maxNOPs-1) : MAX_NOP_COUNT;

    if (code == 4) {
      address = resolveSegmentAddress(static_cast<u32>(Mem::Reg::rdpHigh)) - 8;
      //printf_info("Branch to %08X (%08X)", address, static_cast<u32>(Mem::Reg::rdpHigh))
    } else
    if (cmdFunc) {
      printf_trace("DPL[%08X:%d] %02X: %08X-%08X (%s)", address, i, code, (u32)(dword>>32), (u32)dword, CODE_NAMES[code]);
      cmdFunc(dword);
    } else if (code == CODE_JUMP) {
      maxNOPs = MAX_NOP_COUNT;
      bool putOnStack = (dword & 0x00FF'0000'0000'0000) == 0;
      u32 newAddress = resolveSegmentAddress(static_cast<u32>(dword));

      if(newAddress == 0) {
        if(putOnStack) {
          address += 8; // skip NULL jump that returns
          continue;
        } else {
          break; // stop on NULL branches that don't return
        }
      }

      if (putOnStack) {
        addressStack[addressStackPos++] = address;
      }

      printf_trace("DPL[%08X:%d] %02X: jump [stack=%d] %08X [org: %08X] (%s)", address, i, code, putOnStack, newAddress, static_cast<u32>(dword), CODE_NAMES[code]);
      address = newAddress - 8;
    } else if (code == CODE_END) {
      if(addressStackPos == 0)break;

      printf_trace("DPL[%08X:%d] %02X: return %08X (%s)", address, i, code, addressStack[addressStackPos-1], CODE_NAMES[code]);
      address = addressStack[--addressStackPos];
    } else {
      printf_warn("???[%08X:%d] %02X: %08X-%08X (%s)", address, i, code, (u32)(dword>>32), (u32)dword, CODE_NAMES[code]);
    }

    address += 8;
  }

  if(maxNOPs == 0) {
    print_warn("Too many NOPs or errors, abort")
  }

  return Mem::outVerticesIdx * sizeof(VertexGL);
}

void F3DEX2::reset() {
  Mem::Reg::geomMode = 0;
  Mem::Reg::otherModeLow = 0;
  Mem::Reg::otherModeHigh = 0;

  Mem::Reg::useLighting = false;

  Mem::addressPAL = 2_KB;
  Mem::matrixStackIdx = 0;
  Mem::outVerticesIdx = 0;
  Mem::currentTextureIdx = 0;
  Mem::lightCount = 0;
  Mem::textureUUIDs.clear();

  Mem::matrixStack[0] = Math::Mat4();

  memset(Mem::Reg::colorCombine, 0, sizeof(Mem::Reg::colorCombine));
  memset(Mem::Reg::alphaCombine, 0, sizeof(Mem::Reg::alphaCombine));
  memset(Mem::segmentTable, 0, sizeof(Mem::segmentTable));
  memset(Mem::texMem, 0, sizeof(Mem::texMem));
  memset(addressStack, 0, sizeof(addressStack));
  addressStackPos = 0;

  Mem::Reg::fillColor = RGBA8::White();
  Mem::Reg::fogColor = RGBA8::White();
  Mem::Reg::blendColor = RGBA8::White();
  Mem::Reg::primitiveColor = RGBA8::White();
  Mem::Reg::envColor = RGBA8::White();
  Mem::Reg::primitiveLODFrac = 0.0f;

  //memset(Mem::vertexBuffer, 0, sizeof(Mem::vertexBuffer));
  //memset(Mem::outVertices, 0, sizeof(Mem::outVertices));

  memset(Mem::tiles, 0, sizeof(Mem::tiles));
  for(u32 i=0; i<Mem::TEXTURE_SLOTS; ++i) {
    Mem::tiles[i].tileNum = i;
  }
}

void F3DEX2::setSegmentAddress(u32 segment, u32 address) {
  if(segment < 16) {
    printf_trace("Map Segment[%d] = %08X", segment, address);
    Mem::segmentTable[segment] = address;
  }
}

void F3DEX2::syncSegmentAddress(u32 addressRAM) {
  for(u32 &v : Mem::segmentTable) {
    v = RAM::read<u32>(addressRAM);
    addressRAM += 4;
  }
}
