/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "types.h"
#include "F3DEX2/vertex.h"

namespace F3DEX2::Color
{
  void printRegister();
  void updateColorCombiner(); // maps the N64 values to internal ones
  void colorCombinerVertex(VertexGL &vert);

  namespace Idx
  {
    constexpr u32 A1 = 0;
    constexpr u32 B1 = 1;
    constexpr u32 C1 = 2;
    constexpr u32 D1 = 3;
    constexpr u32 A2 = 4;
    constexpr u32 B2 = 5;
    constexpr u32 C2 = 6;
    constexpr u32 D2 = 7;
  };
}