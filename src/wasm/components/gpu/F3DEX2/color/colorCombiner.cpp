/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "colorCombiner.h"
#include "F3DEX2/memory.h"
#include "F3DEX2/types/RGBA8.h"
#include "F3DEX2/types/faceSettingsGL.h"
#include "F3DEX2/types/flagsOtherModeHigh.h"
#include "F3DEX2/vertex.h"

namespace Reg = F3DEX2::Mem::Reg;
namespace FaceFlag = F3DEX2::GpuObjectFaceSettings;

namespace
{
  enum class CCMux: u8 {
      ZERO, ONE,
      COMBINED, TEXEL0, TEXEL1, PRIMITIVE, SHADE, ENVIRONMENT,
      CENTER, SCALE, NOISE, K4, COMBINED_ALPHA, TEXEL0_ALPHA, TEXEL1_ALPHA,
      PRIM_ALPHA, SHADE_ALPHA, ENV_ALPHA, LOD_FRACTION, PRIM_LOD_FRAC, K5
  };

  enum class ACMux: u8 {
    ZERO, ONE,
    COMBINED, LOD_FRACTION,
    TEXEL0, TEXEL1, PRIMITIVE, SHADE,
    ENVIRONMENT, PRIM_LOD_FRAC
  };

  const char* const CCMuxNames[] = {
    "0", "1",
    "COMBINED", "TEXEL0", "TEXEL1", "PRIMITIVE", "SHADE", "ENVIRONMENT",
    "CENTER", "SCALE", "NOISE", "K4", "COMBINED_ALPHA", "TEXEL0_ALPHA", "TEXEL1_ALPHA",
    "PRIM_ALPHA", "SHADE_ALPHA", "ENV_ALPHA", "LOD_FRACTION", "PRIM_LOD_FRAC", "K5"
  };

  const char* const ACMuxNames[] = {
    "0", "1",
    "COMBINED", "LOD_FRACTION",
    "TEXEL0", "TEXEL1", "PRIMITIVE", "SHADE",
    "ENVIRONMENT", "PRIM_LOD_FRAC"
  };

  constexpr CCMux PARAM_MAP_COLOR_A[0x20] = {
      /* 0x00 - 0x03 */ CCMux::COMBINED, CCMux::TEXEL0, CCMux::TEXEL1, CCMux::PRIMITIVE,
      /* 0x04 - 0x07 */ CCMux::SHADE, CCMux::ENVIRONMENT, CCMux::ONE, CCMux::NOISE,
      /* 0x08 - 0x0B */ CCMux::ZERO, /* Rest is ZERO */
  };
  constexpr CCMux PARAM_MAP_COLOR_B[0x20] = {
      /* 0x00 - 0x03 */ CCMux::COMBINED, CCMux::TEXEL0, CCMux::TEXEL1, CCMux::PRIMITIVE,
      /* 0x04 - 0x07 */ CCMux::SHADE, CCMux::ENVIRONMENT, CCMux::CENTER, CCMux::K4,
      /* 0x08 - 0x0B */ CCMux::ZERO, /* Rest is ZERO */
  };
  constexpr CCMux PARAM_MAP_COLOR_C[0x20] = {
      /* 0x00 - 0x03 */ CCMux::COMBINED, CCMux::TEXEL0, CCMux::TEXEL1, CCMux::PRIMITIVE,
      /* 0x04 - 0x07 */ CCMux::SHADE, CCMux::ENVIRONMENT, CCMux::SCALE, CCMux::COMBINED_ALPHA,
      /* 0x08 - 0x0B */ CCMux::TEXEL0_ALPHA, CCMux::TEXEL1_ALPHA, CCMux::PRIM_ALPHA, CCMux::SHADE_ALPHA,
      /* 0x0C - 0x0F */ CCMux::ENV_ALPHA, CCMux::LOD_FRACTION, CCMux::PRIM_LOD_FRAC, CCMux::K5,
      /* 0x10 - ...  */ CCMux::ZERO, /* Rest is ZERO */
  };
  constexpr CCMux PARAM_MAP_COLOR_D[0x20] = {
      /* 0x00 - 0x03 */ CCMux::COMBINED, CCMux::TEXEL0, CCMux::TEXEL1, CCMux::PRIMITIVE,
      /* 0x04 - 0x07 */ CCMux::SHADE, CCMux::ENVIRONMENT, CCMux::ONE, CCMux::ZERO, /* Rest is ZERO */
  };

  constexpr ACMux PARAM_MAP_ALPHA_ABD[0x20] = {
      /* 0x00 - 0x03 */ ACMux::COMBINED, ACMux::TEXEL0, ACMux::TEXEL1, ACMux::PRIMITIVE,
      /* 0x04 - 0x07 */ ACMux::SHADE, ACMux::ENVIRONMENT, ACMux::ONE, ACMux::ZERO, /* Rest is ZERO */
  };
  constexpr ACMux PARAM_MAP_ALPHA_C[0x20] = {
      /* 0x00 - 0x03 */ ACMux::LOD_FRACTION, ACMux::TEXEL0, ACMux::TEXEL1, ACMux::PRIMITIVE,
      /* 0x04 - 0x07 */ ACMux::SHADE, ACMux::ENVIRONMENT, ACMux::PRIM_LOD_FRAC, ACMux::ZERO, /* Rest is ZERO */
  };

  CCMux colorComb[8]{CCMux::TEXEL0};
  ACMux alphaComb[8]{ACMux::TEXEL0};

  u8 randValue = 0x42;

  F3DEX2::RGBA8 resolveColorValue(u32 idx, CCMux type, F3DEX2::VertexGL &vert, const F3DEX2::RGBA8 &colorVert)
  {
    u32 idxStage = idx % 4;
    u32 flagIdx = idx / 4;
    u32 texelAlpha = (type == CCMux::TEXEL0_ALPHA || type == CCMux::TEXEL1_ALPHA) ? 1 : 0;

    switch (type) {
      using enum CCMux;

      case SHADE: return colorVert; // vertex-color or calc. light
      case PRIMITIVE: return Reg::primitiveColor;
      case ENVIRONMENT: return Reg::envColor;
      case NOISE:
        randValue ^= (u8)(u32)vert.pos[0];
        return {randValue, randValue, randValue, 0xFF}; // TODO

      case PRIM_ALPHA: return F3DEX2::RGBA8::Scalar(Reg::primitiveColor.rgba[3]);
      case SHADE_ALPHA: return F3DEX2::RGBA8::Scalar(colorVert.rgba[3]);
      case ENV_ALPHA: return F3DEX2::RGBA8::Scalar(Reg::envColor.rgba[3]);

      case PRIM_LOD_FRAC: // unknown according to the wiki
      case LOD_FRACTION: return F3DEX2::RGBA8::Scalar(Reg::primitiveLODFrac);

      case TEXEL0:
      case TEXEL0_ALPHA:
        if(idxStage == 0)vert.faceSettings[flagIdx] |= FaceFlag::CCMUX_A1_FETCH | FaceFlag::CCMUX_A1_TEX_0 | (texelAlpha * FaceFlag::CCMUX_A1_ALPHA);
        if(idxStage == 1)vert.faceSettings[flagIdx] |= FaceFlag::CCMUX_B1_FETCH | FaceFlag::CCMUX_B1_TEX_0 | (texelAlpha * FaceFlag::CCMUX_B1_ALPHA);
        if(idxStage == 2)vert.faceSettings[flagIdx] |= FaceFlag::CCMUX_C1_FETCH | FaceFlag::CCMUX_C1_TEX_0 | (texelAlpha * FaceFlag::CCMUX_C1_ALPHA);
        if(idxStage == 3)vert.faceSettings[flagIdx] |= FaceFlag::CCMUX_D1_FETCH | FaceFlag::CCMUX_D1_TEX_0 | (texelAlpha * FaceFlag::CCMUX_D1_ALPHA);
        return {0xFF, 0x00, 0xFF, 0xFF};

      case TEXEL1:
      case TEXEL1_ALPHA:
        if(idxStage == 0)vert.faceSettings[flagIdx] |= FaceFlag::CCMUX_A1_FETCH | FaceFlag::CCMUX_A1_TEX_1 | (texelAlpha * FaceFlag::CCMUX_A1_ALPHA);
        if(idxStage == 1)vert.faceSettings[flagIdx] |= FaceFlag::CCMUX_B1_FETCH | FaceFlag::CCMUX_B1_TEX_1 | (texelAlpha * FaceFlag::CCMUX_B1_ALPHA);
        if(idxStage == 2)vert.faceSettings[flagIdx] |= FaceFlag::CCMUX_C1_FETCH | FaceFlag::CCMUX_C1_TEX_1 | (texelAlpha * FaceFlag::CCMUX_C1_ALPHA);
        if(idxStage == 3)vert.faceSettings[flagIdx] |= FaceFlag::CCMUX_D1_FETCH | FaceFlag::CCMUX_D1_TEX_1 | (texelAlpha * FaceFlag::CCMUX_D1_ALPHA);
        return {0x00, 0xFF, 0xFF, 0xFF};

      case COMBINED:
        if(idxStage == 0)vert.faceSettings[1] |= FaceFlag::CCMUX_A2_COMBINED;
        if(idxStage == 1)vert.faceSettings[1] |= FaceFlag::CCMUX_B2_COMBINED;
        if(idxStage == 2)vert.faceSettings[1] |= FaceFlag::CCMUX_C2_COMBINED;
        if(idxStage == 3)vert.faceSettings[1] |= FaceFlag::CCMUX_D2_COMBINED;
        return {0x00, 0xFF, 0xFF, 0xFF};

      case ZERO: return F3DEX2::RGBA8::Scalar(0);
      case ONE: return F3DEX2::RGBA8::White();

      default:
        printf_warn("Unhandled CCMUX: %s", CCMuxNames[(u32)type])
        return F3DEX2::RGBA8::White();
    }
  }

  u8 resolveAlphaValue(u32 idx, ACMux type, F3DEX2::VertexGL &vert, const F3DEX2::RGBA8 &colorVert)
  {
    u32 idxStage = idx % 4;
    u32 flagIdx = idx / 4;

    switch (type) {
      using enum ACMux;

      case SHADE: return colorVert.rgba[3]; // vertex-color or calc. light
      case PRIMITIVE: return Reg::primitiveColor.rgba[3];
      case ENVIRONMENT: return Reg::envColor.rgba[3];

      case PRIM_LOD_FRAC: // unknown according to the wiki
      case LOD_FRACTION: return Reg::primitiveLODFrac;

      case TEXEL0:
        if(idxStage == 0)vert.faceSettings[flagIdx] |= FaceFlag::ACMUX_A1_FETCH | FaceFlag::ACMUX_A1_TEX_0;
        if(idxStage == 1)vert.faceSettings[flagIdx] |= FaceFlag::ACMUX_B1_FETCH | FaceFlag::ACMUX_B1_TEX_0;
        if(idxStage == 2)vert.faceSettings[flagIdx] |= FaceFlag::ACMUX_C1_FETCH | FaceFlag::ACMUX_C1_TEX_0;
        if(idxStage == 3)vert.faceSettings[flagIdx] |= FaceFlag::ACMUX_D1_FETCH | FaceFlag::ACMUX_D1_TEX_0;
        return 0xFF;

      case TEXEL1:
        if(idxStage == 0)vert.faceSettings[flagIdx] |= FaceFlag::ACMUX_A1_FETCH | FaceFlag::ACMUX_A1_TEX_1;
        if(idxStage == 1)vert.faceSettings[flagIdx] |= FaceFlag::ACMUX_B1_FETCH | FaceFlag::ACMUX_B1_TEX_1;
        if(idxStage == 2)vert.faceSettings[flagIdx] |= FaceFlag::ACMUX_C1_FETCH | FaceFlag::ACMUX_C1_TEX_1;
        if(idxStage == 3)vert.faceSettings[flagIdx] |= FaceFlag::ACMUX_D1_FETCH | FaceFlag::ACMUX_D1_TEX_1;
        return 0xFF;

      case COMBINED:
        if(idxStage == 0)vert.faceSettings[1] |= FaceFlag::ACMUX_A2_COMBINED;
        if(idxStage == 1)vert.faceSettings[1] |= FaceFlag::ACMUX_B2_COMBINED;
        if(idxStage == 2)vert.faceSettings[1] |= FaceFlag::ACMUX_C2_COMBINED;
        if(idxStage == 3)vert.faceSettings[1] |= FaceFlag::ACMUX_D2_COMBINED;
        return 0xFF;

      case ZERO: return 0x00;
      case ONE: return 0xFF;

      default:
        printf_warn("Unhandled ACMUX: %s", ACMuxNames[(u32)type])
        return 0xFF;
    }
  }
}

void F3DEX2::Color::colorCombinerVertex(VertexGL &vert)
{
  auto colorVert = vert.colorA1; // backup color

  vert.colorA1 = resolveColorValue(0, colorComb[0], vert, colorVert);
  vert.colorB1 = resolveColorValue(1, colorComb[1], vert, colorVert);
  vert.colorC1 = resolveColorValue(2, colorComb[2], vert, colorVert);
  vert.colorD1 = resolveColorValue(3, colorComb[3], vert, colorVert);

  vert.colorA1.rgba[3] = resolveAlphaValue(0, alphaComb[0], vert, colorVert);
  vert.colorB1.rgba[3] = resolveAlphaValue(1, alphaComb[1], vert, colorVert);
  vert.colorC1.rgba[3] = resolveAlphaValue(2, alphaComb[2], vert, colorVert);
  vert.colorD1.rgba[3] = resolveAlphaValue(3, alphaComb[3], vert, colorVert);

  vert.colorA2 = resolveColorValue(4, colorComb[4], vert, colorVert);
  vert.colorB2 = resolveColorValue(5, colorComb[5], vert, colorVert);
  vert.colorC2 = resolveColorValue(6, colorComb[6], vert, colorVert);
  vert.colorD2 = resolveColorValue(7, colorComb[7], vert, colorVert);

  vert.colorA2.rgba[3] = resolveAlphaValue(4, alphaComb[4], vert, colorVert);
  vert.colorB2.rgba[3] = resolveAlphaValue(5, alphaComb[5], vert, colorVert);
  vert.colorC2.rgba[3] = resolveAlphaValue(6, alphaComb[6], vert, colorVert);
  vert.colorD2.rgba[3] = resolveAlphaValue(7, alphaComb[7], vert, colorVert);
}

namespace
{
  void printSingleRegister(u32 index)
  {
    using namespace F3DEX2::Color::Idx;
    u32 cycles = (F3DEX2::Mem::Reg::otherModeHigh & F3DEX2::FlagsOtherModeH::CYCLE_TYPE_2) ? 2 : 1;
    u32 offset = index * 4;

    printf_info("CCMUX C[%d/%d]: (%s - %s) * %s + %s | A: (%s - %s) * %s + %s",
      index, cycles,
      CCMuxNames[(u8)colorComb[offset + A1]], CCMuxNames[(u8)colorComb[offset + B1]],
      CCMuxNames[(u8)colorComb[offset + C1]], CCMuxNames[(u8)colorComb[offset + D1]],
      ACMuxNames[(u8)alphaComb[offset + A1]], ACMuxNames[(u8)alphaComb[offset + B1]],
      ACMuxNames[(u8)alphaComb[offset + C1]], ACMuxNames[(u8)alphaComb[offset + D1]]
    )
  }
}

void F3DEX2::Color::printRegister()
{
  if(Mem::Reg::otherModeHigh & F3DEX2::FlagsOtherModeH::CYCLE_TYPE_COPY) {
    print_info("ColorMix: inactive (copy mode)")
    return;
  }

  bool is2Cycle = Mem::Reg::otherModeHigh & F3DEX2::FlagsOtherModeH::CYCLE_TYPE_2;
  printSingleRegister(0);
  //if(Mem::Reg::otherModeHigh & F3DEX2::FlagsOtherModeH::CYCLE_TYPE_2)
  {
    printSingleRegister(1);
  }
}

namespace {
  template<const CCMux* MAP>
  inline void updateColorCombEntry(u32 idx) {
    /*if(Reg::colorCombine[idx] > 0x1F) {
      printf_warn("CCMUX: invalid value: [%d] = %d", idx, Reg::colorCombine[idx])
    }*/
    colorComb[idx] = MAP[Reg::colorCombine[idx] & 0x1Fu];
  }

  template<const ACMux* MAP>
  inline void updateAlphaCombEntry(u32 idx) {
    /*if(Reg::alphaCombine[idx] > 0x1F) {
      printf_warn("ACMUX: invalid value: [%d] = %d", idx, Reg::alphaCombine[idx])
    }*/
    alphaComb[idx] = MAP[Reg::alphaCombine[idx] & 0x1Fu];
  }
}

void F3DEX2::Color::updateColorCombiner() {
  // Color
  updateColorCombEntry<PARAM_MAP_COLOR_A>(Idx::A1);
  updateColorCombEntry<PARAM_MAP_COLOR_B>(Idx::B1);
  updateColorCombEntry<PARAM_MAP_COLOR_C>(Idx::C1);
  updateColorCombEntry<PARAM_MAP_COLOR_D>(Idx::D1);

  updateColorCombEntry<PARAM_MAP_COLOR_A>(Idx::A2);
  updateColorCombEntry<PARAM_MAP_COLOR_B>(Idx::B2);
  updateColorCombEntry<PARAM_MAP_COLOR_C>(Idx::C2);
  updateColorCombEntry<PARAM_MAP_COLOR_D>(Idx::D2);

  // Alpha
  updateAlphaCombEntry<PARAM_MAP_ALPHA_ABD>(Idx::A1);
  updateAlphaCombEntry<PARAM_MAP_ALPHA_ABD>(Idx::B1);
  updateAlphaCombEntry<PARAM_MAP_ALPHA_C  >(Idx::C1);
  updateAlphaCombEntry<PARAM_MAP_ALPHA_ABD>(Idx::D1);

  updateAlphaCombEntry<PARAM_MAP_ALPHA_ABD>(Idx::A2);
  updateAlphaCombEntry<PARAM_MAP_ALPHA_ABD>(Idx::B2);
  updateAlphaCombEntry<PARAM_MAP_ALPHA_C  >(Idx::C2);
  updateAlphaCombEntry<PARAM_MAP_ALPHA_ABD>(Idx::D2);

  // check if we need two textures from now on
  F3DEX2::Mem::needsSecondTex = false;
  for(u32 i=0; i<8; ++i) {
    if(colorComb[i] == CCMux::TEXEL1 || colorComb[i] == CCMux::TEXEL1_ALPHA || alphaComb[i] == ACMux::TEXEL1) {
      Mem::needsSecondTex = true;
      break;
    }
  }

  printRegister();
}


