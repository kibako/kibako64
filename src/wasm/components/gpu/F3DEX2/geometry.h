/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "types.h"
#include "vertex.h"
#include "math/mat4.h"

namespace F3DEX2::Geometry
{
  void parseVertex(VertexGL &vertGL, const VertexN64 &vertN64, u32 activeTileIdx);
  void emitTriangle(u32 v0, u32 v1, u32 v2);
}