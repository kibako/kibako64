/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "map.h"
#include "texture/textureTile.h"
#include "types.h"
#include "vertex.h"
#include "math/mat4.h"
#include "F3DEX2/types/RGBA8.h"
#include "F3DEX2/lighting/light.h"

namespace F3DEX2::Mem
{
  // N64 Memory
  constexpr u32 TEXTURE_SLOTS = 8;
  constexpr u32 VERTEX_SLOTS = 32;
  constexpr u32 MATRIX_SLOTS = 64;
  constexpr u32 LIGHT_SLOTS = 8;
  constexpr u32 TMEM_SIZE = 4096;
  constexpr u32 MAX_TEXTURE_SIZE = 1024;

  namespace Reg {
      inline u32 geomMode{0};
      inline u32 otherModeLow{0};
      inline u32 otherModeHigh{0};

      inline u32 rdpLow{0};
      inline u32 rdpHigh{0};

      inline u8 colorCombine[8]{0};
      inline u8 alphaCombine[8]{0};

      inline RGBA8 fillColor{};
      inline RGBA8 fogColor{};
      inline RGBA8 blendColor{};
      inline RGBA8 primitiveColor{};
      inline RGBA8 envColor{};
      inline u8 primitiveLODFrac{};

      // extracted from registers:
      inline bool useLighting{false};
  }

  inline u32 segmentTable[16]{0};

  struct {
    u32 addressRam{0};
    u32 width{0};
    u32 pixelBitSize{0};
    Tex::ColorFormat format{};
  } currentImage{};

  // https://www.moria.us/blog/2020/11/n64-part20-tmem-format-and-mip-maps
  alignas(16)
  inline u8 texMem[TMEM_SIZE]{0};
  inline u32 addressPAL{0};

  inline Tex::Tile tiles[TEXTURE_SLOTS]{};
  inline VertexGL vertexBuffer[VERTEX_SLOTS]{};
  inline Math::Mat4 matrixStack[MATRIX_SLOTS]{};
  inline Math::Mat4 matrixStackNormal[MATRIX_SLOTS]{}; // converted matrix for normals
  inline u32 matrixStackIdx = 0;

  inline Light lights[LIGHT_SLOTS];
  inline u32 lightCount{0};

  // Emulator Memory
  constexpr u32 MAX_OUT_VERTEX_COUNT = 4096;
  inline VertexGL outVertices[MAX_OUT_VERTEX_COUNT]{};
  inline u32 outVerticesIdx = 0;
  inline u8 outTextureRGBA[MAX_TEXTURE_SIZE * MAX_TEXTURE_SIZE * 4]{};
  inline u32 currentTextureIdx = 0;
  inline Utils::Map<u32, u32, 128> textureUUIDs{};
  inline bool32 needsSecondTex{false}; // set to true if the color combiner needs 2 textures
}