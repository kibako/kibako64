/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "types.h"

namespace F3DEX2
{
  /**
   * Flags for Faces in the output buffer for OpenGL
   */
  namespace GpuObjectFaceSettings
  {
    constexpr u32 NONE = 0;

    // Flags for index 0

      // UV settings for first texture
      constexpr u32 TEX_0_U_CLAMP  = (1 << 0);
      constexpr u32 TEX_0_U_MIRROR = (1 << 1);
      constexpr u32 TEX_0_V_CLAMP  = (1 << 2);
      constexpr u32 TEX_0_V_MIRROR = (1 << 3);

      // UV settings for second texture
      constexpr u32 TEX_1_U_CLAMP  = (1 << 4);
      constexpr u32 TEX_1_U_MIRROR = (1 << 5);
      constexpr u32 TEX_1_V_CLAMP  = (1 << 6);
      constexpr u32 TEX_1_V_MIRROR = (1 << 7);

    // Flags for index 1

      constexpr u32 CCMUX_A2_COMBINED = (1 << 0);
      constexpr u32 CCMUX_B2_COMBINED = (1 << 1);
      constexpr u32 CCMUX_C2_COMBINED = (1 << 2);
      constexpr u32 CCMUX_D2_COMBINED = (1 << 3);

      constexpr u32 ACMUX_A2_COMBINED = (1 << 4);
      constexpr u32 ACMUX_B2_COMBINED = (1 << 5);
      constexpr u32 ACMUX_C2_COMBINED = (1 << 6);
      constexpr u32 ACMUX_D2_COMBINED = (1 << 7);


    // Color/Alpha Mixer settings
    constexpr u32 CCMUX_A1_FETCH = (1 << 8); // overwrite A1 in shader with...
    constexpr u32 CCMUX_A1_TEX_0 = 0;
    constexpr u32 CCMUX_A1_TEX_1 = (1 << 9); // ... texture[0] or texture[1]...
    constexpr u32 CCMUX_A1_ALPHA = (1 << 10); // ...alpha if set, or color if not

    constexpr u32 CCMUX_B1_FETCH = (1 << 11); // overwrite B1 in shader with...
    constexpr u32 CCMUX_B1_TEX_0 = 0;
    constexpr u32 CCMUX_B1_TEX_1 = (1 << 12); // ... texture[0] or texture[1]...
    constexpr u32 CCMUX_B1_ALPHA = (1 << 13); // ...alpha if set, or color if not

    constexpr u32 CCMUX_C1_FETCH = (1 << 14); // overwrite C1 in shader with...
    constexpr u32 CCMUX_C1_TEX_0 = 0;
    constexpr u32 CCMUX_C1_TEX_1 = (1 << 15); // ... texture[0] or texture[1]...
    constexpr u32 CCMUX_C1_ALPHA = (1 << 16); // ...alpha if set, or color if not

    constexpr u32 CCMUX_D1_FETCH = (1 << 17); // overwrite C1 in shader with...
    constexpr u32 CCMUX_D1_TEX_0 = 0;
    constexpr u32 CCMUX_D1_TEX_1 = (1 << 18); // ... texture[0] or texture[1]...
    constexpr u32 CCMUX_D1_ALPHA = (1 << 19); // ...alpha if set, or color if not

    constexpr u32 ACMUX_A1_FETCH = (1 << 20); // overwrite A1 in shader with alpha of...
    constexpr u32 ACMUX_A1_TEX_0 = 0;
    constexpr u32 ACMUX_A1_TEX_1 = (1 << 21); // ... texture[0] or texture[1]...

    constexpr u32 ACMUX_B1_FETCH = (1 << 22); // overwrite B1 in shader with alpha of...
    constexpr u32 ACMUX_B1_TEX_0 = 0;
    constexpr u32 ACMUX_B1_TEX_1 = (1 << 23); // ... texture[0] or texture[1]...

    constexpr u32 ACMUX_C1_FETCH = (1 << 24); // overwrite C1 in shader with alpha of...
    constexpr u32 ACMUX_C1_TEX_0 = 0;
    constexpr u32 ACMUX_C1_TEX_1 = (1 << 25); // ... texture[0] or texture[1]...

    constexpr u32 ACMUX_D1_FETCH = (1 << 26); // overwrite B1 in shader with alpha of...
    constexpr u32 ACMUX_D1_TEX_0 = 0;
    constexpr u32 ACMUX_D1_TEX_1 = (1 << 27); // ... texture[0] or texture[1]...

    constexpr u32 UV_GEN_LINEAR = (1 << 28); // auto. generates UV coords (e.g. rupees)
  };
}