/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "types.h"

namespace F3DEX2::FlagsOtherModeH
{
  constexpr u32 PALETTE_ACTIVE = (1 << 15);
  constexpr u32 PALETTE_IA16   = (1 << 14); // if off, RGBA16

  constexpr u32 CYCLE_TYPE_COPY = (1 << 21);
  constexpr u32 CYCLE_TYPE_2    = (1 << 20);
}