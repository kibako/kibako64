/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "types.h"

namespace F3DEX2::FlagsGeomMode
{
  constexpr u32 ZBUFFER            = (1 << 0);
  constexpr u32 SHADE              = (1 << 2);
  constexpr u32 CULL_FRONT         = (1 << 9);
  constexpr u32 CULL_BACK          = (1 << 10);
  constexpr u32 FOG                = (1 << 16);
  constexpr u32 LIGHTING           = (1 << 17);
  constexpr u32 TEXTURE_GEN        = (1 << 18);
  constexpr u32 TEXTURE_GEN_LINEAR = (1 << 19);
  constexpr u32 SHADING_SMOOTH     = (1 << 21);
  constexpr u32 CLIPPING           = (1 << 23);
}