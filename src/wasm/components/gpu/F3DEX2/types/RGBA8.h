/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "math/vec4.h"
#include "stdlib.h"
#include "types.h"

namespace F3DEX2
{
  struct RGBA8
  {
    u8 rgba[4]{0};

    constexpr static RGBA8 parse(u32 val) {
      return {
        static_cast<u8>((val >> 24) & 0xFF),
        static_cast<u8>((val >> 16) & 0xFF),
        static_cast<u8>((val >>  8) & 0xFF),
        static_cast<u8>((val >>  0) & 0xFF),
      };
    }

    constexpr static RGBA8 Scalar(u8 val) {
      return {val, val, val, val};
    }

    constexpr static RGBA8 White() {
      return {0xFF, 0xFF, 0xFF, 0xFF};
    }

    void setMono(u8 val) {
      rgba[0] = rgba[1] = rgba[2] = val;
    }

    void set(u8 r, u8 g, u8 b, u8 a = 0xFF) {
      rgba[0] = r; rgba[1] = g;
      rgba[2] = b; rgba[3] = a;
    }

    void clear() {
      rgba[0] = rgba[1] = rgba[2] = rgba[3] = 0;
    }

    RGBA8 operator*(const RGBA8 &b) const
    {
      return {
        static_cast<u8>(((u32)rgba[0] * (u32)b.rgba[0]) / 255),
        static_cast<u8>(((u32)rgba[1] * (u32)b.rgba[1]) / 255),
        static_cast<u8>(((u32)rgba[2] * (u32)b.rgba[2]) / 255),
        static_cast<u8>(((u32)rgba[2] * (u32)b.rgba[3]) / 255),
      };
    }

    [[nodiscard]] Math::Vec4 toVec4() const
    {
      return {
        static_cast<f32>(rgba[0]) / 255.0f,
        static_cast<f32>(rgba[1]) / 255.0f,
        static_cast<f32>(rgba[2]) / 255.0f,
        static_cast<f32>(rgba[2]) / 255.0f,
      };
    }

    constexpr static RGBA8 fromVec4(const Math::Vec4 &vec) {
      return {
        static_cast<u8>(clampf(vec[0], 0.0f, 1.0f) * 255.0f),
        static_cast<u8>(clampf(vec[1], 0.0f, 1.0f) * 255.0f),
        static_cast<u8>(clampf(vec[2], 0.0f, 1.0f) * 255.0f),
        static_cast<u8>(clampf(vec[3], 0.0f, 1.0f) * 255.0f),
      };
    }
  };
}