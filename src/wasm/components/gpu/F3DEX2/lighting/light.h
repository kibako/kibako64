/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "F3DEX2/types/RGBA8.h"

namespace F3DEX2
{
  struct Light
  {
    RGBA8 color;
    RGBA8 color2; // ??
    s8 direction[3]{0};
    u8 padding01{0};
  };

  static_assert(sizeof(Light) == 12, "Wrong Light size!");

  RGBA8 calcVertexLight(const Math::Vec3 &normal);
}