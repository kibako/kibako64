/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "light.h"
#include "F3DEX2/memory.h"
#include "F3DEX2/types/RGBA8.h"
#include "math/vec4.h"

F3DEX2::RGBA8 F3DEX2::calcVertexLight(const Math::Vec3 &normal) {
  auto color = Mem::lights[Mem::lightCount].color.toVec4(); // ambient light

  /*printf_info("Light[ambient]: color=%02X%02X%02X, dir=%f %f %f",
    Mem::lights[Mem::lightCount].color.rgba[0], Mem::lights[Mem::lightCount].color.rgba[1], Mem::lights[Mem::lightCount].color.rgba[2]
  )*/

  for (u32 l = 0; l < Mem::lightCount; ++l) {
    const auto &light = Mem::lights[l];
    Math::Vec3 dir{
      (f32) light.direction[0] / 127.0f,
      (f32) light.direction[1] / 127.0f,
      (f32) light.direction[2] / 127.0f,
    };

    f32 lightFactor = clampf(normal.dot(dir), 0.0f, 1.0f);
    color += light.color.toVec4() * lightFactor;

    /*printf_info("Light[%d]: color=%02X%02X%02X, dir=%f %f %f, norm=%f %f %f", l,
      light.color.rgba[0], light.color.rgba[1], light.color.rgba[2],
      dir[0], dir[1], dir[2],
      normal[0], normal[1], normal[2]
    )*/
  }

  return RGBA8::fromVec4(color);
}
