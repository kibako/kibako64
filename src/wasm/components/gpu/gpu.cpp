/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "gpu.h"
#include "../../utils/logging.h"
#include "../../globals.h"

// @TODO read this dynamically
u32 height = 240;
u32 bitdepth =  4;

FramebufferInfo fbInfo = {nullptr, 0,0,0};

using VIReg = VIRegisterOffset;

u32 GPU::getFramebufferAddress()
{
  return memBus.read<u32>(0x0440'0004);
}

void GPU::render()
{
  print_info("Start rendering Frame...");

  u32 framebufferAddress = getFramebufferAddress();
  u32 width = memBus.read<u32>(0x0440'0008);
  
  u32 framebufferSize = (width * height * bitdepth);
  
  u32 status = memBus.read<u32>(0x0440'0000);

  printf_info("Framebuffer: @ 0x%08X - 0x%08X", framebufferAddress, framebufferAddress + framebufferSize);
  printf_info("Line-Width: %d", width);
  printf_info("Pixel-Size: %d", status & 0b11);
}

FramebufferInfo* GPU::getFramebufferInfo()
{  
  auto &vi = memBus.videoInterfaceReg;

  fbInfo.framebuffer = (void*)getFramebufferAddress();
  // Calculate the bounding positions.

  fbInfo.rect.minX = vi.read<u32>((u32)VIReg::VIDEO_HORIZONTAL) >> 16 & 0x03FF;
  fbInfo.rect.maxX = vi.read<u32>((u32)VIReg::VIDEO_HORIZONTAL)       & 0x03FF;
  fbInfo.rect.minY = vi.read<u32>((u32)VIReg::VIDEO_VERTICAL) >> 16   & 0x03FF;
  fbInfo.rect.maxY = vi.read<u32>((u32)VIReg::VIDEO_VERTICAL)         & 0x03FF;

  f32 hcoeff = (f32)(vi.read<u32>((u32)VIReg::SCALE_X) & 0x0FFF) / (1 << 10);
  f32 vcoeff = (f32)(vi.read<u32>((u32)VIReg::SCALE_Y) & 0x0FFF) / (1 << 10);

// Calculate the height and width of the frame.
  fbInfo.resolution[0] = ((fbInfo.rect.maxX - fbInfo.rect.minX)) * hcoeff;
  fbInfo.resolution[1] = ((fbInfo.rect.maxY - fbInfo.rect.minY) >> 1) * vcoeff;
  fbInfo.horizontalSkip = vi.read<u32>((u32)VIReg::PIXEL_WIDTH) - fbInfo.resolution[0];
  fbInfo.pixelFormat = vi.read<u32>((u32)VIReg::STATUS) & 0b11;

  return &fbInfo;
}
