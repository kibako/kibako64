/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "../main.h"
#include "interrupt.h"
#include "timing.h"

void printRcpInterrupts(const char* name, u32 mask)
{
  printf_info("%s: %s %s %s %s %s %s", name,
    mask & (1 << 0) ? "SP" : "-",
    mask & (1 << 1) ? "SI" : "-",
    mask & (1 << 2) ? "AI" : "-",
    mask & (1 << 3) ? "VI" : "-",
    mask & (1 << 4) ? "PI" : "-",
    mask & (1 << 5) ? "DP" : "-"
  ); 
}

void printPendingInterrupts(const char* name, u32 mask)
{
  printf_info("%s[0-7]: %s %s %s %s %s %s %s %s", name,
    mask & (1 << 0) ? "?"   : "-",
    mask & (1 << 1) ? "?"   : "-",
    mask & (1 << 2) ? "TYP0"   : "-",
    mask & (1 << 3) ? "TYP1"   : "-",
    mask & (1 << 4) ? "TYP2" : "-",
    mask & (1 << 5) ? "TYP3" : "-",
    mask & (1 << 6) ? "TYP4" : "-",
    mask & (1 << 7) ? "TYP5" : "-"
  ); 

  printf_info("%s[8-15]: %s %s %s %s %s %s %s %s", name,
    mask & (1 <<  8) ? "SW0"   : "-",
    mask & (1 <<  9) ? "SW1"   : "-",
    mask & (1 << 10) ? "RCP"   : "-",
    mask & (1 << 11) ? "ROM"   : "-",
    mask & (1 << 12) ? "RESET" : "-",
    mask & (1 << 13) ? "RDB_R" : "-",
    mask & (1 << 14) ? "RDB_W" : "-",
    mask & (1 << 15) ? "TIMER" : "-"
  );
  printf_info("%s[24-31]: %s %s %s %s %s %s %s %s", name,
    mask & (1 << 24) ? "?"     : "-",
    mask & (1 << 25) ? "?"     : "-",
    mask & (1 << 26) ? "?"     : "-",
    mask & (1 << 27) ? "?"     : "-",
    mask & (1 << 28) ? "COP0"  : "-",
    mask & (1 << 29) ? "COP1"  : "-",
    mask & (1 << 30) ? "?"     : "-",
    mask & (1 << 31) ? "DELAY" : "-"
  );
}

void Interrupt::Handler::interrupt(Interrupt::Type interrupt, RcpType rcpType)
{
  printPendingInterrupts("Try to interupt with type: ", (u32)interrupt);
  printRcpInterrupts("With RCP-Type: ", (u32)rcpType);

  cpu.cop0.printStatus(cpu.cop0.getRegisterU32(Cop0Register::STATUS));
  logState();

  if(interrupt == Type::RCP) 
  {
    if(rcpType == RcpType::NONE) {
      // If you see this, it's a programming error, not a runtime-error.
      print_error("Interrupting with 'RCP' without an RCP-Type!");
    }

    // Only set the RCP interrupt and its type in MI if the MI interrupt mask
    // would have at least one active interrupt after enabling it.
    memBus.mipsInterfaceReg.setInterruptBit(rcpType);
    if(memBus.mipsInterfaceReg.getMaskedInterruptBits() == 0) {
      //memBus.mipsInterfaceReg.clearInterruptBit(rcpType);
      return;
    }
  }

  cpu.cop0.setPendingInterrupt(interrupt);
  printPendingInterrupts("Pending-Interrupts", cpu.cop0.getRegisterU32(Cop0Register::CAUSE));
}

void Interrupt::Handler::clearInterrupt(Type interrupt)
{
  cpu.cop0.clearPendingInterrupt(interrupt);
}

void Interrupt::Handler::clearRcpInterrupt(RcpType interrupt)
{
  memBus.mipsInterfaceReg.clearInterruptBit(interrupt);
  
  if(memBus.mipsInterfaceReg.getMaskedInterruptBits() == 0) {
    clearInterrupt(Interrupt::Type::RCP);
  }
}

void Interrupt::Handler::handlePending()
{
  if(!hasPendingInterrupt()) {
    return;
  }

  // Prevent interrupts if globally disabled
  if(!cpu.cop0.getStatusFlag(Cop0StatusFlag::INTERRUPT_ENABLE)) {
    return;
  }

  // Prevent interrupt within an interrupt
  if(cpu.cop0.getStatusFlag(Cop0StatusFlag::EXCEPTION_LEVEL)) {
    return;
  }

  cpu.cop0.enableStatusFlag(Cop0StatusFlag::EXCEPTION_LEVEL);
  cpu.cop0.setRegisterU32(Cop0Register::EXCEPTION_PC, cpu.getPc());

  // @TODO: check when this needs to be used instead
  //cpu.cop0.enableStatusFlag(Cop0StatusFlag::ERROR_LEVEL);
  //cpu.cop0.disableStatusFlag(Cop0StatusFlag::EXCEPTION_LEVEL);
  //cpu.cop0.setRegisterU32(Cop0Register::ERROR_EPC, cpu.getPc());

  cpu.setPc(Interrupt::Address::DEFAULT);

  printf_info("Interrupted to @0x%08X", cpu.getPc());
  printPendingInterrupts("Pending-Interrupts", cpu.cop0.getRegisterU32(Cop0Register::CAUSE));

  debugger_hook_event(DebuggerEvent::INTERRUPT);
}

bool32 Interrupt::Handler::hasPendingInterrupt()
{
  return cpu.cop0.hasPendingInterrupt();
}

void Interrupt::Handler::logState()
{
  u32 cop0Status     = cpu.cop0.getRegisterU32(Cop0Register::CAUSE);
  u32 cop0StatusMask = cpu.cop0.getRegisterU32(Cop0Register::STATUS);

  u32 interruptRcp     = memBus.read<u32>(0x04300008);
  u32 interruptRcpMask = memBus.read<u32>(0x0430000C);

  printPendingInterrupts("Interrupt:", (u32)cop0Status);
  printPendingInterrupts("Int. Mask:", (u32)cop0StatusMask);
  printRcpInterrupts("Interrupt RCP: ", interruptRcp);
  printRcpInterrupts("Int. RCP Mask: ", interruptRcpMask);

  printf_info("Count/Compare: %08X / %08X", 
    cpu.cop0.getRegisterU32(Cop0Register::COUNT),
    cpu.cop0.getRegisterU32(Cop0Register::COMPARE)
  );

  printf_info("VI-Scanline: %08X / %08X", 
    memBus.videoInterfaceReg.read<u32>((u32)VIRegisterOffset::VLINE_CURRENT),
    memBus.videoInterfaceReg.read<u32>((u32)VIRegisterOffset::VLINE_INTERRUPT)
  );  

  Timing::logState();
}