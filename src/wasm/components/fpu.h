/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../types.h"
#include "fpuStatusReg.h"

#define FPU_REGISTER_COUNT 32
#define FPU_CONTROL_REGISTER_COUNT 32
#define FPU_CONTROL_REG_FCSR 31

class MipsFPU
{
  private:
    f64 registers[FPU_REGISTER_COUNT] = {0};
    u32 controlRegs[FPU_CONTROL_REGISTER_COUNT] = {0};

    [[nodiscard]] u32 get32BitRegIndex(u32 reg) const {
      if(!fullMode) {
        // N64: emulates undefined behaviour, see: https://n64brew.dev/wiki/COP1
        u32 newReg = reg >> 1;
        newReg <<= 2;
        newReg += (reg & 1) ? 1 : 0;
        return newReg;
      }
      return reg*2;
    }

    void checkForExceptions();

  public:
    bool32 fullMode = true;

    void clear();
    void logState();

    template<typename T>
    void setRegister(s32 reg, T value)
    {
      if constexpr (sizeof(T) == sizeof(u32)) {
        ((T*)registers)[get32BitRegIndex(reg)] = value;
      } else {
        ((T*)registers)[reg] = value;

        if(!fullMode && (reg&1)) { // N64: undefined behaviour, see: https://n64brew.dev/wiki/COP1
          ((T*)registers)[reg-1] = value;
        }
      }
    }

    template<typename T> 
    T getRegister(u32 reg)
    {
      if constexpr (sizeof(T) == sizeof(u32)) {
        return ((T*)registers)[get32BitRegIndex(reg)];
      } else {
        return ((T*)registers)[reg];
      }
    }

    void setControlRegister(u32 reg, u32 value) {
      controlRegs[reg] = value;
      checkForExceptions();
    }

    u32 getControlRegister(u32 reg) {
      return controlRegs[reg];
    }

    void setStatusFlag(FPUStatusReg flag, bool active) {
      if(active) {
        controlRegs[FPU_CONTROL_REG_FCSR] |= (u64)flag;
      } else {
        controlRegs[FPU_CONTROL_REG_FCSR] &= ~(u64)flag;
      }
      checkForExceptions();
    }

    bool getStatusFlag(FPUStatusReg flag) {
      return (controlRegs[FPU_CONTROL_REG_FCSR] & (u32)flag) != 0;
    }

    inline void setCompare(bool newVal) {
      setStatusFlag(FPUStatusReg::FCC_COMPARE, newVal);
    }

    template<typename DST, typename SRC> 
    void convertRegister(s32 regDst, s32 regSrc) 
    {
      setRegister<DST>(regDst, getRegister<SRC>(regSrc));
    }

    template<typename T> 
    T opAddRegister(s32 regA, s32 regB) {
      return getRegister<T>(regA) + getRegister<T>(regB);
    }

    template<typename T> 
    T opSubtractRegister(s32 regA, s32 regB) {
      return getRegister<T>(regA) - getRegister<T>(regB);
    }

    template<typename T> 
    T opMultiplyRegister(s32 regA, s32 regB) {
      return getRegister<T>(regA) * getRegister<T>(regB);
    }

    template<typename T> 
    T opDivideRegister(s32 regA, s32 regB) {
      return getRegister<T>(regA) / getRegister<T>(regB);
    }
};
