/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "cache.h"
#include "main.h"
#include "components/cpuRegisters.h"
#include "instructions/instructions.h"
#include "components/memory/memoryBus.h"
#include "components/system.h"
#include "components/timing.h"
#include "disassembler.h"
#include "stdlib/stdlib.h"

extern "C" {
void ext_putchar(char c) { print_char(c); }
};

LoggingLevelMask loggingLevel = LoggingLevelMask::ERROR | LoggingLevelMask::WARN | LoggingLevelMask::INFO;
//LoggingLevelMask loggingLevel = LoggingLevelMask::ALL;
//LoggingLevelMask loggingLevel = LoggingLevelMask::PROG | LoggingLevelMask::ERROR;

// Globals
System system = System();

Peripherals::Controller gameController[4] = {
        Peripherals::Controller(), Peripherals::Controller(),
        Peripherals::Controller(), Peripherals::Controller()
};

MipsCPU cpu{};
GPU gpu{};
MemoryBus memBus{};

void WASM_EXPORT(setLogLevel)(u32 level) {
  loggingLevel = (LoggingLevelMask) level;
}

// Initializes the emulator itself, independent of any game
void WASM_EXPORT(init)() {
  print_info("\n"
     " _  ___ _          _         __ _ _  \n"
     "| |/ (_) |__  __ _| |_____  / /| | | \n"
     "| ' <| | '_ \\/ _` | / / _ \\/ _ \\_  _|\n"
     "|_|\\_\\_|_.__/\\__,_|_\\_\\___/\\___/ |_| \n"
     "\n"
  );

  print_info("Initialize WASM...");

  MipsiOp::fillOpcodeNameTable();
  memBus.reset();
  Cache::clearCache();

  print_info("Done!");
}

void WASM_EXPORT(reset)() {
  cpu.reset();
  memBus.reset();
  Timing::counterHLine = (u32) Timing::Cycles::H_LINE;
}

void WASM_EXPORT(clearCache)() {
  Cache::clearCache();
}

// boots a game and starts execution
void WASM_EXPORT(boot)() {
  print_info("Reading ROM Info...");

  auto romHeader = memBus.rom.readHeader();
  memBus.rom.printHeader();

  print_info("Start booting game...");
  system.boot();
}

void WASM_EXPORT(setPc)(u32 pc) {
  cpu.setPc(pc);
}

u32 WASM_EXPORT(getPc)() {
  return cpu.getPc();
}

void WASM_EXPORT(run)(u32 limit) {
  system.run(limit);
}

u32 WASM_EXPORT(getCommandCount)() {
  return cpu.getCommandCount();
}

void WASM_EXPORT(callFunction)(u32 pc, u32 stackPointer, s32 limit, u32 argCount, u32 arg0, u32 arg1, u32 arg2, u32 arg3) {
  cpu.reset();

  // Function arguments
  if (argCount > 0)cpu.setRegister<u32>(CpuReg::a0, arg0);
  if (argCount > 1)cpu.setRegister<u32>(CpuReg::a1, arg1);
  if (argCount > 2)cpu.setRegister<u32>(CpuReg::a2, arg2);
  if (argCount > 3)cpu.setRegister<u32>(CpuReg::a3, arg3);

  // Stack and function-return setup
  cpu.setRegister<u32>(CpuReg::ra, 0);
  cpu.setRegister<u32>(CpuReg::sp, stackPointer);

  cpu.setPc(pc);
  cpu.run(limit);
}

void WASM_EXPORT(stop)() {
  cpu.stop();
}

void WASM_EXPORT(interrupt)(u32 interruptType, u32 rcpType) {
  system.interruptHandler.interrupt(
          (Interrupt::Type) interruptType,
          (Interrupt::RcpType) rcpType
  );
}

void WASM_EXPORT(mockFunction)(u32 address) {
  memBus.write<u32>(address + 0, 0xFFFF'FF01);// x_inject
  memBus.write<u32>(address + 4, 0x03E0'0008);// jr $ra
  memBus.write<u32>(address + 8, 0x0000'0000);// nop
}

u32 WASM_EXPORT(getFramebufferInfo)() {
  return (u32) gpu.getFramebufferInfo();
}

void WASM_EXPORT(render)() {
  gpu.render();
}

u32 WASM_EXPORT(getFramebufferAddress)() {
  return gpu.getFramebufferAddress();
}

u32 WASM_EXPORT(getArgFromStackU32)(u32 idx) {
  u32 sp = cpu.getRegister<u32>((u32) CpuReg::sp);
  return memBus.read<u32>(sp + (idx * sizeof(u32)));
}

f32 WASM_EXPORT(getArgFromStackF32)(u32 idx) {
  u32 sp = cpu.getRegister<u32>((u32) CpuReg::sp);
  return std::bit_cast<f32>(memBus.read<u32>(sp + (idx * sizeof(u32))));
}

void WASM_EXPORT(setGameControllerButton)(u32 idx, u16 button)
{
  gameController[idx].setButtons(button);
}

void WASM_EXPORT(connectGameController)(u32 idx)
{
  gameController[idx].connect();
}

void WASM_EXPORT(disconnectGameController)(u32 idx)
{
  gameController[idx].disconnect();
}

void WASM_EXPORT(setGameControllerJoystick)(u32 idx, f32 x, f32 y)
{
  gameController[idx].setJoystick(x, y);
}

#ifdef FEATURE_DEBUG_LOG

  void WASM_EXPORT(cpuLogState)(){ cpu.logState();     }
  void WASM_EXPORT(fpuLogState)(){ cpu.fpu.logState(); }
  void WASM_EXPORT(cop0LogState)(){ cpu.cop0.printStatus(cpu.cop0.getRegisterU32(Cop0Register::STATUS)); }
  void WASM_EXPORT(interruptLogState)(){ system.interruptHandler.logState(); }

  void WASM_EXPORT(printNextInstructions)(s32 count) { 
    auto oldPc = cpu.getPc();

    for(s32 i=0; i<count; ++i) 
    {
      cpu.setPc(oldPc+ (i*4));
      u32 currentWord = memBus.read<u32>(cpu.getPc());
      auto instruction = Disassembler::getOpcodeFunction(currentWord);
      MipsiOp::printInstruction((u32)instruction, currentWord);
    }

    cpu.setPc(oldPc);
  }

#endif

#ifdef FEATURE_DEBUGGER

  // enable or disable calling the external-hooks, way faster if disabled
  void WASM_EXPORT(debuggerHookStepEnable)(){ cpu.hookStepEnabled = true; }
  void WASM_EXPORT(debuggerHookStepDisable)(){ cpu.hookStepEnabled = false; }

  // used by the debugger's memory viewer
  u8 WASM_EXPORT(memBusReadU8)(u32 address){ return memBus.read<u8>(address); }

  void WASM_EXPORT(tlbPrintStatus)(){ memBus.tlb.printStatus(); }
  u32 WASM_EXPORT(tlbResolveAddress)(u32 address){ return memBus.tlb.resolveAddress(address); }

#endif

// Register Functions

// CPU
u32 WASM_EXPORT(cpuGetRegisterU32)(s32 reg) { return cpu.getRegister<u32>(reg); }
s32 WASM_EXPORT(cpuGetRegisterS32)(s32 reg) { return cpu.getRegister<s32>(reg); }
u64 WASM_EXPORT(cpuGetRegisterU64)(s32 reg) { return cpu.getRegister<u64>(reg); }
s64 WASM_EXPORT(cpuGetRegisterS64)(s32 reg) { return cpu.getRegister<s64>(reg); }

void WASM_EXPORT(cpuSetRegisterU32)(s32 reg, u32 value) { cpu.setRegister<u32>(reg, value); }
void WASM_EXPORT(cpuSetRegisterS32)(s32 reg, u32 value) { cpu.setRegister<s32>(reg, value); }
void WASM_EXPORT(cpuSetRegisterU64)(s32 reg, u64 value) { cpu.setRegister<u64>(reg, value); }
void WASM_EXPORT(cpuSetRegisterS64)(s32 reg, s64 value) { cpu.setRegister<s64>(reg, value); }

// FPU
f32 WASM_EXPORT(fpuGetRegisterF32)(s32 reg) { return cpu.fpu.getRegister<f32>(reg); }
f64 WASM_EXPORT(fpuGetRegisterF64)(s32 reg) { return cpu.fpu.getRegister<f64>(reg); }
s32 WASM_EXPORT(fpuGetRegisterS32)(s32 reg) { return cpu.fpu.getRegister<s32>(reg); }
s64 WASM_EXPORT(fpuGetRegisterS64)(s32 reg) { return cpu.fpu.getRegister<s64>(reg); }

void WASM_EXPORT(fpuSetRegisterF32)(s32 reg, f32 value) { cpu.fpu.setRegister(reg, value); }
void WASM_EXPORT(fpuSetRegisterF64)(s32 reg, f64 value) { cpu.fpu.setRegister(reg, value); }
void WASM_EXPORT(fpuSetRegisterS32)(s32 reg, s32 value) { cpu.fpu.setRegister(reg, value); }
void WASM_EXPORT(fpuSetRegisterS64)(s32 reg, s64 value) { cpu.fpu.setRegister(reg, value); }

// Z64