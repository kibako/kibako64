/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "./types.h"
#include "./globalPointer.h"
#include "featureFlags.h"

// flag to expose a function to the JS host
#define WASM_EXPORT(name) __attribute__((visibility("default"), export_name(#name))) name
#define WASM_IMPORT(name) __attribute__((visibility("default"), import_name(#name))) name

#define WASM_IMPORT_NAMED(name) __attribute__((visibility("default"), import_name(#name)))
#define WASM_EXPORT_NAMED(name) __attribute__((visibility("default"), export_name(#name)))

#ifdef FEATURE_DEBUG_LOG
  // All WASM imports from JS
  void WASM_IMPORT(log_call)(s32 pc);
  void WASM_IMPORT(print_char)(u8 c);
#else
  #define log_call(a) 
  #define print_char(c)
#endif

// @TODO put debugging stuff into extra file
enum class DebuggerEvent 
{
  ERET = 1, 
  SET_EXCEPTION_LEVEL,
  SET_EXCEPTION_PC,
  INTERRUPT,
};

#ifdef FEATURE_DEBUGGER
  void WASM_IMPORT(debugger_hook_event)(DebuggerEvent eventType);
  void WASM_IMPORT(debugger_hook_step)(u32 pc);
  void WASM_IMPORT(debugger_hook_mem_write)(u32 address, u32 value);
  void WASM_IMPORT(debugger_hook_stack_push)(u32 pc);
  void WASM_IMPORT(debugger_hook_stack_pop)();
#else
  #define debugger_hook_event(eventType)
  #define debugger_hook_step(pc) 
  #define debugger_hook_mem_write(address, value) 
  #define debugger_hook_stack_push(pc)
  #define debugger_hook_stack_pop()
#endif

#ifdef FEATURE_PERFORMANCE_TIMER
  void WASM_IMPORT(performance_timer_start)(u32 id);
  void WASM_IMPORT(performance_timer_stop)(u32 id);
#else
  #define performance_timer_start(id) 
  #define performance_timer_stop(id) 
#endif

void WASM_IMPORT(hook_inject)(u32 pc, u32 arg0, u32 arg1, u32 arg2, u32 arg3);

// Debugging & Logging
#include "utils/logging.h"

#include "globals.h"
