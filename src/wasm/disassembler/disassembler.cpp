/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "../main.h"
#include "../instructions/opcodeArgs.h"
#include "disassembler.h"

namespace MOP = MipsiOp;

// macros to show the intention of a NOP
#define _UNIMPL_ nullptr
#define OP_TABLE MOP::nop

namespace Disassembler 
{
  const MipsiOpFunction opsNormal[1 << 6] = {
  /* 0b000_000 */ MOP::nop, // -> Special-function table (index: FUNC)
  /* 0b000_001 */ OP_TABLE,    // -> Branch table (index: RT)
  /* 0b000_010 */ MOP::j, 
  /* 0b000_011 */ MOP::jal, 
  /* 0b000_100 */ MOP::beq, 
  /* 0b000_101 */ MOP::bne, 
  /* 0b000_110 */ MOP::blez, 
  /* 0b000_111 */ MOP::bgtz, 
  /* 0b001_000 */ MOP::addi, 
  /* 0b001_001 */ MOP::addiu, 
  /* 0b001_010 */ MOP::slti, 
  /* 0b001_011 */ MOP::sltiu, 
  /* 0b001_100 */ MOP::andi, 
  /* 0b001_101 */ MOP::ori, 
  /* 0b001_110 */ MOP::xori, 
  /* 0b001_111 */ MOP::lui, 
  /* 0b010_000 */ OP_TABLE, // -> COP0 table (index: RS)
  /* 0b010_001 */ OP_TABLE, // -> FPU table (index: RS or FUNC)
  /* 0b010_010 */ _UNIMPL_, 
  /* 0b010_011 */ _UNIMPL_, 
  /* 0b010_100 */ MOP::beql, 
  /* 0b010_101 */ MOP::bnel, 
  /* 0b010_110 */ MOP::blezl, 
  /* 0b010_111 */ MOP::bgtzl, 
  /* 0b011_000 */ MOP::daddi, 
  /* 0b011_001 */ MOP::daddiu, 
  /* 0b011_010 */ MOP::ldl, 
  /* 0b011_011 */ MOP::ldr, 
  /* 0b011_100 */ _UNIMPL_, 
  /* 0b011_101 */ _UNIMPL_, 
  /* 0b011_110 */ _UNIMPL_, 
  /* 0b011_111 */ _UNIMPL_, 
  /* 0b100_000 */ MOP::lb, 
  /* 0b100_001 */ MOP::lh, 
  /* 0b100_010 */ MOP::lwl, 
  /* 0b100_011 */ MOP::lw, 
  /* 0b100_100 */ MOP::lbu, 
  /* 0b100_101 */ MOP::lhu, 
  /* 0b100_110 */ MOP::lwr, 
  /* 0b100_111 */ MOP::lwu, 
  /* 0b101_000 */ MOP::sb, 
  /* 0b101_001 */ MOP::sh, 
  /* 0b101_010 */ MOP::swl, 
  /* 0b101_011 */ MOP::sw, 
  /* 0b101_100 */ MOP::sdl, 
  /* 0b101_101 */ MOP::sdr, 
  /* 0b101_110 */ MOP::swr, 
  /* 0b101_111 */ MOP::cache, // @TODO check if this can be a _UNIMPL_ in non-debug modes
  /* 0b110_000 */ MOP::ll, 
  /* 0b110_001 */ MOP::lwc1, 
  /* 0b110_010 */ _UNIMPL_, 
  /* 0b110_011 */ _UNIMPL_, 
  /* 0b110_100 */ MOP::lld, 
  /* 0b110_101 */ MOP::ldc1, 
  /* 0b110_110 */ _UNIMPL_,//MOP::ldc2, 
  /* 0b110_111 */ MOP::ld, 
  /* 0b111_000 */ MOP::sc, 
  /* 0b111_001 */ MOP::swc1, 
  /* 0b111_010 */ _UNIMPL_, 
  /* 0b111_011 */ _UNIMPL_, 
  /* 0b111_100 */ MOP::scd, 
  /* 0b111_101 */ MOP::sdc1, 
  /* 0b111_110 */ _UNIMPL_,//MOP::sdc2, 
  /* 0b111_111 */ MOP::sd
  };

  const MipsiOpFunction opsSpecial[1 << 6] = {
  /* 0b000_000 */ MOP::sll,
  /* 0b000_001 */ _UNIMPL_, 
  /* 0b000_010 */ MOP::srl, 
  /* 0b000_011 */ MOP::sra, 
  /* 0b000_100 */ MOP::sllv, 
  /* 0b000_101 */ _UNIMPL_, 
  /* 0b000_110 */ MOP::srlv, 
  /* 0b000_111 */ MOP::srav, 
  /* 0b001_000 */ MOP::jr, 
  /* 0b001_001 */ MOP::jalr, 
  /* 0b001_010 */ MOP::movz, 
  /* 0b001_011 */ MOP::movn, 
  /* 0b001_100 */ _UNIMPL_, 
  /* 0b001_101 */ MOP::breakpoint, 
  /* 0b001_110 */ _UNIMPL_, 
  /* 0b001_111 */ MOP::sync, 
  /* 0b010_000 */ MOP::mfhi, 
  /* 0b010_001 */ MOP::mthi, 
  /* 0b010_010 */ MOP::mflo, 
  /* 0b010_011 */ MOP::mtlo,
  /* 0b010_100 */ MOP::dsllv, 
  /* 0b010_101 */ _UNIMPL_, 
  /* 0b010_110 */ MOP::dsrlv, 
  /* 0b010_111 */ MOP::dsrav, 
  /* 0b011_000 */ MOP::mult, 
  /* 0b011_001 */ MOP::multu, 
  /* 0b011_010 */ MOP::div, 
  /* 0b011_010 */ MOP::divu, 
  /* 0b011_100 */ MOP::dmult, 
  /* 0b011_101 */ MOP::dmultu, 
  /* 0b011_110 */ MOP::ddiv, 
  /* 0b011_111 */ MOP::ddivu, 
  /* 0b100_000 */ MOP::add, 
  /* 0b100_001 */ MOP::addu, 
  /* 0b100_010 */ MOP::sub,
  /* 0b100_011 */ MOP::subu, 
  /* 0b100_100 */ MOP::op_and, 
  /* 0b100_101 */ MOP::op_or, 
  /* 0b100_110 */ MOP::op_xor, 
  /* 0b100_111 */ MOP::nor, 
  /* 0b101_000 */ _UNIMPL_, 
  /* 0b101_001 */ _UNIMPL_, 
  /* 0b101_010 */ MOP::slt, 
  /* 0b101_011 */ MOP::sltu, 
  /* 0b101_100 */ MOP::dadd, 
  /* 0b101_101 */ MOP::daddu, 
  /* 0b101_110 */ MOP::dsub, 
  /* 0b101_111 */ MOP::dsubu, 
  /* 0b110_000 */ _UNIMPL_, 
  /* 0b110_001 */ _UNIMPL_, 
  /* 0b110_010 */ _UNIMPL_, 
  /* 0b110_011 */ _UNIMPL_, 
  /* 0b110_100 */ MOP::teq, 
  /* 0b110_101 */ _UNIMPL_, 
  /* 0b110_110 */ _UNIMPL_, 
  /* 0b110_111 */ _UNIMPL_, 
  /* 0b111_000 */ MOP::dsll, 
  /* 0b111_001 */ _UNIMPL_, 
  /* 0b111_010 */ MOP::dsrl, 
  /* 0b111_011 */ MOP::dsra, 
  /* 0b111_100 */ MOP::dsll32, 
  /* 0b111_101 */ _UNIMPL_, 
  /* 0b111_111 */ MOP::dsrl32,
  /* 0b111_111 */ MOP::dsra32
  };

  const MipsiOpFunction opsBranch[1 << 5] = {
  /* 0b000_00 */ MOP::bltz,
  /* 0b000_01 */ MOP::bgez,
  /* 0b000_10 */ MOP::bltzl,
  /* 0b000_11 */ MOP::bgezl,
  /* 0b001_00 */ _UNIMPL_,
  /* 0b001_01 */ _UNIMPL_,
  /* 0b001_10 */ _UNIMPL_,
  /* 0b001_11 */ _UNIMPL_,
  /* 0b010_00 */ _UNIMPL_,
  /* 0b010_01 */ _UNIMPL_,
  /* 0b010_10 */ _UNIMPL_,
  /* 0b010_11 */ _UNIMPL_,
  /* 0b011_00 */ _UNIMPL_,
  /* 0b011_01 */ _UNIMPL_,
  /* 0b011_10 */ _UNIMPL_,
  /* 0b011_11 */ _UNIMPL_,
  /* 0b100_00 */ _UNIMPL_,
  /* 0b100_01 */ MOP::bgezal,
  /* 0b100_10 */ _UNIMPL_,
  /* 0b100_11 */ _UNIMPL_,
  /* 0b101_00 */ _UNIMPL_,
  /* 0b101_01 */ _UNIMPL_,
  /* 0b101_10 */ _UNIMPL_,
  /* 0b101_11 */ _UNIMPL_,
  /* 0b110_00 */ _UNIMPL_,
  /* 0b110_01 */ _UNIMPL_,
  /* 0b110_10 */ _UNIMPL_,
  /* 0b110_11 */ _UNIMPL_,
  /* 0b111_00 */ _UNIMPL_,
  /* 0b111_01 */ _UNIMPL_,
  /* 0b111_10 */ _UNIMPL_,
  /* 0b111_11 */ _UNIMPL_
  };

  const MipsiOpFunction opsFPU[1 << 5] = {
  /* 0b000_00 */ MOP::mfc1,
  /* 0b000_01 */ MOP::dmfc1,
  /* 0b000_10 */ MOP::cfc1,
  /* 0b000_11 */ _UNIMPL_,
  /* 0b001_00 */ MOP::mtc1,
  /* 0b001_01 */ MOP::dmtc1,
  /* 0b001_10 */ MOP::ctc1,
  /* 0b001_11 */ _UNIMPL_,
  };

  const MipsiOpFunction opsCop0[1 << 5] = {
  /* 0b000_00 */ MOP::mfc0,
  /* 0b000_01 */ _UNIMPL_,
  /* 0b000_10 */ _UNIMPL_,
  /* 0b000_11 */ _UNIMPL_,
  /* 0b001_00 */ MOP::mtc0
  };

  const MipsiOpFunction opsFPUFunction[1 << 6][4] = {
  //              {   single,             double,        word,          long  }
  /* 0b000_000 */ {MOP::add_s,     MOP::add_d,           _UNIMPL_,    _UNIMPL_},
  /* 0b000_001 */ {MOP::sub_s,     MOP::sub_d,           _UNIMPL_,    _UNIMPL_},
  /* 0b000_010 */ {MOP::mul_s,     MOP::mul_d,           _UNIMPL_,    _UNIMPL_},
  /* 0b000_011 */ {MOP::div_s,     MOP::div_d,           _UNIMPL_,    _UNIMPL_},
  /* 0b000_100 */ {MOP::sqrt_s,    MOP::sqrt_d,          _UNIMPL_,    _UNIMPL_},
  /* 0b000_101 */ {MOP::abs_s,     MOP::abs_d,           _UNIMPL_,    _UNIMPL_},
  /* 0b000_110 */ {MOP::mov_s,     MOP::mov_d,           _UNIMPL_,    _UNIMPL_},
  /* 0b000_111 */ {MOP::neg_s,     MOP::neg_d,           _UNIMPL_,    _UNIMPL_},
  /* 0b001_000 */ {MOP::round_l_s, MOP::round_l_d,       _UNIMPL_,    _UNIMPL_},
  /* 0b001_001 */ {MOP::trunc_l_s, MOP::trunc_l_d,       _UNIMPL_,    _UNIMPL_},
  /* 0b001_010 */ {MOP::ceil_l_s,  MOP::ceil_l_d,        _UNIMPL_,    _UNIMPL_}, 
  /* 0b001_011 */ {MOP::floor_l_s, MOP::floor_l_d,       _UNIMPL_,    _UNIMPL_},
  /* 0b001_100 */ {MOP::round_w_s, MOP::round_w_d,       _UNIMPL_,    _UNIMPL_},
  /* 0b001_101 */ {MOP::trunc_w_s, MOP::trunc_w_d,       _UNIMPL_,    _UNIMPL_},
  /* 0b001_110 */ {MOP::ceil_w_s,  MOP::ceil_w_d,        _UNIMPL_,    _UNIMPL_}, 
  /* 0b001_111 */ {MOP::floor_w_s, MOP::floor_w_d,       _UNIMPL_,    _UNIMPL_},
  /* 0b010_000 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b010_001 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_},
  /* 0b010_010 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b010_011 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b010_100 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b010_101 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b010_110 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b010_111 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b011_000 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b011_001 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b011_010 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b011_011 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b011_100 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b011_101 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b011_110 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b011_111 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b100_000 */ {_UNIMPL_,      MOP::cvt_s_d,      MOP::cvt_s_w, MOP::cvt_s_l}, 
  /* 0b100_001 */ {MOP::cvt_d_s,      _UNIMPL_,      MOP::cvt_d_w, MOP::cvt_d_l}, 
  /* 0b100_010 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b100_011 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b100_100 */ {MOP::cvt_w_s,  MOP::cvt_w_d,          _UNIMPL_,    _UNIMPL_},
  /* 0b100_101 */ {MOP::cvt_l_s,  MOP::cvt_l_d,          _UNIMPL_,    _UNIMPL_},
  /* 0b100_110 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b100_111 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b101_000 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b101_001 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b101_010 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b101_011 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b101_100 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b101_101 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b101_110 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 
  /* 0b101_111 */ {_UNIMPL_,          _UNIMPL_,          _UNIMPL_,    _UNIMPL_}, 

  // FPU Compare functions
  // __11_XXXX =  11 = fixed, XXXX = condition
  /* 0b11_0000 */ {MOP::c_f_s,       MOP::c_f_d,         _UNIMPL_, _UNIMPL_},
  /* 0b11_0001 */ {MOP::c_un_s,      MOP::c_un_d,        _UNIMPL_, _UNIMPL_},
  /* 0b11_0010 */ {MOP::c_eq_s,      MOP::c_eq_d,        _UNIMPL_, _UNIMPL_},
  /* 0b11_0011 */ {MOP::c_ueq_s,     MOP::c_ueq_d,       _UNIMPL_, _UNIMPL_},
  /* 0b11_0100 */ {MOP::c_olt_s,     MOP::c_olt_d,       _UNIMPL_, _UNIMPL_},
  /* 0b11_0101 */ {MOP::c_ult_s,     MOP::c_ult_d,       _UNIMPL_, _UNIMPL_},
  /* 0b11_0110 */ {MOP::c_ole_s,     MOP::c_ole_d,       _UNIMPL_, _UNIMPL_},
  /* 0b11_0111 */ {MOP::c_ule_s,     MOP::c_ule_d,       _UNIMPL_, _UNIMPL_},
  /* 0b11_1000 */ {MOP::c_sf_s,      MOP::c_sf_d,        _UNIMPL_, _UNIMPL_},
  /* 0b11_1001 */ {MOP::c_ngle_s,    MOP::c_ngle_d,      _UNIMPL_, _UNIMPL_},
  /* 0b11_1010 */ {MOP::c_seq_s,     MOP::c_seq_d,       _UNIMPL_, _UNIMPL_},
  /* 0b11_1011 */ {MOP::c_ngl_s,     MOP::c_ngl_d,       _UNIMPL_, _UNIMPL_},
  /* 0b11_1100 */ {MOP::c_lt_s,      MOP::c_lt_d,        _UNIMPL_, _UNIMPL_},
  /* 0b11_1101 */ {MOP::c_nge_s,     MOP::c_nge_d,       _UNIMPL_, _UNIMPL_},
  /* 0b11_1110 */ {MOP::c_le_s,      MOP::c_le_d,        _UNIMPL_, _UNIMPL_},
  /* 0b11_1111 */ {MOP::c_ngt_s,     MOP::c_ngt_d,       _UNIMPL_, _UNIMPL_},
  };

  inline s32 fpuTypeToIndex(s32 type)
  {
    return (type < (s32)Disassembler::FpuType::WORD)
      ? (type - 16) 
      : (type - 18);
  }

  MipsiOpFunction getOpcodeFunction(u32 word)
  {
    if(word == 0)return MOP::nop;
    s32 op = ARG_OP;

    // Custom extensions (NOT official and only used for debugging / unit-testing in Kibako64)
    #ifdef FEATURE_MIPS_EXTENSION
      if(word == 0xFFFFFF00) {
        return MOP::x_log;
      }
    #endif

    switch(op) 
    {
      case 0b000000: return opsSpecial[ARG_FUNC];
      case 0b000001: return opsBranch[ARG_RT];

      // Co-Processor 0 (RCP)
      case 0b010000: 
        if(word == 0x42000002)return MOP::tlbwi;
        if(word == 0x42000018)return MOP::eret;
        return ARG_FUNC ? _UNIMPL_ : opsCop0[ARG_RS];

      // Co-Processor 1 (FPU)
      case 0b010001: {
        s32 rs = ARG_RS;
        s32 rt = ARG_RT;

        if (rs & 0b10000) {
          return opsFPUFunction[ARG_FUNC][fpuTypeToIndex(rs)];
        }

        if (rs == 0b01000) {
          return (rt & 1)
                 ? ((rt & 0b10) ? MOP::bc1tl : MOP::bc1t)
                 : ((rt & 0b10) ? MOP::bc1fl : MOP::bc1f);
        }

        return opsFPU[rs];
      }
    }

    if(word == 0xFFFFFF01) { // would otherwise be an invalid "sd"
      return MOP::x_inject;
    }
    return opsNormal[op];
  }
};
