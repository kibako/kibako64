/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../main.h"
#include "../instructions/instructions.h"

namespace Disassembler 
{
  enum class FpuType {
      INVALID = 0,
      SINGLE = 16, // f32
      DOUBLE = 17, // f64
      WORD = 20,   // s32
      LONG = 21,   // s64
  };

  enum class FpuTypeIndex {
      SINGLE = 0, // f32
      DOUBLE = 1, // f64
      WORD = 2,   // s32
      LONG = 3,   // s64
  };

  MipsiOpFunction getOpcodeFunction(u32 word);
};