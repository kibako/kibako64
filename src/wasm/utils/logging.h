/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../types.h"
#include "../featureFlags.h"
#include "../utils/printf.h"
#include "../stdlib/stdlib.h"

// __LINE__ as a string, only use S__LINE__
#define S_LINE_STR1(x) #x
#define S_LINE_STR2(x) S_LINE_STR1(x)
#define S_LINE_ S_LINE_STR2(__LINE__)

#define PRINTF_U32_BIN_ARG "%c%c%c%c%c%c%c%c_%c%c%c%c%c%c%c%c_%c%c%c%c%c%c%c%c_%c%c%c%c%c%c%c%c"
#define PRINTF_U32_BIN_VAL(x) \
  (x & (1 << 31) ? '1' : '0'), (x & (1 << 30) ? '1' : '0'), (x & (1 << 29) ? '1' : '0'), (x & (1 << 28) ? '1' : '0'), \
  (x & (1 << 27) ? '1' : '0'), (x & (1 << 26) ? '1' : '0'), (x & (1 << 25) ? '1' : '0'), (x & (1 << 24) ? '1' : '0'), \
  (x & (1 << 23) ? '1' : '0'), (x & (1 << 22) ? '1' : '0'), (x & (1 << 21) ? '1' : '0'), (x & (1 << 20) ? '1' : '0'), \
  (x & (1 << 19) ? '1' : '0'), (x & (1 << 18) ? '1' : '0'), (x & (1 << 17) ? '1' : '0'), (x & (1 << 16) ? '1' : '0'), \
  (x & (1 << 15) ? '1' : '0'), (x & (1 << 14) ? '1' : '0'), (x & (1 << 13) ? '1' : '0'), (x & (1 << 12) ? '1' : '0'), \
  (x & (1 << 11) ? '1' : '0'), (x & (1 << 10) ? '1' : '0'), (x & (1 <<  9) ? '1' : '0'), (x & (1 <<  8) ? '1' : '0'), \
  (x & (1 <<  7) ? '1' : '0'), (x & (1 <<  6) ? '1' : '0'), (x & (1 <<  5) ? '1' : '0'), (x & (1 <<  4) ? '1' : '0'), \
  (x & (1 <<  3) ? '1' : '0'), (x & (1 <<  2) ? '1' : '0'), (x & (1 <<  1) ? '1' : '0'), (x & (1 <<  0) ? '1' : '0')

enum class LoggingLevelMask
{
  NONE  = 0,

  ERROR = (1 << 1),
  WARN  = (1 << 2),
  INFO  = (1 << 3),
  TRACE = (1 << 4),

  PROG  = (1 << 5),

  ALL   = 0xFFFF,
};

constexpr LoggingLevelMask operator |(LoggingLevelMask a, LoggingLevelMask b) {
    return (LoggingLevelMask)((s32)a | (s32)b);
}
constexpr LoggingLevelMask operator &(LoggingLevelMask a, LoggingLevelMask b) {
    return (LoggingLevelMask)((s32)a & (s32)b);
}
constexpr bool32 operator !(LoggingLevelMask a) {
    return !(bool32)a;
}

extern LoggingLevelMask loggingLevel;

#ifdef FEATURE_DEBUG_LOG
  #define TRACE_STR "[" __FILE__  ":" S_LINE_ "] "

  #define printf_prog(str, ...)  if((s32)loggingLevel & (s32)LoggingLevelMask::PROG)  { printf("\033[34;7m" "[PROG] " TRACE_STR str "\033[0m\n", __VA_ARGS__); }
  #define printf_error(str, ...) if((s32)loggingLevel & (s32)LoggingLevelMask::ERROR) { printf("\033[31;7m" "[ERRO] " TRACE_STR str "\033[0m\n", __VA_ARGS__); }
  #define printf_warn(str, ...)  if((s32)loggingLevel & (s32)LoggingLevelMask::WARN ) { printf("\033[33;7m" "[WARN] " TRACE_STR str "\033[0m\n", __VA_ARGS__); }
  #define printf_info(str, ...)  if((s32)loggingLevel & (s32)LoggingLevelMask::INFO ) { printf(             "[INFO] " TRACE_STR str        "\n", __VA_ARGS__); }
  #define printf_trace(str, ...) if((s32)loggingLevel & (s32)LoggingLevelMask::TRACE) { printf("\033[30;1m" "[TRAC] " TRACE_STR str "\033[0m\n", __VA_ARGS__); }
  //#define printf_trace(...) 
#else
  #define TRACE_STR 
  
  #define printf_prog(...) 
  #define printf_error(...) 
  #define printf_warn(...) 
  #define printf_info(...) 
  #define printf_trace(...) 
#endif

#define print_prog(str) printf_prog(str, "")
#define print_error(str) printf_error(str, "")
#define print_warn(str) printf_warn(str, "")
#define print_info(str) printf_info(str, "")
#define print_trace(str) printf_trace(str, "")

// resolves a given data-type into a string at compile-time without any overhead
template<typename T>
constexpr const char* getTypeName() 
{
  if constexpr(std::is_same<T,  u8>::value)return "u8";
  if constexpr(std::is_same<T,  s8>::value)return "s8";
  if constexpr(std::is_same<T, u16>::value)return "u16";
  if constexpr(std::is_same<T, s16>::value)return "s16";
  if constexpr(std::is_same<T, u32>::value)return "u32";
  if constexpr(std::is_same<T, s32>::value)return "s32";
  if constexpr(std::is_same<T, u64>::value)return "u64";
  if constexpr(std::is_same<T, s64>::value)return "s63";

  if constexpr(std::is_same<T, f32>::value)return "f32";
  if constexpr(std::is_same<T, f64>::value)return "f64";
  return "??";
}

// prints a byte-array as hex-values in groups of 16-bytes per row
void printf_bytearray(u8* data, u32 size);

#define U32_FROM_CHARS(a,b,c,d) ((a << 24) | (b << 16) | (c << 8) | d)