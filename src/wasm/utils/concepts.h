/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "types.h"
#include "stdlib/stdlib.h"

namespace Concepts
{
    template<typename T>
    concept CpuRegType = requires (T val) {
      { std::is_same<T, u32>::value };
      { std::is_same<T, s32>::value };
      { std::is_same<T, u64>::value };
      { std::is_same<T, s64>::value };
    };

    template<typename T>
    concept FpuRegType = requires (T val) {
      { std::is_same<T, u32>::value };
      { std::is_same<T, s32>::value };
      { std::is_same<T, u64>::value };
      { std::is_same<T, s64>::value };
      { std::is_same<T, f32>::value };
      { std::is_same<T, f64>::value };
    };
}