/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "stdlib.h"
#include "types.h"

namespace Utils
{
  template<typename KEY, typename VAL, u32 SIZE>
  class Map
  {
    private:
      KEY keys[SIZE]{};
      VAL values[SIZE]{};

      u32 pos{0};

    public:
      void clear() {
        memset(keys, 0, sizeof(keys));
        pos = 0;
      }

      void insert(KEY key, VAL value)
      {
        keys[pos] = key;
        values[pos] = value;
        pos = (pos + 1) % SIZE;
      }

      bool find(KEY key, u32 &value)
      {
        u32 i=0;
        for(const auto &d : keys) {
          if(d == 0)return false; // untouched data, stop search
          if(d == key) {
            value = values[i];
            return true;
          }
          ++i;
        }
        return false;
      }
  };
}