/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "../types.h"

inline u32 checksumU8(u8* const dataPtr, u8* const dataPtrEnd)
{
  u32 result = 0;
  for(u8* currentPtr = dataPtr; currentPtr<dataPtrEnd; ++currentPtr) 
  {
    result += *currentPtr;
  }
  return result;
}
