/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "logging.h"

void printf_bytearray(u8* data, u32 size)
{
  printf("0x0000: ");
  for(u32 i=0; i<size; ++i)
  {
    printf("%02X ", data[i]);

    if((i+1) % 16 == 0) {
      printf("\n");
      printf("0x%04X: ", i);
    }
    else if((i+1) % 8 == 0) {
      printf("| ");
    }
  }
}
