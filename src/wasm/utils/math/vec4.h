/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "types.h"
#include "vec3.h"
#include "vec2.h"

namespace Math
{
  struct Vec4
  {
    f32 data[4]{0.0f};

    // Constructors
    constexpr Vec4() = default;
    constexpr Vec4(f32 s): data{s,s,s,s} {};
    constexpr Vec4(f32 x, f32 y, f32 z, f32 w): data{x,y,z,w} {};
    constexpr Vec4(const Vec3 &vec, f32 w = 0.0f): data{vec[0], vec[1], vec[2], w} {};
    constexpr Vec4(const Vec2 &a, const Vec2 &b): data{a[0], a[1], b[0], b[1]} {};

    // conversion
    [[nodiscard]] const Vec3& toVec3() const {
      return *(const Math::Vec3*)data;
    }
    [[nodiscard]] Vec3& toVec3() {
      return *(Math::Vec3*)data;
    }

    Vec3 cross(const Vec3 &y) {
      return {
        data[1]*y[2] - y[1]*data[2],
        data[2]*y[0] - y[2]*data[0],
        data[0]*y[1] - y[0]*data[1],
      };
    }

    // Data accessor
    f32& operator[](u64 index) { return data[index]; }
    constexpr const f32& operator[](u64 index) const { return data[index]; }

    [[nodiscard]] const f32* ptr() const { return data; }
    f32* ptr() { return data; }

    [[nodiscard]] f32& x() { return data[0]; }
    [[nodiscard]] f32& y() { return data[1]; }
    [[nodiscard]] f32& z() { return data[2]; }
    [[nodiscard]] f32& w() { return data[3]; }

    [[nodiscard]] const f32& x() const { return data[0]; }
    [[nodiscard]] const f32& y() const { return data[1]; }
    [[nodiscard]] const f32& z() const { return data[2]; }
    [[nodiscard]] const f32& w() const { return data[3]; }

    [[nodiscard]] f32& r() { return data[0]; }
    [[nodiscard]] f32& g() { return data[1]; }
    [[nodiscard]] f32& b() { return data[2]; }
    [[nodiscard]] f32& a() { return data[3]; }

    [[nodiscard]] const f32& r() const { return data[0]; }
    [[nodiscard]] const f32& g() const { return data[1]; }
    [[nodiscard]] const f32& b() const { return data[2]; }
    [[nodiscard]] const f32& a() const { return data[3]; }

    // Helper
    constexpr void clear() {
      data[0] = 0.0f; data[1] = 0.0f;
      data[2] = 0.0f; data[3] = 0.0f;
    }

    [[nodiscard]] f32 dot(const Vec4 a) const {
      return a[0] * data[0] +
             a[1] * data[1] +
             a[2] * data[2] +
             a[3] * data[3];
    }

    // Operations (Vector)
    constexpr Vec4 operator-() const {
      return { -data[0], -data[1], -data[2], -data[3] };
    }

    constexpr Vec4 operator+(const Vec4& b) const {
      return {
        data[0] + b[0], data[1] + b[1],
        data[2] + b[2], data[3] + b[3],
      };
    }

    constexpr Vec4 operator-(const Vec4& b) const {
      return {
        data[0] - b[0], data[1] - b[1],
        data[2] - b[2], data[3] - b[3],
      };
    }

    constexpr Vec4 operator*(const Vec4& b) const {
      return {
        data[0] * b[0], data[1] * b[1],
        data[2] * b[2], data[3] * b[3],
      };
    }

    constexpr Vec4 operator/(const Vec4& b) const {
      return {
        data[0] / b[0], data[1] / b[1],
        data[2] / b[2], data[3] / b[3],
      };
    }

    Vec4& operator+=(const Vec4& b) { return *this = *this + b; }
    Vec4& operator-=(const Vec4& b) { return *this = *this - b; }
    Vec4& operator*=(const Vec4& b) { return *this = *this * b; }
    Vec4& operator/=(const Vec4& b) { return *this = *this / b; }

    // Operations (Scalar)
    Vec4 operator+(float b) const {
      return {
        data[0] + b, data[1] + b,
        data[2] + b, data[3] + b,
      };
    }

    Vec4 operator-(float b) const {
      return {
        data[0] - b, data[1] - b,
        data[2] - b, data[3] - b,
      };
    }

    Vec4 operator*(float b) const {
      return {
        data[0] * b, data[1] * b,
        data[2] * b, data[3] * b,
      };
    }

    Vec4 operator/(float b) const {
      return {
        data[0] / b, data[1] / b,
        data[2] / b, data[3] / b,
      };
    }

    Vec4& operator+=(float b) { return *this = *this + b; }
    Vec4& operator-=(float b) { return *this = *this - b; }
    Vec4& operator*=(float b) { return *this = *this * b; }
    Vec4& operator/=(float b) { return *this = *this / b; }

    // Comparison
    bool operator==(const Vec4&) const = default;
  };
}