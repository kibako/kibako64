/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include "stdlib.h"
#include "types.h"
#include "vec3.h"

namespace Math
{
  struct Vec3S8
  {
    s8 data[3]{0};

    // Constructors
    constexpr Vec3S8() = default;
    constexpr Vec3S8(s8 s): data{s,s,s} {};
    constexpr Vec3S8(s8 x, s8 y, s8 z): data{x,y,z} {};

    constexpr Vec3S8(const Math::Vec3 &vec) : data{
      static_cast<s8>(vec.x() * 127.0f),
      static_cast<s8>(vec.y() * 127.0f),
      static_cast<s8>(vec.z() * 127.0f)
    }
    {}
  };
}